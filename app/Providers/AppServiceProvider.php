<?php

namespace App\Providers;

use App\Models\Word;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public $words;

    public function boot()
    {

        $w = Word::get();
        foreach ($w as $r) {
            $this->words[$r['name']] = $r['value'];
        }

        \Debugbar::disable();


        Blade::directive('t', function ($p1 = "", $p2 = null) {


            if (isset($this->words[$p1])) {
                $str = $this->words[$p1];
            } else {
                Word::create([
                    'name' => $p1,
                    'value' => $p1,
                ]);
                $str = $p1;
            }

            if (gettype($p2) == "string") {
                $str = str_replace("$$1", $p2, $str);
            }

            return $str;
        });
    }
}
