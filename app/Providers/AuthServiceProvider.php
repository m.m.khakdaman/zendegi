<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /**
         * register all gate
         */
        foreach ($this->getPermissions() as $permission) {
            Gate::define($permission->title, function ($user) use ($permission) {
                return $user->hasRole($permission->Role);
            });
        }
    }

    protected function getPermissions()
    {
        return Permission::with('Role')->get();
    }
}
