<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\LogActivity;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class LoginController extends Controller
{
    protected $redirectTo = RouteServiceProvider::ADMIN;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(array_merge($request->only('mobile', 'password'),['user_type'=>'admin']))) {
            $request->session()->regenerate();
            LogActivity::addToLog('ورود به سامانه', null, null, null);
            return redirect()->intended($this->redirectPath());
        } else {
            return redirect('/login')->withErrors(['user' => 'نام کاربری یا رمز عبر اشتباه است.'])->withInput();
        }
        return redirect('/login')->withErrors(['user' => 'نام کاربری یا رمز عبر اشتباه است.'])->withInput();
    }

    public function logout(Request $request)
    {
        if (auth()->check()) {
            auth()->logout();
            LogActivity::addToLog('خروج از سامانه', null, null, null);
        }
        return redirect('/');
    }

    public function reset_password(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('auth.reset_password');
        }
        
        // return $status = Password::sendResetLink(
        //     $request->only('phone')
        // );
    }
}
