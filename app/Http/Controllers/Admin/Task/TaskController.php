<?php

namespace App\Http\Controllers\Admin\Task;

use App\Http\Controllers\Controller;
use App\Models\History;
use App\Models\Role;
use App\Models\User;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['tasks'] = History::where('resiver_id',auth()->user()->id)->where('status', 1)->latest()->with(['sender'])->get();
        $data['off_tasks'] = History::where('resiver_id',auth()->user()->id)->where('status', '0')->latest()->with(['sender'])->get();
        $data['admins'] = User::where('user_type','customer')->get();
        return view('components.Admin.Task.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $data = [];
        $data['customer'] = $user;
        $data['advisors'] = User::where('user_type','advisor')->get();
        $data['admins'] = User::where('user_type','customer')->get();
        return view('components.Admin.Task.create',$data);
        // return view('components.Admin.Task.create',compact('user','data'));
    }

    public function create_persona(User $user)
    {
        // return $user;
        $data = [];
        $data['customer'] = $user;
        $data['advisors'] = User::where('user_type','advisor')->get();
        $data['admins'] = User::where('user_type','customer')->get();
        return view('components.Admin.Task.create',$data);
    }

    public function activation(History $history)
    {
        // return $history->history_id;
        $tmp = '1';
        if ($history->status) {
            $tmp = '0';
        }
        History::where('history_id', $history->history_id)->update(['status' => '0']);

        $data = [];
        $data['tasks'] = History::where('resiver_id',auth()->user()->id)->where('status', 1)->latest()->with(['sender'])->get();
        $data['off_tasks'] = History::where('resiver_id',auth()->user()->id)->where('status', '0')->latest()->with(['sender'])->get();
        $data['admins'] = User::where('user_type','customer')->get();
        return view('components.Admin.Task.index',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        
        $josndata = 'اولویت اول: '.$request->suggestion1.' اولویت دوم: '.$request->suggestion2.' اولویت سوم:'.$request->suggestion3;
            //   $data = json_decode($josndata);
            //   return  $singledata = $data->suggestions->first;
        History::create([
            'history_title' => $request -> history_title,
            'sender_id' => auth()->user()->id,
            'resiver_id' => $request->follow_up,
            'customer_id' => $request->customer_id,
            'suggestions' => $josndata,
            'importance' => $request->importance,
            'note' => $request->note,
            'task_flag' => $request->task_flag,
            'date_of_send' =>  verta()->format('Y/n/j H:i'),
            'date_of_answer' => verta()->format('Y/n/j'),
           ]);


        $data = [];
        $data['tasks'] = History::where('resiver_id', auth()->user()->id,)->where('status', 1)->latest()->with(['sender'])->get();
        $data['off_tasks'] = History::where('resiver_id',auth()->user()->id)->where('status', '0')->latest()->with(['sender'])->get();
        $data['admins'] = User::where('user_type','customer')->get();
        return view('components.Admin.Task.index',$data);
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return $request;
    }

    public function update_task(Request $request)
    {
        // return $request;
        History::create([
            'history_title' => $request -> history_title,
            'sender_id' => auth()->user()->id,
            'resiver_id' => $request->resiver_user,
            'customer_id' => $request->customer_id,
            'suggestions' => $request->suggestions,
            'importance' => $request->importance,
            'note' => $request->note,
            'task_flag' => $request->task_flag,
            'date_of_send' =>  verta()->format('Y/n/j H:i'),
            'date_of_answer' => verta()->format('Y/n/j'),
        ]);
        History::where('history_id', $request->task_id)->update([
            'status' => '0'
        ]);

        $data = [];
        $data['tasks'] = History::where('resiver_id', auth()->user()->id,)->where('status',1)->latest()->with(['sender'])->get();
        $data['off_tasks'] = History::where('resiver_id',auth()->user()->id)->where('status', '0')->latest()->with(['sender'])->get();
        $data['admins'] = User::where('user_type','customer')->get();
        return view('components.Admin.Task.index',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
