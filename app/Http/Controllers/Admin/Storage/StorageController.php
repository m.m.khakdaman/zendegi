<?php

namespace App\Http\Controllers\Admin\Storage;

use App\Http\Controllers\Controller;
use App\Models\Storage;
use App\Models\Role;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['allitems'] = Storage::with(['keeper'])->where('type','=','اداری')->get();
        $data['nowitems'] = Storage::where('keeper_id', 16241)->get();
        $data['keepingitems'] = Storage::where('keeper_id','<>', 16241)->get();
        $data['brokenitems'] = Storage::where('status','<>', 'سالم')->get();
        $data['admins'] = User::where('user_type','customer')->get();
        return view('components.Admin.Storage.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.Admin.Storage.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = $request->type;
        // return $request;
      
        // $tmp = int::$request->kala_type_id;
    
        Storage::create([
            'title'=> $request->product_name,
            'kala_type_id'=>$request->kala_type_id,
            'label_code'=> $request->label_code,
            'fi'=> $request->fi,
            'buy_price'=> $request->buy_price,
            'factor_number'=> $request->factor_number,
            'date_of_buy'=> $request->date_of_buy,
            'status'=>$request->status,
            'type'=>$request->type,
            'initial_number'=>$request->number,
            'fi'=>$request->fi,
            'remaining_number'=>$request->remaining_number,
            'seller'=>$request->seller,
            'unit'=>$request->unit,
            'note'=> $request->note,
            'keeper_id'=>$request->keeper_id
        ]);
        if ($type == 'اداری') {
            return redirect()->route('storage.index');
        }else{
            return redirect()->route('storage.kalalist');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function kala_type()
    {
        $data = [];
        $data['kala_types'] = Setting::where('key','REGEXP', 'kala')->get();
        return view('components.Admin.Settings.Storage.index',$data);
    }
    
    public function new_kala_type()
    {
        $data=[];
        $data['kala_types'] = Setting::where('key','REGEXP', 'sms_list')->get();
        return view('components\Admin\Settings\Storage\create' ,$data);
    }

    public function kala_type_store(Request $request)
    {
        Setting::create([
            'key'=>$request->key,
            'value' => $request -> value,
            'status' => 1
        ]);
        return redirect()->route('storage.kala_type');
    }

    public function kalalist()
    {
        $data = [];
        $data['allitems'] = Storage::with('kala_type')->where('type', '=', 'مصرفی')->get();
        $data['nowitems'] = Storage::with('kala_type')->where('type', '=', 'مصرفی')->where('remaining_number', '>', 0)->get();
        $data['keepingitems'] = Storage::where('keeper_id','<>', 16241)->get();
        $data['brokenitems'] = Storage::where('status','<>', 'سالم')->get();
        $data['admins'] = User::where('user_type','customer')->get();
        $data['kala_types'] = Setting::where('key','REGEXP', 'kala')->get();

        return view('components.Admin.Storage.kalalist', $data);
    }

    public function use_kala($id)
    {
        // return $id;
        $data = [];
        $data['kala'] = Storage::find($id);
        $data['kala_types'] = Setting::where('key','REGEXP', 'kala')->get();

        return view('components.Admin.Storage.use_kala', $data);
    }
    public function keep_kala($id)
    {
        // return $id;
        $data = [];
        $data['kala'] = Storage::find($id);
        $data['kala_types'] = Setting::where('key','REGEXP', 'kala')->get();
        $data['admins'] = User::where('user_type','customer')->get();

        return view('components.Admin.Storage.keep_kala', $data);
    }
    // public function save_kala(Request $request)
    // {
    //     // return $request;
    //     if ($request->unit == 'فایل' or $request->unit == 'حلقه') {
    //         $tmp = 4;
    //     }else{
    //         $tmp = 5;
    //     }
    //     Product::create([
    //         'product_name'=>$request->product_name,
    //         'product_type_id' =>$tmp,
    //         'session_count'=>$request->number,
    //         'unit'=>$request->unit,
    //         'buy_price'=>$request->buy_price,
    //         'factor_number'=>$request->factor_number,
    //         'seller_name'=>$request->seller_name,
    //         'date_of_buy'=>$request->date_of_buy,
    //         'comment'=>$request->note,
    //         // 'keeper_id'=>$request->,
    //         // 'type''اداری'
    //     ]);
    //     return redirect()->route('storage.kalalist');
    // }
}
