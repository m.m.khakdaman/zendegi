<?php

namespace App\Http\Controllers\Admin\Base;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Base\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['categories'] = Category::all();
        return view('components.Admin.Base.Category.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['categories'] = Category::where(['parent_id'=>'0'])->get();
        return view('components.Admin.Base.Category.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        Category::create([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id
        ]);
        return redirect(route('category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data = [];
        $data['category'] = $category;
        $data['categories'] = Category::where(['parent_id'=>'0',['id','!=',$category->id]])->get();
        return view('components.Admin.Base.Category.edit',$data);
    }
    /**
     * Undocumented function
     *
     * @param Category $category
     * @return void
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id
        ]);
        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        foreach ($category->children as $value) {
            $value->delete();
        }
        $category->delete();
        return back();
    }
}
