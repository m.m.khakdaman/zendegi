<?php

namespace App\Http\Controllers\Admin\Base;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Base\EducationLevelRequest;
use App\Models\EducationLevel;
use Illuminate\Support\Facades\Gate;

class EducationLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('education_level.index');
        $data = [];
        $data['education_levels'] = EducationLevel::all();
        return view('components.Admin.Base.EducationLevel.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('education_level.create');
        return view('components.Admin.Base.EducationLevel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EducationLevelRequest $request)
    {
        Gate::authorize('education_level.create');
        EducationLevel::create([
            'education_level' => $request->title
        ]);


        return redirect(route('education_level.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EducationLevel  $education_level
     * @return \Illuminate\Http\Response
     */
    public function show(EducationLevel $education_level)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EducationLevel  $education_level
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationLevel $education_level)
    {
        Gate::authorize('education_level.edit');
        $data = [];
        $data['education_level'] = $education_level;
        return view('components.Admin.Base.EducationLevel.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EducationLevel  $education_level
     * @return \Illuminate\Http\Response
     */
    public function update(EducationLevelRequest $request, EducationLevel $education_level)
    {
        Gate::authorize('education_level.edit');
        $education_level->update([
            'education_level' => $request->title
        ]);


        return redirect(route('education_level.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EducationLevel  $education_level
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationLevel $education_level)
    {
        Gate::authorize('education_level.delete');
        $education_level->delete();
        return back();
    }
}
