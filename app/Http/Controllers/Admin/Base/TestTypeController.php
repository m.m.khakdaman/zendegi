<?php

namespace App\Http\Controllers\Admin\Base;

use App\Http\Controllers\Controller;
use App\Models\TestType;
use Illuminate\Http\Request;

class TestTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('components.Admin.Base.TestType.index', [
            'types' => TestType::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.Admin.Base.TestType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid_data = $request->validate([
            'title'=>'required',
        ]);

        TestType::create($valid_data);
        return redirect(route('TestType.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TestType  $TestType
     * @return \Illuminate\Http\Response
     */
    public function show(TestType $TestType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TestType  $TestType
     * @return \Illuminate\Http\Response
     */
    public function edit(TestType $TestType)
    {
        return view('components.Admin.Base.TestType.edit',['type'=>$TestType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TestType  $TestType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TestType $TestType)
    {
        $valid_data = $request->validate([
            'title'=>'required',
        ]);

        $TestType->update($valid_data);
        return redirect(route('TestType.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TestType  $TestType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestType $TestType)
    {
        if($TestType->tests()->count() == 0){
            $TestType->delete();
        }
        return redirect(route('TestType.index'));
    }
}
