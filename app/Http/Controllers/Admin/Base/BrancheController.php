<?php

namespace App\Http\Controllers\Admin\Base;

use App\Http\Controllers\Controller;
use App\Models\Branche;
use Illuminate\Http\Request;

class BrancheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['branches'] = Branche::latest()->get();
        return view('components.Admin.Base.Branche.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.Admin.Base.Branche.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Branche::create([
            'name'=>$request->name
        ]);
        return redirect(route('branche.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branche  $branche
     * @return \Illuminate\Http\Response
     */
    public function show(Branche $branche)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branche  $branche
     * @return \Illuminate\Http\Response
     */
    public function edit(Branche $branche)
    {
        return view('components.Admin.Base.Branche.edit',compact('branche'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branche  $branche
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branche $branche)
    {
        $branche->update([
            'name'=>$request->name
        ]);
        return redirect(route('branche.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branche  $branche
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branche $branche)
    {
        $branche->delete();
        return back();
    }
}
