<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{

    public function index()
    {
        $data = [];
        $data['title'] = 'نقش';
        $data['roles'] = Role::latest();
        return view('components.Admin.Role.index', $data);
    }

    public function create()
    {
        $data = [];
        $data['permissions'] = Permission::all();
        $data['title'] = 'نقش';
        return view('components.Admin.Role.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|unique:roles,id',
            'label' => 'string|nullable',
            'per' => 'required|array|exists:permissions,id',
        ]);

        $role = Role::create($request->all());

        /**
         * sync the $role with $request->per
         */

        Role::find($role->id)->Permissions()->sync($request->per);

        return redirect(route('role.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return $role;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $data = [];
        $data['role'] = $role;
        $data['title'] = 'نقش';
        $data['permissions'] = Permission::all();
        return view('components.Admin.Role.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {

        $request->validate([
            'title' => ['required', 'string', Rule::unique('roles')->ignore($role->id)],
            'label' => ['string', 'nullable'],
            'per' => ['array', 'exists:permissions,id'],
        ]);
        $role->update([
            'title' => $request->title,
            'label' => $request->label
        ]);
        /**
         * sync role with permission
         */
        Role::find($role->id)->Permissions()->sync($request->per);
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return back();
    }
}
