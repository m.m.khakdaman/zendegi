<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\LogActivity;
use App\Models\Product;
use App\Models\ProductDay;
use App\Models\Room;
use App\Models\Rooms;
use Illuminate\Http\Request;
use App\Models\Sale;
use App\Models\User;
use Illuminate\Support\Facades\Gate;

class ReportController extends Controller
{
    public function index()
    {
        //
    }

    // صدا کردن فرم انتخاب بازه زمانی برای محاسبه جلسات لغو شده
    public function canceled_session()
    {
        Gate::authorize('report.canceled.sales');
        return view('components.Admin/Report/canceled_sessions_report');
    }

    // محاسبه و چاپ کردن جلسات لغو شده
    public function canceled_session_print(Request $request)
    {
        Gate::authorize('report.canceled.sales');
        // return $request;
        $data = [];
        $data['start_date'] = request('start_date');
        $data['end_date'] = request('end_date');
        $data['start'] = $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $data['end'] = $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
        
        
        $data['sessions'] = Sale::where([
            [
                'updated_at', '>=', $start
            ],
            [
                'updated_at', '<=', $end
            ],
            [
                'products_type_id','=',1
            ],
            [
                'delete_type' , '!=' , '0'
            ]
        ])->with('transactions', 'operator')->get();

        $data['canceleds'] = LogActivity::where([
            [
                'created_at', '>=', $start
            ],
            [
                'created_at', '<=', $end
            ],
            [
               'subject','=','حذف جلسه' 
            ]
        ])->get();

        return view('components.Admin.Report.canceled_session_report', $data);
    }

    public function select_customer_date_form()
    {
        return view('components.Admin.Report.customer_report_select_date');
    }
    public function select_customer()
    {
        $data=[];
        $data['start_date'] = request('start_date');
        $data['end_date'] = request('end_date');
        $data['start'] = $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $data['end'] = $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
        $data['customers'] = User::where([
                    [
                        'created_at', '>=', $start
                    ],
                    [
                        'created_at', '<=', $end
                    ],
                    [
                        'user_type', 'customer'
                    ]
            ])->orderBy('id', 'DESC')->with('operator')->get();
        return view('components.Admin/Report/customer_report_print', $data);
    }
     // صدا کردن فرم انتخاب بازه زمانی برای انتخاب اتاق و گزارشگیری از آن اتاق
     public function select_room()
     {
        //  Gate::authorize('report.canceled.sales');
        $data = [];
        $data['rooms'] = Rooms::all();
         return view('components.Admin.Report.room_report_select_date', $data);
     }
     public function room_report_print(Request $request)
     {        
        $data = [];
        $data['start_date'] = request('start_date');
        $data['end_date'] = request('end_date');
        $data['reserved'] = Product::with('advisor', 'ProductDay')->where('room_id', $request->room_id)->get();
        $data['full'] = ProductDay::with('product')->where('room_id', $request->room_id)->get();
        
        return view('components.Admin/Report/room_report_print', $data);
     }
}
