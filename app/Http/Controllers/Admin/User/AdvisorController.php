<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Advisor\CreateRequest;
use App\Http\Requests\Admin\User\Advisor\EditRequest;
use App\Models\Category;
use App\Models\EducationLevel;
// use App\Models\session_per_day_with_time;
use App\Models\PriceList;
use App\Models\Sale;
use App\Models\Product;
use App\Models\ProductDay;
use App\Models\Role;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserRelUserType;
use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AdvisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::where('user_type', 'advisor')->get();
        return view('components.Admin.User.Advisor.index', $data);
    }
    public function get_all_ajax(Request $request)
    {
        $result = [];

        foreach ($request->columns as $item => $value) {
            if ($value['search']['value'] != null) {
                $result[$item][0] = $value['name'];
                $result[$item][1] = 'like';
                $result[$item][2] = '%' . $value['search']['value'] . '%';
            }
        }
        $users = User::where('user_type', 'advisor')->where($result);
        return [
            "draw" => intval($request->draw),
            "recordsTotal" => $users->count(),
            "recordsFiltered" => $users->count(),
            "data" => $users->skip($request->start)->take($request->length)->orderBy($request->columns[($request->order[0]['column'])]['name'], $request->order[0]['dir'])->get()
        ];
    }
    public function get_all_selection_ajax(Request $request)
    {
        if (strlen($request->term) <= 2) {
            return [];
        }
        $a = ['results' => User::where('user_type', 'advisor')
            ->where(function (Builder $q) use ($request) {
                return $q->where(DB::raw('CONCAT(name,family)'), 'like', '%' . $request->term . '%')
                    ->orWhere(function ($query) use ($request) {
                        $query->where(
                            DB::raw('CONCAT(name," ",family)'),
                            'like',
                            '%' . $request->term . '%'
                        )->orWhere(
                            'mobile',
                            'like',
                            '%' . $request->term . '%'
                        );
                    });
            })->get([
                'id',
                DB::raw('CONCAT(name," ",family,"  «",mobile,"»") as text')
            ]), 'pagination' => true];
        echo json_encode($a, JSON_UNESCAPED_UNICODE);
    }
    public function price_list(User $advisor)
    {
        $price_lists =  $advisor->PriceList;
        foreach ($price_lists as $key => $value) {
            $value->text = $value->name;
        }
        return $price_lists;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['education_levels'] = EducationLevel::all();
        $data['categories'] = Category::all();
        $data['price_lists'] = PriceList::whereActive(1)->get();
        return view('components.Admin.User.Advisor.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'father_name' => $request->father,
            'date_of_birth' => $request->date_of_birth,
            'national_code' => $request->national_code,
            'education_level_id' => $request->education_level_id,
            'field_of_study' => $request->field_of_study,
            'gender' => $request->gender,
            'mobile' => $request->mobile,
            'mobile2' => $request->mobile2,
            'phone' => $request->phone,
            'note' => $request->note,
            'user_type' => 'advisor'
        ]);
        $user->PriceList()->sync($request->price_list);
        $user->Category()->sync($request->category);

        return redirect(route('advisor.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $advisor)
    {
        $data = [];
        $data['user'] = $advisor;
        $data['price_lists'] = PriceList::whereActive(1)->get();

        return view('components.Admin.User.Advisor.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $advisor)
    {
        $data = [];
        $data['user'] = $advisor;
        $data['categories'] = Category::all();
        $data['price_lists'] = PriceList::whereActive(1)->get();
        $data['education_levels'] = EducationLevel::all();
        return view('components.Admin.User.Advisor.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $request, user $advisor)
    {
        $advisor->update([
            'name' => $request->name,
            'family' => $request->family,
            'father_name' => $request->father,
            'date_of_birth' => $request->date_of_birth,
            'national_code' => $request->national_code,
            'education_level_id' => $request->education_level_id,
            'field_of_study' => $request->field_of_study,
            'gender' => $request->gender,
            'mobile' => $request->mobile,
            'mobile2' => $request->mobile2,
            'phone' => $request->phone,
            'note' => $request->note,
        ]);
        $advisor->PriceList()->sync($request->price_list);
        $advisor->Category()->sync($request->category);
        return redirect(route('advisor.index'));
    }
    public function activation (User $advisor)
    {
        if($advisor->status == 'فعال')
        {
            $advisor->update(['status'=>'غیر فعال']);
            flash('وضعیت مشاور '.$advisor->name.' '.$advisor->family.' غیر فعال شد.', 'danger');
        }elseif ($advisor->status == 'غیر فعال') 
        {
            $advisor->update(['status'=>'فعال']);
            flash('وضعیت مشاور '.$advisor->name.' '.$advisor->family.' فعال شد.', 'success');
        }
        return redirect()->route('advisor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return back();
    }

    public function advisor_report()
    {
        Gate::authorize('report.advisor.activities');
        $data['advisors'] = User::where('user_type', 'advisor')->get();
        return view('components.Admin/User/Advisor/advisor_report', $data);
    }
    public function print_report(Request $request)
    {
        Gate::authorize('report.advisor.activities');
        // return $request->admin_id;
        $data = [];

        $data['advisor'] = User::find($request->advisor_id);

        //$data['wsh'] = Sale::where('operator_id', $request->admin_id)->where('products_type_id', 2)->count();
        // $data['amount'] = Sale::where('operator_id', $request->admin_id)->sum('orginal_price');

        // $time = "1394,10,4";
        // return Verta::getGregorian($time,);
        // return Verta::parseFormat($time);
        // return $request->start_date;
        // return verta(str_replace('-', '/', request('start_date')));
        // Verta::getGregorian(1394,10,4);
        // return str_replace('/', ',', request('start_date'));
        // return request('start');

        //محاسبه تمام جلسات مشاور مورد نظر در سال گذشته
        //این آمار برای رسم نمودار به کار می رود
       $data['all_sessions'] = Sale::where([
            [
                'date', '>=', verta()->startYear()->formatGregorian('Y-m-d')
            ],
            [
                'date', '<=', verta()->endYear()->formatGregorian('Y-m-d')
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ],
            [
                'session_status_id', '=', 2
            ]
        ])->get();
      

        $data['from'] = request('start_date');
        $data['to'] = request('end_date');
        $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');


        $data['session'] = Sale::where([
            [
                'date', '>=', $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ]
        ])->count();

        $data['successful_session'] = Sale::where([
            [
                'date', '>=',         $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ],
            [
                'session_status_id', '=', 2
            ]
        ])->count();

        $data['waiting_session'] = Sale::where([
            [
                'date', '>=',         $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ],
            [
                'session_status_id', '=', 1
            ]
        ])->count();

        $data['cancel_payback_session'] = Sale::where([
            [
                'date', '>=',         $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ],
            [
                'session_status_id', '=', 3
            ]
        ])->count();

        $data['cancel_no_payback_session'] = Sale::where([
            [
                'date', '>=',         $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ],
            [
                'session_status_id', '=', 4
            ]
        ])->count();

        $data['customers'] = Sale::where([
            [
                'date', '>=',         $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ],
            [
                'session_status_id', '=', 2
            ]
        ])->with('customer')->get();

    //   return  $collection = collect([1, 2, 2, 2, 3]);

    //     $counted = $collection->countBy();
        
    //     $counted->all();
    


        // return $data['test'] = Sale::select('customer_id')->GroupBY('customer_id')->with('customer')->get();
        // return $data['customers']->GroupBY('customer_id')->count();

        return view('components.Admin/User/Advisor/print', $data);
    }
}
