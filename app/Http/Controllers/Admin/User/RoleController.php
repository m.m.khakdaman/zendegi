<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('role.index');
        $data = [];
        $data['roles'] = Role::latest()->get();
        return view('components.Admin.User.Role.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [];
        $data['permissions'] = Permission::all();
        return view('components.Admin.User.Role.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|unique:roles,title',
            'label' => 'string|nullable',
            'per' => 'required|array|exists:permissions,id',
        ]);

        $role = Role::create($request->all());

        /**
         * sync the $role with $request->per
         */

        Role::find($role->id)->Permissions()->sync($request->per);

        return redirect(route('role.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $data = [];
        $data['role'] = $role;
        $data['permissions'] = Permission::all();
        return view('components.Admin.User.Role.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {

        $request->validate([
            'title' => ['required', 'string', Rule::unique('roles')->ignore($role->id)],
            'label' => ['string', 'nullable'],
            'per' => ['required','array', 'exists:permissions,id'],
        ]);
        $role->update([
            'title' => $request->title,
            'label' => $request->label
        ]);
        /**
         * sync role with permission
         */
        Role::find($role->id)->Permissions()->sync($request->per);
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return back();
    }
}
