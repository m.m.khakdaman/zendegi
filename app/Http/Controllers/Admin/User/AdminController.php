<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Models\Base;
use App\Models\Sale;
use Hekmatinasser\Verta\Verta;
use App\Helpers\LogActivity;
use App\Models\LogActivity as ModelsLogActivity;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['users'] = User::where('user_type', 'admin')->get();
        return view('components.Admin.User.Admin.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $_education_level = new Base('_education_level');
        $data['education_levels'] = $_education_level->get();
        $data['roles'] = Role::latest()->get();

        $_mood = new Base('_mood');
        $data['moods'] = $_mood->get();
        return view('components.Admin.User.Admin.create', $data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, [
            'mobile' => 'required|unique:users,mobile'
        ]);

        $filename = 'avatar.jpg';
        if ($file = $request->file('file')) {

            $destination = base_path() . '/public/assets/images/profile/admin/avatar';
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $filename = rand(1111111, 99999999);
            $file = $request->file('file');
            $filename = $filename . $request->file->getClientOriginalName();
            $file->move($destination, $filename);
        }

        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile,
            'gender' => $request->gender,
            'mobile2' => $request->mobile2,
            'phone' => $request->phone,
            'id_number' => $request->id_number,
            'national_code' => $request->national_code,
            'father_name' => $request->father_name,
            'spouse_name' => $request->spouse_name,
            'reference_source_id' => $request->Introduction_method,
            'note' => $request->note,
            'date_of_birth' => $request->birthday,
            'education_level_id' => $request->education_level,
            'field_of_study' => $request->field_of_study,
            'marital_status_id' => $request->marital_status,
            'date_of_marrige' => $request->date_of_marrige,
            'date_of_divorce' => $request->date_of_divorce,
            'number_of_children' => $request->number_of_children,
            'job' => $request->job,
            'Income_level' => $request->Income_level,
            'connection_method_id' => $request->connection_method,
            'best_time_to_call' => $request->best_time_to_call,
            'time_to_use_internet' => $request->time_to_use_internet,
            'categories_id' => $request->categories,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'educational_resources' => $request->educational_resources,
            'book_and_mag' => $request->book_and_mag,
            'brands' => $request->brands,
            'entertainment' => $request->entertainment,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'advertising_way' => $request->advertising_way,
            'everyday_events' => $request->everyday_events,
            'best_gift' => $request->best_gift,
            'color' => $request->color,
            'status' => $request->status,
            'password' => Hash::make($request->password),
            'pic' => $request->pic,
            'moods_id' => $request->moods_id,
            'user_type' => 'admin',

        ]);
        if (Gate::has('admin.edit_role')) {
            $user->Roles()->sync($request->roles);
        }



        return redirect(route('admin.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $admin)
    {
        $data = [];
        $data['user'] = $admin;
        $comment = $admin->name . ' ' . $admin->family;
        LogActivity::addToLog('مشاهده پروفایل', null, null, $comment);
        return view('components.Admin.User.Admin.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $admin)
    {
        $data = [];

        $_education_level = new Base('_education_level');
        $data['education_levels'] = $_education_level->get();

        $_mood = new Base('_mood');
        $data['moods'] = $_mood->get();
        $data['roles'] = Role::latest()->get();

        $data['user'] = $admin;
        $comment = $admin->name . ' ' . $admin->family;
        LogActivity::addToLog('قصد ویرایش', null, null, $comment);
        return view('components.Admin.User.Admin.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $admin)
    {
        // $this->validate($request, [
        //     // 'mobile1' => ['required', Rule::unique('users','mobile')->ignore($admin->id)]
        // ]);
        if ($file = $request->file('file')) {
            $destination = base_path() . '/public/assets/images/profile/admin/avatar';
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $filename = rand(1111111, 99999999);


            $filename = $filename . $request->file->getClientOriginalName();
            $file->move($destination, $filename);
            $admin->update([
                'pic' => $filename
            ]);
        }
        if (isset($request->password)) {
            $request->validate([
                'password' => 'required|min:6',
                'old_password' => ['required', function ($attribute, $value, $fail) use ($admin) {
                    if (!Hash::check($value, $admin->password)) {
                        LogActivity::addToLog('قصد تغییر رمز', $admin->id, null, $value);
                        $fail('رمز عبور نامعتبر است');
                    }
                }],
            ]);
        }

        $admin->update([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile1,
            'gender' => $request->gender,
            'mobile2' => $request->mobile2,
            'phone' => $request->phone,
            'id_number' => $request->id_number,
            'national_code' => $request->national_code,
            'father_name' => $request->father_name,
            'spouse_name' => $request->spouse_name,
            'reference_source_id' => $request->Introduction_method,
            'note' => $request->note,
            'date_of_birth' => $request->birthday,
            'education_level_id' => $request->education_level,
            'field_of_study' => $request->field_of_study,
            'marital_status_id' => $request->marital_status,
            'date_of_marrige' => $request->date_of_marrige,
            'date_of_divorce' => $request->date_of_divorce,
            'number_of_children' => $request->number_of_children,
            'job' => $request->job,
            'Income_level' => $request->Income_level,
            'connection_method_id' => $request->connection_method,
            'best_time_to_call' => $request->best_time_to_call,
            'time_to_use_internet' => $request->time_to_use_internet,
            'categories_id' => $request->categories,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'educational_resources' => $request->educational_resources,
            'book_and_mag' => $request->book_and_mag,
            'brands' => $request->brands,
            'entertainment' => $request->entertainment,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'advertising_way' => $request->advertising_way,
            'everyday_events' => $request->everyday_events,
            'best_gift' => $request->best_gift,
            'color' => $request->color,
            'status' => $request->status,


            'moods_id' => $request->moods_id,
            'password' => $request->password != null ? Hash::make($request->password) : $admin->password
        ]);
        if (Gate::has('admin.edit_role')) {
            $admin->Roles()->sync($request->roles);
        }

        return redirect(route('admin.index'));
    }

    public function activation (User $admin)
    {
        if($admin->status == 'فعال')
        {
            $admin->update(['status'=>'غیر فعال']);
            flash('وضعیت کاربر '.$admin->name.' '.$admin->family.' غیر فعال شد.', 'danger');
        }elseif ($admin->status == 'غیر فعال') 
        {
            $admin->update(['status'=>'فعال']);
            flash('وضعیت کاربر '.$admin->name.' '.$admin->family.' فعال شد.', 'success');
        }
        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    // فراخوانی صفحه انتخاب کاربر برای گزارش گیری از کارکنان
    public function admin_report()
    {
        Gate::authorize('report.admin.activities');

        $data['admins'] = User::where('user_type', 'admin')->where('status', 'فعال')->get();
        return view('components.Admin/User/Admin/admin_report', $data);
    }
    // تهیه گزارش از کارکنان برای گزارش عملکرد کارکنان
    public function print_report(Request $request)
    {
        Gate::authorize('report.admin.activities');
        // return $request->admin_id;
        $data = [];

        $data['admin'] = User::find($request->admin_id);
        // $data['wsh'] = Sale::where('operator_id', $request->admin_id)->where('products_type_id', 2)->count();
        // $data['amount'] = Sale::where('operator_id', $request->admin_id)->sum('orginal_price');
        $data['start_date'] = request('start_date');
        $data['end_date'] = request('end_date');
        $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');

        $data['all_session'] = Sale::where([
            [
                'created_at', '>=', $start
            ],
            [
                'created_at', '<=', $end
            ],
            [
                'products_type_id', '=', 1
            ]
        ])->with('transactions')->get();

        //تعداد جلساتی که این کاربر بسته است
        $data['closed_all_session_num'] = $data['all_session']->where('session_status_id', '=', 2)->count();


        $data['session'] = $data['all_session']->where('operator_id', $request->admin_id);
        ////////////////////////////////////////////////////////////////////////
        $data['registred_session'] = $data['session']->count();
        $data['registred_session_amount'] = $data['session']->sum('orginal_price');
        $data['payed_session'] =  $data['session']->where('transactions.amount_standing', 0);
        $data['debt_session'] =  $data['session']->where('transactions.amount_standing', '!=', 0);
        $data['payed_session_amount'] =  $data['registred_session_amount'] + $data['session']->sum('transactions.amount_standing');
        $data['closed_session'] = $data['session']->where('session_status_id', '=', 2);

        //تعداد جلساتی که این کاربر بسته است و خودش نیز آنها را ثبت کرده است
        $data['closed_session_num'] = $data['closed_session']->count();
        // $data['closed_session_amount'] = $data['closed_session']->sum('orginal_price');

        $data['open_session_num'] = $data['session']->where('delete_type', '=', 0)->where('session_status_id', '=', 1)->count();
        $data['open_good_session_num'] = $data['session']->where('delete_type', '=', 0)->where('session_status_id', '=', 1)->where('date', '>=', $end)->count();
        $data['open_bad_session'] = $data['session']->where('delete_type', '=', 0)->where('session_status_id', '=', 1)->where('date', '<', $end);
        $data['canceled_session_num'] = $data['session']->where('delete_type', '>', 0)->count();
        $data['session_total_amount'] = $data['session']->where('delete_type', '>', 0)->sum('total_price');
        $data['session_discount'] = $data['session']->sum('discount');

        $data['amount'] = Sale::where([
            [
                'date', '>=', $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'operator_id', '=', $request->admin_id
            ]
        ])->sum('orginal_price');

        $data['wsh'] = Sale::where([
            [
                'date', '>=', $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'operator_id', '=', $request->admin_id
            ],
            [
                'products_type_id', '=', 2
            ]
        ])->get();
        $data['wsh_num'] = $data['wsh']->count();
        $data['wsh_amount'] = $data['wsh']->sum('total_price');
        $data['wsh_discount'] = $data['wsh']->sum('discount');



        $data['members'] = Sale::where([
            [
                'date', '>=', $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'operator_id', '=', $request->admin_id
            ],
            [
                'products_type_id', '=', 1
            ]
        ])->with('customer', 'advisor', 'day')->get();


        $data['lists'] = Sale::where([
            [
                'created_at', '>=', $start
            ],
            [
                'created_at', '<=', $end
            ],
            [
                'operator_id', '=', $request->admin_id
            ],
        ])->with('transactions')->orderBy('date', 'ASC')->get();
        #$data['session'] = Sale::where('operator_id', $request->admin_id)->where('products_type_id', 1)->count();

        return view('components.Admin.User.Admin.print', $data);
    }

    public function admin_full_report()
    {
        Gate::authorize('report.full');
        $data['admins'] = User::where('user_type', 'admin')->get();
        return view('components.Admin/User/Admin/full_report_admin', $data);
    }

    public function admin_full_report_print(Request $request)
    {
        Gate::authorize('report.full');
        // return $request->admin_id;
        $data = [];
        $data['admin'] = 'all';
        $data['start_date'] = request('start_date');
        $data['end_date'] = request('end_date');
        $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');

        $data['logs'] = ModelsLogActivity::where([
            [
                'created_at', '>=', $start
            ],
            [
                'created_at', '<=', $end
            ]
        ])->with('user')->orderBy('id', 'desc')->limit(1000)->get();

        if ($request->admin_id != 'all') {
            $data['admin'] = User::find($request->admin_id);
            $data['logs'] = $data['logs']->where('user_id', $request->admin_id);
        }

        return view('components.Admin/User/Admin/full_report_admin_print', $data);
    }
    public function shift_print_report(Request $request)
    {
        // return $request->admin_id;
        $data = [];
        $data['start_date'] = request('start_date');
        $data['end_date'] = request('end_date');
        $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');

        //هنگام گزارش تحویل شیفت تمام کارکنان این شرط فعال می شود و گزارش تمام کارکنان را نمایش می دهد
        if ($request->admin_id == 'all') {
            $data['transactions'] = Transaction::where([
                [
                    'created_at', '>=', $start
                ],
                [
                    'created_at', '<=', $end
                ],
                [
                    'transaction_type_id', '!=', 0
                ],
                [
                    'user_id', '!=', setting('getting_acount')
                ]
            ])->with('cash', 'sale', 'transaction_type', 'operator')->get();

            // $data['transactions'] = $data['transactions']->groupBy('operator_id');

            return view('components.Admin/User/Admin/admins_shift_report_print', $data);
            //اما هنگام گزارش تحویل شیفت یک نفر از کارکنان این بخش شرط فعال می شود و گزارش کاربر مورد نظر گرفته می شود
        } else {
            $data['admin'] = User::find($request->admin_id);

            // $data['sessions'] = Sale::where([
            //     [
            //         'created_at', '>=', $start
            //     ],
            //     [
            //         'created_at', '<=', $end
            //     ],
            //     [
            //         'operator_id', $request->admin_id
            //     ]
            // ])->with('transactions', 'products_type')->get();

            $data['transactions'] = Transaction::where([
                [
                    'created_at', '>=', $start
                ],
                [
                    'created_at', '<=', $end
                ],
                [
                    'operator_id', $request->admin_id
                ],
                [
                    'transaction_type_id', '!=', 0
                ],
                [
                    'user_id', '!=', setting('getting_acount')
                ]
            ])->with('cash', 'sale', 'transaction_type')->get();
            return view('components.Admin/User/Admin/admin_shift_report_print', $data);
        }
    }
}
