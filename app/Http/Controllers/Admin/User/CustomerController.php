<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\CustomerRequest;
use App\Models\Base;
use App\Models\Category;
use App\Models\Role;
use App\Models\Sale;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Mood;
use App\Models\Marital_status;
use App\Models\Introduction_method;
use App\Models\History;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        // return $data['users'] = Role::where('name', 'admin')->first()->users()->update(['user_type'=>'admin']);
        $data['users'] = User::where('user_type', 'customer')->orderBy('id', 'DESC')->paginate(10);
        return view('components.Admin.User.Customer.index', $data);
    }
    public function get_all_ajax(Request $request)
    {
        if (!$request->term) {
            $a = ['results' => User::where('user_type', 'customer')
                ->take(10)
                ->get([
                    'id',
                    DB::raw('CONCAT(name," ",family,"  «",mobile,"»") as text')
                ]), 'pagination' => true];
            echo json_encode($a, JSON_UNESCAPED_UNICODE);
            return;
        }
        if (strlen($request->term) <= 2) {
            return [];
        }
        $a = ['results' => User::where('user_type', 'customer')
            ->where(function (Builder $q) use ($request) {
                return $q->where(DB::raw('CONCAT(name,family)'), 'like', '%' . $request->term . '%')
                    ->orWhere(function ($query) use ($request) {
                        $query->where(
                            DB::raw('CONCAT(name," ",family)'),
                            'like',
                            '%' . $request->term . '%'
                        )->orWhere(
                            'mobile',
                            'like',
                            '%' . $request->term . '%'
                        );
                    });
            })->get([
                'id',
                DB::raw('CONCAT(name," ",family,"  «",mobile,"»") as text')
            ]), 'pagination' => true];
        echo json_encode($a, JSON_UNESCAPED_UNICODE);
    }
    public function get_all_datatable_ajax(Request $request)
    {
        $result = [];
        foreach ($request->columns as $item => $value) {
            if ($value['search']['value'] != null) {
                $result[$item][0] = $value['name'];
                $result[$item][1] = 'like';
                $result[$item][2] = '%' . $value['search']['value'] . '%';
            }
        }
        $users = User::where('user_type', 'customer')->where($result);
        return [
            "draw" => intval($request->draw),
            "recordsTotal" => $users->count(),
            "recordsFiltered" => $users->count(),
            "data" => $users->skip($request->start)->take($request->length)->orderBy($request->columns[($request->order[0]['column'])]['name'], $request->order[0]['dir'])->get()
        ];
    }
    public function customer_paginate(Request $request)
    {
        return $request;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $_education_level = new Base('_education_level');
        $data['education_levels'] = $_education_level->get();

        $_connection_method = new Base('_connection_method');
        $data['connection_methods'] = $_connection_method->get();

        $_categories = new Base('_categories');
        $data['categories'] = $_categories->get();

        $data['advisors'] = User::where('user_type', 'advisor')->get();
        $data['admins'] = User::where('user_type', 'customer')->get();
        $data['introduction_methods'] = Introduction_method::where('status', '1')->get();
        // $data['introduction_methods'] = Introduction_method::all();
        return view('components.Admin.User.Customer.create', $data);
    }
    public function simple_create()
    {
        $data['introduction_methods'] = Introduction_method::where('status', '1')->get();
        return view('components.Admin.User.Customer.simple_craete', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {

        $filename = 'avatar.jpg';
        if ($file = $request->file('file')) {

            $destination = base_path() . '/public/assets/images/profile/customer/avatar';
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $filename = rand(1111111, 99999999);
            $file = $request->file('file');
            $filename = $filename . $request->file->getClientOriginalName();
            $file->move($destination, $filename);
        }

        $user = User::create([
            'name' => $request->firstName,
            'family' => $request->lastName,
            'mobile' => $request->Fmobile,
            'gender' => $request->gender,
            'mobile2' => $request->Smobile,
            'phone' => $request->phone,
            'id_number' => $request->id_number,
            'national_code' => $request->national_code,
            'account_number' => $request->account_number,
            'old_file_number' => $request->old_file_number,
            'father_name' => $request->father_name,
            'spouse_name' => $request->spouse_name,
            'reference_source_id' => $request->Introduction_method,
            'note' => $request->note,
            'date_of_birth' => $request->birthday,
            'education_level_id' => $request->education_level,
            'field_of_study' => $request->field_of_study,
            'marital_status_id' => $request->marital_status,
            'date_of_marrige' => $request->date_of_marrige,
            'date_of_divorce' => $request->date_of_divorce,
            'number_of_children' => $request->number_of_children,
            'job' => $request->job,
            'Income_level' => $request->Income_level,
            'connection_method_id' => $request->connection_method,
            'best_time_to_call' => $request->best_time_to_call,
            'time_to_use_internet' => $request->time_to_use_internet,
            'categories_id' => $request->categories,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'educational_resources' => $request->educational_resources,
            'book_and_mag' => $request->book_and_mag,
            'brands' => $request->brands,
            'entertainment' => $request->entertainment,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'advertising_way' => $request->advertising_way,
            'everyday_events' => $request->everyday_events,
            'best_gift' => $request->best_gift,
            'color' => $request->color,
            'user_type' => 'customer',


            'pic' => $filename,
            'moods_id' => $request->mood,
            'SpecialUser' => ($request->SpecialUser ? 1 : false),

        ]);


        //    return $customer_id = User::
        //    return $data['last_customer'] = User::latest();
        $history = History::create([
            'history_title' => $request->history_title,
            'sender_id' => auth()->user()->id,
            'resiver_id' => $request->resiver_id,
            'suggestions' => $request->suggestions,
            'customer_id' => $user->id,
            'importance' => 'آنی',
            'task_flag' => 'persona',
            'date_of_send' =>  verta()->format('Y/n/j H:i'),
            'date_of_answer' => verta()->format('Y/n/j')
        ]);
        SendSMS(setting('sms_list113654'), $user->mobile);

        // sms()

        return redirect(route('customer.index'));
    }

    public function simple_store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'family' => 'required|string',
            'mobile1' => 'required',
            'Introduction_method' => 'required',
        ]);
        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile1,
            'reference_source_id' => $request->Introduction_method,
            'gender' => $request->gender,
            'user_type' => 'customer',
        ]);
        SendSMS(setting('sms_list113654'), $user->mobile);
        if ($request->back_url == "") {
            return redirect(route('customer.index'));
        } else {
            return redirect($request->back_url);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $customer)
    {
        $data = [];
        $data['user'] = $customer;
        $data['sessions'] = $customer->sales()->where(['products_type_id' => Setting::get('session_type_id'), 'delete_type' => '0'])->with(['advisor', 'operator', 'day'])->latest()->get();
        $data['wshs'] = $customer->sales()->where('products_type_id', Setting::get('workshop_type_id'))->with(['advisor', 'operator', 'product'])->latest()->get();
        $data['transaction'] = $customer->transactions()->with(['operator', 'transaction_type', 'sale.product'])->latest()->get();
        // $data['transaction'] = Transaction::where([
        //     [
        //         "user_id",
        //         "=",
        //         $customer->id,
        //     ]
        // ])->with(['operator', 'sale.product'])->latest()->get();

        // $data['advisors'] = User::where('user_type','advisor')->get();
        // $data['customers'] = User::where('user_type','customer')->get();

        return view('components.Admin.User.Customer.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $customer)
    {
        $data = [];
        $data['user'] = $customer;
        $data['category'] = Category::all();
        $data['mood'] = Mood::all();
        $data['introduction_methods'] = Introduction_method::where('status', '1')->get();
        $data['marital_status'] = Marital_status::all();
        return view('components.Admin.User.Customer.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, User $customer)
    // {
    //     // return $request;
    //     $customer->updated([
    //         'name' => $request->name
    //     ]);

    // }
    public function update(Request $request, User $customer)
    {
        // return $request;
        if ($file = $request->file('file')) {
            $destination = base_path() . '/public/assets/images/profile/customer/avatar';
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $filename = rand(1111111, 99999999);


            $filename = $filename . $request->file->getClientOriginalName();
            $file->move($destination, $filename);
            $customer->update([
                'pic' => $filename
            ]);
        };

        $customer->update([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile1,
            'gender' => $request->gender,
            'mobile2' => $request->mobile2,
            'phone' => $request->phone,
            'id_number' => $request->id_number,
            'national_code' => $request->national_code,
            'account_number' => $request->account_number,
            'old_file_number' => $request->old_file_number,
            'father_name' => $request->father_name,
            'spouse_name' => $request->spouse_name,
            'reference_source_id' => $request->Introduction_method,
            'note' => $request->note,
            'date_of_birth' => $request->date_of_birth,
            'education_level_id' => $request->education_level,
            'field_of_study' => $request->field_of_study,
            'marital_status_id' => $request->marital_status,
            'date_of_marrige' => $request->date_of_marrige,
            'date_of_divorce' => $request->date_of_divorce,
            'number_of_children' => $request->number_of_children,
            'job' => $request->job,
            'Income_level' => $request->Income_level,
            'connection_method_id' => $request->connection_method,
            'best_time_to_call' => $request->best_time_to_call,
            'time_to_use_internet' => $request->time_to_use_internet,
            'categories_id' => $request->categories,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'educational_resources' => $request->educational_resources,
            'book_and_mag' => $request->book_and_mag,
            'brands' => $request->brands,
            'entertainment' => $request->entertainment,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'advertising_way' => $request->advertising_way,
            'everyday_events' => $request->everyday_events,
            'best_gift' => $request->best_gift,
            'color' => $request->color,
            'SpecialUser' => ($request->SpecialUser ? 1 : false),

            'moods_id' => $request->moods_id,
            // 'password' => $request->password != null ? Hash::make($request->password) : $admin->password
        ]);

        return redirect(route('customer.index'));
    }
    public function activation(User $customer)
    {
        if ($customer->status == 'فعال') {
            $customer->update(['status' => 'غیر فعال']);
            flash('وضعیت مراجع ' . $customer->name . ' ' . $customer->family . ' غیر فعال شد.', 'danger');
        } else {
            $customer->update(['status' => 'فعال']);
            flash('وضعیت مراجع ' . $customer->name . ' ' . $customer->family . ' فعال شد.', 'success');
        }
        return redirect()->route('customer.index');
    }
    public function vip(User $customer)
    {
        if ($customer->SpecialUser == 1) {
            $customer->update(['SpecialUser' => 0]);
            flash($customer->name . ' ' . $customer->family . ' مشتری معمولی شد.', 'warning');
        } else {
            $customer->update(['SpecialUser' => 1]);
            flash($customer->name . ' ' . $customer->family . ' مشتری ویژه شد.', 'success');
        }
        return redirect()->route('customer.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $customer)
    {
        $customer->delete();
        return back();
    }
}
