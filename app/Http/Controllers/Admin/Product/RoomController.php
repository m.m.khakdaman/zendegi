<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Branche;
use App\Models\Building;
use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['rooms'] = Room::orderBy('order', 'ASC')->with('building')->get();
        return view('components.Admin.Product.Room.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['branchs'] = Branche::all();
        $data['buildings'] = Building::all();
        return view('components.Admin.Product.Room.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Room::create([
            'name' => $request->name,
            'branch_id' => $request->branch,
            'building_id' => $request->building,
        ]);
        return redirect(route('room.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $data['branchs'] = Branche::all();
        $data['room'] = $room;
        $data['buildings'] = Building::all();
        return view('components.Admin.Product.Room.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $room->update([
            'name' => $request->name,
            'branch_id' => $request->branch,
            'building_id' => $request->building,
        ]);
        return redirect(route('room.index'));
    }
    // این تابع برای تغییر چیدمان اتاقها کاربرد دارد
    public function order($id, $side)
    {
        $room = Room::find($id);
        $tmp = $room->order;
        if ($side == 'up') {
            $tmp = $tmp-1;
            $uproom = Room::select('id')->where('order', $room->order-1)->get()->first();
            Room::where('id', $id)->decrement('order');
            Room::where('id', $uproom->id)->increment('order');
        } elseif ($side == 'down') {
            $tmp = $tmp+1;
            $downroom = Room::where('order', $room->order+1)->first();
            Room::where('id', $id)->increment('order');
            Room::where('id', $downroom->id)->decrement('order');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();
        return back();
    }
}
