<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductDay;
use App\Models\Room;
use App\Models\Sale;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdvisorSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\User  $advisor
     * @return \Illuminate\Http\Response
     */
    public function index(User $advisor)
    {
        $data = [];
        $data['advisor'] = $advisor;
        $data['sessions'] = $advisor->products->where('product_type_id', setting('prudcot_session_type_id'));
        return view('components.Admin.User.Advisor.Session.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\User  $advisor
     * @return \Illuminate\Http\Response
     */
    public function create(User $advisor)
    {
        if (request('type')) {

            $data = [];
            $data['advisor'] = $advisor;
            $data['categories'] = Category::where(['parent_id' => '0'])->get();
            $data['rooms'] = Room::all();
            return view('components.Admin.User.Advisor.Session.create_not_present', $data);
        }
        $data = [];
        $data['advisor'] = $advisor;
        $data['categories'] = Category::where(['parent_id' => '0'])->get();
        $data['rooms'] = Room::all();
        return view('components.Admin.User.Advisor.Session.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $advisor
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $advisor)
    {

        if (isset($request->contact) && isset($request->type)) {

            foreach ($request->contact as $value) {
                $x[] = DB::select('SELECT
                        day_of_product.*,
                        days.dey_of_week,
                        days.miladi_date 
                    FROM
                        products
                        INNER JOIN day_of_product ON products.id = day_of_product.product_id
                        INNER JOIN users ON products.advisor_id = users.id
                        INNER JOIN days ON days.dey_of_week = day_of_product.day_of_week 
                    WHERE
                        products.`start` <= days.date AND products.`end` >= days.date
                        And day_of_product.day_of_week = "' . $value['day'] . '"
                        And days.miladi_date < "' . Verta::parse($request->end_date)->formatGregorian('Y-m-d') . '" 
                        And days.miladi_date >= "' . Verta::parse($request->start_date)->formatGregorian('Y-m-d') . '"
                        And day_of_product.start < "' . $value['end_time'] . '" 
                        And day_of_product.end >= "' . $value['start_time'] . '"
                        And day_of_product.room_id = "' . $value['room'] . '"
                ');
            }
            $errors = [];
            foreach ($x as $d) {
                if (count($d) != 0) {
                    $product = Product::find($d[0]->product_id);
                    $errors[]['products'] = 'تداخل با برنامه ی ' . $product->name . ' در مشاور ' . $product->advisor->name . ' ' . $product->advisor->family;
                }
            }

            if (count($errors) != '0') {
                return redirect(route('advisor.session.create', $advisor->id))->withErrors($errors)
                    ->withInput();
            }
        }
        $product = Product::create([
            'product_name' => $request->title,
            'advisor_id' => $advisor->id,
            'product_type_id' => setting('prudcot_session_type_id'),
            'start' => $request->start_date,
            'type' => isset($request->type) ? 2 : 1,
            'end' => $request->end_date,
        ]);
        if (isset($request->contact)) {
            foreach ($request->contact as $day) {
                ProductDay::insert([
                    'product_id' => $product->id,
                    'start' => $day['start_time'],
                    'room_id' => $day['room'],
                    'color' => $day['color'],
                    'end' => $day['end_time'],
                    'day_of_week' => $day['day']
                ]);
            }
        }

        return redirect(route('advisor.session.index', $advisor->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $advisor
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $advisor
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($session)
    {
        $data['rooms'] = Room::all();
        $data['session'] = Product::whereId($session)->with('advisor')->first();
        return view('components.Admin.User.Advisor.Session.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $advisor
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, product $session)
    {
        if (isset($request->contact)) {

            foreach ($request->contact as $value) {
                $x[] = DB::select('SELECT
                        day_of_product.*,
                        days.dey_of_week,
                        days.miladi_date 
                    FROM
                        products
                        INNER JOIN day_of_product ON products.id = day_of_product.product_id
                        INNER JOIN users ON products.advisor_id = users.id
                        INNER JOIN days ON days.dey_of_week = day_of_product.day_of_week 
                    WHERE
                        products.`start` <= days.date AND products.`end` >= days.date
                        And day_of_product.day_of_week = "' . $value['day'] . '"
                        And day_of_product.product_id != "' . $session->id . '"
                        And days.miladi_date < "' . Verta::parse($request->end_date)->formatGregorian('Y-m-d') . '" 
                        And days.miladi_date >= "' . Verta::parse($request->start_date)->formatGregorian('Y-m-d') . '"
                        And day_of_product.start < "' . $value['end_time'] . '" 
                        And day_of_product.end >= "' . $value['start_time'] . '"
                        And day_of_product.room_id = "' . $value['room'] . '"
                ');
            }
            $errors = [];
            foreach ($x as $d) {
                if (count($d) != 0) {
                    $product = Product::find($d[0]->product_id);
                    $errors[]['product'] = 'تداخل با برنامه ی شماره ' . $product->id . ' در مشاور ' . $product->advisor->name . ' ' . $product->advisor->family;
                }
            }

            if (count($errors) != '0') {
                return redirect(route('session.edit', $session))->withErrors($errors)
                    ->withInput();
            }
        }

        $session->ProductDay()->delete();
        $product = $session->update([
            'product_name' => $request->title,
            'advisor_id' => $session->advisor_id,
            'product_type_id' => setting('prudcot_session_type_id'),
            'start' => $request->start_date,
            'end' => $request->end_date,
        ]);
        if (isset($request->contact)) {
            foreach ($request->contact as $day) {
                ProductDay::insert([
                    'product_id' => $session->id,
                    'room_id' => $day['room'],
                    'color' => $day['color'],
                    'start' => $day['start_time'],
                    'end' => $day['end_time'],
                    'day_of_week' => $day['day']
                ]);
            }
        }

        return redirect(route('advisor.session.index', $session->advisor_id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $advisor
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(product $session)
    {
        $session->ProductDay()->delete();
        $session->delete();
        return redirect(route('advisor.session.index', $session->advisor_id));
    }
    public function send_product_session($request)
    {
        return $request;
    }

    public function print_session(User $advisor)
    {
        if (request('start_date')) {

            $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');;

            if (request('end_date')) {
                $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
            } else {
                $end = verta()->parse(request('start_date'))->addDays(1)->formatGregorian('Y-m-d');
            }

            $data['sales'] = Sale::where([
                [
                    'date',
                    '<',
                    $end
                ],
                [
                    'date',
                    '>=',
                    $start
                ],
                [
                    'delete_type',
                    '=',
                    '0'
                ],
                [
                    'advisor_id',
                    '=',
                    $advisor->id
                ]
            ])->orderBy('session_start')->with('customer')->get();
            $data['advisor'] = $advisor;
            return view('components.Admin.User.Advisor.Session.print_page', $data);
        }
        $data['advisor'] = $advisor;
        return view('components.Admin.User.Advisor.Session.print', $data);
    }
}
