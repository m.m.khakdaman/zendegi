<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Sale;
use App\Models\Transaction;
use App\Models\Workshop_session;
use App\Models\Workshop_attendance;


class WorkShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $customer)
    {
        $a=Setting::get('workshop_type_id');
        $data = [];
        $data['workshops'] = Product::where('product_type_id',$a)->with('advisor','operator')->latest()->get();
        return view('components.Admin.Product.WorkShop.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $a=Setting::get('workshop_type_id');
        $data = [];
        $data['advisors'] = User::where('user_type','advisor')->get();
        $data['categories'] = Category::where('parent_id', 11)->get();
        return view('components.Admin.Product.WorkShop.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $s_number = $request->s_number;

        $product = Product::create([
            'product_name'=>$request->wsh_title ,
            'product_type_id' => Setting::get('workshop_type_id'),
            'advisor_id'=>$request->wsh_advisor ,
            'category_id'=>$request->wsh_category ,
            'holding'=>$request->wsh_type,
            'start'=>$request->date_of_start ,
            'start_hour'=>$request->start_hour ,
            'location'=>$request->location ,
            'end'=>$request->date_of_end ,
            'session_count'=>$request->s_number ,
            'hour_sum'=>$request->sumtime ,
            'price'=>$request->price ,
            'internet_price'=>$request->internet_price ,
            'internet_access'=>$request->internet_register ,
            'sahm_moshaver_sabet'=>$request-> sahm_moshaver_sabet,
            'status'=>$request->status ,
            'internet_note'=>$request->internet_note ,
            'comment'=>$request->comment,
            'capacity'=>$request->wsh_capacity,
            'operator_id'=> auth()->user()->id 
        ]);

        // for ($i = 1; $i <= $s_number; $i++){
        // Workshop_session::create([
        //     'session_number'=>$i,
        //     'product_id'=>$product->id,
        //     'date'=>verta()->format('Y/m//D')
        // ]);
        // }   
        return redirect()->route('workshop.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $workshop
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Product $workshop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $workshop
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $workshop)
    {
        $data = [];
        $data['advisors'] = User::where('user_type','advisor')->get();
        $data['categories'] = Category::where('parent_id', 11)->get();
        $data['wsh'] = $workshop;
        return view('components.Admin.Product.WorkShop.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $workshop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Product::where('id', $id)->update([
            'product_name'=>$request->wsh_title,
            'advisor_id'=>$request->wsh_advisor,
            'category_id'=>$request->wsh_category,
            'holding'=>$request->holding,
            'start'=>$request->date_of_start ,
            'location'=>$request->location ,
            'end'=>$request->date_of_end ,
            'session_count'=>$request->s_number,
            'hour_sum'=>$request->sumtime,
            'price'=>$request->price,
            'internet_price'=>$request->internet_price ,
            'internet_access'=>$request->internet_register,
            'sahm_moshaver_sabet'=>$request->sahm_moshaver_sabet ,
            'sahm_moshaver_darsad'=>$request->sahm_moshaver_darsad ,
            'comment'=>$request->comment,
            'capacity'=>$request->wsh_capacity,
            'internet_note'=>$request->internet_note,
            'editor_id'=>auth()->user()->id,
        ]);
        return redirect()->route('workshop.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $workshop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $workshop)
    {
        //
    }

    public function about($id)
    {
        $data = [];
        $data['wsh'] = Product::with('advisor', 'category')->find($id);
        $data['members'] = Sale::with('customer')->where('product_id',$id)->get();
        return view('components.Admin/Product/WorkShop/wsh_about', $data);
    }

    public function print_about($id)
    {
        $data = [];
        $data['wsh'] = Product::with('advisor', 'category')->find($id);
        $data['members'] = Sale::with('customer')->where('product_id',$id)->get();
        return view('components.Admin/Product/WorkShop/wsh_print_about', $data);  
    }

    public function members($id)
    {
        $data = [];
        $data['members'] = Sale::with('customer', 'operator', 'why_cancel')->where('product_id',$id)->where('products_type_id', 2)->get();
        $data['wsh'] = product::find($id);
        // $data['customers'] = User::where('user_type','customer')->orderBy('id', 'DESC')->paginate(10);
        return view('components.Admin/Product/WorkShop/wsh_members', $data);
    }

    public function workshop_sessions($id)
    {
        // return $id;
        $data = [];
        $data['product_id'] = $id;
        $data['product_name'] = Product::find($id);
        $data['wsh_sessions'] = Workshop_session::with('wsh_name')->where('product_id','=',$id)->get();

        return view('components.Admin/Product/WorkShop/wsh_sessions', $data);
    }

    public function add_session(Request $request)
    {
        $id = $request->product_id;
        $session_number = Workshop_session::where('product_id','=',$id)->max('session_number') + 1;
        // return $request;
        Workshop_session::create([
            'session_number' => $session_number,
            'product_id' => $id,
            'date'=>$request->date,
            'time'=>$request->time
        ]);

        return redirect()->route('workshop.sessions',$id);
    }

    public function edit_session($id)
    {
        $data['session'] = Workshop_session::with('wsh_name')->find($id);
        return view('components.Admin/Product/WorkShop/wsh_session_edit', $data);
    }

    public function update_session(Request $request, $id)
    {
         Workshop_session::where('id', $id)->update([
            'date'=>$request->date,
            'time'=>$request->time
        ]);
        return redirect()->route('workshop.sessions', $request->product_id);
    }

    public function add_member(Request $request)
    {
    // return $request;
    $id = $request->product_id;
        $sale = Sale::create([
            'advisor_id'=>$request->advisor_id,
            'customer_id'=>$request->customer_id,
            'operator_id'=>auth()->user()->id,
            'date'=>verta()->formatGregorian('Y-m-d'),
            'date2'=>verta()->format('Y/m/d'),
            'time'=>verta()->format('H:j'),
            'session_time',
            'session_start',
            'session_length',
            'session_type_id',
            'session_status_id',
            'category_id',
            'room_id',
            'reservation_type_id',
            'orginal_price'=>$request->orginal_price,
            'qty'=>1,
            'discount'=>$request->orgibal_price - $request->price,
            'total_price'=>$request->price,
            'product_id'=>$request->product_id,
            'products_type_id'=>$request->products_type_id,
            'modekhele',
            'advisor_report',
            'diagnosis1_id',
            'diagnosis2_id',
            'diagnosis3_id',
            'price_list_id',
            'costomer_commant',
            'costomer_score',
            'comment'=>$request->comment,
            'delete_comment',
            'delete_type',
            'status'=> 1
        ]);
        Transaction::create([
            'user_id'=>$request->customer_id,
            'amount'=>-intval($request->price),
            'amount_standing'=>-intval($request->price),
            'cash_id'=> '1',
            'operator_id'=>auth()->user()->id,
            'sale_id'=> $sale->id,
            'transaction_type_id',
            'comment'=>$request->comment,
            'tracking_code',
            'status',
            'deposit_date'=>verta()->format('Y/m/d H:s'),
            'yaribarg_date_of_use',
            'yaribarg_status',
            'yaribarg_type_id'
        ]);
        return redirect()->route('workshop.members',$id);
    }

    public function cancel_member_form($id, $sale_id)
    {

        $data = [];
        $data['customer'] = User::find($id);
        $data['wsh'] = Sale::find($sale_id);
        $data['wsh_name'] = Product::find($data['wsh']->product_id);
        $data['debt'] = Transaction::where('user_id', $id)->sum('amount_standing');
        return view('components.Admin/Product/WorkShop/wsh_cancel_form', $data);
    }

    public function cancel_member(Request $request)
    {
        // return $request;
        Sale::where('id', $request->sale_id)->update([
            'status'=> 0,
            'delete_type'=> $request->delete_type
        ]);
        Transaction::create([
            'user_id'=>$request->user_id,
            'amount'=>intval($request->payback),
            'amount_standing'=>intval($request->payback),
            'cash_id'=> '1',
            'operator_id'=>auth()->user()->id,
            'sale_id'=> $request->sale_id,
            'transaction_type_id'=>1,
            'comment'=>$request->comment,
            'tracking_code',
            'status',
            'deposit_date'=>verta()->format('Y/m/d H:s'),
            'yaribarg_date_of_use',
            'yaribarg_status',
            'yaribarg_type_id'
        ]);

        return redirect()->route('workshop.members', $request->wsh_num);
    }

    public function attendance_list($id)
    {
        $data['session_numbers'] = Workshop_session::where('product_id','=',$id)->get();
        $data['members'] = Sale::with('customer')->where('product_id',$id)->where('products_type_id', 2)->get();
        $data['attendance_lists'] = Workshop_attendance::where('product_id',$id)->get();
        return view('components.Admin/Product/WorkShop/wsh_attendance_list', $data);
    }

    public function attendance($customer_id,$product_id,$session_number,$status)
    {
        $deletedRows = Workshop_attendance::where('shenaseh', 'REGEXP' ,$customer_id.$product_id.$session_number)->delete();

        Workshop_attendance::create([
            'customer_id' => $customer_id,
            'product_id' => $product_id,
            'session_number' => $session_number,
            'shenaseh' => $customer_id.$product_id.$session_number.$status,
        ]);
        return redirect()->route('workshop.attendance_list', $product_id);
       
    }

    public function print_attendance_list($id)
    {
        $data['wsh_name'] = Product::find($id);
        $data['session_numbers'] = Workshop_session::where('product_id','=',$id)->get();
        $data['members'] = Sale::with('customer')->where('product_id',$id)->get();
        $data['attendance_lists'] = Workshop_attendance::where('product_id',$id)->get();
        return view('components.Admin/Product/WorkShop/wsh_print_attendance_list', $data);
    }

    public function print_receipt($wsh_id, $customer_id)
    {
        $data = [];
        $data['user'] = User::find($customer_id);
        $data['wsh'] = Product::with('advisor', 'category')->find($wsh_id);
        $data['test'] = Sale::with('transactions')->where('customer_id',$customer_id)->where('product_id', $wsh_id)->get();
        $data['members'] = Sale::with('customer')->where('product_id',$wsh_id)->get();
        return view('components.Admin/Product/WorkShop/wsh_print_receipt', $data);
    }
   
}
