<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cancel_reason;

class CancelReasonController extends Controller
{
    public function index()
    {
        $data = [];
        $data['reasons'] = Cancel_reason::where('delete_flag',0)->get();
        return view('components.Admin.Settings.Cancel_reasons.Cancel_reasons_list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.Admin.Settings.Cancel_reasons.Cancel_reason_new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cancel_reason::create([
            'title' => $request->title,
            'note' => $request->note,
            'operator_id' => auth()->id(),
            'delete_flag'=>0
        ]);
        return redirect(route('Cancel_reasons_list'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['reason'] = cancel_reason::find($id);
        return view('components.Admin/Settings/Cancel_reasons/Cancel_reason_edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        Cancel_reason::where('id', $id)->update([
            'title' => $request->title,
            'note'=> $request->note
        ]);
        return redirect(route('Cancel_reasons_list'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cancel_reason::where('id', $id)->update([
            'delete_flag' => 1
        ]);
        return back();
    }
}
