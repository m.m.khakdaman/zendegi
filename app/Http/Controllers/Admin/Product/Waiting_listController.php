<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use App\Models\Waiting_list;
use App\Models\Role;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;

class Waiting_listController extends Controller
{
    public function list($id)
    {
        $data = [];
        $data['customer'] = User::find($id);
        $data['categories'] = Category::all();
        $data['advisors'] = User::where('user_type','advisor')->get();
        $data['customers'] = User::where('user_type','customer')->paginate(100);
        $data['lists'] = Waiting_list::with('advisor','customer','category')->get();
        return view('components.Admin.Product.Session.waiting_list', $data);
    }

    public function save_to_wl( Request $request)
    {
        // return $request;
        Waiting_list::create([
            'customer_id'=>$request->customer_id,
            'advisor_id'=>$request->advisor_id,
            'category_id'=>$request->category_id,
            'status'=>$request->status,
            'note'=>$request->note,
            'date'=>$request->date,
            'time'=>$request->time
        ]);

        $id = $request->id;

        return redirect()->route('wl.list',$id);
    }
    public function destroy($id) {
        $data = Waiting_list::find($id);
        $data->delete();
        return redirect()->route('wl.list',0);
    }
}
