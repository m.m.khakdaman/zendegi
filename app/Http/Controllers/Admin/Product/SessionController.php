<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\Base;
use App\Models\Cancel_reason;
use App\Models\Role;
use App\Models\Room;
use App\Models\Sale;
use App\Models\User;
// use Illuminate\Support\Facades\Session;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexـpaid()
    {
        // return 'a';
        //SendSMS('test','09194524028');
        $data['rooms'] = Room::orderBy('order', 'asc')->get(['id', 'name as title', 'order']);
        $data['advisors'] = User::where('user_type', 'advisor')->get();
        $data['customers'] = User::where('user_type', 'customer')->get(['name', 'family', 'mobile', 'id']);
        $base = new Base('_session_type');
        $data['session_types'] = $base->get();
        $price_lists = new Base('price_lists');
        $data['price_lists'] =  $price_lists->where([['active', '=', '1']])->get();
        $categories = new Base('categories');
        $data['categories'] =  $categories->get();
        $data['cancel_reasons'] = Cancel_reason::all();

        // $data['paid'] = 1;


        return view('components.Admin.Product.Session.index', $data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'a';
        //SendSMS('test','09194524028');
        $data['rooms'] = Room::orderBy('order', 'asc')->get(['id', 'name as title', 'order']);
        $data['advisors'] = User::where('user_type', 'advisor')->get();
        $data['customers'] = User::where('user_type', 'customer')->get(['name', 'family', 'mobile', 'id']);
        $base = new Base('_session_type');
        $data['session_types'] = $base->get();
        $price_lists = new Base('price_lists');
        $data['price_lists'] =  $price_lists->where([['active', '=', '1']])->get();
        $categories = new Base('categories');
        $data['categories'] =  $categories->get();
        $data['cancel_reasons'] = Cancel_reason::all();


        $data['paid'] = 1;

        return view('components.Admin.Product.Session.index', $data);
    }
    public function session_print($session)
    {
        $data['session'] = Sale::whereId($session)->with(['product', 'operator', 'customer', 'advisor', 'transactions', 'transactions.distinctions', 'transactions.distinctions.source','room.building'])->first();
        return view('components.Admin.Product.Session.print_session', $data);
    }
    public function send_sms_to_advisor($advisor)
    {
        $a = Sale::whereId($advisor)->with('advisor')->first();
        $sales = Sale::where([
            'advisor_id' => $a->advisor_id, 'date' => $a->date,
            [
                'delete_type',
                '=',
                '0'
            ]
        ])->with('customer')->get();
        $str = "";
        foreach ($sales as $sale) {
            $str = $str . "
            " . setting('send_program_of_product_session_sms_to_advisor', [verta($sale->date)->format('Y/m/d') . " " . $sale->session_start, $sale->customer->name . " " . $sale->customer->family]);
        }
        SendSMS("برنامه ی کاری شما در تاریخ " . verta($sale->date)->format('Y/m/d') . "به شرح زیل می باشد 
        " . $str, $a->advisor->mobile);
        return redirect(route('session.index', ['date' => $a->date]));
        // ->with(['product','operator','customer','advisor','transactions','transactions.distinctions','transactions.distinctions.source'])->first();
        // return view('components.Admin.Product.Session.print_session', $data);
    }
    public function send_alert_sms_to_advisor(sale $sale)
    {
        SendSMS(
            setting('sabt_jalase_sms', [
                $sale->customer->name . ' ' . $sale->customer->family,
                verta($sale->date)->format('Y/m/d') . " ساعت:" . $sale->session_start,
            ]),
            $sale->customer->mobile
        );
        return response('send sms succesfuly');
    }
    public function send_alert_sms_to_costomer(sale $sale)
    {
        SendSMS(
            setting('sabt_jalase_sms', [
                $sale->advisor->name . ' ' . $sale->advisor->family,
                verta($sale->date)->format('Y/m/d') . " ساعت:" . $sale->session_start,
            ]),
            $sale->customer->mobile
        );
        return response('send sms succesfuly');
    }

    public function cancled()
    {

        $data['rooms'] = Room::orderBy('order', 'asc')->get(['id', 'name as title', 'order']);
        $data['advisors'] = User::where('user_type', 'advisor')->get();
        $data['customers'] = User::where('user_type', 'customer')->get(['name', 'family', 'mobile', 'id']);
        $base = new Base('_session_type');
        $data['session_types'] = $base->get();
        $price_lists = new Base('price_lists');
        $data['price_lists'] =  $price_lists->where([['active', '=', '1']])->get();
        $categories = new Base('categories');
        $data['categories'] =  $categories->get();



        return view('components.Admin.Product.Session.cancled', $data);
    }

    // فراخوانی صفحه انتخاب روز برای گزارش گیری
    public function daily_report()
    {
        Gate::authorize('reoprt.daily');
        $data['admins'] = User::where('user_type', 'admin')->get();
        return view('components.Admin.Report.daily_report', $data);
    }

    //چاپ گزارش روزانه
    public function daily_report_print(Request $request)
    {
        Gate::authorize('reoprt.daily');
        $data = [];
        $data['start_date'] = request('start_date');
        $data['end_date'] = request('end_date');
        $data['start'] = $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $data['end'] = $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');


        $data['sessions'] = Sale::where([
            [
                'created_at', '>=', $start
            ],
            [
                'created_at', '<=', $end
            ],
            [
                'products_type_id', '=', 1
            ]
        ])->with('transactions', 'operator')->get();

        return view('components.Admin/Report/daily_report_print', $data);
    }

    // لیست جلسات کنسل شده در شرایط فورس ماژور که جهت رد یا تایید برای مدیر نمایش داده می شود
    public function force_major_canceled_sessions_list()
    {
        $data = [];
        $data['fm_sessions'] = Sale::orderBy('id', 'DESC')->with('operator' ,'cancel_operator','customer', 'advisor','cancel_reason')->where('force_major_delete', 'force_major')->get();
        return view('components.Admin.Product.Session.force_major_canceled_sessions_list', $data);
    }

    public function force_major_result($id, $result)
    {
        if ($result == 'accepted') {
            $sale = Sale::whereId($id)->with('transactions')->first();

            $transaction_details = TransactionDetail::where([
                'distinction_transaction_id' => $sale->transactions->id,
                ['amount', '!=', 0]
            ])->with('source')->get();
            $sale->transactions->update([
                'amount_standing' => 0,
                'status' => 0,
            ]);
            foreach ($transaction_details as $tran) {
                $tran->source->update(['amount_standing' => $tran->source->amount_standing + $tran->amount]);
                $tran->update(['amount' => 0]);
            }
            $sale->update([
                'force_major_delete' => 'accepted',
            ]);
            flash('با کنسل کردن جلسه و عودت وجه موافقت شد.', 'success');
        } else {
            $sale = Sale::whereId($id)->with('transactions')->first();
            $sale->update([
                'force_major_delete' => 'rejected',
            ]);
            flash('با کنسل کردن جلسه و عودت وجه مخالفت شد.', 'danger');
        }
        return redirect()->route('force_major_canceled_sessions_list');
       
        
    }
}
