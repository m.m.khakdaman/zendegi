<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;
use App\Models\Base;
use App\Models\Chat;
use App\Models\PriceList;
use App\Models\Sale;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Models\Waiting_list;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function calender_store(Request $request)
    {
        $request->validate([
            'advisor_id' => 'required',
            'session_type_id' => 'required',
            'category_id' => 'required',
            'room_id' => 'required',
            'product_id' => 'required',
            'price_list_id' => 'required',
            'session_length' => 'required',
            'customer_id' => 'required',
            'session_start' => 'required',
            'date' => 'required'
        ]);

        $prd = Sale::where([
            'advisor_id' => $request->advisor_id,
            'customer_id' => $request->customer_id,
            'operator_id' => auth()->id(),
            'session_start' => $request->session_start,
            'session_length' => $request->session_length,
            'date' => $request->date,
            [
                'created_at', '>=', Carbon::now()->subMinutes(1)->format('Y-m-d H:i')
            ]
        ]);
        if ($prd->exists()) {
            return redirect(back())->with('error_message', 'لطفا ۱ دقیقه دیگر دوباره امتحان کنید');
        }
        $sale = Sale::create([
            'advisor_id' => $request->advisor_id,
            'customer_id' => $request->customer_id,
            'operator_id' => auth()->id(),
            'date' => $request->date,
            'session_status_id' => '1',
            'session_start' => $request->session_start,
            'session_length' => $request->session_length,
            'session_type_id' => $request->session_type_id,
            'category_id' => $request->category_id,
            'room_id' => $request->room_id,
            'orginal_price' => PriceList::find($request->price_list_id)->total_price,
            'qty' => 1,
            'product_id' => $request->product_id,
            'total_price' => PriceList::find($request->price_list_id)->total_price,
            'discount' => $request->discount,
            'discount_type' => $request->discount_type,
            'products_type_id' => setting('prudcot_session_type_id'),
            'price_list_id' => $request->price_list_id,
        ]);
        $orginal_price = round($sale->price_list->total_price * $request->session_length / $sale->price_list->session_length / 1000) * 1000;
        $total_price = $orginal_price - $request->discount;

        Sale::where('id', $sale->id)
            ->update([
                'orginal_price' => $orginal_price,
                'total_price' => $total_price,
            ]);
        Transaction::create([
            'user_id' => $sale->customer_id,
            'amount' => -intval($total_price),
            'amount_standing' => -intval($total_price),
            'cash_id' => '1',
            'operator_id' => auth()->id(),
            'sale_id' => $sale->id,
        ]);

        if ($request->has('Waitinglist')) {
            Waiting_list::whereId($request->Waitinglist)->update(['status' => 2]);
        }

        /*
        
        SendSMS(
            setting('sabt_jalase_sms', [
                 $sale->advisor->name .' '. $sale->advisor->family,
                verta($request->date)->format('Y/m/d') . " " . $request->session_start,
            ]),
            $sale->customer->mobile
        );
        
        */
        return response('success');
        // return redirect(route('session.index', ['date' => $request->date]));
    }

    public function change_session_date(Request $request)
    {

        Sale::where('id', $request->id)
            ->update([
                'date' => substr($request->date, 0, 10),
                'session_start' => substr($request->date, 11, 5),
                'room_id' => $request->room_id
            ]);
        $sale = Sale::where('id', $request->id)->get();
        //SendSMS(setting('change_session_date_sms'), $sale->customer->mobile);
        return true;
    }
    public function change_session_length(Request $request)
    {
        return $this->change_session_amount($request);
    }

    public function change_session_amount($request)
    {
        $sale = Sale::whereId($request->id)->with(['transactions', 'price_list'])->first();
        $old_totoal_price = $sale->total_price;
        $orginal_price = round($sale->price_list->total_price * $request->session_length / $sale->price_list->session_length / 1000) * 1000;
        $total_price = $orginal_price - $sale->discount;


        $sale->update([
            'session_length' => $request->session_length,
            'orginal_price' => $orginal_price,
            'total_price' => $total_price
        ]);


        if ($total_price > $old_totoal_price) {
            $sale->transactions->update([
                'amount' => -intval($total_price),
                'amount_standing' => ($old_totoal_price + ($sale->transactions->amount_standing)) + (-intval($total_price)),
                'operator_id' => auth()->id(),
            ]);
        } else {
            $dif = $sale->transactions->amount  + $total_price;
            $new_amount_standing = $sale->transactions->amount_standing - $dif;
            $overflow = 0;
            if ($new_amount_standing > 0) {
                $overflow = $new_amount_standing;
                $new_amount_standing = 0;
            }


            $sale->transactions->update([
                'amount' => -intval($total_price),
                'amount_standing' => $new_amount_standing,
                'operator_id' => auth()->id(),
            ]);
            $transaction_details = TransactionDetail::where([
                'distinction_transaction_id' => $sale->transactions->id,
            ])->with(['source'])->get();



            foreach ($transaction_details as $tran) {
                // $tran->update('amount','0');

                $temp_id = $tran->id;
                $d = $tran->source->amount - $tran->source->amount_standing;
                if ($overflow > 0) {

                    if ($overflow <= $d) {
                        $tran->update(['amount' => ($tran->amount - $overflow)]);
                        $tran->source->update(['amount_standing' => ($tran->source->amount_standing) + $overflow]);
                        $overflow = 0;
                    } else {
                        $tran->update(['amount' => ($tran->amount - $d)]);
                        $tran->source->update(['amount_standing' => $tran->source->amount]);

                        if ($tran->amount == 0) {
                            $tran->delete();
                        }

                        $overflow = $overflow - $d;
                    }
                }
                /*
                TransactionDetail::whereId($temp_id)->update('amount','0');
                TransactionDetail::create([
                    'source_transaction_id' => $tran->distinction_transaction_id,
                    'distinction_transaction_id' => $tran->source_transaction_id,
                    'amount' => $tran->amount,
                ]);*/
            }
        }
        //SendSMS(setting('change_session_length_sms'), $sale->customer->mobile);
        return true;
    }
    public function calender_sale_update(Request $request)
    {
        $request->validate([
            'sale_id' => 'required',
            'advisor_id' => 'required',
            // 'customer_id' => 'required',
            'session_start' => 'required',
            'session_length' => 'required',
            'session_type_id' => 'required',
            'category_id' => 'required',
            'room_id' => 'required',
            'product_id' => 'required',
            'price_list_id' => 'required',
            'date' => 'required',
            'date2' => 'required',
        ]);

        $sale = Sale::find($request->sale_id);
        $old_totoal_price = $sale->total_price;
        $price_list = PriceList::find($request->price_list_id);

        $advisor_rate = 1 / ($price_list->total_price / $price_list->advisor_rate);

        $total_price = round($price_list->total_price * $request->session_length / $price_list->session_length / 1000) * 1000;







        if ($sale->discount_type < 1) $sale->discount_type = 1;



        switch ($sale->discount_type) {
            case 1:
                $total_price = $total_price - $request->discount;
                $advisor_price = $total_price * $advisor_rate;
                $center_price = $total_price - $advisor_price;
                break;
            case 2:
                $advisor_price = $total_price * $advisor_rate;
                $center_price = $total_price - $request->discount - $advisor_price;
                break;
            case 3:
                $advisor_price = ($total_price * $advisor_rate) - $request->discount;
                $center_price = $total_price - $advisor_price;
                break;
            default:
                //$total_price = $total_price-$request->discount;
        }

        $total_price = $total_price - $request->discount;


        $sale->update([
            'advisor_id' => $request->advisor_id,
            // 'customer_id' => $request->customer_id,
            'operator_id' => auth()->id(),
            'date' => verta()->parse($request->date2)->formatGregorian('Y-m-d'),
            'session_start' => $request->session_start,
            //'session_length' => $request->session_length,
            'session_type_id' => $request->session_type_id,
            'category_id' => $request->category_id,
            'room_id' => $request->room_id,
            //'orginal_price' => $total_price,
            'qty' => 1,
            'price_list_id' => $request->price_list_id,
            'product_id' => $request->product_id,
            'discount' => $request->discount,
            'discount_type' => $request->discount_type,
            //'total_price' => $total_price,
            'products_type_id' => setting('prudcot_session_type_id'),
            'price_list_id' => $request->price_list_id,
        ]);



        $comment = 'زمان جلسه' . verta($sale->date)->format('Y/m/d') . $sale->session_start;
        LogActivity::addToLog('ویرایش جلسه', $sale->customer_id ?? 0, $sale->advisor_id, $comment);


        // if(auth()->id() == 1){
        //     return $total_price;
        // }
        if ($old_totoal_price != $total_price) {
            $request->id = $request->sale_id;
            $this->change_session_amount($request);
        }

        /*
        if ($old_totoal_price != $total_price) {
            if ($old_totoal_price < $total_price) {
                $p = $total_price - $old_totoal_price;
                $sale->transactions->update([
                    'user_id' => $request->customer_id,
                    'amount' => -intval($total_price),
                    //'amount_standing' => - ($old_totoal_price + $p),
                    'amount_standing' => ($old_totoal_price + ($sale->transactions->amount_standing)) + (-intval($total_price)),
                    'operator_id' => auth()->id(),
                ]);
            } else {
                $a = -$sale->transactions->amount + $sale->transactions->amount_standing;
                $p = $a - $total_price;
                $h = $a - $total_price;
                $transaction_details = TransactionDetail::where([
                    'distinction_transaction_id' => $sale->transactions->id,
                    ['amount', '!=', 0]
                ])->with('source')->get();
                if ($p > 0) {
                    foreach ($transaction_details as $value) {
                        $a = $p;
                        $value->amount =  $p < $value->amount ? $p :  $value->amount;
                        $p =  $p < $value->amount ? 0 :  $p - $value->amount;
                        $value->save();
                        $value->source()->update([
                            'amount_standing' => $a - $p,
                        ]);
                    }
                }
                $sale->transactions->update([
                    'user_id' => $request->customer_id,
                    'amount' => -intval($total_price),
                    'amount_standing' => $h > 0 ? 0 : $h - $total_price,
                    'operator_id' => auth()->id(),
                ]);
            }
        }*/


        switch ($request->command) {
            case 'end_session':


                $sale->update(['session_status_id' => '2']);
                Transaction::create(
                    [
                        'user_id' => setting('center_user_id'),
                        'amount' => $center_price,
                        'amount_standing' => $center_price,
                        'cash_id' => '1',
                        'operator_id' => auth()->id(),
                        'sale_id' => $sale->id,
                    ]
                );
                Transaction::create([
                    'user_id' =>  $sale->advisor_id,
                    'amount' => $advisor_price,
                    'amount_standing' => $advisor_price,
                    'cash_id' => '1',
                    'operator_id' => auth()->id(),
                    'sale_id' => $sale->id,
                ]);

            default:
                break;
        }
        // SendSMS(setting('calender_sale_update_sms'), $sale->customer->mobile);


        return response('update session successfuly');
        // return redirect(route('session.index', ['date' => $request->date]));
    }

    // لغو جلسه به صورت معمولی توسط تابع ذیل انجام می گردد
    public function calender_sale_delete(Request $request)
    {
        $request->validate([
            'sale_id' => 'required',
            'delete_type' => 'required',
            'reason' => 'nullable',
            'comment' => 'nullable',
        ]);
        $sale = Sale::whereId($request->sale_id)->with('transactions')->first();

        $transaction_details = TransactionDetail::where([
            'distinction_transaction_id' => $sale->transactions->id,
            ['amount', '!=', 0]
        ])->with('source')->get();
        $sale->transactions->update([
            'amount_standing' => 0,
            'status' => 0,
        ]);
        foreach ($transaction_details as $tran) {
            $tran->source->update(['amount_standing' => $tran->source->amount_standing + $tran->amount]);
            $tran->update(['amount' => 0]);
        }
        $sale->update([
            'delete_type' => $request->delete_type,
            'delete_comment' => $request->comment,
            'cancel_reason_id' => $request->reason,
            'cancel_operator_id' => auth()->id(),
        ]);
        $comment = 'زمان جلسه' . verta($sale->date)->format('Y/m/d') . $sale->session_start;
        LogActivity::addToLog('حذف جلسه', $sale->customer_id, $sale->advisor_id, $comment);
        // SendSMS(setting('calender_sale_end_sms', [$sale->date, $sale->advisor->name . ' ' . $sale->advisor->family]), $sale->customer->mobile);
        return response('delete session successfuly');
    }

    // لغو جلسه به صورت فورس ماژور توسط تابع ذیل انجام می گردد
    public function force_major_calender_sale_delete(Request $request)
    {
        $request->validate([
            'sale_id' => 'required',
            'delete_type' => 'required',
            'reason' => 'nullable|required',
            'comment' => 'nullable|required',
        ]);
        $sale = Sale::whereId($request->sale_id)->with('transactions')->first();

        // $transaction_details = TransactionDetail::where([
        //     'distinction_transaction_id' => $sale->transactions->id,
        //     ['amount', '!=', 0]
        // ])->with('source')->get();
        // $sale->transactions->update([
        //     'amount_standing' => 0,
        //     'status' => 0,
        // ]);
        // foreach ($transaction_details as $tran) {
        //     $tran->source->update(['amount_standing' => $tran->source->amount_standing + $tran->amount]);
        //     $tran->update(['amount' => 0]);
        // }
        $sale->update([
            'delete_type' => $request->delete_type,
            'delete_comment' => $request->comment,
            'force_major_delete' => 'force_major',
            'cancel_reason_id' => $request->reason,
            'cancel_operator_id' => auth()->id(),
        ]);
        $comment = 'زمان جلسه' . verta($sale->date)->format('Y/m/d') . $sale->session_start;
        LogActivity::addToLog('حذف فورس ماژور', $sale->customer_id, $sale->advisor_id, $comment);
        // SendSMS(setting('calender_sale_end_sms', [$sale->date, $sale->advisor->name . ' ' . $sale->advisor->family]), $sale->customer->mobile);
        return response('delete session successfuly');
    }

    public function calender_sale_end_session(Request $request)
    {
        return $request;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        return $sale;
        // $data['sale'] = $sale;
        // $data['sales'] = $sale->customer->sales()->with('debts')->has('debts')->get();
        // $data['customer'] = $data['sale']->customer;
        // $data['adviosr'] = $data['sale']->advisor;
        // $data['operator'] = $data['sale']->operator;
        // return view('components.Admin.Financial.Transaction.invoice', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }

    public function sale_chat(Sale $sale)
    {
        $data['messages'] = Chat::where('sale_id', $sale->id)->with('sender')->get();
        $data['sale'] = $sale;

        return view('components.Admin.Product.Sale.chat', $data);
    }
    public function send_message(Request $request, Sale $sale)
    {
        Chat::create([
            'sale_id' => $sale->id,
            'text' => $request->text,
            'sender_id' => auth()->id()
        ]);

        return redirect(route('sale.chat', $sale->id));
    }

    public function sale_comment(Sale $sale)
    {
        $messages = Chat::where('sale_id', $sale->id)->with('sender')->get();
        foreach ($messages as $message) {
            $message->created_at = verta($message->created_at)->format('Y/m/d H:i');
        }
        echo json_encode($messages, JSON_UNESCAPED_UNICODE);
    }
    public function send_comment(Request $request, Sale $sale)
    {
        $request->validate([
            'text' => 'required',
        ]);
        return Chat::create([
            'sale_id' => $sale->id,
            'text' => $request->text,
            'sender_id' => auth()->id()
        ]);

        // return redirect(route('sale.chat',$sale->id));
    }

    public function cancel_wsh_member(Request $request)
    {
        return $request;
    }
}
