<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Cash;
use Illuminate\Support\Facades\Gate;

class PayrollController_Copy extends Controller
{
     /**
     * نمایش صفحه انتخاب کارمند و یک بازه زمانی برای صدور فیش حقوقی آن کارمند 
     */
    public function admin_select()
    {
        Gate::authorize('payrole.admin');
        $data = [];
        $data['admins'] = User::where('user_type','admin')->get();
        return view('components.Admin.User.Admin.payroll_admin_select', $data);
    }

    /**
     * نمایش صفحه فرم پیش نویس فیش حقوقی کارمند 
     */
    public function admin_create(Request $request)
    {
        Gate::authorize('payrole.admin');
        $data = [];
        $data['admin'] = User::find($request->admin_id);
        // $data['wsh'] = Sale::where('operator_id', $request->admin_id)->where('products_type_id', 2)->count();
        // $data['amount'] = Sale::where('operator_id', $request->admin_id)->sum('orginal_price');
        $data['start_jalali'] = request('start_date');
        $data['end_jalali'] = request('end_date');
        $data['start'] = $start =verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
        return view('components.Admin/User/Admin/payroll_admin_create', $data);
        
    }

    /**
     * نمایش صفحه پرینت فیش حقوقی کارمند 
     */
    public function admin_print(Request $request)
    {
        // return $request;
        Gate::authorize('payrole.admin');
        $data = [];
        $data['admin'] = User::find($request->admin_id);
        $data['payroll'] = $request;
        return view('components.Admin/User/Admin/payroll_admin_print', $data);
    }

    /**
     * نمایش صفحه انتخاب مشاور و یک بازه زمانی برای صدور فیش حقوقی آن مشاور 
     */
    public function advisor_select()
    {
        Gate::authorize('payrole.advisor');
        $data = [];
        $data['advisors'] = User::where('user_type','advisor')->get();
        return view('components.Admin.User.Advisor.payroll_advisor_select', $data);
    }

    /**
     * نمایش صفحه پرینت فیش حقوقی کارمند 
     * /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function advisor_print(Request $request)
    {
        Gate::authorize('payrole.advisor');
        // return $request;
        $data = [];
        $data['advisor'] = User::find($request->advisor_id);
        $data['payroll'] = $request;

        $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
        $Transactions = Transaction::where([
            [
                'user_id',
                '!=',
                setting('center_user_id'),
            ],
            [
                'user_id',
                '!=',
                setting('getting_acount'),
            ],

        ]);

        if (request('advisors') || request('advisors') == "0") {
            $Transactions->whereHas('sale', function ($query) use ($end, $start) {
                if (request('advisors') != "0") {
                    $a = [

                        'delete_type' => '0',
                        [
                            'date',
                            '<',
                            $end
                        ],
                        [
                            'date',
                            '>=',
                            $start
                        ],
                        [
                            'amount',
                            '<',
                            '0'
                        ],
                        'delete_type' => '0',
                        'products_type_id'=> setting('prudcot_session_type_id') ,
                        'advisor_id' => request('advisors')
                    ];
                } else {
                    $a = [
                        'delete_type' => '0',

                        [
                            'date',
                            '<',
                            $end
                        ],
                        [
                            'date',
                            '>=',
                            $start
                        ],
                        [
                            'amount',
                            '<',
                            '0'
                        ],
                        'products_type_id'=> setting('prudcot_session_type_id'),
                    ];
                }
                $query->where($a);
            });
        } else {

            $Transactions->where([
                [
                    'amount',
                    '>',
                    '0'
                ],
                [
                    'cash_id',
                    '!=',
                    1,
                ],

                [
                    'created_at',
                    '<',
                    $end
                ],
                [
                    'created_at',
                    '>=',
                    $start
                ],
            ]);
        }
        $data['print_advisor'] = 1;
            if (request('advisors') != intval(0)) {
                $data['print_advisor'] = 1;
            }
            if (request('advisors') == '0') {
                $data['print_advisor'] = 1;
            }
            if (request('operators') != 0) {
                $Transactions->where('operator_id', request('operators'));
            }
            if (request('cash') != 0) {
                $Transactions->where('cash_id', request('cash'));
            }
            $Transactions->get();
            $data['Transactions'] = $Transactions->with([
                'operator',
                'sale.category',
                'sale.price_list',
                'transaction_type',
                'cash',
                'cash.cashe_type',
                'sources.distinction.sale.products_type',
                'sale',
                'sale.advisor',
            ])->get()->sortBy([
                [
                    $data['print_advisor'] ? 'sale.advisor' : 'operator',
                    'asc'
                ],
                ['sale.date', 'desc']
            ]);
            $data['start'] = $start;
            $data['end'] = $end;
         
        if (request()->input('print') == 'advisors') {
            $data['advisors'] = User::where('user_type','advisor')->get();
            $data['print_advisors'] = 1;
        } else {
            $data['operators'] = User::where('user_type','admin')->get();
            $data['print_advisors'] = 0;
        }
        $data['cashs'] = Cash::latest()->get();
       

        return view('components.Admin/User/Advisor/payroll_advisor_print', $data);
    }
}
