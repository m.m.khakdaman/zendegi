<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\Sale;
use Illuminate\Support\Facades\Gate;

class PayrollController extends Controller
{
    /**
     * نمایش صفحه انتخاب کارمند و یک بازه زمانی برای صدور فیش حقوقی آن کارمند 
     */
    public function admin_select()
    {
        Gate::authorize('payrole.admin');
        $data = [];
        $data['admins'] = User::where('user_type', 'admin')->get();
        return view('components.Admin.User.Admin.payroll_admin_select', $data);
    }

    /**
     * نمایش صفحه فرم پیش نویس فیش حقوقی کارمند 
     */
    public function admin_create(Request $request)
    {
        Gate::authorize('payrole.admin');
        $data = [];
        $data['admin'] = User::find($request->admin_id);
        // $data['wsh'] = Sale::where('operator_id', $request->admin_id)->where('products_type_id', 2)->count();
        // $data['amount'] = Sale::where('operator_id', $request->admin_id)->sum('orginal_price');
        $data['start_jalali'] = request('start_date');
        $data['end_jalali'] = request('end_date');
        $data['start'] = $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
        return view('components.Admin/User/Admin/payroll_admin_create', $data);
    }

    /**
     * نمایش صفحه پرینت فیش حقوقی کارمند 
     */
    public function admin_print(Request $request)
    {
        // return $request;
        Gate::authorize('payrole.admin');
        $data = [];
        $data['admin'] = User::find($request->admin_id);
        $data['payroll'] = $request;
        return view('components.Admin/User/Admin/payroll_admin_print', $data);
    }

    /**
     * نمایش صفحه انتخاب مشاور و یک بازه زمانی برای صدور فیش حقوقی آن مشاور 
     */
    public function advisor_select()
    {
        Gate::authorize('payrole.advisor');
        $data = [];
        $data['advisors'] = User::where('user_type', 'advisor')->get();
        return view('components.Admin.User.Advisor.payroll_advisor_select', $data);
    }

    /**
     * نمایش صفحه پرینت فیش حقوقی کارمند 
     * /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function advisor_print(Request $request)
    {
        Gate::authorize('payrole.advisor');
        $data = [];
        $data['advisor'] = User::find($request->advisor_id);
        $data['payroll'] = $request;

        $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');
        $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
        $data['sessions'] = Sale::where([
            [
                'date', '>=', $start
            ],
            [
                'date', '<=', $end
            ],
            [
                'orginal_price', '>=', $request->start_price
            ],
            [
                'orginal_price', '<=', $request->end_price
            ],
            [
                'advisor_id', '=', $request->advisor_id
            ],
            [
                'products_type_id', '=', 1
            ],
            [
                'delete_type', '=', 0
            ]
        ])->with('transactions', 'advisor', 'customer', 'price_list')->OrderBy('date', 'asc')->get();


        return view('components.Admin.User.Advisor.payroll_advisor_print', $data);
    }
}
