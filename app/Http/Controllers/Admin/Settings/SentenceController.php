<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Models\Sentence;
use Illuminate\Http\Request;

class SentenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['sentences'] = sentence::all();
        return view('components.Admin/Settings/Sentences/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.Admin/Settings/Sentences/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Sentence::create([
            'sentence'=> $request->sentence,
            'from'=> $request->from,
            'status'=> 1,
            'operator_id'=> auth()->user()->id
        ]);
        flash('جمله جدید درج شد', 'success');
        return redirect()->route('sentence.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['sentence'] = Sentence::Find($id);
        return view('components.Admin.Settings.Sentences.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sentence = Sentence::find($id);
        $sentence->from = $request->from;
        $sentence->sentence = $request->sentence;
        $sentence->operator_id = auth()->user()->id;
        $sentence->save();
        flash('جمله بروزرسانی شد', 'warning');
        return redirect()->route('sentence.index');
    }

    public function activation($id)
    {
        $tmp = Sentence::find($id);
        Sentence::where('id', $id)->update(['status' => $tmp->status xor 1]);
        flash('وضعیت جمله با موفقیت تغییر کرد.', 'secondary');
        return redirect()->route('sentence.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Sentence::find($id)->delete())
        {
            flash('جمله حذف شد', 'danger');
        }
        return back();
    }
}
