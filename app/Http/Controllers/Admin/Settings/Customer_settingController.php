<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Introduction_method;
use Illuminate\Http\Request;

class Customer_settingController extends Controller
{
    public function index()
    {
        $data = [];
        $data['introductions'] = Introduction_method::with('operator')->get();
        return view('components.Admin/Settings/Introduction/index', $data);
    }
    //create
    public function create()
    {
        return view('components.Admin/Settings/Introduction/create');
    }

    public function store(Request $request)
    {
        //  return $request;
        Introduction_method::create([
            'Introduction_method' =>$request->introduction_method,
            'status' => '1',
            'operator_id' => auth()->user()->id
        ]);
        return redirect()->route('introduction.index');
    }

    public function edit($id)
    {
        $data = [];
        $data['introduction'] = Introduction_method::find($id);
        return view('components.Admin/Settings/Introduction/edit', $data);

    }

    public function update(Request $request, $id)
    {
        Introduction_method::find($id)->update([
            'Introduction_method' => $request->introduction,
            'operator_id' => auth()->user()->id 
        ]);
        return redirect()->route('introduction.index');        
    }

    public function activation($id)
    {
        $tmp = Introduction_method::find($id);
        Introduction_method::where('id', $id)->update(['status' => $tmp->status xor 1]);
        return redirect()->route('introduction.index');
    }
}