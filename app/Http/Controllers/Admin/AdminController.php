<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Base;
use App\Models\Product;
use App\Models\ProductDay;
use App\Models\Role;
use App\Models\Room;
use App\Models\Sale;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Zarinpal;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\DB;
use SoapClient;

class AdminController extends Controller
{
    private $MerchantID = '853e124c-c124-11e7-bdd7-005056a205be';

    static public function convert2english($string)
    {
        $newNumbers = range(0, 9);
        // 1. Persian HTML decimal
        $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
        // 2. Arabic HTML decimal
        $arabicDecimal = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;');
        // 3. Arabic Numeric
        $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        // 4. Persian Numeric
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');

        $string =  str_replace($persianDecimal, $newNumbers, $string);
        $string =  str_replace($arabicDecimal, $newNumbers, $string);
        $string =  str_replace($arabic, $newNumbers, $string);
        return str_replace($persian, $newNumbers, $string);
    }
    public function index()
    {
        $data = [];
        // $data['customers'] = User::where('user_type','customer')->get();
        // $data['advisors'] = User::where('user_type','advisor')->get();
        $data['sales'] = Sale::all();
        return view('components.Admin.home', $data);
    }
    public function dropzone_upload(Request $request)
    {
        dd($request);
    }


    public function product_getevent()
    {
        $start = verta(str_replace('-', '/', request('start')));
        $end = verta(str_replace('-', '/', request('end')));
        $start->format('Y/m/d');
        $sessions = Product::where([
            [
                'start', '>=', $start->format('Y/m/d')
            ],
            [
                'end', '<=', $end->format('Y/m/d')
            ]
        ])->with('ProductDay')->has('ProductDay')->get();

        function convert_session_to_45($session, $date, $color)
        {
            $start_time = $session->start;
            $times = [];
            $i = 0;
            while (verta($start_time)->addMinute(45)->format('H:i') < $session->end) {
                $times[$i]['color'] = $color;
                // $times[$i]['title'] = $session->product->advisor->name . $session->product->advisor->family;
                // $times[$i]['advisor'] = $session->product->advisor;
                $times[$i]['start'] = Carbon::instance(Verta::parse($date)->datetime())->format('Y-m-d') . ' ' . $start_time;
                $times[$i]['end'] =  Carbon::instance(Verta::parse($date)->datetime())->format('Y-m-d') . ' ' . verta($start_time)->addMinute(45)->format('H:i');
                $start_time = verta($start_time)->addMinute(45)->format('H:i');
                $i++;
            }
            if ($times == []) {
            } else {
                return $times;
            }
        }

        function canvert_day_to_date($day_of_week, $start, $end, $session, $color)
        {
            $product_start = Verta::parse($start);
            $product_end = Verta::parse($end);
            $data = [];
            if ($product_start->dayOfWeek + 1 == $day_of_week) {
                $i = 0;
                $data[$i]['date'] = $product_start->format('Y-m-d');
                $data[$i]['session'] = $session;
                $i++;
                while ($product_start->addDays(7) <= $product_end) {
                    $data[$i]['date'] = $product_start->format('Y-m-d');
                    $data[$i]['session'] = $session;
                    $i++;
                }
            }
            if ($product_start->dayOfWeek + 1 < $day_of_week) {
                $product_start = $product_start->addDays(intval($day_of_week) - ($product_start->dayOfWeek + 1));
                if ($product_start->dayOfWeek + 1 == $day_of_week) {
                    $i = 0;
                    $data[$i]['date'] = $product_start->format('Y-m-d');
                    $data[$i]['session'] = $session;
                    $i++;
                    while ($product_start->addDays(7) <= $product_end) {
                        $data[$i]['date'] = $product_start->format('Y-m-d');
                        $data[$i]['session'] = $session;
                        $i++;
                    }
                }
            }

            if ($product_start->dayOfWeek + 1 > $day_of_week) {
                $product_start = $product_start->addDays(7 - ($product_start->dayOfWeek + 1) + $day_of_week);
                if ($product_start->dayOfWeek + 1 == $day_of_week) {
                    $i = 0;
                    $data[$i]['date'] = $product_start->format('Y-m-d');
                    $data[$i]['session'] = $session;
                    $i++;
                    while ($product_start->addDays(7) <= $product_end) {
                        $data[$i]['date'] = $product_start->format('Y-m-d');
                        $data[$i]['session'] = $session;
                        $i++;
                    }
                }
            }
            $data2 = [];
            foreach ($data as $key => $value) {
                $a = convert_session_to_45($value['session'], $value['date'], $color);
                if ($a != []) {
                    $data2 = array_merge($data2, $a);
                }
            }
            return $data2;
        }
        $data = [];
        foreach ($sessions as $session) {
            foreach ($session->ProductDay as $day) {
                $b = canvert_day_to_date($day->day_of_week, $session->start, $session->end, $day, $session->color);
                $data = array_merge($data, $b);
            };
        }
        return $data;
    }
    public function session_getevent()
    {
        $start = verta(str_replace('-', '/', request('start')));
        $end = verta(str_replace('-', '/', request('end')));
        $event =  DB::select(
            'SELECT
        concat(users.name," ",users.family," ",case when  users.mobile2 IS NULL then users.mobile else users.mobile2 end) as title ,
        concat(days.miladi_date," ",day_of_product.`start`) as `start`,
        concat(days.miladi_date," ",day_of_product.`end`) as `end`,
        day_of_product.day_of_week,
        day_of_product.room_id as "resourceId",
        day_of_product.color as "color",
        products.type as "type",
        products.advisor_id,
        products.id as product_id,
        "background" as "display",
        days.dey_of_week,
        days.miladi_date 
    FROM
        products
        INNER JOIN day_of_product ON products.id = day_of_product.product_id
        INNER JOIN users ON products.advisor_id = users.id
        INNER JOIN days ON days.dey_of_week = day_of_product.day_of_week 
    WHERE
         products.`start` <= days.date AND products.`end` >= days.date
         And days.miladi_date < "' . Carbon::parse(request('end'))->format('Y-m-d') . '" 
         And days.miladi_date >= "' . Carbon::parse(request('start'))->format('Y-m-d') . '"'
        );



        $sales = Sale::where([
            [
                'date',
                '<',
                Carbon::parse(request('end'))->format('Y-m-d')
            ],
            [
                'date',
                '>=',
                Carbon::parse(request('start'))->format('Y-m-d')
            ],
            [
                'delete_type',
                '=',
                '0'
            ],
            [
                'products_type_id', '=', 1
            ]
        ])->with(['customer', 'advisor', 'transactions', 'messages'])->get();
        $data = [];
        $i = 0;
        foreach ($sales as $key => $sale) {

            $data[$i] = $sale;
            $data[$i]['title'] = $sale->customer->name . ' ' . $sale->customer->family . ' «' . $sale->customer->sales()->where('delete_type', '0')->count() . '»';
            if ($sale->transactions) {
                switch ($sale->transactions->amount_standing) {
                    case ($sale->transactions->amount_standing > $sale->transactions->amount):
                        // if (!request()->paid) {
                        //     unset($data[$i]);
                        //     continue 2;
                        // }
                        $data[$i]['color'] = '#FDAC41'; //yellow
                        break;
                    case 0:
                        // if (!request()->paid) {
                        //     unset($data[$i]);
                        //     continue 2;
                        // }
                        $data[$i]['color'] = '#00FA9A'; //green
                        break;
                    default:
                        // if (request()->paid) {
                        //     unset($data[$i]);
                        //     continue 2;
                        // }
                        $data[$i]['color'] = '#DC143C'; //red
                        break;
                }
            }
            // dd($sale->date);
            $data[$i]['start'] = $sale->date . ' ' . $sale->session_start;
            $data[$i]['can_cancel'] = Carbon::parse($sale->date)->subHour(48)->format('Y-m-d') > Carbon::now() ? 1 : 0;
            $data[$i]['messages'] = $sale->messages;
            $data[$i]['end'] = $sale->date . ' ' . Carbon::parse($sale->session_start)->addMinute($sale->session_length)->format('H:i');
            $data[$i]['resourceId'] = $sale->room_id;
            if ($sale->session_status_id != '1') {
                $data[$i]['startEditable'] = 0;
                $data[$i]['durationEditable'] = 0;
                $data[$i]['resourceEditable'] = 0;
            }

            $i++;
        }
        // return $data;
        $a = array_merge($data, $event);
        echo json_encode($a, JSON_UNESCAPED_UNICODE);


        // $event = collect($event);
        // foreach ($event as $e) {
        //     if ($e->type == 2) {
        //         return $event->where('advisor_id', $e->advisor_id)->where(function ($q) use ($e) {
        //             $q->where('start','>',$e);
        //         });
        //     }
        // }

        // $sessions = Product::where([
        //     [
        //         'start', '<', $end->format('Y/m/d')
        //     ],
        //     [
        //         'end', '>', $start->format('Y/m/d')
        //     ]
        // ])->with('ProductDay')->has('ProductDay')->get();

        // function convert_session_to_45($session, $date, $room, $product)
        // {
        //     $start_time = $session->start;
        //     $times = [];
        //     $i = 0;
        //     while (verta($start_time)->addMinute(45)->format('H:i') < $session->end) {
        //         $times[$i]['resourceId'] = $room;
        //         $times[$i]['color'] = $room;
        //         $times[$i]['title'] = $product->product_name;
        //         $times[$i]['advisor']['name'] = $product->advisor->name . ' ' . $product->advisor->family;
        //         $times[$i]['advisor']['id'] = $product->advisor->id;
        //         $times[$i]['start'] = Carbon::instance(Verta::parse($date)->datetime())->format('Y-m-d') . ' ' . $start_time;
        //         $times[$i]['end'] =  Carbon::instance(Verta::parse($date)->datetime())->format('Y-m-d') . ' ' . verta($start_time)->addMinute(45)->format('H:i');
        //         $start_time = verta($start_time)->addMinute(45)->format('H:i');
        //         $i++;
        //     }
        //     if ($times == []) {
        //     } else {
        //         return $times;
        //     }
        // }

        // function canvert_day_to_date($day_of_week, $start, $end, $session, $room)
        // {
        //     $product_start = Verta::parse($start);
        //     $product_end = Verta::parse($end);
        //     $data = [];
        //     if ($product_start->dayOfWeek + 1 == $day_of_week) {
        //         $i = 0;
        //         $data[$i]['resourceId'] = $room;
        //         $data[$i]['display'] = 'background';
        //         $data[$i]['color'] = 'green';
        //         $data[$i]['title'] = $session->product->product_name;
        //         $data[$i]['advisor'] = $session->product->advisor_id;
        //         $data[$i]['start'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->start;
        //         $data[$i]['end'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->end;
        //         $i++;
        //         while ($product_start->addDays(7) <= $product_end) {
        //             $data[$i]['resourceId'] = $room;
        //             $data[$i]['display'] = 'background';
        //             $data[$i]['color'] = 'green';
        //             $data[$i]['title'] = $session->product->product_name;
        //             $data[$i]['advisor'] = $session->product->advisor_id;
        //             $data[$i]['start'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->start;
        //             $data[$i]['end'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->end;
        //             $i++;
        //         }
        //     }
        //     if ($product_start->dayOfWeek + 1 < $day_of_week) {
        //         $product_start = $product_start->addDays(intval($day_of_week) - ($product_start->dayOfWeek + 1));
        //         if ($product_start->dayOfWeek + 1 == $day_of_week) {
        //             $i = 0;
        //             $data[$i]['resourceId'] = $room;
        //             $data[$i]['display'] = 'background';
        //             $data[$i]['color'] = 'green';
        //             $data[$i]['title'] = $session->product->product_name;
        //             $data[$i]['advisor'] = $session->product->advisor_id;
        //             $data[$i]['start'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->start;
        //             $data[$i]['end'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->end;
        //             $i++;
        //             while ($product_start->addDays(7) <= $product_end) {
        //                 $data[$i]['resourceId'] = $room;
        //                 $data[$i]['display'] = 'background';
        //                 $data[$i]['color'] = 'green';
        //                 $data[$i]['title'] = $session->product->product_name;
        //                 $data[$i]['advisor'] = $session->product->advisor_id;
        //                 $data[$i]['start'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->start;
        //                 $data[$i]['end'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->end;
        //                 $i++;
        //             }
        //         }
        //     }

        //     if ($product_start->dayOfWeek + 1 > $day_of_week) {
        //         $product_start = $product_start->addDays(7 - ($product_start->dayOfWeek + 1) + $day_of_week);
        //         if ($product_start->dayOfWeek + 1 == $day_of_week) {
        //             $i = 0;
        //             $data[$i]['resourceId'] = $room;
        //             $data[$i]['display'] = 'background';
        //             $data[$i]['color'] = 'green';
        //             $data[$i]['title'] = $session->product->product_name;
        //             $data[$i]['advisor'] = $session->product->advisor_id;
        //             $data[$i]['start'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->start;
        //             $data[$i]['end'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->end;
        //             $i++;
        //             while ($product_start->addDays(7) <= $product_end) {
        //                 $data[$i]['resourceId'] = $room;
        //                 $data[$i]['display'] = 'background';
        //                 $data[$i]['color'] = 'green';
        //                 $data[$i]['title'] = $session->product->product_name;
        //                 $data[$i]['advisor'] = $session->product->advisor_id;
        //                 $data[$i]['start'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->start;
        //                 $data[$i]['end'] = Carbon::instance($product_start->datetime())->format('Y-m-d') . ' ' . $session->end;
        //                 $i++;
        //             }
        //         }
        //     }
        //     // $data2 = [];
        //     // foreach ($data as $key => $value) {
        //     //     $a = convert_session_to_45($value['session'], $value['date'], $room,$session->product);
        //     //     if ($a != []) {
        //     //         $data2 = array_merge($data2, $a);
        //     //     }
        //     // }
        //     return $data;
        // }
        // $data = [];
        // foreach ($sessions as $session) {
        //     foreach ($session->ProductDay as $day) {
        //         $b = canvert_day_to_date($day->day_of_week, $session->start, $session->end, $day, $session->room_id);
        //         $data = array_merge($data, $b);
        //     };
        // }
        // return $data;
    }
    public function session_getevent_deleted()
    {
        $start = verta(str_replace('-', '/', request('start')));
        $end = verta(str_replace('-', '/', request('end')));
        $event =  DB::select(
            'SELECT
        concat(users.name," ",users.family) as title ,
        concat(days.miladi_date," ",day_of_product.`start`) as `start`,
        concat(days.miladi_date," ",day_of_product.`end`) as `end`,
        day_of_product.day_of_week,
        day_of_product.room_id as "resourceId",
        day_of_product.color as "color",
        products.advisor_id,
        products.id as product_id,
        "background" as "display",
        days.dey_of_week,
        days.miladi_date 
    FROM
        products
        INNER JOIN day_of_product ON products.id = day_of_product.product_id
        INNER JOIN users ON products.advisor_id = users.id
        INNER JOIN days ON days.dey_of_week = day_of_product.day_of_week 
    WHERE
         products.`start` <= days.date AND products.`end` >= days.date
         And days.miladi_date < "' . Carbon::parse(request('end'))->format('Y-m-d') . '" 
         And days.miladi_date >= "' . Carbon::parse(request('start'))->format('Y-m-d') . '"'
        );
        $sales = Sale::where([
            [
                'date',
                '<',
                Carbon::parse(request('end'))->format('Y-m-d')
            ],
            [
                'date',
                '>=',
                Carbon::parse(request('start'))->format('Y-m-d')
            ],
            [
                'delete_type',
                '=',
                '1'
            ]
        ])->with(['customer', 'transactions'])->get();
        $data = [];
        $i = 0;
        foreach ($sales as $sale) {
            $data[$i] = $sale;
            $data[$i]['title'] = $sale->customer->name . ' ' . $sale->customer->family;
            $data[$i]['start'] = $sale->date . ' ' . $sale->session_start;
            $data[$i]['end'] = $sale->date . ' ' . Carbon::parse($sale->session_start)->addMinute($sale->session_length)->format('H:i');
            $data[$i]['resourceId'] = $sale->room_id;

            $i++;
        }
        return array_merge($data, $event);
    }
    public static function _sond_to_zarinpall($Amount, $Mobile, $data_for_zarrinpal)
    {
        //$MerchantID = '390d7cec-862c-11e7-b757-000c295eb8fc'; //Required
        $Description = setting("name_markaz"); //$data_for_zarrinpal['tozihat']; // Required'توضيحات تراکنش تستي';
        $Email = ""; // Optional
        $CallbackURL = route('zarinpal.success'); // Required
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        @$result = $client->PaymentRequest(
            [
                'MerchantID' => '853e124c-c124-11e7-bdd7-005056a205be',
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );
        if ($result->Status == 100) {
            $add = "https://www.zarinpal.com/pg/StartPay/" . $result->Authority;
            SendSMS('لینک پرداخت زرین پال  ' . $add, $Mobile);
            return $result->Authority;
            //براي استفاده از زرين گيت بايد ادرس به صورت زير تغيير کند:
            //Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority.'/ZarinGate');
        } else {
            return '<a id="forget_btn" href="' . route('zarinpal.success') . '" class="easyui-linkbutton l-btn l-btn-small" style="width: 198px; height: 38px; margin-top: 20px;"  group=""><span class="l-btn-left" style="margin-top: 7px;"><span class="l-btn-text">  یا  ارتباط با سرور پرداخت آنلاین امکان ندارد اطلاعات پرداخت شما کامل نمي باشد</span></span></a>';
        }
    }
    public function zarinpal_success(Request $request)
    {
        return $request;
    }
    public function GhaboolePardakht(Request $request)
    {
        if ($request->Status == 'OK') {
            $kharid = Zarinpal::where('token', $request->Authority)->first();
            if ($kharid) {
                $kharide_state = $this->_chek_kharid($kharid);
                if ($kharide_state->Status == '100') {
                    $sales_selected = explode(',', $kharid->model_id);
                    $transaction_selected = Transaction::where(['id' => $sales_selected, ['amount_standing', '<', '0']])->get();

                    $sum_transaction = $transaction_selected->sum('amount_standing');
                    $transaction = Transaction::create([
                        'user_id' => $kharid->user_id,
                        'amount' => $kharid->price,
                        'deposit_date' => verta()->format('Y/m/d H:i'),
                        'transaction_type_id' => '2',
                        'amount_standing' => ($sum_transaction +  $kharid->price > 0) ? $sum_transaction +  $kharid->price : 0,
                        'cash_id' => '1',
                        'comment' => $kharid->comment,
                        // 'operator_id' => auth()->id(),
                    ]);
                    $price = $kharid->price;
                    foreach ($transaction_selected as $item) {
                        if ($price > '0') {
                            $a = $item->amount_standing;
                            $item->amount_standing = ($item->amount_standing + $price > 0) ? 0 : $item->amount_standing + $price;
                            $price = ($a + $price > 0) ? $a + $price : 0;
                            $item->save();
                            TransactionDetail::create([
                                'source_transaction_id' => $transaction->id,
                                'distinction_transaction_id' => $item->id,
                                'amount' => $item->amount_standing - $a,
                            ]);
                        }
                    }
                    Transaction::create([
                        'user_id' => setting('getting_acount'),
                        'amount' => $request->price,
                        'transaction_type_id' => '2',
                        'deposit_date' => $request->date . ' ' . $request->time,
                        'amount_standing' => $request->price,
                        'cash_id' => 1,
                        'operator_id' => auth()->id(),
                    ]);
                    return 'پرداخت با موفقیت انجام شد';
                }
            }
        } else {
            return 'پرداخت شکست خورد';
        }
    }
    private function _chek_kharid($kharid)
    {
        $MerchantID = $this->MerchantID;
        $Amount = $kharid->price; //Amount will be based on Toman
        $Authority = $kharid->token;
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentVerification(
            [
                'MerchantID' => $MerchantID,
                'Authority' => $Authority,
                'Amount' => $Amount,
            ]
        );
        return ($result);
    }
}
