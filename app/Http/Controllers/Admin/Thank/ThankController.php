<?php

namespace App\Http\Controllers\Admin\Thank;

use App\Http\Controllers\Controller;
use App\Models\Dars;
use App\Models\Thank;
use App\Models\User;
use App\Models\Role;
use App\Models\Ostad;
use App\Models\Thanksreason;
use Illuminate\Http\Request;

class ThankController extends Controller
{
    public function index()
    {
        $data = [];
        $reasons = Thanksreason::select(['title','id','flag'])->where('status', 'active')->get();
        $data['like_reasons'] = $reasons->where('flag', 'like');
        $data['dislike_reasons'] = $reasons->where('flag', 'dislike');
        $thanks = Thank::select(['id','note','flag','user_id','audience_id','operator_id'])->with('user:id,name,family', 'audience:id,name,family', 'operator:id,name,family')->orderBy('id', 'desc')->get();
        $data['likes'] = $thanks->where('flag', 'like');
        $data['dislikes'] = $thanks->where('flag', 'dislike');
        $users = User::select(['id','name','family','user_type'])->whereIn('user_type',['advisor','customer'])->get();
        $data['advisors'] = $users->where('user_type','advisor');
        $data['admins'] = $users->where('user_type','advisor');
        return view('components.Admin.Thank.Thank_list', $data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user' => ['required'],
            'audience' => ['required'],
            'reason_id' => ['required'],
            'note' => ['required'],
            'flag' => ['required']
        ]);
        Thank::create([
            'user_id'=>$request->user,
            'audience_id'=> $request->audience,
            'note' => $request->note,
            'flag' => $request->flag,
            'status' => 'active',
            'reason_id' => $request->reason_id,
            'operator_id' => auth()->id()
        ]);

        return redirect()->route('thank.index');
    }

    public function Thanks_reason_list(){
        $data = [];
        $data['reasons'] = Thanksreason::where('status', '!=', 'deleted')->get();
        return view('components.Admin.Thank.Thanks_reasons_list', $data);
    }
    public function Thanks_reason_create(){
        return view('components.Admin.Thank.Thank_reasons_create');
    }
    public function Thanks_reason_store(Request $request){
        // return $request;
        Thanksreason::create([
            "title" => $request->title,
            "status" => $request->status,
            "flag" => $request->flag,
            "comment" => $request->comment,
            "operator_id" => auth()->id()
        ]);
        return redirect()->route('Thanks_reason_list');
    }
    public function Thanks_reason_edit($id){
        // return $id;
        $data = [];
        $data['reason'] = Thanksreason::find($id);
        return view('components.Admin.Thank.Thank_reasons_edit', $data);
    }
    public function Thanks_reason_update(Request $request, $id)
    {
        // return $request;
        Thanksreason::where('id', $id)->update([
            "title" => $request->title,
            "status"=> $request->status,
            "flag"=> $request->flag,
            "comment"=> $request->comment,
            "operator_id" => auth()->user()->id
        ]);
        // $reason->save();
        return redirect()->route('Thanks_reason_list');
    }
    public function Thanks_reason_destroy($id){
        // return $id;
        Thanksreason::find($id)->update([
            "status" => 'deleted'
        ]);
        return redirect()->route('Thanks_reason_list');
    }


    public function dadkar_list(){
        $data = [];
        $data['ostads'] = Ostad::all();
        return view('components.Admin/Dadkar/dadkar_ostad', $data);
    }

    public function dadkar_ostad($id)
    {
        $data['ostad'] = Ostad::where('id_card', $id)->first();
        $data['darses'] = Dars::where('id_card', $id)->get();
        return view('components.Admin/Dadkar/dadkar_gharardad', $data);
    }

    public function dadkar_edit($id)
    {
        $data['ostad'] = Ostad::where('id_card', $id)->first();
        return view('components.Admin.Dadkar.dadkar_edit', $data);
    }

    public function dadkar_delete($id)
    {
        $ostad = Ostad::find($id);
        $ostad->delete();
        return redirect()->route('dadkar_list');
    }

    public function dadkar_update(Request $request, $id)
    {
       Ostad::where('id_card', $id)->update([
            'name'=>$request->name ,
            'father'=>$request->father,
            'sh_sh'=>$request->sh_sh ,
            'birthday'=>$request->birthday,
            'id_card'=>$request-> id_card,
            'national_code'=>$request->national_code,
            'phone'=>$request->phone,
            'mobile'=>$request->mobile,
            'reshte'=>$request->reshte,
            'madrak'=>$request->madrak,
            'univercity'=>$request->univercity,
            'country'=>$request->country ,
            'address'=>$request->address ,
            'email'=>$request->email,
            'mablagh'=>$request->mablagh,
            'acount_number'=>$request->acount_number,
            'date'=>$request->date,
            'number'=>$request->number
        ]);
        return redirect()->route('dadkar_list');
    }

    public function dadkar_create()
    {
        return view('components.Admin/Dadkar/dadkar_new_ostad');
    }

    public function dadkar_store(Request $request)
    {
        // return $request;
        Ostad::create([
            'name'=>$request->name ,
            'father'=>$request->father,
            'sh_sh'=>$request->sh_sh ,
            'birthday'=>$request->birthday,
            'id_card'=>$request-> id_card,
            'national_code'=>$request->national_code,
            'phone'=>$request->phone,
            'mobile'=>$request->mobile,
            'reshte'=>$request->reshte,
            'madrak'=>$request->madrak,
            'univercity'=>$request->univercity,
            'country'=>$request->country ,
            'address'=>$request->address ,
            'email'=>$request->email,
            'mablagh'=>$request->mablagh,
            'acount_number'=>$request->acount_number,
            'date'=>$request->date,
            'number'=>$request->number
        ]);
        return redirect()->route('dadkar_list');
    }

    public function dadkar_darse_ostad($id)
    {
        $data['ostad'] = Ostad::where('id_card', $id)->first();
        $data['darses'] = Dars::where('id_card', $id)->get();
        return view('components.Admin/Dadkar/dadkar_darse_ostad', $data);
    }

    public function dadkar_edit_dars($id){
        $data['dars'] = Dars::where('id', $id)->get();
        return view('components.Admin/Dadkar/dadkar_edit_dars', $data);
    }
}
