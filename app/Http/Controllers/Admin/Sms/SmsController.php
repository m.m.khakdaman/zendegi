<?php

namespace App\Http\Controllers\Admin\Sms;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['sms'] = Setting::with('operator')->select('*')->where('key', 'like', '%sms%')->get();
        $data['sms_wsh'] = Setting::with('operator')->select('*')->where('key', 'like', '%s_m_s_wsh%')->get();
        return view('components.Admin.Settings.Sms.index', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.Admin.Settings.Sms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Setting::create([
            'key' => $request->sms_type,
            'value' => $request->sms_text,
            'operator_id' => auth()->user()->id,
            'status' => 1
        ]);
        return redirect()->route('sms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['sms'] = Setting::find($id);
        // return strpos("Hello php!", "prhp");
        // return ;


        return view('components.Admin.Settings.Sms.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sms = Setting::find($id);
        $sms->value = $request->sms_text;
        $sms->operator_id = auth()->user()->id;
        $sms->save();
        return redirect()->route('sms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $sms = Setting::find($id)->delete();
        return back();
    }

    public function send(User $user)
    {
        $smses = Setting::select('*')->where('key', 'REGEXP', 'sms_list')->get();
        return view('components.Admin.Sms.send', compact('user', 'smses'));
    }

    public function activation($id)
    {
        $tmp = Setting::find($id);
        Setting::where('id', $id)->update(['status' => $tmp->status xor 1]);
        return redirect()->route('sms.index');
    }

    public function send_sms()
    {
        request()->validate([
            'customers' => 'required',
            'message' => 'required',
        ]);

        $user = User::find(request('customers'));
        $a = SendSMS(request('message'), $user->mobile);
        //dd($a);
        return redirect(request('back_url'));
    }
}
