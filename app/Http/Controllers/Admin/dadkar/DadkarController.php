<?php

namespace App\Http\Controllers\Admin\dadkar;

use App\Http\Controllers\Controller;
use App\Models\Dars;
use Illuminate\Http\Request;
use App\Models\Ostad;

class DadkarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 45;
        $data = [];
        $data['ostads'] = Ostad::all();
        return view('Dadkar.dadkar_ostad', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        Ostad::create([
            'name'=>$request->name ,
            'father'=>$request->father,
            'sh_sh'=>$request->sh_sh ,
            'birthday'=>$request->birthday,
            'id_card'=>$request-> id_card,
            'national_code'=>$request->national_code,
            'phone'=>$request->phone,
            'mobile'=>$request->mobile,
            'reshte'=>$request->reshte,
            'madrak'=>$request->madrak,
            'univercity'=>$request->univercity,
            'country'=>$request->country ,
            'address'=>$request->address ,
            'email'=>$request->email,
            'mablagh'=>$request->mablagh,
            'acount_number'=>$request->acount_number,
            'date'=>$request->date,
            'number'=>$request->number
        ]);
        return redirect()->route('dadkar.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data['ostad'] = Ostad::where('id_card', $id)->first();
        $data['darses'] = Dars::where('id_card', $id)->get();
        return view('components.Admin/dadkar_gharardad', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['ostad'] = Ostad::where('id_card', $id)->first();
        return view('components.Admin/dadkar_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       Ostad::where('id_card', $id)->update([
            'name'=>$request->name ,
            'father'=>$request->father,
            'sh_sh'=>$request->sh_sh ,
            'birthday'=>$request->birthday,
            'id_card'=>$request-> id_card,
            'national_code'=>$request->national_code,
            'phone'=>$request->phone,
            'mobile'=>$request->mobile,
            'reshte'=>$request->reshte,
            'madrak'=>$request->madrak,
            'univercity'=>$request->univercity,
            'country'=>$request->country ,
            'address'=>$request->address ,
            'email'=>$request->email,
            'mablagh'=>$request->mablagh,
            'acount_number'=>$request->acount_number,
            'date'=>$request->date,
            'number'=>$request->number
        ]);
        return redirect()->route('dadkar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    private function salam()
    {
        Ostad::salam();
        // return view('components.Admin/Dadkar/dadkar_salam');
    }
}
