<?php

namespace App\Http\Controllers\Admin\Financial;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\Cash;
use App\Models\Role;
use App\Models\Sale;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\TransactionType;
use App\Models\YaribargType;
use App\Models\User;
use App\Models\Zarinpal;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $sale = Sale::find(request('sale'));
        $data['sale'] = $sale;
        $data['transactions'] = $user->transactions()->with(['user', 'operator', 'sale.advisor', 'sale.product', 'distinctions', 'sources', 'transaction_type'])->latest()->get();
        $data['accounts'] = auth()->user()->Cashes()->whereAccount_status(1)->with('cashe_type')->withSum('transactions', 'amount_standing')->get()->groupBy('cashe_type_id');
        $data['customer'] = $user;
        $data['YaribargType'] = YaribargType::where(['status' => 1])->get();

        return view('components.Admin.Financial.Transaction.invoice', $data);
    }
    public function invoise(User $user)
    {
        $sale = Sale::find(request('sale'));
        $data['sale'] = $sale;
        $data['transactions'] = $user->transactions()->where('amount_standing', '!=', '0')->with('user', 'operator', 'sale.advisor', 'sale.product', 'distinctions', 'distinctions.distinction', 'sources', 'sources.source', 'transaction_type')->latest()->get();
        $data['accounts'] = auth()->user()->Cashes()->whereAccount_status(1)->with('cashe_type')->withSum('transactions', 'amount_standing')->get()->groupBy('cashe_type_id');
        $data['accounts'][1] = $data['accounts'][1] ?? [];
        $data['accounts'][2] = $data['accounts'][2] ?? [];
        $data['customer'] = $user;
        //$data['yaribarg'] = Transaction::where('transaction_type_id', '=', '3')->get();
        $data['YaribargType'] = YaribargType::where(['status' => 1])->get();
        return view('components.Admin.Financial.Transaction.invoice', $data);
    }

    public function invoise2(User $user)
    {
        $data = [];
        $sale = Sale::find(request('sale'));
        $data['sale'] = $sale;
        $data['transactions'] = $user->transactions()->with('user', 'operator', 'sale.advisor', 'sale.product', 'distinctions', 'distinctions.distinction', 'sources', 'sources.source', 'transaction_type')->latest()->get();
        $data['accounts'] = Cash::whereAccount_status(1)->with('cashe_type')->get()->groupBy('cashe_type_id');
        $data['accounts'][1] = $data['accounts'][1] ?? [];
        $data['accounts'][2] = $data['accounts'][2] ?? [];
        $data['customer'] = $user;
        // //$data['yaribarg'] = Transaction::where('transaction_type_id', '=', '3')->get();
        $data['YaribargType'] = YaribargType::where(['status' => 1])->get();
        return view('components.Admin.Financial.Transaction.invoice2', $data);
    }




    public function all()
    {
        Gate::authorize('transactions.index');
        $data['transaction_types'] = TransactionType::all();
        $data['cashes'] = Cash::all();
        $data['admins'] = User::where('user_type', 'admin')->get();
        $data['YaribargType'] = YaribargType::where(['status' => 1])->get();
        return view('components.Admin.Financial.Transaction.all', $data);
    }
    public function print()
    {
        $data = [];
        if (Gate::check('transactions.advisor.function') || Gate::check('transactions.admin.function')) {
        } else {
            return abort(403, 'عدم دسترسی');
        }
        if (request('start_date')) {

            $start = verta()->parse(request('start_date'))->formatGregorian('Y-m-d');;

            if (request('end_date')) {
                $end = verta()->parse(request('end_date'))->formatGregorian('Y-m-d');
            } else {
                $end = verta()->parse($start)->addDays(1)->formatGregorian('Y-m-d');
            }
            $Transactions = Transaction::where([
                [
                    'user_id',
                    '!=',
                    setting('center_user_id'),
                ],
                [
                    'user_id',
                    '!=',
                    setting('getting_acount'),
                ],

            ]);

            if (request('advisors') || request('advisors') == "0") {
                $Transactions->whereHas('sale', function ($query) use ($end, $start) {
                    if (request('advisors') != "0") {
                        $a = [

                            'delete_type' => '0',
                            [
                                'date',
                                '<',
                                $end
                            ],
                            [
                                'orginal_price', '>=', request('start_price')
                            ],
                            [
                                'orginal_price', '<=', request('end_price')
                            ],
                            [
                                'date',
                                '>=',
                                $start
                            ],
                            [
                                'amount',
                                '<',
                                '0'
                            ],
                            'delete_type' => '0',
                            'products_type_id' => setting('prudcot_session_type_id'),
                            'advisor_id' => request('advisors')
                        ];
                    } else {
                        $a = [
                            'delete_type' => '0',

                            [
                                'date',
                                '<',
                                $end
                            ],
                            [
                                'orginal_price', '>=', request('start_price')
                            ],
                            [
                                'orginal_price', '<=', request('end_price')
                            ],
                            [
                                'date',
                                '>=',
                                $start
                            ],
                            [
                                'amount',
                                '<',
                                '0'
                            ],
                            'products_type_id' => setting('prudcot_session_type_id'),
                        ];
                    }
                    $query->where($a);
                });
            } else {

                $Transactions->where([
                    [
                        'amount',
                        '>',
                        '0'
                    ],
                    [
                        'cash_id',
                        '!=',
                        1,
                    ],

                    [
                        'created_at',
                        '<',
                        $end
                    ],
                    [
                        'created_at',
                        '>=',
                        $start
                    ],
                ]);
            }

            $data['print_advisor'] = 0;
            if (request('advisors') != intval(0)) {
                $data['print_advisor'] = 1;
            }
            if (request('advisors') == '0') {
                $data['print_advisor'] = 1;
            }
            if (request('operators') != 0) {
                $Transactions->where('operator_id', request('operators'));
            }
            if (request('cash') != 0) {
                $Transactions->where('cash_id', request('cash'));
            }
            $Transactions->get();
            $data['Transactions'] = $Transactions->with([
                'operator',
                'sale.category',
                'sale.price_list',
                'transaction_type',
                'cash',
                'cash.cashe_type',
                'sources.distinction.sale.products_type',
                'sale',
                'sale.advisor',
            ])->get()->sortBy([
                [
                    $data['print_advisor'] ? 'sale.advisor' : 'operator',
                    'asc'
                ],
                ['sale.date', 'desc']
            ]);
            $data['start'] = $start;
            $data['end'] = $end;
            return view('components.Admin.Financial.Transaction.print_page', $data);
        }
        if (request()->input('print') == 'advisors') {
            Gate::authorize('transactions.advisor.function');
            $data['advisors'] = User::where('user_type', 'advisor')->get();
            $data['print_advisors'] = 1;
        } else {
            Gate::authorize('transactions.admin.function');
            $data['operators'] = User::where('user_type', 'admin')->get();
            $data['print_advisors'] = 0;
        }
        // return 'a';
        $data['cashs'] = Cash::latest()->get();
        return view('components.Admin.Financial.Transaction.print', $data);
    }
    public function get_ajax(Request $request)
    {
        $result = [];
        $transaction_where = [];
        $cash_where = [];
        $fullname_where = '';
        $operator_fullname_where = '';
        foreach ($request->columns as $item => $value) {
            if ($value['search']['value'] != null) {
                if ($value['name'] == 'transaction_type') {
                    $transaction_where = [['name', 'like', '%' . $value['search']['value'] . '%']];
                    continue;
                }
                if ($value['name'] == 'cash') {
                    $cash_where = [['name', 'like', '%' . $value['search']['value'] . '%']];
                    continue;
                }
                if ($value['name'] == 'fullname') {
                    $fullname_where = $value['search']['value'];
                    continue;
                }
                if ($value['name'] == 'operator') {
                    $operator_fullname_where = $value['search']['value'];
                    continue;
                }
                $result[$item][0] = $value['name'];
                $result[$item][1] = 'like';
                $result[$item][2] = '%' . $value['search']['value'] . '%';
            }
        }
        $transactions = Transaction::with(['cash', 'user', 'transaction_type', 'operator'])->where($result);
        if ($transaction_where != null) {
            $transactions->whereHas('transaction_type', function ($query) use ($transaction_where) {
                return $query->where($transaction_where);
            });
        }

        if ($cash_where != null) {
            $transactions->whereHas('cash',  function ($query) use ($cash_where) {
                return $query->where($cash_where);
            });
        }
        if ($fullname_where != '') {
            $transactions->whereHas('user',  function ($query) use ($fullname_where) {
                return $query->where(DB::raw('CONCAT(name,family)'), 'like', '%' . $fullname_where . '%')
                    ->orWhere(function ($query) use ($fullname_where) {
                        $query->where(
                            DB::raw('CONCAT(name," ",family)'),
                            'like',
                            '%' . $fullname_where . '%'
                        )->orWhere(
                            'mobile',
                            'like',
                            '%' . $fullname_where . '%'
                        );
                    });
            });
        }
        if ($operator_fullname_where != '') {
            $transactions->whereHas('operator',  function ($query) use ($operator_fullname_where) {
                return $query->where(DB::raw('CONCAT(name,family)'), 'like', '%' . $operator_fullname_where . '%')
                    ->orWhere(function ($query) use ($operator_fullname_where) {
                        $query->where(
                            DB::raw('CONCAT(name," ",family)'),
                            'like',
                            '%' . $operator_fullname_where . '%'
                        )->orWhere(
                            'mobile',
                            'like',
                            '%' . $operator_fullname_where . '%'
                        );
                    });
            });
        }
        return [
            "draw" => intval($request->draw),
            "recordsTotal" => $transactions->count(),
            "recordsFiltered" => $transactions->count(),
            "data" => $transactions->skip($request->start)->take($request->length)->orderBy($request->columns[($request->order[0]['column'])]['name'], $request->order[0]['dir'])->get()
        ];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    function convert2english($string)
    {
        $newNumbers = range(0, 9);
        // 1. Persian HTML decimal
        $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
        // 2. Arabic HTML decimal
        $arabicDecimal = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;');
        // 3. Arabic Numeric
        $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        // 4. Persian Numeric
        $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');

        $string =  str_replace($persianDecimal, $newNumbers, $string);
        $string =  str_replace($arabicDecimal, $newNumbers, $string);
        $string =  str_replace($arabic, $newNumbers, $string);
        return str_replace($persian, $newNumbers, $string);
    }
    public function cash_payment_store(Request $request)
    {
        $sales_selected = explode(',', $request->sales_selected);
        $transaction_selected = Transaction::where(['id' => $sales_selected, ['amount_standing', '<', '0']])->get();

        $sum_transaction = $transaction_selected->sum('amount_standing');

        $prd = Transaction::where([
            'operator_id' => auth()->id(),
            'user_id' => $request->customer_id,
            'amount' => $this->convert2english($request->price),
            'cash_id' => $request->account,
            'transaction_type_id' => $request->transaction_type_id,
            [
                'created_at', '>=', Carbon::now()->subMinutes(1)->format('Y-m-d H:i')
            ]
        ]);
        if ($prd->exists()) {
            return redirect(route('user.invoise', $request->customer_id))->with('error_message', 'لطفا ۱ دقیقه دیگر دوباره امتحان کنید');
        }
        $data = [
            'user_id' => $request->customer_id,
            'amount' => $this->convert2english($request->price),
            'transaction_type_id' => $request->transaction_type_id,
            'deposit_date' => $request->date . ' ' . $request->time,
            'amount_standing' => ($sum_transaction +  $this->convert2english($request->price) > 0) ? $sum_transaction +  $this->convert2english($request->price) : 0,
            'cash_id' => $request->account,
            'comment' => $request->comment,
            'tracking_code' => $request->tracking_code,
            'operator_id' => auth()->id()
        ];

        // برای یاری برگ ها
        if ($request->transaction_type_id == '3') {
            $data['yaribarg_type_id'] = $request->yaribarg_type_id;
            $data['yaribarg_status'] = 1;
            $data['yaribarg_serial'] = $request->serial;
            $data['yaribarg_date'] = $request->yari_date;
            $data['yaribarg_recived_date'] = $request->recived_date;
            $data['yaribarg_reg_date'] = verta();
        }
        $transaction = Transaction::create($data);
        $price = $this->convert2english($request->price);
        foreach ($transaction_selected as $item) {
            if ($price > '0') {
                $a = $item->amount_standing;
                $item->amount_standing = ($item->amount_standing + $price > 0) ? 0 : $item->amount_standing + $price;
                $price = ($a + $price > 0) ? $a + $price : 0;
                $item->save();
                TransactionDetail::create([
                    'source_transaction_id' => $transaction->id,
                    'distinction_transaction_id' => $item->id,
                    'amount' => $item->amount_standing - $a,
                ]);
            }
        }


        // نوع 3 برای یاری برگ هاست و در هنگام ثبت یاری برگ نباید به حساب دریافتنی ها پول واریز شود
        if ($request->transaction_type_id != '3') {
            Transaction::create([
                'user_id' => setting('getting_acount'),
                'amount' => $this->convert2english($request->price),
                'transaction_type_id' => $request->transaction_type_id,
                'deposit_date' => $request->date . ' ' . $request->time,
                'amount_standing' => $this->convert2english($request->price),
                'cash_id' => $request->account,
                'comment' => $request->comment,
                'operator_id' => auth()->id(),
            ]);
        }
        return redirect(route('user.invoise', $request->customer_id));
    }
    public function pardakht_be_moraje(Request $request)
    {
        $sales_selected = explode(',', $request->sales_selected);
        $transaction_selected = Transaction::where(['id' => $sales_selected, ['amount_standing', '<', '0']])->get();

        $sum_transaction = $transaction_selected->sum('amount_standing');

        $prd = Transaction::where([
            'operator_id' => auth()->id(),
            'user_id' => $request->customer_id,
            'amount' => $this->convert2english($request->price),
            'cash_id' => $request->account,
            'transaction_type_id' => '7',
            [
                'created_at', '>=', Carbon::now()->subMinutes(1)->format('Y-m-d H:i')
            ]
        ]);
        if ($prd->exists()) {
            return redirect(back())->with('error_message', 'لطفا ۱ دقیقه دیگر دوباره امتحان کنید');
        }

        $transaction = Transaction::create([
            'user_id' => $request->customer_id,
            'amount' => -$this->convert2english($request->price),
            'transaction_type_id' => '7',
            'deposit_date' => $request->date . ' ' . $request->time,
            'amount_standing' => ($sum_transaction +  $this->convert2english($request->price) > 0) ? $sum_transaction +  (-$this->convert2english($request->price)) : 0,
            'cash_id' => $request->account,
            'comment' => $request->comment,
            'operator_id' => auth()->id(),
        ]);
        $price = $this->convert2english($request->price);
        // foreach ($transaction_selected as $item) {
        //     if ($price > '0') {
        //         $a = $item->amount_standing;
        //         $item->amount_standing = ($item->amount_standing + $price > 0) ? 0 : $item->amount_standing + $price;
        //         $price = ($a + $price > 0) ? $a + $price : 0;
        //         $item->save();
        //         TransactionDetail::create([
        //             'source_transaction_id' => $transaction->id,
        //             'distinction_transaction_id' => $item->id,
        //             'amount' => $item->amount_standing - $a,
        //         ]);
        //     }
        // }
        Transaction::create([
            'user_id' => setting('getting_acount'),
            'amount' => - ($this->convert2english($request->price)),
            'transaction_type_id' => '7',
            'deposit_date' => $request->date . ' ' . $request->time,
            'amount_standing' => - ($this->convert2english($request->price)),
            'cash_id' => $request->account,
            'operator_id' => auth()->id(),
        ]);
        return redirect(route('user.invoise', $request->customer_id));
    }
    public function cartkhan_store(Request $request)
    {
        $sales_selected = explode(',', $request->sales_selected);
        $transaction_selected = Transaction::where(['id' => $sales_selected, ['amount_standing', '<', '0']])->get();

        $sum_transaction = $transaction_selected->sum('amount_standing');

        $prd = Transaction::where([
            'operator_id' => auth()->id(),
            'user_id' => $request->customer_id,
            'amount' => $this->convert2english($request->price),
            'cash_id' => $request->account,
            'transaction_type_id' => '5',
            [
                'created_at', '>=', Carbon::now()->subMinutes(1)->format('Y-m-d H:i')
            ]
        ]);
        if ($prd->exists()) {
            return redirect(back())->with('error_message', 'لطفا ۱ دقیقه دیگر دوباره امتحان کنید');
        }
        // ->with('message', 'انتقال پول با موفقیت انجام شد!');
        $transaction = Transaction::create([
            'user_id' => $request->customer_id,
            'amount' => $this->convert2english($request->price),
            'deposit_date' => $request->date . ' ' . $request->time,
            'transaction_type_id' => '5',
            'amount_standing' => ($sum_transaction +  $this->convert2english($request->price) > 0) ? $sum_transaction +  $this->convert2english($request->price) : 0,
            'cash_id' => $request->account,
            'comment' => $request->comment,
            'operator_id' => auth()->id(),
        ]);
        $price = $this->convert2english($request->price);
        foreach ($transaction_selected as $item) {
            if ($price > '0') {
                $a = $item->amount_standing;
                $item->amount_standing = ($item->amount_standing + $price > 0) ? 0 : $item->amount_standing + $price;
                $price = ($a + $price > 0) ? $a + $price : 0;
                $item->save();
                TransactionDetail::create([
                    'source_transaction_id' => $transaction->id,
                    'distinction_transaction_id' => $item->id,
                    'amount' => $item->amount_standing - $a,
                ]);
            }
        }
        Transaction::create([
            'user_id' => setting('getting_acount'),
            'amount' => $this->convert2english($request->price),
            'transaction_type_id' => '5',
            'deposit_date' => $request->date . ' ' . $request->time,
            'amount_standing' => $this->convert2english($request->price),
            'cash_id' => $request->account,
            'operator_id' => auth()->id(),
        ]);
        return redirect(route('user.invoise', $request->customer_id));
    }
    public function cartkh_to_cart_store(Request $request)
    {
        $sales_selected = explode(',', $request->sales_selected);
        $transaction_selected = Transaction::where(['id' => $sales_selected, ['amount_standing', '<', '0']])->get();


        $prd = Transaction::where([
            'operator_id' => auth()->id(),
            'user_id' => $request->customer_id,
            'amount' => $this->convert2english($request->price),
            'cash_id' => $request->account,
            'transaction_type_id' => '7',
            [
                'created_at', '>=', Carbon::now()->subMinutes(1)->format('Y-m-d H:i')
            ]
        ]);
        if ($prd->exists()) {
            return redirect(back())->with('error_message', 'لطفا ۱ دقیقه دیگر دوباره امتحان کنید');
        }

        $sum_transaction = $transaction_selected->sum('amount_standing');
        $transaction = Transaction::create([
            'user_id' => $request->customer_id,
            'amount' => $this->convert2english($request->price),
            'deposit_date' => $request->date . ' ' . $request->time,
            'transaction_type_id' => '4',
            'amount_standing' => ($sum_transaction +  $this->convert2english($request->price) > 0) ? $sum_transaction +  $this->convert2english($request->price) : 0,
            'cash_id' => $request->account,
            'comment' => $request->comment,
            'operator_id' => auth()->id(),
        ]);
        $price = $this->convert2english($request->price);
        foreach ($transaction_selected as $item) {
            if ($price > '0') {
                $a = $item->amount_standing;
                $item->amount_standing = ($item->amount_standing + $price > 0) ? 0 : $item->amount_standing + $price;
                $price = ($a + $price > 0) ? $a + $price : 0;
                $item->save();
                TransactionDetail::create([
                    'source_transaction_id' => $transaction->id,
                    'distinction_transaction_id' => $item->id,
                    'amount' => $item->amount_standing - $a,
                ]);
            }
        }
        Transaction::create([
            'user_id' => setting('getting_acount'),
            'amount' => $this->convert2english($request->price),
            'transaction_type_id' => '4',
            'deposit_date' => $request->date . ' ' . $request->time,
            'amount_standing' => $this->convert2english($request->price),
            'cash_id' => $request->account,
            'operator_id' => auth()->id(),
        ]);
        return redirect(route('user.invoise', $request->customer_id));
    }
    public function offset_store(Request $request)
    {
        // return $request;
        $sales_selected = explode(',', $request->sales_selected);
        $transaction_selected = Transaction::find($sales_selected);
        $mosbat = $transaction_selected->where('amount_standing', '>', '0');
        $manfi = $transaction_selected->where('amount_standing', '<', '0');
        foreach ($manfi as  $value) {
            if ($value->amount_standing < '0') {
                foreach ($mosbat as $item) {
                    if ($item->amount_standing > '0' && $value->amount_standing < '0') {
                        $a = $value->amount_standing;
                        $value->amount_standing = $value->amount_standing + $item->amount_standing > 0 ? 0 : $value->amount_standing + $item->amount_standing;
                        $item->amount_standing = $a + $item->amount_standing < 0 ? 0 : $a + $item->amount_standing;
                        $item->save();
                        TransactionDetail::create([
                            'source_transaction_id' => $item->id,
                            'distinction_transaction_id' => $value->id,
                            'amount' => $value->amount_standing - $a,
                        ]);
                    } else {
                        break;
                    }
                }
            }
            $value->save();
        }
        return redirect(route('user.invoise', $request->customer_id));
    }
    public function internet_peyment(Request $request)
    {
        // return User::find($request->customer_id);
        $sales_selected = explode(',', $request->sales_selected);
        $transaction_selected = Transaction::where(['id' => $sales_selected, ['amount_standing', '<', '0']])->get();
        Zarinpal::create([
            'model' => 'Transaction',
            'model_id' => $request->sales_selected,
            'user_id' => $request->customer_id,
            'status' => 0,
            'price' => $this->convert2english($request->price),
            'token' => AdminController::_sond_to_zarinpall($this->convert2english($request->price), User::find($request->customer_id)->mobile, $transaction_selected),
            'comment' => $request->commment,
        ]);
        return back();
    }


    public function pardakht_be_advisor()
    {
        $data['advisors'] = User::where('user_type', 'advisor')->get();
        $data['cashes'] = Cash::latest()->get();
        return view('components.Admin.Financial.Transaction.pardakht_be_advisor', $data);
    }
    public function pardakht_be_advisor_store(Request $request)
    {
        $request->validate([
            'price' => 'numeric|min:0|required',
            'advisors' => 'required|exists:users,id',
            'cash_id' => 'required|exists:cashes,id',
            'comment' => 'nullable|string',
        ]);
        Transaction::create([
            'user_id' => $request->advisors,
            'amount' => - ($request->price),
            'amount_standing' => - ($request->price),
            'cash_id' => $request->cash_id,
            'operator_id' => auth()->id(),
            'comment' => $request->comment,
        ]);
        Transaction::create([
            'user_id' => setting('getting_acount'),
            'amount' => - ($request->price),
            'amount_standing' => - ($request->price),
            'cash_id' => $request->cash_id,
            'operator_id' => auth()->id(),
            'comment' => $request->comment,
        ]);
        return redirect(route('admin.home'))->with('message', 'پرداخت به مراجع با موفقیت انجام شد!');
    }

    public function transfer_to_account()
    {
        Gate::authorize('transfer_to_account');
        $data['advisors'] = User::where('user_type', 'advisor')->get();
        $data['cashes'] = Cash::latest()->get();
        return view('components.Admin.Financial.Transaction.transfer_to_account', $data);
    }
    public function transfer_to_account_store(Request $request)
    {
        Gate::authorize('transfer_to_account');
        $request->validate([
            'price' => 'numeric|min:0|required',
            'from_cash_id' => 'required|exists:cashes,id',
            'to_cash_id' => 'required|exists:cashes,id',
            'comment' => 'nullable|string',
            'fish_num' => 'nullable',
            'fish_date' => 'nullable',
        ]);
        Transaction::create([
            'user_id' => setting('getting_acount'),
            'amount' => - ($request->price),
            'amount_standing' => - ($request->price),
            'cash_id' => $request->from_cash_id,
            'operator_id' => auth()->id(),
            'comment' => $request->comment,
            'tracking_code' => $request->fish_num,
            'deposit_date' => $request->fish_date
        ]);
        Transaction::create([
            'user_id' => setting('getting_acount'),
            'amount' => ($request->price),
            'amount_standing' => ($request->price),
            'cash_id' => $request->to_cash_id,
            'operator_id' => auth()->id(),
            'comment' => $request->comment,
            'tracking_code' => $request->fish_num,
            'deposit_date' => $request->fish_date
        ]);
        return redirect(route('admin.home'))->with('message', 'انتقال پول با موفقیت انجام شد!');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($transaction)
    {
        $data['transaction'] = Transaction::whereId($transaction)->with(['user', 'sources'])->first();
        $data['cashes'] = Cash::all();
        return view('components.Admin.Financial.Transaction.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $request->validate([
            'amount' => 'required|numeric|min:0',
            'cash' => 'required|numeric',
            'comment' => 'nullable|string',
        ]);

        $transaction->update([
            'amount' => $request->amount,
            'cash_id' => $request->cash,
            'comment' => $request->comment,
        ]);

        return redirect(route('admin.home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($transaction)
    {
        $transaction = Transaction::whereId($transaction)->with('sources.distinction')->first();
        if($transaction->amount < 0){
            return back();
        }
        foreach ($transaction->sources as $key => $value) {
            $value->distinction ? $value->distinction->update(['amount_standing', - (intval($value->amount))]) : null;
            $value->delete();
        }
        Transaction::whereId($transaction->id)->delete();
        return back();
    }

    public function back_transaction(TransactionDetail $back_transaction)
    {
        return $back_transaction;
        $distinction_transaction = Transaction::whereId($back_transaction->distinction_transaction_id)->first();
        $distinction_transaction->update([
            'amount_standing' => $distinction_transaction->amount_standing - $back_transaction->amount,
        ]);
        $source_transaction = Transaction::whereId($back_transaction->source_transaction_id)->first();
        $source_transaction->update([
            'amount_standing' => $source_transaction->amount_standing + $back_transaction->amount,
        ]);
        $back_transaction->delete();
        return back();
    }
}
