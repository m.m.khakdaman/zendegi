<?php

namespace App\Http\Controllers\Admin\Financial\Costs;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Cash;
use App\Models\Cost_title;
use Illuminate\Http\Request;

class CostController extends Controller
{
    public function index()
    {
        $data = [];
        $data['costs']=Transaction::where('cost_titles_id', !null)->with('operator')->limit(100)->get();
        return view('components.Admin.Financial.Costs.costs_list',$data);
    }
    public function cost_store()
    {
        $data = [];
        $data['costtitles']=Cost_title::all();
        $data['cashs']=Cash::all();
        return view('components.Admin.Financial.Costs.cost_store',$data);
    }
    public function cost_create(Request $request)
    {
        Transaction::create([
            'cost_titles_id' => $request->cost_title,
            'amount' => $request->amount_cost,
            'cash_id' => $request->cash,
            'operator_id' => auth()->id()
        ]);
        return redirect(route('costs_list'));
    }
}
