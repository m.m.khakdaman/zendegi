<?php

namespace App\Http\Controllers\Admin\Financial;

use App\Http\Controllers\Controller;
use App\Models\PriceList;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PriceListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::denies('price_list.index')){
            return abort(403,'دسترسی به نرخ نامه ها برای شما فعال نیست');
        }
        $data['price_lists'] = PriceList::latest()->get();
        return view('components.Admin.Financial.PriceList.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::denies('price_list.create')){
            return abort(403,'دسترسی به نرخ نامه ها برای شما فعال نیست');
        }
        return view('components.Admin.Financial.PriceList.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::denies('price_list.create')){
            return abort(403,'دسترسی به نرخ نامه ها برای شما فعال نیست');
        }
        PriceList::create([
            'name'=>$request->name,
            'advisor_rate'=>$request->advisor_rate,
            'center_rate'=>$request->center_rate,
            'total_price'=>$request->total_price,
            'session_length'=>$request->session_length,
            'active'=>$request->active?'1':'0',
            'ID_ADD'=>auth()->id(),
            'ID_EDIT'=>auth()->id()
        ]);
        return redirect(route('price_list.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PriceList  $priceList
     * @return \Illuminate\Http\Response
     */
    public function show(PriceList $priceList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PriceList  $priceList
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceList $priceList)
    {
        if(Gate::denies('price_list.edit')){
            return abort(403,'دسترسی به نرخ نامه ها برای شما فعال نیست');
        }
        return view('components.Admin.Financial.PriceList.edit',compact('priceList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PriceList  $priceList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PriceList $priceList)
    {
        $priceList->update([
            'name'=>$request->name,
            'advisor_rate'=>$request->advisor_rate,
            'center_rate'=>$request->center_rate,
            'total_price'=>$request->total_price,
            'session_length'=>$request->session_length,
            'active'=>$request->active?'1':'0',
            'ID_EDIT'=>auth()->id()
        ]);

        return redirect(route('price_list.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PriceList  $priceList
     * @return \Illuminate\Http\Response
     */
    public function destroy(PriceList $priceList)
    {
        $priceList->delete();
        return  back();
    }
}
