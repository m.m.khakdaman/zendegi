<?php
namespace App\Http\Controllers\Admin\Financial\Costs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\cost_title;

class CostTitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['cost_titles']=cost_title::where('delete_flag',0)->get();
        return view('components.Admin.Financial.Costtitle.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.Admin.Financial.Costtitle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        cost_title::create([
            'title' => $request->title,
            'status' => $request->status,
            'note' => $request->note,
            'operator_id' => auth()->id(),
            'delete_flag'=>0
        ]);


        return redirect(route('cost_titles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(cost_title $cost_title)
    {
        $data = [];
        $data['cost_title'] = $cost_title;
        return view('components.Admin.Financial.Costtitle.edit',$data);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cost_title $cost_title)
    {
        $cost_title->update([
            'title' => $request->title,
            'status'=> $request->status,
            'note'=> $request->note
        ]);


        return redirect(route('cost_titles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(cost_title $cost_title)
    {
        $cost_title->update([
            'delete_flag' => 1
        ]);
        return back();
    }
}