<?php

namespace App\Http\Controllers\Admin\Financial;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Role;
use App\Models\User;
use App\Models\YaribargType;
use Illuminate\Support\Facades\Gate;

class YaribargController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('yaribarg.index');
        $data['transactions'] = Transaction::where([
            [
                "transaction_type_id",
                "=",
                "3"
            ]
        ])->with('user', 'operator', 'yaribargtype', 'yaribargstatus')->latest()->get();

        $data['yaribargtype'] = YaribargType::all();
        $data['customers'] = User::where('user_type','customer')->paginate(10);

        

        return view('components.Admin.Financial.Yaribarg.index', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('yaribarg.create');
        //  return $request;
        Transaction::create([
            'yaribarg_date_of_use'=>$request->yaribarg_date_of_use,
            'user_id'=>$request->customer_id,
            'operator_id'=>auth()->user()->id,
            'amount'=>$request->ammount,
            'tracking_code'=>$request->tracking_code,
            'yaribarg_type_id'=>$request->yaribarg_type_id,
            'yaribarg_status'=>1,
            'comment'=>$request->note,
            'transaction_type_id'=>3,
        ]);
        return redirect(route('yaribarg.index'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*****************************************yaribarg_type_controller******************************** */

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function yaribarg2()
    {
        Gate::authorize('yaribarg.index');
        return $data['yaribargtype'] = YaribargType::all();
        // return view('components.Admin.Financial.Yaribarg.YaribargTypes.index',$data);
    }
}
