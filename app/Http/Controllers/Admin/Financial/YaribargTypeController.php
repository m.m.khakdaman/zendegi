<?php

namespace App\Http\Controllers\Admin\Financial;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\YaribargType;
use Illuminate\Support\Facades\Gate;

class YaribargTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('yaribarg.type.index');
        $data['yaribargtype'] = YaribargType::with('operator')->orderBy('id', 'DESC')->get();
        return view('components.Admin.Financial.Yaribarg.YaribargTypes.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('yaribarg.type.create');
        return view('components.Admin.Financial.Yaribarg.YaribargTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('yaribarg.type.create');
        YaribargType::create([
            'name'=>$request->name,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'number'=> AdminController::convert2english($request->number),
            'note'=>$request->note,
            'status'=>$request->status,
            // 'operator_id'=>$request->number,
            'ID_ADD'=>auth()->id(),
            'ID_EDIT'=>auth()->id()
        ]);
        return redirect(route('yaribargtype.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(YaribargType $yaribargtype)
    {
        Gate::authorize('yaribarg.type.edit');
        return view('components.Admin.Financial.Yaribarg.YaribargTypes.edit',compact('yaribargtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, YaribargType $yaribargtype)
    {
        Gate::authorize('yaribarg.type.edit');
        $yaribargtype->update([
            'name'=>$request->name,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'number'=>AdminController::convert2english($request->number),
            'note'=>$request->note,
            'status'=>$request->status,
        ]);
        return redirect(route('yaribargtype.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(YaribargType $yaribargtype)
    {
        Gate::authorize('yaribarg.type.delete');
        $yaribargtype->delete();
        return back();
    }
}
