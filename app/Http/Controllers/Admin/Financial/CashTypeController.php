<?php

namespace App\Http\Controllers\Admin\Financial;

use App\Http\Controllers\Controller;
use App\Models\TransactionType;
use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use PHPUnit\Framework\Constraint\Operator;

class CashTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['cashtype'] = TransactionType::with('operator')->get();
        return view('components.Admin.Financial.Cashtype.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['date'] = new Verta();
        return view('components.Admin.Financial.Cashtype.create', $data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        TransactionType::create([
            'name'=>$request->name,
            'type'=>$request->type,
            'date'=>$request->created_at,
            'status'=>$request->account_status,
            'note'=>$request->note,
            'operator_id'=>auth()->id()
            ]
        );
        return redirect()->route('cashtype.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['date'] = new Verta();
        $data['type'] = TransactionType::find($id);
        return view('components.Admin.Financial.Cashtype.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        TransactionType::find($id)->update([
            'name'=>$request->name,
            'type'=>$request->type,
            'date'=>$request->created_at,
            'status'=>$request->account_status,
            'note'=>$request->note,
            'operator_id'=>auth()->id()
            ]
        );
        return redirect()->route('cashtype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
