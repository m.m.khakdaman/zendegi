<?php

namespace App\Http\Controllers\Admin\Financial;

use App\Http\Controllers\Controller;
use App\Models\Cash;
use App\Models\TransactionType;
use App\Models\User;
use Illuminate\Http\Request;
use Hekmatinasser\Verta\Verta;
use Illuminate\Support\Facades\Gate;

class CashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (Gate::authorize('cash.index')->denied()) {
        //     return abort(403);
        // }
        $data['cash'] = Cash::withSum(
            ['transactions' => function ($query) {
                $query->where('created_at', '>=', '2022-02-20');
            }],
            'amount_standing'
        )->get();
        return view('components.Admin.Financial.Cash.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['date'] = new Verta();
        $data['cash_types'] = TransactionType::all();
        $data['admins'] = User::admins()->get();
        return view('components.Admin.Financial.Cash.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'account_name' => 'required|string',
            'account_number' => 'nullable',
            'cash_type' => 'required|integer',
            'note' => 'nullable|string',
            'account_status' => 'required',
            'admins' => 'required',
        ]);
        $cash = Cash::create([
            'name' => $request->account_name,
            'account_number' => $request->account_number,
            'cashe_type_id' => $request->cash_type,
            'account_balance' => $request->initial_amount,
            'initial_amount' => $request->initial_amount,
            'note' => $request->note,
            'account_status' => $request->account_status,
        ]);

        $cash->User()->sync($request->admins);

        return redirect(route('cash.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cash $cash)
    {
        $data['date'] = new Verta();
        $data['cash_types'] = TransactionType::all();
        $data['cash'] = $cash;
        $data['admins'] = User::admins()->get();
        return view('components.Admin.Financial.Cash.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cash $cash)
    {
        $request->validate([
            'account_name' => 'required|string',
            'account_number' => 'nullable',
            'cash_type' => 'required|integer',
            'note' => 'nullable|string',
            'account_status' => 'required',
            'admins' => 'required',
        ]);
        $cash->update([
            'name' => $request->account_name,
            'account_number' => $request->account_number,
            'cashe_type_id' => $request->cash_type,
            'account_balance' => $request->initial_amount,
            'initial_amount' => $request->initial_amount,
            'note' => $request->note,
            'account_status' => $request->account_status,
        ]);
        $cash->User()->sync($request->admins);

        return redirect(route('cash.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
