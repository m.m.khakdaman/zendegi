<?php

namespace App\Http\Controllers\Admin\Waitinglist;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\CustomerRequest;
use App\Models\Base;
use App\Models\Category;
use App\Models\Waiting_list;
use App\Models\Mood;
use App\Models\Marital_status;
use App\Models\Introduction_method;
use App\Models\History;
use App\Models\Role;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WaitinglistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['categories'] = Category::all();
        return view('components.Admin.Waitinglist.index', $data);
    }
    public function ajax_get(Request $request)
    {
        $result = [];

        $customer_where = '';
        $advisor_where = '';
        $category_where = [];
        foreach ($request->columns as $item => $value) {
            if ($value['search']['value'] != null) {
                if ($value['name'] == 'customer') {
                    $customer_where =$value['search']['value'];
                    continue;
                }
                if ($value['name'] == 'advisor') {
                    $advisor_where = $value['search']['value'];
                    continue;
                }
                if ($value['name'] == 'category') {
                    $category_where = [['name', 'like', '%' . $value['search']['value'] . '%']];
                    continue;
                }
                $result[$item][0] = $value['name'];
                $result[$item][1] = 'like';
                $result[$item][2] = '%' . $value['search']['value'] . '%';
            }
        }
        $waiting_list = Waiting_list::with(['customer', 'advisor', 'category'])->where($result);
        if ($category_where != null) {
            $waiting_list->whereHas('category', function ($query) use ($category_where) {
                return $query->where($category_where);
            });
        }
        if ($advisor_where != '') {
            $waiting_list->whereHas('advisor',  function ($query) use ($advisor_where) {
                return $query->where(DB::raw('CONCAT(name,family)'), 'like', '%' . $advisor_where . '%')
                    ->orWhere(function ($query) use ($advisor_where) {
                        $query->where(
                            DB::raw('CONCAT(name," ",family)'),
                            'like',
                            '%' . $advisor_where . '%'
                        )->orWhere(
                            'mobile',
                            'like',
                            '%' . $advisor_where . '%'
                        );
                    });
            });
        }
        if ($customer_where != '') {
            $waiting_list->whereHas('customer',  function ($query) use ($customer_where) {
                return $query->where(DB::raw('CONCAT(name,family)'), 'like', '%' . $customer_where . '%')
                    ->orWhere(function ($query) use ($customer_where) {
                        $query->where(
                            DB::raw('CONCAT(name," ",family)'),
                            'like',
                            '%' . $customer_where . '%'
                        )->orWhere(
                            'mobile',
                            'like',
                            '%' . $customer_where . '%'
                        );
                    });
            });
        }
        return $data = [
            "draw" => intval($request->draw),
            "recordsTotal" => $waiting_list->count(),
            "recordsFiltered" => $waiting_list->count(),
            "data" => $waiting_list->skip($request->start)->take($request->length)->get()
        ];
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    public function store_waiting_list_ajax(Request $request)
    {
        $request->validate([
            'customer_id' => 'required',
            'advisor_id' => 'required',
            'category_id' => 'required',
            'note' => 'string|nullable',
            'date' => 'required',
            'time' => 'required',
        ]);
        return Waiting_list::create([
            'customer_id' => $request->customer_id,
            'advisor_id' => $request->advisor_id,
            'category_id' => $request->category_id,
            'status' => 1,
            'note' => $request->note,
            'date' => $request->date,
            'time' => $request->time
        ]);
    }
    public function get_all_ajax(Request $request)
    {
        if (strlen($request->term) <= 2) {
            return [];
        }
        $a = ['results' => User::where('user_type','customer')
            ->where(function (Builder $q) use ($request) {
                return $q->where(DB::raw('CONCAT(name,family)'), 'like', '%' . $request->term . '%')
                    ->orWhere(function ($query) use ($request) {
                        $query->where(
                            DB::raw('CONCAT(name," ",family)'),
                            'like',
                            '%' . $request->term . '%'
                        )->orWhere(
                            'mobile',
                            'like',
                            '%' . $request->term . '%'
                        );
                    });
            })->get([
                'id',
                DB::raw('CONCAT(name," ",family,"  «",mobile,"»") as text')
            ]), 'pagination' => true];
        echo json_encode($a, JSON_UNESCAPED_UNICODE);
    }
    public function get_all_datatable_ajax(Request $request)
    {
        $result = [];
        foreach ($request->columns as $item => $value) {
            if ($value['search']['value'] != null) {
                $result[$item][0] = $value['name'];
                $result[$item][1] = 'like';
                $result[$item][2] = '%' . $value['search']['value'] . '%';
            }
        }
        $users = User::where('user_type','customer')->where($result);
        return [
            "draw" => intval($request->draw),
            "recordsTotal" => $users->count(),
            "recordsFiltered" => $users->count(),
            "data" => $users->skip($request->start)->take($request->length)->orderBy($request->columns[($request->order[0]['column'])]['name'], $request->order[0]['dir'])->get()
        ];
    }
    public function customer_paginate(Request $request)
    {
        return $request;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $_introduction_method = new Base('_introduction_method');
        $data['introduction_methods'] = $_introduction_method->get();

        $_education_level = new Base('_education_level');
        $data['education_levels'] = $_education_level->get();

        $_connection_method = new Base('_connection_method');
        $data['connection_methods'] = $_connection_method->get();

        $_categories = new Base('_categories');
        $data['categories'] = $_categories->get();

        $data['advisors'] = User::where('user_type','advisor')->get();
        $data['admins'] = User::where('user_type','customer')->get();



        return view('components.Admin.User.Customer.create', $data);
    }
    public function simple_create()
    {
        return view('components.Admin.User.Customer.simple_craete');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {

        $filename = 'avatar.jpg';
        if ($file = $request->file('file')) {

            $destination = base_path() . '/public/assets/images/profile/customer/avatar';
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $filename = rand(1111111, 99999999);
            $file = $request->file('file');
            $filename = $filename . $request->file->getClientOriginalName();
            $file->move($destination, $filename);
        }

        $user = User::create([
            'name' => $request->firstName,
            'family' => $request->lastName,
            'mobile' => $request->Fmobile,
            'gender' => $request->gender,
            'mobile2' => $request->Smobile,
            'phone' => $request->phone,
            'id_number' => $request->id_number,
            'national_code' => $request->national_code,
            'father_name' => $request->father_name,
            'spouse_name' => $request->spouse_name,
            'reference_source_id' => $request->Introduction_method,
            'note' => $request->note,
            'date_of_birth' => $request->birthday,
            'education_level_id' => $request->education_level,
            'field_of_study' => $request->field_of_study,
            'marital_status_id' => $request->marital_status,
            'date_of_marrige' => $request->date_of_marrige,
            'date_of_divorce' => $request->date_of_divorce,
            'number_of_children' => $request->number_of_children,
            'job' => $request->job,
            'Income_level' => $request->Income_level,
            'connection_method_id' => $request->connection_method,
            'best_time_to_call' => $request->best_time_to_call,
            'time_to_use_internet' => $request->time_to_use_internet,
            'categories_id' => $request->categories,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'educational_resources' => $request->educational_resources,
            'book_and_mag' => $request->book_and_mag,
            'brands' => $request->brands,
            'entertainment' => $request->entertainment,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'advertising_way' => $request->advertising_way,
            'everyday_events' => $request->everyday_events,
            'best_gift' => $request->best_gift,
            'color' => $request->color,

            'user_type'=>'customer',


            'pic' => $filename,
            'moods_id' => $request->mood,

        ]);


        //    return $customer_id = User::
        //    return $data['last_customer'] = User::latest();
        $history = History::create([
            'history_title' => $request->history_title,
            'sender_id' => auth()->user()->id,
            'resiver_id' => $request->resiver_id,
            'suggestions' => $request->suggestions,
            'customer_id' => $user->id,
            'importance' => 'آنی',
            'task_flag' => 'persona',
            'date_of_send' =>  verta()->format('Y/n/j H:i'),
            'date_of_answer' => verta()->format('Y/n/j')
        ]);

        return redirect(route('customer.index'));
    }

    public function simple_store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'family' => 'required|string',
            'mobile1' => 'required',
        ]);
        $user = User::create([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile1,
            'gender' => $request->gender,
            'user_type'=>'customer',
        ]);

        return redirect(route('customer.index'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $customer)
    {
        $data = [];
        $data['user'] = $customer;
        $data['sessions'] = $customer->sales()->with(['advisor', 'operator'])->latest()->get();

        // return $data['session'] = Sale::where(["session_type_id"=>"1","customer_id"=>$customer->id])->with('advisor')->latest()->get();

        $data['transaction'] = Transaction::where([
            [
                "user_id",
                "=",
                $customer->id,
            ]
        ])->with(['operator', 'sale'])->latest()->get();

        // $data['advisors'] = User::where('user_type','advisor')->get();
        // $data['customers'] = User::where('user_type','customer')->get();

        return view('components.Admin.User.Customer.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $customer)
    {
        $data = [];
        $data['user'] = $customer;
        $data['category'] = Category::all();
        $data['mood'] = Mood::all();
        $data['introduction_methods'] = Introduction_method::all();
        $data['marital_status'] = Marital_status::all();
        return view('components.Admin.User.Customer.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, User $customer)
    // {
    //     // return $request;
    //     $customer->updated([
    //         'name' => $request->name
    //     ]);

    // }
    public function update(Request $request, User $customer)
    {
        // return $request;
        if ($file = $request->file('file')) {
            $destination = base_path() . '/public/assets/images/profile/customer/avatar';
            if (!is_dir($destination)) {
                mkdir($destination, 0777, true);
            }
            $destination = $destination . '/';
            $filename = rand(1111111, 99999999);


            $filename = $filename . $request->file->getClientOriginalName();
            $file->move($destination, $filename);
            $customer->update([
                'pic' => $filename
            ]);
        };

        $customer->update([
            'name' => $request->name,
            'family' => $request->family,
            'mobile' => $request->mobile1,
            'gender' => $request->gender,
            'mobile2' => $request->mobile2,
            'phone' => $request->phone,
            'id_number' => $request->id_number,
            'national_code' => $request->national_code,
            'father_name' => $request->father_name,
            'spouse_name' => $request->spouse_name,
            'reference_source_id' => $request->Introduction_method,
            'note' => $request->note,
            'date_of_birth' => $request->date_of_birth,
            'education_level_id' => $request->education_level,
            'field_of_study' => $request->field_of_study,
            'marital_status_id' => $request->marital_status,
            'date_of_marrige' => $request->date_of_marrige,
            'date_of_divorce' => $request->date_of_divorce,
            'number_of_children' => $request->number_of_children,
            'job' => $request->job,
            'Income_level' => $request->Income_level,
            'connection_method_id' => $request->connection_method,
            'best_time_to_call' => $request->best_time_to_call,
            'time_to_use_internet' => $request->time_to_use_internet,
            'categories_id' => $request->categories,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'educational_resources' => $request->educational_resources,
            'book_and_mag' => $request->book_and_mag,
            'brands' => $request->brands,
            'entertainment' => $request->entertainment,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'advertising_way' => $request->advertising_way,
            'everyday_events' => $request->everyday_events,
            'best_gift' => $request->best_gift,
            'color' => $request->color,
            'status' => $request->status,


            'moods_id' => $request->moods_id,
            // 'password' => $request->password != null ? Hash::make($request->password) : $admin->password
        ]);

        return redirect(route('customer.index'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $customer)
    {
        $customer->delete();
        return back();
    }
}
