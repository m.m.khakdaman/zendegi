<?php

namespace App\Http\Controllers\Admin\Reminder;

use App\Http\Controllers\Controller;
use App\Models\Reminder;
use Illuminate\Http\Request;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['reminders'] = Reminder::where('resiver_id',auth()->user()->id)->latest()->get();
        return view('components.Admin.Reminder.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        Reminder::create([
            'title' => $request->title,
            'resiver_id' =>auth()->user()->id,
            'date' => $request->date,
            'time' => $request->time,
            'status' =>1,
            'note' => $request->note
        ]);

        $data = [];
        $data['reminders'] = Reminder::where('resiver_id',auth()->user()->id)->latest()->get();
        return view('components.Admin.Reminder.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reminder $id)
    {
        return $id;
        // Reminder::where('id', $id)->delete();
    }
}
