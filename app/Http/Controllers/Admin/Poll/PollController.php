<?php

namespace App\Http\Controllers\Admin\Poll;

use App\Http\Controllers\Controller;
use App\Models\EducationLevel;
use App\Models\Introduction_method;
// use App\Models\User;
use App\Models\Poll;
use App\Models\Role;
use Illuminate\Http\Request;

class PollController extends Controller
{
    public function poll1()
    {
        $data = [];
        $data['advisors'] = User::where('user_type','advisor')->get();
        $data['introduction_methods'] = Introduction_method::where('status', '1')->get();
        $data['educations'] = EducationLevel::all();
        return view('components.Admin/Poll/poll1', $data);
    }

    public function poll2()
    {
        $data = [];
        $data['poll'] = Poll::all();
        return view('components.Admin/Poll/poll2', $data);
    }
    public function poll_save(Request $request)
    {
        $myArr = array($request->poll2_11_1, $request->poll2_11_2, $request->poll2_11_3, $request->poll2_11_4, $request->poll2_11_5, $request->poll2_11_6, $request->poll2_11_7, $request->poll2_11_8, $request->poll2_11_9);

        $myJSON = json_encode($myArr, JSON_UNESCAPED_UNICODE);

        // $j_encoded = json_encode(utf8_encode($myArr)); 


        // return $myJSON;
       

        // return $request;
        Poll::create([
            'customer_id'=>auth()->user()->id,
            'advisor_id'=>$request->advisor,
            'anonymous'=>$request->anonymous,
            'gender'=>$request->gender,
            'age'=>$request->age,
            'education_level'=>$request->education,
            'job'=>$request->job,
            'mobile'=>$request->mobile,
            'session_num'=>$request->session_num,
            'introduction_method'=>$request->method,
            'q1'=>$request->q1,
            'q2'=>$request->q2,
            'q3'=>$request->q3,
            'q4'=>$request->q4,
            'q4_note'=>$request->q4_note,
            'q5'=>$request->q5,
            'q6'=>$request->q6,
            'comment'=>$request->comment,
            'poll2_1'=>$request->poll2_1,
            'poll2_1_note'=>$request->poll2_1_note,
            'poll2_2'=>$request->poll2_2,
            'poll2_2_note'=>$request->poll2_2_note,
            'poll2_3'=>$request->poll2_3,
            'poll2_3_note'=>$request->poll2_3_note,
            'poll2_4'=>$request->poll2_4,
            'poll2_4_note'=>$request->poll2_4_note,
            'poll2_5'=>$request->poll2_5,
            'poll2_5_note'=>$request->poll2_5_note,
            'poll2_6'=>$request->poll2_6,
            'poll2_6_note'=>$request->poll2_6_note,
            'poll2_7'=>$request->poll2_7,
            'poll2_7_note'=>$request->poll2_7_note,
            'poll2_8'=>$request->poll2_8,
            'poll2_8_note'=>$request->poll2_8_note,
            'poll2_9'=>$request->poll2_9,
            'poll2_9_note'=>$request->poll2_9_note,
            'poll2_10_name1'=>$request->poll2_name1,
            'poll2_10_mobile1'=>$request->poll2_mobile1,
            'poll2_10_name2'=>$request->poll2_name2,
            'poll2_10_mobile2'=>$request->poll2_mobile2,
            'poll2_10_name3'=>$request->poll2_name3,
            'poll2_10_mobile3'=>$request->poll2_mobile3,
            'poll2_11'=>$myJSON,
        ]);
        // return redirect()->route('poll2');
    }
}
