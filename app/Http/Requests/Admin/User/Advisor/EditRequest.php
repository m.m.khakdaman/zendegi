<?php

namespace App\Http\Requests\Admin\User\Advisor;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'family' => ['required','string'],
            'father' => ['nullable','string'],
            'date_of_birth' => ['nullable'],
            'national_code' => ['nullable'],
            'education_level_id' => ['nullable','integer','exists:_education_level,id'],
            'field_of_study' => ['nullable','string'],
            'gender' => ['nullable','integer'],
            'mobile' => ['required'],
            'mobile2' => ['nullable'],
            'phone' => ['nullable'],
            'note' => ['nullable','string']
        ];
    }
}
