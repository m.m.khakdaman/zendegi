<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|string|max:40',
            'lastName' => 'required|string|max:50',
            'Fmobile' => 'required|numeric',
            'Smobile' => 'nullable|numeric',
            'phone' => 'nullable|numeric',
            'birthday' =>'nullable|string',
            'field_of_study' => 'nullable|string',
            
            
        
        ];
    }
}
