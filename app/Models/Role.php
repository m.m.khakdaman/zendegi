<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'title',
        'label',
    ];

    public function Permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function Users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @param $Permissions
     * @return bool
     * if a role has permission * return True else return false
     */
    public function hasPermissions($Permissions)
    {
        if (is_string($Permissions)) {
            return $this->Permissions->contains('title', $Permissions);
        }

        return !!$Permissions->intersect($this->Permissions)->count();
    }
}
