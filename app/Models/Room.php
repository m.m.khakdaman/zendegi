<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;
    protected $fillable = ['name','branch_id','building_id'];

    public function branch()
    {
        return $this->belongsTo(Branche::class,'branch_id','id');
    }
    public function building()
    {
        return $this->belongsTo(Building::class,'building_id','id');
    }
}
