<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $filable = [
        'title',
        'resiver_id',
        'status',
        'date',
        'time',
        'note',
        'created_at',
        'updated_at'
    ];
}
