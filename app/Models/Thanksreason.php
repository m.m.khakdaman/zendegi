<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thanksreason extends Model
{
    use HasFactory;
    protected $table = "thanks_reasons";
    protected $fillable = [
        "title",
        "status",
        "flag",
        "comment",
        "operator_id"
    ];
}

