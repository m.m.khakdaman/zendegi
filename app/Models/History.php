<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;
    protected $primaryKey = 'history_id';

    // protected $casts = [
    //     'suggestions' => 'array',
    // ];

    protected $fillable = [
        'history_title',
        'sale_id',
        'sender_id',
        'customer_id',
        'resiver_id',
        'follow_up',
        'date_of_answer',
        'suggestions',
        'importance',
        'note',
        'status',
        'date_of_send',
        'task_flag',
        'updated_at',
        'created_at'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id','id');
    }
    public function resiver()
    {
        return $this->belongsTo(User::class,'resiver_id','id');
    }
    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id','id');
    }
}