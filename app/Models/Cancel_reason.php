<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cancel_reason extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'note',
        'operator_id',
        'delete_flag'
    ];
}
