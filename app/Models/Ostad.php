<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ostad extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'name',
        'father',
        'sh_sh',
        'birth_day',
        'id_card',
        'national_code',
        'reshte',
        'madrak',
        'univercity',
        'country',
        'address',
        'phone',
        'mobile',
        'email',
        'mablagh',
        'acount_number',
        'date',
        'number'
    ];

    private function salam()
    {
        return view('components.Admin/Dadkar/dadkar_salam');
    }
}
