<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workshop_attendance extends Model
{
    use HasFactory;
    protected $fillable = [
        'session_number',
        'product_id',
        'customer_id',
        'date',
        'status',
        'shenaseh'
    ];
}
