<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = '_transaction_type';
    protected $fillable = [
        'name',
        'type',
        'date',
        'status',
        'note',
        'operator_id'
    ];

    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id', 'id');
    }
}
