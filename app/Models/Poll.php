<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'advisor_id',
        'anonymous',
        'gender',
        'age',
        'education_level',
        'job',
        'mobile',
        'session_num',
        'introduction_method',
        'q1',
        'q2',
        'q3',
        'q4',
        'q4_note',
        'q5',
        'q6',
        'comment',
        'poll2_1',
        'poll2_1_note',
        'poll2_2',
        'poll2_2_note',
        'poll2_3',
        'poll2_3_note',
        'poll2_4',
        'poll2_4_note',
        'poll2_5',
        'poll2_5_note',
        'poll2_6',
        'poll2_6_note',
        'poll2-7',
        'poll2-7_note',
        'poll2_8',
        'poll2_8_note',
        'poll2_9',
        'poll2_9_note',
        'poll2_10_name1',
        'poll2_10_mobile1',
        'poll2_10_name2',
        'poll2_10_mobile2',
        'poll2_10_name3',
        'poll2_10_mobile3',
        'poll2_11',
    ];
}
