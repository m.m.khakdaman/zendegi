<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workshop_session extends Model
{
    use HasFactory;

    protected $fillable = [
        'session_number',
        'product_id',
        'date',
        'time'
    ];

    public function wsh_name()
    {
       return $this->belongsTo(Product::class,'product_id','id');
    }
}
