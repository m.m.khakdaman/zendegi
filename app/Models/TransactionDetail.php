<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;
    protected $table = 'transaction_detail';
    protected $fillable = [
        'source_transaction_id',
        'distinction_transaction_id',
        'amount',
    ];
    public function source()
    {
        return $this->belongsTo(Transaction::class,'source_transaction_id','id');
    }
    public function distinction()
    {
        return $this->belongsTo(Transaction::class,'distinction_transaction_id','id');
    }
}
