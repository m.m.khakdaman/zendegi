<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sentence extends Model
{
    use HasFactory;
    protected $fillable = ([
        'sentence',
        'from',
        'status',
        'operator_id'
    ]);
    public $timestamps = false;

    public function operator()
    {
        return $this-> belongsTo(User::class, 'operator_id', 'id');
    }
}
