<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zarinpal extends Model
{
    use HasFactory;
    protected $table = 'zarinpal';
    protected $fillable = [
        'model',
        'model_id',
        'price',
        'token',
        'user_id',
        'status',
        'date',
        'Authority',
        'comment',
    ];
}
