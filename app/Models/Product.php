<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_name',
        'product_type_id',
        'advisor_id',
        'type',
        'category_id',
        'holding',
        'start',
        'start_hour',
        'location',
        'end',
        'mablagh_moshaver',
        'session_count',
        'hour_sum',
        'price',
        'internet_price',
        'internet_access',
        'internet_note',
        'capacity',
        'sahm_moshaver_sabet',
        'sahm_moshaver_darsad',
        'status',
        'comment',
        'color',
        'sessions',
        'editor_id',
        'operator_id',
    ];
    public function advisor()
    {
        return $this->belongsTo(User::class,'advisor_id','id');
    }
    public function room()
    {
        return $this->belongsTo(Room::class,'room_id','id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
    public function ProductDay()
    {
        return $this->hasMany(ProductDay::class,'product_id','id');
    }
    public function operator()
    {
        return $this->belongsTo(User::class,'operator_id','id');
    }
}
