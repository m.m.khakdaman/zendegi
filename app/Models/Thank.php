<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class thank extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'audience_id',
        'note',
        'flag',
        'status',
        'reason_id',
        'operator_id',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function audience()
    {
        return $this->belongsTo(User::class, 'audience_id', 'id');
    }
    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id', 'id');
    }
}
