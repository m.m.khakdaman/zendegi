<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Introduction_method extends Model
{
    use HasFactory;
    protected $table = '_introduction_method';
    protected $fillable = [
        'Introduction_method',
        'status',
        'operator_id',
        'updated_at',
        'created_at',
    ];
    public function operator()
    {
        return $this->belongsTo(User::class,'operator_id','id');
    }
}
