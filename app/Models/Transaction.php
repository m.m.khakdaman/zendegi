<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'amount',
        'amount_standing',
        'cash_id',
        'operator_id',
        'sale_id',
        'transaction_type_id',
        'comment',
        'tracking_code',
        'status',
        'deposit_date',
        'yaribarg_date_of_use',
        'yaribarg_status',
        'yaribarg_type_id',
        'yaribarg_serial',
        'yaribarg_date',
        'yaribarg_recived_date',
        'yaribarg_reg_date'
    ];

    public function getCreatedAtAttribute($value)
    {
        return verta($value)->format('Y-m-d H:i');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function operator()
    {
        return $this->belongsTo(User::class,'operator_id','id');
    }
    public function cash()
    {
        return $this->belongsTo(Cash::class, 'cash_id','id');
    }
    public function yaribargtype()
    {
        return $this->belongsTo(YaribargType::class,'yaribarg_type_id','id');
    }
    public function yaribargstatus()
    {
        return $this->belongsTo(YaribargStatus::class,'yaribarg_status','id');
    }
    public function sale()
    {
        return $this->belongsTo(Sale::class,'sale_id','id');
    }
    public function transaction_type()
    {
        return $this->belongsTo(TransactionType::class,'transaction_type_id','id');
    }

    public function distinctions()
    {
        return $this->hasMany(TransactionDetail::class,'distinction_transaction_id','id');
    }
    public function sources()
    {
        return $this->hasMany(TransactionDetail::class,'source_transaction_id','id');
    }
    // public function add($param){
    //     return true;
    // }
}
