<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestType extends Model
{
    protected $fillable = ['title'];
    public $timestamps = false;
    
    protected $table = '_test_type';

    public function tests(){
        return $this->hasMany(Sale::class,'test_type_id','id')->where('products_type_id',setting('prudcot_type_test'));
    }
}
