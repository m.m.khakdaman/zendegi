<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "family",
        "mobile",
        "user_type",
        "gender",
        "mobile2",
        "phone",
        "id_number",
        "national_code",
        "father_name",
        "spouse_name",
        "reference_source_id",
        "note",
        "date_of_birth",
        "education_level_id",
        "field_of_study",
        "marital_status_id",
        "date_of_marrige",
        "date_of_divorce",
        "number_of_children",
        "job",
        "account_number",
        "Income_level",
        "connection_method_id",
        "best_time_to_call",
        "time_to_use_internet",
        "categories",
        "email",
        "instagram",
        "educational_resources",
        "book_and_mag",
        "brands",
        "entertainment",
        "address",
        "postal_code",
        "advertising_way",
        "everyday_events",
        "best_gift",
        "color",
        "pic",

        "moods_id",
        "spouse_date_of_birth",
        "password",
        "status",
        "register_date",
        "old_file_number",
        "employment_status_id",
        "account_number",
        "presenter",
        "operator_id",
        "editor_id",
        "SpecialUser"

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    // protected $casts = [
    //     'moarfi'=>'json'
    // ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * get all Role for any user
     */
    public function Roles()
    {
        return $this->belongsToMany(Role::class);
    }
    public function scopeAdmins()
    {
        return $this->where('user_type', 'admin');
    }

    public function fullname(){
        return $this->name . ' ' . $this->family;
    }
    public function education_level()
    {
        return $this->belongsTo(EducationLevel::class, 'education_level_id', 'id');
    }
    public function operator()
    {
        return $this->belongsTo(EducationLevel::class, 'operator_id', 'id');
    }
    public function PriceList()
    {
        return $this->belongsToMany(PriceList::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id', 'id');
    }

    public function Category()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @param $role
     * @return bool
     * if a user has role * return True else return false
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->Roles->contains('title', $role);
        }

        return !!$role->intersect($this->roles)->count();
    }
    public function products()
    {
        return $this->hasMany(Product::class, 'advisor_id', 'id');
    }
    public function sales()
    {
        return $this->hasMany(Sale::class, 'customer_id', 'id');
    }
    public function Cashes()
    {
        return $this->belongsToMany(Cash::class);
    }
    public function hasCash($cash){
        if(is_string($cash))
            return $this->Cashes->contains('name',$cash);
        return !!$cash->intersect($this->Cashes)->count();
    }
}
