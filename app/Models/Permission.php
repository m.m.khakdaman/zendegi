<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'title',
        'label',
    ];
    public function Role()
    {
        return $this->belongsToMany(Role::class);
    }
}
