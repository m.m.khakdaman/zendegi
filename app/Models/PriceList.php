<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
    use HasFactory;
    protected $fillable = [
        "name",
        "advisor_rate",
        "center_rate",
        "total_price",
        "session_length",
        "active",
        "ID_ADD",
        "ID_EDIT",
    ];
    public function User()
    {
        return $this->belongsToMany(User::class);
    }

}
