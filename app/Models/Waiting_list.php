<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Waiting_list extends Model
{
    use HasFactory;
    protected $table = 'waiting_list';
    protected $fillable = [
        'customer_id',
        'advisor_id',
        'category_id',
        'status',
        'note',
        'date',
        'time',
    ];

    public function advisor()
    {
        return $this->belongsTo(User::class,'advisor_id','id');
    }
    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id','id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
}
