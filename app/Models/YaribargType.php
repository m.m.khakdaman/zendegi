<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YaribargType extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'number',
        'note',
        'status',
        'operator_id'
    ];

    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id', 'id');
    }
}
