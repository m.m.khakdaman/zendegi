<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'account_number',
        'account_balance',
        'cashe_type_id',
        'note',
        'ID_ADD',
        'ID_EDIT',
        'account_status',
        'initial_amount'
    ];
    public function cashe_type()
    {
        return $this->belongsTo(TransactionType::class, 'cashe_type_id', 'id');
    }
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'cash_id', 'id');
    }
    public function User()
    {
        return $this->belongsToMany(User::class);
    }
}
