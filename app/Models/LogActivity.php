<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'subject',
        'url',
        'method',
        'ip',
        'agent',
        'user_id',
        'customer_id',
        'advisor_id',
        'comment'
    ];

    public function user()
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(user::class, 'customer_id', 'id');
    }

    public function advisor()
    {
        return $this->belongsTo(user::class, 'advisor_id', 'id');
    }
}
