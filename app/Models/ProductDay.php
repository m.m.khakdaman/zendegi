<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDay extends Model
{
    use HasFactory;
    protected $table = 'day_of_product';
    public $timestamps = false;
    protected $fillable = [
        'day_of_week',
        'start',
        'room_id',
        'color',
        'end',
        'product_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
