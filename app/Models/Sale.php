<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $fillable = [
        'advisor_id',
        'customer_id',
        'operator_id',
        'date',
        'date2',
        'time',
        'session_time',
        'session_start',
        'session_length',
        'session_type_id',
        'session_status_id',
        'category_id',
        'room_id',
        'reservation_type_id',
        'orginal_price',
        'qty',
        'discount',
        'discount_type',
        'total_price',
        'product_id',
        'products_type_id',
        'modekhele',
        'advisor_report',
        'diagnosis1_id',
        'diagnosis2_id',
        'diagnosis3_id',
        'price_list_id',
        'costomer_commant',
        'costomer_score',
        'comment',
        'delete_comment',
        'cancel_operator_id',
        'delete_type',
        'force_major_delete',
        'cancel_reason_id',
        'status'
    ];

    

    public function products_type()
    {
        return $this->belongsTo(ProductType::class,'products_type_id','id');
    }
    public function debts()
    {
        return $this->belongsTo(Transaction::class,'id','sale_id')->where([
            ['amount_standing' ,'<','0'],
        ]);
    }
    public function demands()
    {
        return $this->belongsTo(Transaction::class,'id','sale_id')->where([
            ['amount_standing' ,'>','0'],
        ]);
    }
    public function transactions()
    {
        return $this->belongsTo(Transaction::class,'id','sale_id')->orderBy('id','desc');
    }

    public function advisor()
    {
        return $this->belongsTo(User::class,'advisor_id','id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
    public function room()
    {
        return $this->belongsTo(Room::class,'room_id','id');
    }
    public function rooms()
    {
        return $this->belongsTo(Room::class,'room_id','id');
    }

    public function price_list()
    {
        return $this->belongsTo(PriceList::class,'price_list_id','id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
    public function operator()
    {
        return $this->belongsTo(User::class,'operator_id','id');
    }
    public function cancel_operator()
    {
        return $this->belongsTo(User::class,'cancel_operator_id','id');
    }
    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id','id');
    }
    public function cancel_reason()
    {
        return $this->belongsTo(Cancel_reason::class, 'cancel_reason_id');
    }
    
    public function messages()
    {
        return $this->hasMany(Chat::class,'sale_id','id');
    }
    public function add($param){
        $sale = Sale::create($param);

        Transaction::create([
            'user_id' => $sale->customer_id,
            'amount' => -intval($sale->total_price),
            'amount_standing' => -intval($sale->total_price),
            'cash_id' => '1',
            'operator_id' => auth()->id(),
            'sale_id' => $sale->id,
        ]);
        return $sale;
        
    }

    public function day()
    {
        return $this->belongsTo(Day::class,'date','miladi_date');
    }
    public function test_type()
    {
        return $this->belongsTo(TestType::class,'test_type_id');
    }
    public function why_cancel()
    {
        return $this->belongsTo(Setting::class,'delete_type', 'key');
    }
}
