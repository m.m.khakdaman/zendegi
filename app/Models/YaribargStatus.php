<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YaribargStatus extends Model
{
    use HasFactory;
    protected $table = '_yaribarg_status';
    protected $fillable = [
        'key',
        'value',
    ];
}
