<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'title',
        'id',
        'kala_type_id',
        'label_code',
        'buy_price',
        'factor_number',
        'date_of_buy',
        'ststus',
        'type',
        'initial_number',
        'fi',
        'remaining_number',
        'seller',
        'unit',
        'note',
        'keeper_id'
    ];

    public function keeper()
    {
        return $this->belongsTo(User::class,'keeper_id','id');
    }
    public function kala_type()
    {
        return $this->belongsTo(Setting::class,'kala_type_id' ,'id');
    }
}
