<?php

namespace App\View\Components;

use App\Models\Role;
use Illuminate\View\Component;

class base extends Component
{
    public $title;
    public $script;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($script = null)
    {
        $this->script = $script;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.base');
    }
}
