<?php

use App\Models\Sentence;
use App\Models\Setting;
use App\Models\Word;

function setting($key, $p2 = null)
{
    $setting = Setting::where('key', $key)->first();
    if ($setting == null) {
        $setting = Setting::create(['key' => $key, 'value' => '']);
    }
    $str = $setting->value;
    if (gettype($p2) == "array") {
        foreach ($p2 as $key => $value) {
            $a = $key + 1;
            $str = str_replace("%$a$", $value, $str);
        }
    }
    return $str;
}

function SendSMS($Message, $Receptors, $SenderNumber = '', $type = 'normal')
{
    $username = setting("user_s_ms");
    $password = setting("pass_s_ms");
    $from = setting("SenderNumber_s_ms");
    $url = "http://tsms.ir/url/tsmshttp.php?from=$from&to=$Receptors&username=$username&password=$password&message=" . urlencode($Message);
    $handle = fopen($url, "r");
    return $handle; 
}

function flash($message, $level = 'info')
{
    session()->flash('flash_message', $message);
    session()->flash('flash_message_level', $level);
}

function sentence()
{    
    $sentence = Sentence::inRandomOrder()->first();
    return $sentence->sentence . ' ....' . $sentence->from;
}

function t($p1 = "", $p2 = null)
{
    $w = Word::get();
    $words = new \stdClass;
    foreach ($w as $r) {
        $r->words[$r['name']] = $r['value'];
    }
    if (isset($words->words[$p1])) {
        $str = $words->words[$p1];
    } else {
        Word::create([
            'name' => $p1,
            'value' => $p1,
        ]);
        $str = $p1;
    }

    if (gettype($p2) == "string") {
        $str = str_replace("$$1", $p2, $str);
    }

    return $str;
}
