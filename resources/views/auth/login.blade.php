<!DOCTYPE html>
<html class="loading" lang="fa" data-textdirection="rtl" dir="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>صفحه ورود - {{ env('APP_NAME') }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/images/ico/favicon.ico">
    <meta name="theme-color" content="#5A8DEE">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/pages/authentication.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
    <!-- END: Page CSS-->


</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body
    class="horizontal-layout horizontal-menu navbar-static 1-column   footer-static bg-full-screen-image  blank-page blank-page"
    data-open="hover" data-menu="horizontal-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- login page start -->
                <section id="auth-login" class="row flexbox-container">
                    <div class="col-xl-8 col-11">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <!-- left section-login -->
                                <div class="col-md-6 col-12 px-0">
                                    <div
                                        class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                        <div class="card-header pb-1">
                                            <div class="card-title">
                                                <h4 class="text-center mb-2">@t(سامانه مدیریت مرکز مشاوره)
                                                    {{ env('APP_NAME') }}</h4>
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <div class="card-body">

                                                <div class="divider">
                                                    <div class="divider-text text-uppercase text-muted"><small>@t(با
                                                            شماره موبایل و رمز عبور وارد شوید.)</small>
                                                    </div>
                                                </div>
                                                <form action="{{ route('login') }}" method="POST">
                                                    @csrf
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-700" for="mobile">موبایل</label>
                                                        <div class="position-relative has-icon-left">
                                                            <div class="controls">
                                                                <input type="tel" id="mobile" class="form-control"
                                                                    pattern="0[0-9]{10}"
                                                                    name="mobile" placeholder="موبایل" required
                                                                    data-validation-pattern-message="شماره فرمت معتبری نیست."
                                                                    aria-invalid="{{ $errors->any() ? 'true' : 'false' }}">
                                                            </div>
                                                            <div class="form-control-position">
                                                                <i class="bx bx-mobile"></i>
                                                            </div>
                                                            @if ($errors->any())
                                                                <div class="help-block">
                                                                    <ul role="alert">
                                                                        @foreach ($errors->all() as $error)
                                                                            <li>{{ $error }}</li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-bold-700" for="password">رمز عبور</label>
                                                        <div class="position-relative has-icon-left">
                                                            <input type="password" id="password" class="form-control"
                                                                name="password" placeholder="رمز عبور" required
                                                                data-validation-required-message="وارد کردن رمز عبور اجباری است">
                                                            <div class="form-control-position">
                                                                <i class="bx bx-lock"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                                                        <div class="text-left">
                                                            <div class="checkbox checkbox-sm">
                                                                <input type="checkbox" name="remember" id="remember"
                                                                    {{ old('remember') ? 'checked' : '' }}
                                                                    class="form-check-input">
                                                                <label class="checkboxsmall" for="remember"><small>مرا
                                                                        به خاطر بسپار</small></label>
                                                            </div>
                                                        </div>
                                                        <div class="text-right line-height-2"><a
                                                                href="{{ route('reset_password') }}"
                                                                class="card-link"><small>رمز عبورتان را فراموش کرده
                                                                    اید؟</small></a></div>
                                                    </div>
                                                    <button type="submit"
                                                        class="btn btn-primary glow w-100 position-relative">ورود<i
                                                            id="icon-arrow" class="bx bx-left-arrow-alt"></i></button>
                                                </form>
                                                <hr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- right section image -->
                                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                    <div class="card-content">
                                        <img class="img-fluid" src="/assets/images/pages/login.png" alt="branding logo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- login page ends -->
            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
    <!-- END: Page JS-->
    <script>
        $(function() {
            $('input[type=tel]').each(function(index, obj) {
                $(obj).keypress(function(e) {
                    b = $(this).val()
                        .replace(/\u0660/g, "0")
                        .replace(/\u0661/g, "1")
                        .replace(/\u0662/g, "2")
                        .replace(/\u0663/g, "3")
                        .replace(/\u0664/g, "4")
                        .replace(/\u0665/g, "5")
                        .replace(/\u0666/g, "6")
                        .replace(/\u0667/g, "7")
                        .replace(/\u0668/g, "8")
                        .replace(/\u0669/g, "9")
                        .replace(/\u06F0/g, "0")
                        .replace(/\u06F1/g, "1")
                        .replace(/\u06F2/g, "2")
                        .replace(/\u06F3/g, "3")
                        .replace(/\u06F4/g, "4")
                        .replace(/\u06F5/g, "5")
                        .replace(/\u06F6/g, "6")
                        .replace(/\u06F7/g, "7")
                        .replace(/\u06F8/g, "8")
                        .replace(/\u06F9/g, "9")
                        .replace(/\D/g, "");
                    $(this).val(b);
                })
            })
        });

    </script>
</body>
<!-- END: Body-->

</html>
