<div>
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                {{ $content ?? '' }}
            </div>
        </div>
    </div>
</div>
