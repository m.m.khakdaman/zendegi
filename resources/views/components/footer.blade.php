<div>
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0">
            <span class="float-left d-inline-block">
                @t(کلیه حقوق مادی و معنوی این وب‌سایت متعلق به )
            </span>
            <span>
                <a href="" class="text-secondary" target="_blank">@t( مرکز مشاوره زندگی عاقلانه)</a>
            </span>
            <span>
                @t( است.)
            </span>
            <span  class="float-right d-sm-inline-block d-none">
                <a href="#">
                    @t(نت افزار هیرو)
                </a>
            </span>
            <span  class="float-right d-sm-inline-block d-none">
                @t(طراحی و پیاده سازی:)
            </span>
            
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i
                    class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->
</div>
