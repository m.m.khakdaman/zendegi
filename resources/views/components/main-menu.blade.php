<div>
    <!-- BEGIN: Main Menu-->
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow"
        role="navigation" data-menu="menu-wrapper">
        <div class="navbar-header d-xl-none d-block">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="index.html">
                        <div class="brand-logo">
                            <img class="logo" src="/assets/images/logo/logo.png">
                        </div>
                        <h2 class="brand-text mb-0">{{ env('APP_NAME') }}</h2>
                    </a>
                </li>
                <li class="nav-item nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon">
                        </i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom">

        </div>
        <!-- Horizontal menu content-->
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <!-- include /includes/mixins-->
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="filled">

                <li data-menu="">
                    <a class="nav-link" href="{{ route('admin.home') }}">
                        <i class="menu-livicon" data-icon="dashboard">
                        </i>
                        <span data-i18n="Dashboard">داشبورد</span>
                    </a>
                </li>
                <li data-menu="">
                    <a class="nav-link" href="{{ route('session.index') }}">
                        <i class="menu-livicon" data-icon="thumbnails-small">
                        </i>
                        <span data-i18n="Thumbnails-small">@t(جلسات)</span>
                    </a>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                        <i class="menu-livicon" data-icon="user">
                        </i>
                        <span>@t(مراجعین)</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('customer.create') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(مراجع جدید)
                            </a>
                        </li>
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('customer.index') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(لیست مراجعین)
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                        <i class="menu-livicon" data-icon="briefcase">
                        </i>
                        <span>@t(مشاورین)</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('advisor.create') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(مشاور جدید)
                            </a>
                        </li>
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('advisor.index') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(لیست مشاورین)
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                        <i class="menu-livicon" data-icon="users">
                        </i>
                        <span>@t(کارکنان)</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('admin.create') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(کاربر جدید)
                            </a>
                        </li>
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('admin.index') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(لیست کارکنان)
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                        <i class="menu-livicon" data-icon="notebook">
                        </i>
                        <span>@t(کارگاه ها)</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href=" {{ route('workshop.create') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt"></i>
                                @t(کارگاه جدید)
                            </a>
                        </li>
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('workshop.index') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt"></i>
                                @t(لیست کارگاه ها)
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                        <i class="menu-livicon" data-icon="coins"></i>
                        <span>@t(مالی)</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        @can('transactions.index')
                            <li data-menu="">
                                <a class="dropdown-item align-items-center" href="{{ route('transaction.all') }}"
                                    data-toggle="dropdown">
                                    <i class="bx bx-left-arrow-alt"></i>
                                    @t(تراکنش ها)
                                </a>
                            </li>
                        @endcan
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            @canany('payrole.admin','payrole.advisor')
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt"></i>@t(واریز حقوق)
                            </a>
                            @endcanany
                            <ul class="dropdown-menu dropdown-menu-right">
                                @can('payrole.admin')
                                <li class="dropdown dropdown-submenu" data-menu="dropdown">
                                    <a class="dropdown-item align-items-center"
                                        href="{{ route('payroll_admin_select') }}" data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>@t(صدور فیش حقوقی کارکنان)
                                    </a>
                                </li> 
                                @endcan
                                @can('payrole.advisor')
                                <li class="dropdown dropdown-submenu" data-menu="dropdown">
                                    <a class="dropdown-item align-items-center"
                                        href="{{ route('payroll_advisor_select') }}" data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>@t(صدور فیش حقوقی مشاوران)
                                    </a>
                                </li>  
                                @endcan
                            </ul>
                        </li>
                        @can('transfer_to_account')
                        <li data-menu="">
                            <a class="dropdown-item align-items-center"
                                href="{{ route('transaction.transfer_to_account') }}" data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(جا به جایی بین حساب ها)
                            </a>
                        </li>
                        @endcan
                        @can('transactions.admin.function')
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('transaction.print') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(عمکرد مالی هر فرد)
                            </a>
                        </li>    
                        @endcan
                        @can('transactions.advisor.function')
                        <li data-menu="">
                            <a class="dropdown-item align-items-center"
                                href="{{ route('transaction.print', ['print' => 'advisors']) }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(عمکرد مالی هر مشاور)
                            </a>
                        </li> 
                        @endcan                        
                        @can('price_list.index')
                            <li data-menu="">
                                <a class="dropdown-item align-items-center" href="{{ route('price_list.index') }}"
                                    data-toggle="dropdown">
                                    <i class="bx bx-left-arrow-alt">
                                    </i>
                                    @t(نرخ نامه)
                                </a>
                            </li>
                        @endcan
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            @canany(['yaribarg.index', 'yaribarg.type.index', 'create'])
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(یاری برگ)
                            </a>   
                            @endcanany
                            <ul class="dropdown-menu dropdown-menu-right">
                                @can('yaribarg.index')
                                <li class="dropdown dropdown-submenu" data-menu="dropdown">
                                    <a class="dropdown-item align-items-center" href="{{ route('yaribarg.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(لیست یاری برگ ها)
                                    </a>
                                </li> 
                                @endcan
                                @can('create')
                                <li class="dropdown dropdown-submenu" data-menu="dropdown">
                                    <a class="dropdown-item align-items-center"
                                        href="{{ route('yaribargtype.create') }}" data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>
                                        @t(نوع جدید یاری برگ)
                                    </a>
                                </li> 
                                @endcan
                                @can('yaribarg.type.index')
                                <li class="dropdown dropdown-submenu" data-menu="dropdown">
                                    <a class="dropdown-item align-items-center"
                                        href="{{ route('yaribargtype.index') }}" data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(انواع یاری برگ)
                                    </a>
                                </li>   
                                @endcan
                                
                            </ul>
                        </li>
                        @can('cash.index')
                            <li data-menu="">
                                <a class="dropdown-item align-items-center" href="{{ route('cash.index') }}"
                                    data-toggle="dropdown">
                                    <i class="bx bx-left-arrow-alt">
                                    </i>
                                    @t(حساب ها و صندوق)
                                </a>
                            </li>
                        @endcan
                        @can('transactions.debtor')
                        <li data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('debtor.index') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt"></i>
                                @t(لیست بدهکاران)
                            </a>
                        </li>  
                        @endcan
                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                        <i class="menu-livicon" data-icon="gear">
                        </i>
                        <span>@t(تنظیمات)</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(تنظیمات عمومی)
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                @can('education_level.index')
                                <li class="dropdown dropdown-submenu" data-menu="dropdown">
                                    <a class="dropdown-item align-items-center"
                                        href="{{ route('education_level.index') }}" data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(مدرک تحصیلی)
                                    </a>
                                </li>    
                                @endcan
                                
                                <li class="" data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('role.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>
                                        @t(نقش ها)
                                    </a>
                                </li>
                                <li class="" data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('room.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>
                                        @t(اتاق ها)
                                    </a>
                                </li>
                                <li class="" data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('branche.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>
                                        @t(شعبه ها)
                                    </a>
                                </li>
                                <li class="" data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('building.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>
                                        @t(ساختمان ها)
                                    </a>
                                </li>
                                <li class="" data-menu="">
                                    <a class="dropdown-item align-items-center"
                                        href="{{ route('Thanks_reason_list') }}" data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>
                                        @t(دسته بندی شکایات و رضایتمندی)
                                    </a>
                                </li>
                                <li class="" data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('Cancel_reasons_list') }}"
                                        data-toggle="dropdown"><i class="bx bx-left-arrow-alt"></i>@t(دلایل کنسلی)
                                    </a>
                                </li>
                                <li class="" data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('sentence.index') }}"
                                        data-toggle="dropdown"><i class="bx bx-left-arrow-alt"></i>@t(سخن بزرگان)
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(تنظیمات مالی)
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown dropdown-submenu" data-menu="dropdown">
                                    <a class="dropdown-item align-items-center" href="{{ route('cashtype.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(نوع حساب)
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(تنظیمات مراجعین)
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li data-menu="">
                                    <a class="dropdown-item align-items-center"
                                        href="{{ route('introduction.index') }}" data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt"></i>@t(تعریف نحوه آشنایی)
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(تنظیمات کارکنان)
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('semat.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(سمت ها)
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(تنظیمات مشاورین)
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('approach.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(رویکردها)
                                    </a>
                                </li>
                                <li data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('category.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(حیطه ها)
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(پیامک ها)
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('sms.index') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(پیامک های پیش فرض)
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                            <a class="dropdown-item align-items-center dropdown-toggle" href="#"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(نظرسنجی)
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('poll1') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(فرم نظرسنجی شماره 1)
                                    </a>
                                </li>
                                <li data-menu="">
                                    <a class="dropdown-item align-items-center" href="{{ route('poll2') }}"
                                        data-toggle="dropdown">
                                        <i class="bx bx-left-arrow-alt">
                                        </i>
                                        @t(فرم نظرسنجی شماره 2)
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                        <i class="menu-livicon" data-icon="morph-folder">
                        </i>
                        <span>@t(انبار)</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="" data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('storage.index') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt"></i>
                                @t(اموال اداری)
                            </a>
                        </li>
                        <li class="" data-menu="">
                            <a class="dropdown-item align-items-center" href="{{ route('storage.kalalist') }}"
                                data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt"></i>@t(اموال مصرفی)
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown nav-item" data-menu="dropdown">
                    @canany(['report.canceled.sales', 'reoprt.daily', 'report.advisor.activities', 'report.admin.activities'])
                        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                            <i class="menu-livicon" data-icon="morph-folder">
                            </i>
                            <span>@t(گزارش ها)</span>
                        </a>    
                    @endcanany
                    <ul class="dropdown-menu dropdown-menu-right">
                        @can('report.admin.activities')
                            <li class="" data-menu="">
                                <a class="dropdown-item align-items-center" href="{{ route('admin.report') }}"
                                    data-toggle="dropdown">
                                    <i class="bx bx-left-arrow-alt"></i><span>@t(عملکرد کارکنان)</span>
                                </a>
                            </li>    
                        @endcan
                        @can('report.advisor.activities')
                            <li class="" data-menu="">
                                <a class="dropdown-item align-items-center" href="{{ route('advisor.report') }}"
                                    data-toggle="dropdown">
                                    <i class="bx bx-left-arrow-alt"></i>@t(عملکرد مشاوران)</a>
                            </li>
                        @endcan                        
                        @can('reoprt.daily')
                            <li class="" data-menu="">
                                <a class="dropdown-item align-items-center" href="{{ route('daily.report') }}"
                                    data-toggle="dropdown">
                                    <i class="bx bx-left-arrow-alt"></i>@t(گزارش روزانه)</a>
                            </li>  
                        @endcan
                        @can('report.canceled.sales')
                            <li class="" data-menu="">
                                <a class="dropdown-item align-items-center"
                                    href="{{ route('canceled_session.report') }}" data-toggle="dropdown">
                                    <i class="bx bx-left-arrow-alt"></i>@t(گزارش جلسات کنسلی)</a>
                            </li> 
                        @endcan   
                        <li class="" data-menu="">
                            <a class="dropdown-item align-items-center"
                                href="{{ route('select_room') }}" data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt"></i>@t(گزارش اتاق ها)</a>
                        </li>   
                        
                        <li class="dropdown dropdown-submenu" data-menu="dropdown">
                            <a class="dropdown-item align-items-center"
                                href="{{ route('force_major_canceled_sessions_list') }}" data-toggle="dropdown">
                                <i class="bx bx-left-arrow-alt">
                                </i>
                                @t(رسیدگی به کنسلی های فورس ماژور)
                            </a>
                        </li>                     
                    </ul>
                </li>

            </ul>
        </div>
        <!-- /horizontal menu content-->
    </div>
    <!-- END: Main Menu-->
</div>
