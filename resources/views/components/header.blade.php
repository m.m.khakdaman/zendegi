<div>
    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed bg-primary navbar-brand-center">
        <div class="navbar-header d-xl-block d-none">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item">
                    <a class="navbar-brand" href="/html/horizontal-menu-template/index.html">
                        <div class="brand-logo">
                            <img class="logo" src="/assets/images/logo/logo-light.png">
                        </div>
                        <h2 class="brand-text mb-0">مرکز مشاوره {{ env('APP_NAME') }}</h2>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu mr-auto">
                                <a class="nav-link nav-menu-main menu-toggle" href="#">
                                    <i class="bx bx-menu">
                                    </i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            <li class="nav-item d-none d-lg-block">
                                <a class="nav-link" href="app-email.html" data-toggle="tooltip" data-placement="bottom"
                                    title="ایمیل">
                                    <i class="ficon bx bx-envelope">
                                    </i>
                                </a>
                            </li>
                            <li class="nav-item d-none d-lg-block">
                                <a class="nav-link" href="app-chat.html" data-toggle="tooltip" data-placement="bottom"
                                    title="گفتگو">
                                    <i class="ficon bx bx-chat">
                                    </i>
                                </a>
                            </li>
                            <li class="nav-item d-none d-lg-block">
                                <a class="nav-link nav-link-label position-relative" href="{{ route('task.index') }}" data-toggle="tooltip"
                                data-placement="bottom" title="وظایف">
                                    <i class="ficon bx bx-check-circle bx-tada bx-flip-horizontal">
                                    </i>
                                    <span class="badge badge-pill badge-danger badge-up">
                                        {{ \App\Models\History::where(['resiver_id'=>auth()->id() , 'status'=>1])->count() }}
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item d-none d-lg-block">
                                <a class="nav-link" href="{{ route('thank.index') }}" data-toggle="tooltip" data-placement="bottom" title="@t(شکایات و رضایتمندی ها)">
                                    <i class="ficon bx bx-phone-call"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="nav-item d-none d-lg-block">
                                <a class="nav-link bookmark-star">
                                    <i class="ficon bx bx-star warning">
                                    </i>
                                </a>
                                <div class="bookmark-input search-input">
                                    <div class="bookmark-input-icon">
                                        <i class="bx bx-search primary">
                                        </i>
                                    </div>
                                    <input class="form-control input" type="text" placeholder="جستجو ..." tabindex="0"
                                        data-search="template-search">
                                    <ul class="search-list">

                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right d-flex align-items-center">
                        {{-- <li class="dropdown dropdown-language nav-item">
                            <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon flag-icon-ir">
                                </i>
                                <span class="selected-language d-lg-inline d-none">فارسی</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag">
                                <a class="dropdown-item" href="#" data-language="fa">
                                    <i class="flag-icon flag-icon-ir mr-50">
                                    </i>فارسی</a>
                                <a class="dropdown-item" href="#" data-language="en">
                                    <i class="flag-icon flag-icon-us mr-50">
                                    </i>انگلیسی</a>
                                <a class="dropdown-item" href="#" data-language="fr">
                                    <i class="flag-icon flag-icon-fr mr-50">
                                    </i>فرانسوی</a>
                                <a class="dropdown-item" href="#" data-language="de">
                                    <i class="flag-icon flag-icon-de mr-50">
                                    </i>آلمانی</a>
                                <a class="dropdown-item" href="#" data-language="pt">
                                    <i class="flag-icon flag-icon-pt mr-50">
                                    </i>پرتغالی</a>
                            </div>
                        </li> --}}
                        <li class="nav-item d-none d-lg-block">
                            <a class="nav-link nav-link-expand">
                                <i class="ficon bx bx-fullscreen">
                                </i>
                            </a>
                        </li>
                        <li class="nav-item nav-search">
                            <a class="nav-link nav-link-search pt-2">
                                <i class="ficon bx bx-search">
                                </i>
                            </a>
                            <div class="search-input">
                                <div class="search-input-icon">
                                    <i class="bx bx-search primary">
                                    </i>
                                </div>
                                <input class="input" type="text" placeholder="جستجو ..." tabindex="-1"
                                    data-search="template-search">
                                <div class="search-input-close">
                                    <i class="bx bx-x">
                                    </i>
                                </div>
                                <ul class="search-list">

                                </ul>
                            </div>
                        </li>
                        <li class="dropdown dropdown-notification nav-item">
                            <a class="nav-link nav-link-label" href="#" data-toggle="dropdown" title="@t(کنسلی های امروز)">
                                <i class="ficon bx bx-calendar-x bx-tada bx-flip-horizontal"></i>
                                <span class="badge badge-pill badge-danger badge-up">                                    
                                    {{ \App\Models\Sale::where('delete_type' , '!=' , '0')->where('date', date('Y-m-d') )->count() }}
                                </span>
                            </a>
                            @php
                             $deleted_customers = \App\Models\Sale::where('delete_type' , '!=' , '0')->where('date', date('Y-m-d') )->get();
                            @endphp
                            <ul class="dropdown-menu dropdown-menu-media">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header px-1 py-75 d-flex justify-content-between">
                                        <span class="notification-title">@t(کنسلی های امروز)</span>
                                        {{-- <span class="text-bold-400 cursor-pointer" data-toggle="modal"  data-target="#reminder">@t(درج یادآور)</span> --}}
                                    </div>
                                </li>
                                <li class="scrollable-container media-list">
                                   @foreach ($deleted_customers as $delcu)
                                    <div class="d-flex justify-content-between cursor-pointer">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar bg-primary bg-lighten-5 mr-1 m-0 p-25">
                                                    <span class="avatar-content text-primary font-medium-2">ل‌د</span>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">
                                                    <span class="text-bold-500">{{ $delcu->customer->name.' '.$delcu->customer->family.' ' }}</span>
                                                    <small class="text-primary">@t(جلسه مشاوره با)</small>
                                                    <span class="text-bold-500">{{ ' '.$delcu->advisor->name.' '.$delcu->advisor->family }}</span>
                                                </h6>
                                                <small class="notification-text">@t(اپراتور:){{ ' '.$delcu->operator->name.' '.$delcu->operator->family }}</small>
                                            </div>
                                        </div>
                                    </div>
                                   @endforeach
                                </li>
                                <li class="dropdown-menu-footer">
                                    <a class="dropdown-item p-50 text-primary justify-content-center"
                                        href="{{ route('canceled_session_print.report') }}">دیدن همه کنسلی ها</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-notification nav-item">
                            <a class="nav-link nav-link-label" href="#" data-toggle="dropdown" title="@t(یادآوری)">
                                <i class="ficon bx bx-pin bx-tada bx-flip-horizontal"></i>
                                <span class="badge badge-pill badge-danger badge-up">
                                    {{ \App\Models\Reminder::where(['resiver_id'=>auth()->id() , 'status'=>1])->count() }}
                                </span>
                            </a>
                            @php
                             $reminders = \App\Models\Reminder::where(['resiver_id'=>auth()->id() , 'status'=>1])->get();
                            @endphp
                            <ul class="dropdown-menu dropdown-menu-media">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header px-1 py-75 d-flex justify-content-between">
                                        <span class="notification-title">@t(یادآوری های جدید)</span>
                                        <span class="text-bold-400 cursor-pointer" data-toggle="modal"  data-target="#reminder">@t(درج یادآور)</span>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list">
                                   @foreach ($reminders as $reminder)
                                    <div class="d-flex justify-content-between cursor-pointer">
                                        <div class="media d-flex align-items-center">
                                            <div class="media-left pr-0">
                                                <div class="avatar bg-primary bg-lighten-5 mr-1 m-0 p-25">
                                                    <span class="avatar-content text-primary font-medium-2">ل‌د</span>
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">
                                                    <span class="text-bold-500">{{ $reminder->title }}</span>
                                                </h6>
                                                <small class="notification-text">{{ $reminder->note }}</small>
                                            </div>
                                        </div>
                                    </div>
                                   @endforeach
                                </li>
                                <li class="dropdown-menu-footer">
                                    <a class="dropdown-item p-50 text-primary justify-content-center"
                                        href="{{ route('reminder.index') }}">خواندن همه اعلان ها</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-lg-flex d-none">
                                    <span
                                        class="user-name">{{ auth()->user()->name . ' ' . auth()->user()->family }}</span>
                                    <span class="user-status" style="direction: rtl">{{ verta()->format('%d %B %Y') ?? '' }}</span>
                                </div>
                                <span>
                                    @php
                                        if (auth()->user()->pic){
                                        $avatar = auth()->user()->pic;
                                        }else{
                                        $avatar = 'avatar.jpg';
                                        };
                                    @endphp
                                    <img class="round" src="/assets/images/profile/admin/avatar/{{ $avatar }}" alt="avatar" height="40" width="40">
                                </span>
                            </a>
                            <div class="dropdown-menu pb-0">
                                <a class="dropdown-item" href="page-user-profile.html">
                                    <i class="bx bx-user mr-50">
                                    </i> ویرایش پروفایل</a>
                                <a class="dropdown-item" href="app-email.html">
                                    <i class="bx bx-envelope mr-50">
                                    </i> صندوق ورودی من</a>
                                <a class="dropdown-item" href="app-todo.html">
                                    <i class="bx bx-check-square mr-50">

                                    </i>
                                    وظیفه</a>
                                <a class="dropdown-item" href="app-chat.html">
                                    <i class="bx bx-message mr-50">
                                    </i> گفتگو ها</a>
                                <div class="dropdown-divider mb-0">
                                </div>
                                <a class="dropdown-item" href="{{ route('logout') }}">
                                    <i class="bx bx-power-off mr-50">
                                    </i> خروج</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->
    <!--Default size Modal -->
    <div class="modal fade text-left" id="reminder" tabindex="-1" role="dialog" aria-labelledby="reminderLable" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
          <div class="modal-content">
            <div class="modal-header bg-primary">
              <h4 class="modal-title" id="reminderLable">@t(درج یادآور جدید)</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                <i class="bx bx-x"></i>
              </button>
            </div>
            <form action="{{ route('reminder.store') }}" 
                class="wizard-validation" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div class="modal-body line-height-2">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-label-group">
                                <input type="text" name="title" class="form-control" id="reminder_title" placeholder="@t(عنوان یادآور)">
                                <label for="reminder_title">@t(عنوان یادآور)</label>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-label-group">
                                <input type="date" name="date" class="form-control" id="reminder_date" placeholder="@t(تاریخ یادآوری)">
                                <label for="reminder_date">@t(تاریخ یادآوری)</label>
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-label-group">
                                <input type="time" name="time" class="form-control" id="reminder_time" placeholder="@t(ساعت یادآوری)">
                                <label for="reminder_time">@t(ساعت یادآوری)</label>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-label-group">
                                <textarea name="note" id="reminder_note" class="form-control" rows="3"></textarea>
                                <label for="reminder_note">@t(توضیحات)</label>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="text" class="btn btn-secondary mt-75" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">@t(بستن)</span>
                </button>
                <input type="reset" class="btn btn-warning mt-75 float-right" value="@t(پاک کن)" name="" id="">
                <input type="submit" id="submit_form" class="btn btn-success mt-75 float-right" value="@t(درج)">
                </div>
            </form>
          </div>
        </div>
      </div>
</div>
