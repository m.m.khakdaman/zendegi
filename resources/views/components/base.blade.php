<!DOCTYPE html>

<html class="loading" lang="fa" data-textdirection="rtl" dir="rtl">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="/assets/images/ico/favicon.ico">
    <meta name="theme-color" content="#5A8DEE">



    <title>{{ env('APP_NAME') }} - {{ $title ?? '' }}</title>


    {{ $css ?? '' }}

    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/extensions/sweetalert2.min.css">
    <base href="/">
</head>

<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns calendar-application  footer-static  "
    data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
    <x-header />
    <x-main-menu />
    <!-- BEGIN: Content-->
    <x-content :content="$slot" />
    <!-- END: Content-->
    <x-customizer />
    <x-footer />

    {{ $script ?? '' }}

    <script src="/assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/assets/vendors/js/extensions/polyfill.min.js"></script>

    <script src="/assets/js/scripts/extensions/sweet-alerts.js"></script>

    <script>
        @if(session('ali'))

        Swal.fire({
            title: "{{ session('ali') }}",
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: 'باشه!',
            buttonsStyling: false,
            });
        @endif
        @if (session('message'))
            Swal.fire({
            title: "{{ session('message') }}",
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: 'باشه!',
            buttonsStyling: false,
            });
        @endif
        @if (session('error_message'))
            Swal.fire({
            title: "{{ session('error_message') }}",
            confirmButtonClass: 'btn btn-danget',
            confirmButtonText: 'باشه!',
            buttonsStyling: false,
            });
        @endif
        $(function() {
            $('[data-toggle=toltip]').tooltip();
            $('input[type=tel]').each(function(index, obj) {
                $(obj).keypress(function(e) {
                    b = $(this).val()
                        .replace(/\u0660/g, "0")
                        .replace(/\u0661/g, "1")
                        .replace(/\u0662/g, "2")
                        .replace(/\u0663/g, "3")
                        .replace(/\u0664/g, "4")
                        .replace(/\u0665/g, "5")
                        .replace(/\u0666/g, "6")
                        .replace(/\u0667/g, "7")
                        .replace(/\u0668/g, "8")
                        .replace(/\u0669/g, "9")
                        .replace(/\u06F0/g, "0")
                        .replace(/\u06F1/g, "1")
                        .replace(/\u06F2/g, "2")
                        .replace(/\u06F3/g, "3")
                        .replace(/\u06F4/g, "4")
                        .replace(/\u06F5/g, "5")
                        .replace(/\u06F6/g, "6")
                        .replace(/\u06F7/g, "7")
                        .replace(/\u06F8/g, "8")
                        .replace(/\u06F9/g, "9")
                        .replace(/\D/g, "");
                    $(this).val(b);
                })
            })
        });
    </script>
</body>

</html>
