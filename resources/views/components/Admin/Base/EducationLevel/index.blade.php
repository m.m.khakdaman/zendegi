<x-base >
    <x-slot name='title'>
        مدارک تحصیلی
    </x-slot>

    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>

    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">        
                        <!-- create invoice button-->
                        {{-- @can('education_level_create') --}}
                        <div class="invoice-create-btn mb-1">
                            <a href="{{ route('education_level.create') }}" class="btn btn-primary glow invoice-create" role="button"
                                aria-pressed="true">ایجاد مدرک تحصیلی جدید</a>
                        </div>
                        {{-- @endcan --}}
                        <div class="table-responsive ">
                            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام</th>
                                        <th>تنظیمات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($education_levels as $education_level)
                                        <tr>
                                            <td>{{ $education_level->id }}</td>
                                            <td>{{ $education_level->education_level }}</td>
                                            <td>
                                                <div class="btn-group btn-group-sm">
                                                    {{-- @can('education_level.education_level.update') --}}
                                                        <a href="{{ route('education_level.edit', $education_level->id) }}"
                                                            class="btn btn-warning text-white">ویرایش</a>
                                                    {{-- @endcan --}}
                                                    {{-- @can('education_level.education_level.destroy') --}}
                                                        <button class="btn btn-danger text-white" data-toggle="modal"
                                                            data-target="#_{{ $education_level->id }}">حذف</button>
                                                    {{-- @endcan --}}
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade" id="_{{ $education_level->id }}" tabindex="-1" education_level="dialog"
                                                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" education_level="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                آیا می خواهید -- {{ $education_level->education_level }} -- را حذف نمایید ؟
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form action="{{ route('education_level.destroy', $education_level->id) }}"
                                                                    method="post">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">خیر</button>
                                                                    <button type="submit" class="btn btn-success">بله</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>نام</th>
                                        <th>تنظیمات</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->

        <script>
            $(document).ready(function() {
                // Setup - add a text input to each footer cell
                $('.invoice-data-table tfoot th').each(function() {
                    var title = $(this).text();
                    if (title != 'تنظیمات') {
                        $(this).html('<input type="text" placeholder="جست جو در ' + title +
                            '"  class="form-control form-control-sm" />');

                    }
                });

                // DataTable
                var table = $('.invoice-data-table').DataTable({
                    language: {
                        "sEmptyTable": "هیچ داده‌ای در جدول وجود ندارد",
                        "sInfo": "نمایش _START_ تا _END_ از _TOTAL_ ردیف",
                        "sInfoEmpty": "نمایش 0 تا 0 از 0 ردیف",
                        "sInfoFiltered": "(فیلتر شده از _MAX_ ردیف)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "نمایش _MENU_ ردیف",
                        "sLoadingRecords": "در حال بارگزاری...",
                        "sProcessing": "در حال پردازش...",
                        "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                        "oPaginate": {
                            "sFirst": "برگه‌ی نخست",
                            "sLast": "برگه‌ی آخر",
                            "sNext": "بعدی",
                            "sPrevious": "قبلی"
                        },
                        "oAria": {
                            "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                            "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                        },
                        "sSearch": "",
                        "sSearchPlaceholder": "جستجوی صورتحساب",
                    },
                    dom: '<"top d-flex flex-wrap"><"clear">rt<"bottom"p>',
                    initComplete: function() {
                        // Apply the search
                        this.api().columns().every(function() {
                            var that = this;

                            $('input', this.footer()).on('keyup change clear', function() {
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                        });
                    }
                });

            });

        </script>

    </x-slot>
</x-base>
