<x-base>
    <x-slot name='title'>
        @t(لیست انتظار)
    </x-slot>

    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(لیست انتظار)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('customer.index') }}">@t(لیست انتظار)</a>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-3">
                <div class="card background-color">
                    <div class="card-header">
                        
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                          <form action="https://nnmmm.ir/admin/waiting_list/save_to_wl" class="wizard-vertical" method="POST" enctype="multipart/form-data">
                          <input type="hidden" name="_token" value="K49d4gmlWMmMpet5WrVBDYZCeg7dxfWhMvXV48jH">                            <fieldset class="form-group">
                                <label for="customer_id">نام مراجع</label>
                                <select name="customer_id" id="customer_id" class="form-control select2 select2-hidden-accessible" data-select2-id="customer_id" tabindex="-1" aria-hidden="true">
                                    <option value="" data-select2-id="2">نام یا موبایل مراجع را وارد کنید</option>
                                                                            <option value="45">محمد مهدی خاکدامن</option>
                                                                            <option value="51">مهدیس  احمدی</option>
                                                                            <option value="52">عباس  فردی</option>
                                                                            <option value="53">محدثه  غنوی</option>
                                                                            <option value="54">مبینا  رستمی</option>
                                                                            <option value="55">جازبی خالی</option>
                                                                            <option value="56">زینب  شب خیز</option>
                                                                            <option value="57">مریم  خان احمد لو</option>
                                                                            <option value="58">فخاران خالی</option>
                                                                            <option value="59">احمدی خالی</option>
                                                                            <option value="60">محمدجواد  الهی</option>
                                                                            <option value="61">گل  محمدی</option>
                                                                            <option value="62">پریسادلیری خالی</option>
                                                                            <option value="63">سمیه  صفدری</option>
                                                                            <option value="64">فاطمه  شمس الدین</option>
                                                                            <option value="65">زهرا  محمدی</option>
                                                                            <option value="66">برومند خالی</option>
                                                                            <option value="67">رافه خالی</option>
                                                                            <option value="68">اقای  حسینی</option>
                                                                            <option value="69">اقای  کلاه دوز</option>
                                                                            <option value="70">عذرا  موسوی</option>
                                                                            <option value="71">خانم  کافی</option>
                                                                            <option value="72">اکبریان  فرد</option>
                                                                            <option value="73">اقای  فروغی</option>
                                                                            <option value="74">سبحان  نفیسی</option>
                                                                            <option value="75">زهرا  مقدسی نیا</option>
                                                                            <option value="76">تمیمی خالی</option>
                                                                            <option value="77">اقای  رمضان پور</option>
                                                                            <option value="78">زارع خالی</option>
                                                                            <option value="79">محمد  رمضانی</option>
                                                                            <option value="80">رویا  سلیمانی</option>
                                                                            <option value="81">حنانه  فتحی</option>
                                                                            <option value="82">بیتا  علامه</option>
                                                                            <option value="83">ارشیا  عمرانی</option>
                                                                            <option value="84">رحیمه  محمدی</option>
                                                                            <option value="85">ناصر  بروفر</option>
                                                                            <option value="86">اقای  رضایی</option>
                                                                            <option value="87">خانم  هاشمی</option>
                                                                            <option value="88">تجویدی خالی</option>
                                                                            <option value="89">فاطمه  خلیلی</option>
                                                                            <option value="90">مهدی  محمدی</option>
                                                                            <option value="91">مرعشی خالی</option>
                                                                            <option value="92">سحر  اکبری</option>
                                                                            <option value="93">طاهره  راحتی</option>
                                                                            <option value="94">مهشید  اسماعیلی</option>
                                                                            <option value="95">فاطمه  نریمان</option>
                                                                            <option value="96">محمدرضا  مزرعه شاهی</option>
                                                                            <option value="97">هانیه  سادات رضوی</option>
                                                                            <option value="98">علیرضا  رضایی</option>
                                                                            <option value="99">زهره  جوکار</option>
                                                                            <option value="100">محدثه  بیرانوند</option>
                                                                            <option value="101">فلاح  نیا</option>
                                                                            <option value="102">علی  حافظی</option>
                                                                            <option value="103">محدثه  مرتضایی</option>
                                                                            <option value="104">فاطمه  قادری</option>
                                                                            <option value="105">معصومه  بهرامی</option>
                                                                            <option value="106">موسوی خالی</option>
                                                                            <option value="107">زرین خالی</option>
                                                                            <option value="108">اقایا خالی</option>
                                                                            <option value="109">سروش خالی</option>
                                                                            <option value="110">فرخی خالی</option>
                                                                            <option value="111">زهرا  سیدی</option>
                                                                            <option value="112">اسماعیلی خالی</option>
                                                                            <option value="113">مهدی  حضرت عینی</option>
                                                                            <option value="114">محدثه  مولایی</option>
                                                                            <option value="115">بنیتا  ترابی</option>
                                                                            <option value="116">لیلا  صادق زاده</option>
                                                                            <option value="117">اسماعیلی خالی</option>
                                                                            <option value="118">قوامیان خالی</option>
                                                                            <option value="119">سمیه  بهمنی</option>
                                                                            <option value="120">مونا  محلاتی</option>
                                                                            <option value="121">سارا  بهزادی پور</option>
                                                                            <option value="122">عبدالحسین  کبیری</option>
                                                                            <option value="123">بهاره  بخشعلی</option>
                                                                            <option value="124">حائری خالی</option>
                                                                            <option value="125">طاهره  حیدری</option>
                                                                            <option value="126">خان  مرادی</option>
                                                                            <option value="127">فاطمه  بخشی</option>
                                                                            <option value="128">فاطمه  گایئنی</option>
                                                                            <option value="129">لیلا  حیدری رزاقی</option>
                                                                            <option value="130">حامده  مهدی زاده</option>
                                                                            <option value="131">نسرین  فتحی زاده</option>
                                                                            <option value="132">مرعشیان خالی</option>
                                                                            <option value="133">ایسا  حیدری</option>
                                                                            <option value="134">حنانه  شکری</option>
                                                                            <option value="135">ابوالفضل  ابراهیمی</option>
                                                                            <option value="136">امیر  عظیمی</option>
                                                                            <option value="137">شهری خالی</option>
                                                                            <option value="138">رهنمون خالی</option>
                                                                            <option value="139">مهدی  کریمی</option>
                                                                            <option value="140">مریم  براتی</option>
                                                                            <option value="141">نرگس  جهاندوست</option>
                                                                            <option value="142">فلاح خالی</option>
                                                                            <option value="143">محمدیان خالی</option>
                                                                            <option value="144">سمانه  مستوره</option>
                                                                            <option value="145">کلهری خالی</option>
                                                                            <option value="146">فاطمه  حاجی زاده</option>
                                                                            <option value="147">مرتضی  حسینی</option>
                                                                            <option value="148">سیدی خالی</option>
                                                                            <option value="149">جهان  گیری</option>
                                                                    </select><span class="select2 select2-container select2-container--default" dir="rtl" data-select2-id="1" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-customer_id-container"><span class="select2-selection__rendered" id="select2-customer_id-container" role="textbox" aria-readonly="true" title="نام یا موبایل مراجع را وارد کنید">نام یا موبایل مراجع را وارد کنید</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="advisor_id">نام مشاور</label>
                                <select name="advisor_id" id="advisor_id" class="form-control select2 select2-hidden-accessible" data-select2-id="advisor_id" tabindex="-1" aria-hidden="true">
                                    <option value="" data-select2-id="4">نام مشاور را وارد کنید</option>
                                                                            <option value="16145">محصولات قسطی 200000 تومان</option>
                                                                            <option value="16146">عاطفه رستمی</option>
                                                                            <option value="16147">سعید نجفی</option>
                                                                            <option value="16148">پریسا  سبویی</option>
                                                                            <option value="16149">فرناز منفرد</option>
                                                                            <option value="16150">امیرهوشنگ ملاحسینی</option>
                                                                            <option value="16151">عارفه خراسانی زاده</option>
                                                                            <option value="16152">محمدجعفر جهانگیرزاده</option>
                                                                            <option value="16153">طوبی موحد نساج</option>
                                                                            <option value="16154">لیلا سروده</option>
                                                                            <option value="16155">فهیمه  فرهودی</option>
                                                                            <option value="16156">سوسن  جهاندوست</option>
                                                                            <option value="16157">خانم  کوثر دانش</option>
                                                                            <option value="16158">مژگان آقابابایی</option>
                                                                            <option value="16159">محمود مظفری</option>
                                                                            <option value="16160">آقای  رحمانی</option>
                                                                            <option value="16161">مهدی ناجی عظیمی</option>
                                                                            <option value="16162">سعیده خاوری</option>
                                                                            <option value="16163">آقای  امیری</option>
                                                                            <option value="16164">خانم  دکتر اکرم السادات حسینی</option>
                                                                            <option value="16165">جمشید  پورجواد</option>
                                                                            <option value="16166">فاطمه مینو</option>
                                                                            <option value="16167">بنت الهدی خدادادی</option>
                                                                            <option value="16168">عاطفه موسوی</option>
                                                                            <option value="16169">آقای  محبی</option>
                                                                            <option value="16170">اقای  مشعل چیان</option>
                                                                            <option value="16171">خانم  رحمانی</option>
                                                                            <option value="16172">حدیث حسین زاده</option>
                                                                            <option value="16173">زهرا شاهمرادی</option>
                                                                            <option value="16174">هدی لاجوردی</option>
                                                                            <option value="16175">محمود  نعمت اللهی</option>
                                                                            <option value="16176">دکتر  هوشیاری</option>
                                                                            <option value="16177">محمدرضا بیک وردی</option>
                                                                            <option value="16178">زهرا خلیفات</option>
                                                                            <option value="16179">دکتر  فیروزیان</option>
                                                                            <option value="16180">حکیم  محسن اسماعیلی</option>
                                                                            <option value="16181">مریم جمال نیک</option>
                                                                            <option value="16182">سارا  گودرزی</option>
                                                                            <option value="16183">دکتر  سیما قدرتی</option>
                                                                            <option value="16184">دکتر  محسن علی بالایی</option>
                                                                            <option value="16185">دکتر  نوروزی</option>
                                                                            <option value="16186">دکتر  دشتی</option>
                                                                            <option value="16187">آقای  نوایی</option>
                                                                            <option value="16188">دکتر  ولی ا... رمضانی</option>
                                                                            <option value="16189">محمد  محمدی</option>
                                                                            <option value="16190">ارویسی </option>
                                                                            <option value="16191">آقای  طباطبایی فر</option>
                                                                            <option value="16192">دکتر  شفیعی فرد</option>
                                                                            <option value="16193">محمد  رضا تقی پور</option>
                                                                            <option value="16194">خانم  بهرک</option>
                                                                            <option value="16195">خانم  نایینی</option>
                                                                            <option value="16196">دکتر  عابدینی</option>
                                                                            <option value="16197">استعدادیابی </option>
                                                                            <option value="16198">انسیه طلیقی</option>
                                                                            <option value="16199">خانم  دکتر داستانی</option>
                                                                            <option value="16200">محمد  رضا دانش</option>
                                                                            <option value="16201">مهدیه محمدی</option>
                                                                            <option value="16202">علیرضا  معماریان</option>
                                                                            <option value="16203">اقای  حسین زاده</option>
                                                                            <option value="16204">اقای  شیخ علی</option>
                                                                            <option value="16205">سمیه غنیمتی</option>
                                                                            <option value="16206">خانم  غلامی پور</option>
                                                                            <option value="16207">خانم  مظاهری</option>
                                                                            <option value="16208">دکتر  خانی</option>
                                                                            <option value="16209">خانم  اسماعیلی</option>
                                                                            <option value="16210">دکتر  حسن حمیدپور</option>
                                                                            <option value="16211">اقای  کمیجانی</option>
                                                                            <option value="16212">خانم  معصومه درودیان</option>
                                                                            <option value="16213">اکرم استواری</option>
                                                                            <option value="16214">فاطمه بداقی</option>
                                                                            <option value="16215">خانم  بهاالدینی</option>
                                                                            <option value="16216">خانم  محبوبه باقری</option>
                                                                            <option value="16217">خانم  سمیرا بهمن</option>
                                                                            <option value="16218">خانم  منصوره بقائی</option>
                                                                            <option value="16219">مریم تمسکی</option>
                                                                            <option value="16220">نفیسه احمدی</option>
                                                                            <option value="16221">شیما پرستاری</option>
                                                                            <option value="16222">خانم  هما شیرازی</option>
                                                                            <option value="16223">نرگس شیخی پور</option>
                                                                            <option value="16224">شیما محمدی</option>
                                                                            <option value="16225">خانم  فرنوش حسینی</option>
                                                                            <option value="16226">خانم  هادی نیا</option>
                                                                            <option value="16227">خانم  زهرا حسینی</option>
                                                                            <option value="16228">جواد قاسمی</option>
                                                                            <option value="16229">آقای  هدایت اله مجیدی</option>
                                                                            <option value="16230">آقای  مجید مجیدی</option>
                                                                            <option value="16231">آقای  حسین سعادت</option>
                                                                            <option value="16232">منصوره چاوشی</option>
                                                                            <option value="16233">دکتر  اروی بختیاری</option>
                                                                            <option value="16234">دکتر  زهرا استادیان خانی</option>
                                                                            <option value="16235">فرزانه احمدی</option>
                                                                            <option value="16236">مهدی قادری</option>
                                                                            <option value="16237">آقای  محس تمدن</option>
                                                                            <option value="16238">آقای  مرتضی غلامی</option>
                                                                            <option value="16239">دکتر  مبین صالحی</option>
                                                                            <option value="16240">دکتر  حسین اقبالی</option>
                                                                            <option value="16241">سید کمال الدین طباطبائی</option>
                                                                            <option value="16242">علی اکبر رضاخو</option>
                                                                            <option value="16243">احمد آقاجانی</option>
                                                                    </select><span class="select2 select2-container select2-container--default" dir="rtl" data-select2-id="3" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-advisor_id-container"><span class="select2-selection__rendered" id="select2-advisor_id-container" role="textbox" aria-readonly="true" title="نام مشاور را وارد کنید">نام مشاور را وارد کنید</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="category_id">علت مراجعه</label>
                                <select name="category_id" id="category_id" class="form-control select2 select2-hidden-accessible" data-select2-id="category_id" tabindex="-1" aria-hidden="true">
                                    <option value="" data-select2-id="6">علت مراجعه را وارد کنید</option>
                                                                            <option value="1">عمومی</option>
                                                                            <option value="4">افسردگی</option>
                                                                            <option value="5">کودک و فرزندپروری</option>
                                                                            <option value="6">بازی درمانی</option>
                                                                            <option value="7">تست هوش</option>
                                                                            <option value="8">استعدادیابی</option>
                                                                            <option value="9">اختلالات یادگیری</option>
                                                                            <option value="10">زوج درمانی</option>
                                                                    </select><span class="select2 select2-container select2-container--default" dir="rtl" data-select2-id="5" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-category_id-container"><span class="select2-selection__rendered" id="select2-category_id-container" role="textbox" aria-readonly="true" title="علت مراجعه را وارد کنید">علت مراجعه را وارد کنید</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </fieldset>
                            <div class="form-group">
                                <label>تاریخ درخواستی</label>
                                <div class="controls form-label-group ">
                                    <input type="text" class="form-control hasDatepicker" name="date" id="date_sw" pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}" data-validation-pattern-message="فرمت فیلد معتبر نیست." value="" placeholder="انتخاب تاریخ" aria-invalid="false" onclick="change_date(this.value)">
                                    <div class="form-control-position">
                                        <i class="bx bx-calendar">
                                        </i>
                                    </div>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <fieldset class="form-group">
                                <label for="time">زمان درخواستی</label>
                                <input type="time" name="time" class="form-control" id="time">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="note">توضیحات</label>
                                <textarea name="note" class="form-control" id="note" rows="1"></textarea>
                            </fieldset>
                            <fieldset>
                                <input type="hidden" value="0" name="id">
                                <input type="hidden" value="در انتظار تخصیص" name="status">
                                <input type="submit" class="btn btn-success float-right" value="ثبت">
                            </fieldset>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="table-responsive ">
                            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%"
                                id="example">
                                <thead>
                                    <tr>

                                        <th>#</th>
                                        <th>نام خانوادگی مراجع</th>
                                        <th>مشاور</th>
                                        <th>موضوع</th>
                                        <th>تاریخ</th>
                                        <th>ساعت</th>
                                        <th><select placeholder="وضعیت"><option>فعال</option><option>غیر فعال</option><option>تخصیص یافته</option></select></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>نام خانوادگی مراجع</th>
                                        <th>مشاور</th>
                                        <th>موضوع</th>
                                        <th>تاریخ</th>
                                        <th>ساعت</th>
                                        <th>وضعیت</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
        <!-- END: Page JS-->
        <script>
            $(document).ready(function() {

                dt='#example';
                columns = [{
                            "data": "id"
                        },
                        {
                            "data": "customer_id"
                        },
                        {
                            "data": "advisor_id"
                        },
                        {
                            "data": "category_id"
                        },
                        {
                            "data": "date"
                        },
                        {
                            "data": "time",
                        },
                        {
                            "data": "status",
                            
                        }
                    ];
                
                $(dt+' thead tr').clone(true).appendTo(dt+' thead');
                $(dt+' thead tr:eq(1) th').each(function(i) {

                    var title = $(this).text();
                    var span = '<span class="sorting" onclick="a=$(\''+dt+' thead tr:eq(0) th\')['+ i +'];a.click();this.className=a.className"></span>';
                    $(this).html('<input type="text" placeholder="جستجوی ' + title + '" />'+span);
                    $('input', this).on('keyup change', function() {
                        if (table.column(i).search() !== this.value) {
                            table
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                });
               // $(dt+' thead tr:eq(0) th').hide();

                table = $(dt).DataTable({
                    "searching": false,
                    orderCellsTop: true,
                    fixedHeader: true,
                    "ordering": true,
                    "paging": true,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{ route('Waitinglist.ajax_get') }}",
                    "language": persian,
                    "responsive": true,
                    "dom": '<"col-12"<"d-flex justify-content-between"lp><t><"d-flex justify-content-between"ip>>',
                    "columns": columns
                })


    



            });
        </script>

    </x-slot>
</x-base>
