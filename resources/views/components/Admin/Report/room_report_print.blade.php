<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    <!-- END: Page CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="container">
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-2">
                <p>
                    <b>بسمه تعالی</b>
                </p>
                <p>
                    <span class="font-medium-3">
                        @t(گزارش اتاق ها)<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>

                </p>
            </div>
            <div class="col-4">

                <div class="row text-center float-right">
                    <div class="mr-4 mt-2">
                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h4 class="card-title">@t(گزارش اتاق ها جلسات)</h4>
                <div class="card">
                    <div class="card-body text-secondary">
                        comming soon
                    </div>
                </div>
            </div>
            <div class="col-6">
                <h4 class="card-title">@t(گزارش اتاق ها کارگاه ها)</h4>
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                            
                        <h4 class="card-title">@t(جزئیات)</h4>
                        <div>
                        {{-- <table class="invoice-data-table dt-responsive nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(شماره اتاق)</th>
                                    <th>@t(نوع)</th>
                                    <th>@t(مشاور)</th>
                                    <th class="text-center">از</th>
                                    <th>@t(تا)</th>
                                    <th>روز</th>
                                    <th class="text-center">@t(شروع)</th>
                                    <th>@t(پایان)</th>
                                    <th>@t(تخفیف)</th>
                                    <th>@t(با تخفیف)</th>
                                    <th class="text-center">@t(پرداختی)</th>
                                    <th class="text-center">@t(بدهی)</th>
                                    <th>5</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $counter = 1;
                                @endphp
                                @foreach ($reserved as $item)
                                    @foreach ($item->ProductDay as $day)
                                    <tr>
                                        <td>{{ $counter++ }}</td>
                                        <td>{{ $item->room_id }}</td>
                                        <td>{{ $item->product_name }}</td>
                                        <td>{{ $item->advisor->name ?? '' }}&nbsp;{{ $item->advisor->family ?? '' }}</td>
                                        <td>{{ $item->start }}</td>
                                        <td>{{ $item->end }}</td>
                                        <td>{{ $day->day_of_week ?? '' }}</td>
                                        <td>{{ $day->start ?? '' }}</td>
                                        <td>{{ $day->end ?? '' }}</td>
                                    </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(شماره اتاق)</th>
                                    <th>@t(نوع)</th>
                                    <th>@t(مشاور)</th>
                                    <th class="text-center">از</th>
                                    <th>@t(تا)</th>
                                    <th>روز</th>
                                    <th class="text-center">@t(شروع)</th>
                                    <th>@t(پایان)</th>
                                    <th>@t(تخفیف)</th>
                                    <th>@t(با تخفیف)</th>
                                    <th class="text-center">@t(پرداختی)</th>
                                    <th class="text-center">@t(بدهی)</th>
                                    <th>5</th>
                                </tr>
                            </tfoot>
                        </table> --}}
                        <table class="table-hover table-bordered" style="width: 100%">
                            <thead>
                                <th>@t(ردیف)</th>
                                <th>@t(شماره اتاق)</th>
                                <th>@t(نوع)</th>
                                <th>@t(مشاور)</th>
                                <th class="text-center">از</th>
                                <th>@t(تا)</th>
                                <th>روز</th>
                                <th class="text-center">@t(شروع)</th>
                                <th>@t(پایان)</th>
                                <th>@t(دق پری اتاق)</th>
                                <th>@t(روز پری اتاق)</th>
                                <th>@t(کل دق پری اتاق)</th>
                            </thead>
                            <tbody>
                                @php
                                    $counter = 1;
                                @endphp
                                @foreach ($full as $item)
                                    <tr>
                                        <td>{{ $counter++ }}</td>
                                        <td>{{ $item->room_id }}</td>
                                        <td>{{ $item->product->product_name ?? '' }}</td>
                                        <td>{{ $item->product->advisor->name ?? '' }}&nbsp;{{ $item->product->advisor->family ?? '' }}</td>
                                        <td>{{ $item->product->start ?? '' }}</td>
                                        <td>{{ $item->product->end ?? '' }}</td>
                                        <td>
                                            @switch($item->day_of_week)
                                                @case(1)
                                                    @t(شنبه)
                                                    @break
                                                @case(2)
                                                    @t(یکشنبه)    
                                                @break 
                                                @case(3)
                                                    @t(دوشنبه)
                                                @break
                                                @case(4)
                                                    @t(سه شنبه)    
                                                @break
                                                @case(5)
                                                    @t(چهارشنبه)
                                                @break
                                                @case(6)
                                                    @t(پنچشنبه)    
                                                @break     
                                                @default
                                                    @t(جمعه)
                                            @endswitch
                                        </td>
                                        <td>{{ $item->start }}</td>                                        
                                        <td>{{ $item->end }}</td>
                                        <td>
                                            @php
                                                $hour = Str::before($item->start, ':');
                                                $minute = Str::after($item->start, ':');
                                                $s_time = $hour * 60 + $minute;
                                                $hour = Str::before($item->end, ':');
                                                $minute = Str::after($item->end, ':');
                                                $e_time = $hour * 60 + $minute;
                                                print_r($e_time - $s_time);
                                            @endphp     
                                        </td>
                                        <td>
                                            @php
                                                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', verta()->parse($item->product->start)->formatGregorian('Y-m-d H:s:i'));
                                                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', verta()->parse($item->product->end)->formatGregorian('Y-m-d H:s:i'));
                                                $diff_in_days = $to->diffInDays($from);
                                                print_r($diff_in_days);
                                            @endphp
                                        </td>
                                        <td>
                                           @php
                                               print_r($diff_in_days * ($e_time - $s_time));
                                           @endphp
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ sentence() }}
        <br>

        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <!-- END: Page Vendor JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
     <!-- END: Page Vendor JS-->
     <script src="/assets/vendors/js/charts/chart.min.js"></script>

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="/assets/js/scripts/charts/admin_report.js"></script> --}}
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
    <script>
        $('.currency').each(function(i, o) {
            $(o).html(numeral($(o).text()).format('0,0'));
        })
    </script>
    {{-- <script>
        $(window).on("load", function() {

            Chart.defaults.global.defaultFontFamily = '"primary-font", "segoe ui", "tahoma"';

            var $primary = '#5A8DEE',
                $success = '#39DA8A',
                $danger = '#FF5B5C',
                $warning = '#FDAC41',
                $info = '#00CFDD',
                $label_color = '#475F7B',
                grid_line_color = '#dae1e7',
                scatter_grid_color = '#f3f3f3',
                $scatter_point_light = '#E6EAEE',
                $scatter_point_dark = '#5A8DEE',
                $white = '#fff',
                $black = '#000';

            var themeColors = [$primary, $warning, $danger, $success, $info, $label_color];


            // Pie Chart
            // --------------------------------
            //Get the context of the Chart canvas element we want to select
            var pieChartctx = $("#simple-pie-chart");

            // Chart Options
            var piechartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                responsiveAnimationDuration: 500,
                title: {
                    display: false,
                    text: '{{ ' '.$admin->name.' '.$admin->family }}'
                }
            };

            // Chart Data
            var piechartData = {
                labels: ["کارگاه", "پرداختی", "لغو شده", " جلسات موفق", " جلسات ثبتی","جلسه باز"],
                datasets: [{
                    label: "سری اطلاعات اول",
                    data: [{{ $wsh_num }}, {{ $payed_session->count() + $debt_session->count() }}, {{ $canceled_session_num }}, {{ $closed_session_num }}, {{ $registred_session }}, {{ $open_session_num }}],
                    backgroundColor: themeColors,
                }]
            };

            var pieChartconfig = {
                type: 'pie',

                // Chart Options
                options: piechartOptions,

                data: piechartData
            };

            // Create the chart
            var pieSimpleChart = new Chart(pieChartctx, pieChartconfig);

        });
    </script> --}}
    <!-- END: Page JS-->
<script>
    // window.print();
</script>

</html>
