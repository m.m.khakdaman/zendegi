<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="container">
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-2">
                <p>
                    <b>بسمه تعالی</b>
                </p>
                <p>
                    <span class="font-medium-3">
                        @t(گزارش جلسات کنسل شده)<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>

                </p>
            </div>
            <div class="col-4">

                <div class="row text-center float-right">
                    <div class="mr-4 mt-2">
                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                            
                        <button class="btn btn-dropbox btn-icon bx bx-right-arrow-alt">دیروز</button>
                        <button class="btn btn-dropbox btn-icon bx bx-arrow-back float-right">فردا</button>
                        <table class="table-hover table-bordered" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(تاریخ ثبت)</th>
                                    <th>@t(تاریخ جلسه)</th>
                                    <th>@t(تاریخ کنسل کردن)</th>
                                    <th>@t(مراجع)</th>
                                    <th>@t(مشاور)</th>
                                    <th class="text-center">وضع</th>
                                    <th>لغو</th>
                                    <th class="text-center">@t(اپراتور)</th>
                                    <th>@t(قیمت)</th>
                                    <th>@t(تخفیف)</th>
                                    <th>@t(با تخفیف)</th>
                                    <th class="text-center">@t(پرداختی)</th>
                                    <th class="text-center">@t(بدهی)</th>
                                    <th><i class="bx bx-info-circle" data-toggle="tooltip" data-placement="top" title="@t(توضیحات)"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $tmp = 0;
                                @endphp
                                @foreach ($sessions as $session)
                                <tr>
                                    <td class="text-center"><span data-toggle="tooltip" data-placement="top" title="{{ $session->id }}">{{ ++$tmp }}</span></td>
                                    <td>{{ verta($session->created_at)->format('Y/m/d') }}</td>
                                    <td>
                                        @if ($session->date < date('Y-m-d') and $session->session_status_id == 1)
                                            <span class="text-danger">{{ $session->day->date ?? '' }}</span>                                                        
                                        @else
                                            {{ $session->day->date ?? '' }}
                                        @endif
                                    </td>
                                    <td>{{ verta($session->updated_at)->format('Y/m/d') }}</td>
                                    <td><span class="text-secondary pl-50">{{ ($session->customer->name ?? '').' '.($session->customer->family ?? '') }}</span></td>
                                    <td><span>{{ $session->advisor->name.' '.$session->advisor->family }}</span></td>
                                    <td class="text-center">
                                        @if ($session->session_status_id == 1)
                                            @if ($session->delete_type == 0)
                                                <i class="bx bx-lock-open text-danger" data-toggle="tooltip" data-placement="top" title="@t(این جلسه هنوز به پایان نرسیده و باز است)"></i>
                                            @endif
                                        @else
                                            <i class="bx bxs-lock text-success" data-toggle="tooltip" data-placement="top" title="@t(این جلسه به  پایان رسیده و بسته شده است)"></i>
                                        @endif
                                    </td>
                                    
                                    <td>
                                        @if ($session->delete_type)
                                            <i class="bx bx-x-circle text-danger" data-toggle="tooltip" data-placement="top" title="{{ setting($session->delete_type) }}"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <small>{{ $session->operator->name.' '.$session->operator->family }}</small>
                                    </td>
                                    <td><span class="currency">{{ $session->orginal_price ?? '' }}</span></td>
                                    <td class="text-center">
                                        @isset($session->discount)
                                            <span class="text-danger currency">{{ $session->discount ?? '' }}</span>
                                        @endisset
                                    </td>
                                    <td class="text-center"><span class="currency">{{ $session->total_price }}</span></td>
                                    <td class="text-center">
                                        <span>
                                            {{@ ($session->total_price + $session->transactions->amount_standing) ?? ''  }}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        @if(@ $session->transactions->amount_standing)
                                            <span class="badge badge-danger currency">{{ $session->transactions->amount_standing * -1 ?? '' }}</span>
                                        @else
                                            <span class="badge badge-light-success ">تسویه</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($session->comment or $session->delete_comment)
                                        <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="top" title="@t({{ $session->delete_comment.' '.$session->comment }})">
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                <tr class="text-bold-600 text-secondary text-center" style="height: 40px">
                                    <td></td>
                                    <td>مجموع</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        {{-- محاسبه جمع قیمت جلساتی که رزرو شده البته کنسلی ها از آن کم شده --}}
                                        <span class="currency">
                                            {{ $price = $sessions->where('delete_type' ,0)->sum('orginal_price') ?? '' }}
                                        </span>
                                    </td>
                                    <td>
                                        {{-- جمع تخفیف داده شده  --}}
                                        <span class="currency">
                                            {{ $discount = $sessions->where('delete_type' ,0)->sum('discount') ?? '' }}
                                        </span>
                                    </td>
                                    <td>
                                        {{--  جمع قیمت جلسات رزرو شده پس از اعمال تخفیف --}}
                                        <span class="currency">
                                            {{ $total_price = ($price - $discount) ?? '' }}
                                        </span>
                                    </td>
                                    <td>
                                        {{--  جمع قیمت پرداخت شده پس از اعمال تخفیف --}}
                                        <span class="btn btn-success p-50 currency">
                                            {{ ($total_price + $sessions->where('delete_type' ,0)->sum('transactions.amount_standing')) ?? '' }}
                                        </span>
                                    </td>
                                    <td>
                                        {{--  جمع بدهی --}}
                                        <span class="btn btn-danger p-50 currency">
                                            {{ (($sessions->where('delete_type' ,0)->sum('transactions.amount_standing')) * -1) ?? '' }}
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        @php
                            $tmp = 0;
                        @endphp
                        <table class="table-hover table-bordered" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>عملیات</th>
                                    <th>مراجع</th>
                                    <th>مشاور</th>
                                    <th>اپراتور</th>
                                    <th>تاریخ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($canceleds as $item)
                                    <tr>
                                        <td>{{ ++$tmp }}</td>
                                        <td>{{ $item->subject }}</td>
                                        <td>{{ $item->customer->name ?? ''}}</td>
                                        <td>{{ $item->advisor->name ?? '' }}</td>
                                        <td>{{ $item->user->name ?? ''}}</td>
                                        <td>{{ verta($item->created_at)->format('Y/m/d') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{ sentence() }}
        <br>

        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <!-- END: Page Vendor JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
     <!-- END: Page Vendor JS-->
     <script src="/assets/vendors/js/charts/chart.min.js"></script>

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="/assets/js/scripts/charts/admin_report.js"></script> --}}
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
    <script>
        $('.currency').each(function(i, o) {
            $(o).html(numeral($(o).text()).format('0,0'));
        })
    </script>
    {{-- <script>
        $(window).on("load", function() {

            Chart.defaults.global.defaultFontFamily = '"primary-font", "segoe ui", "tahoma"';

            var $primary = '#5A8DEE',
                $success = '#39DA8A',
                $danger = '#FF5B5C',
                $warning = '#FDAC41',
                $info = '#00CFDD',
                $label_color = '#475F7B',
                grid_line_color = '#dae1e7',
                scatter_grid_color = '#f3f3f3',
                $scatter_point_light = '#E6EAEE',
                $scatter_point_dark = '#5A8DEE',
                $white = '#fff',
                $black = '#000';

            var themeColors = [$primary, $warning, $danger, $success, $info, $label_color];


            // Pie Chart
            // --------------------------------
            //Get the context of the Chart canvas element we want to select
            var pieChartctx = $("#simple-pie-chart");

            // Chart Options
            var piechartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                responsiveAnimationDuration: 500,
                title: {
                    display: false,
                    text: '{{ ' '.$admin->name.' '.$admin->family }}'
                }
            };

            // Chart Data
            var piechartData = {
                labels: ["کارگاه", "پرداختی", "لغو شده", " جلسات موفق", " جلسات ثبتی","جلسه باز"],
                datasets: [{
                    label: "سری اطلاعات اول",
                    data: [{{ $wsh_num }}, {{ $payed_session->count() + $debt_session->count() }}, {{ $canceled_session_num }}, {{ $closed_session_num }}, {{ $registred_session }}, {{ $open_session_num }}],
                    backgroundColor: themeColors,
                }]
            };

            var pieChartconfig = {
                type: 'pie',

                // Chart Options
                options: piechartOptions,

                data: piechartData
            };

            // Create the chart
            var pieSimpleChart = new Chart(pieChartctx, pieChartconfig);

        });
    </script> --}}
    <!-- END: Page JS-->
<script>
    // window.print();
</script>

</html>
