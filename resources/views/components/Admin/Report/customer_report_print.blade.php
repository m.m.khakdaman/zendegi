<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    <!-- END: Page CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="container">
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-2">
                <p>
                    <b>بسمه تعالی</b>
                </p>
                <p>
                    <span class="font-medium-3">
                        @t(گزارش مراجعین)<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>

                </p>
            </div>
            <div class="col-4">

                <div class="row text-center float-right">
                    <div class="mr-4 mt-2">
                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                            
                        <h4 class="card-title">@t(جزئیات)</h4>
                        <div class="table-responsive" >
                        <table class="invoice-data-table table-bordered dt-responsive nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(شماره پرونده)</th>
                                    <th>@t(نام و نام خانوادگی)</th>
                                    <th>@t(موبایل1)</th>
                                    <th class="text-center">@t(موبایل2)</th>
                                    <th>@t(تلفن)</th>
                                    <th>@t(شماره پرونده قدیم)</th>
                                    <th>@t(تاریخ ثبت نام)</th>

                                    {{-- <th class="text-center">@t(اپراتور)</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($customers as $counter => $customer)
                                    <tr>
                                        <td>{{ ++$counter }}</td>
                                        <td>{{ $customer->id }}</td>
                                        <td>{{ $customer->name ?? '' }}&nbsp;{{ $customer->family ?? '' }}</td>
                                        <td>{{ $customer->mobile }}</td>
                                        <td>{{ $customer->mobile2 }}</td>
                                        <td>{{ $customer->phone }}</td>
                                        <td>{{ $customer->old_file_number }}</td>
                                        <td>{{ verta($customer->created_at)->format('Y/m/d') }}</td>
                                        {{-- <td>{{ $customer->operator->name ?? '' }}&nbsp;{{ $customer->operator->family ?? '' }}</td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                            {{-- <tfoot>
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(تاریخ ثبت)</th>
                                    <th>@t(مراجع)</th>
                                    <th>@t(مشاور)</th>
                                    <th class="text-center">وضع</th>
                                    <th>@t(تاریخ جلسه)</th>
                                    <th>لغو</th>
                                    <th class="text-center">@t(اپراتور)</th>
                                    <th>@t(قیمت)</th>
                                    <th>@t(تخفیف)</th>
                                    <th>@t(با تخفیف)</th>
                                    <th class="text-center">@t(پرداختی)</th>
                                    <th class="text-center">@t(بدهی)</th>
                                    <th>5</th>
                                </tr>
                            </tfoot> --}}
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ sentence() }}
        <br>

        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <!-- END: Page Vendor JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
     <!-- END: Page Vendor JS-->
     <script src="/assets/vendors/js/charts/chart.min.js"></script>

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="/assets/js/scripts/charts/admin_report.js"></script> --}}
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
    <script>
        $('.currency').each(function(i, o) {
            $(o).html(numeral($(o).text()).format('0,0'));
        })
    </script>
    {{-- <script>
        $(window).on("load", function() {

            Chart.defaults.global.defaultFontFamily = '"primary-font", "segoe ui", "tahoma"';

            var $primary = '#5A8DEE',
                $success = '#39DA8A',
                $danger = '#FF5B5C',
                $warning = '#FDAC41',
                $info = '#00CFDD',
                $label_color = '#475F7B',
                grid_line_color = '#dae1e7',
                scatter_grid_color = '#f3f3f3',
                $scatter_point_light = '#E6EAEE',
                $scatter_point_dark = '#5A8DEE',
                $white = '#fff',
                $black = '#000';

            var themeColors = [$primary, $warning, $danger, $success, $info, $label_color];


            // Pie Chart
            // --------------------------------
            //Get the context of the Chart canvas element we want to select
            var pieChartctx = $("#simple-pie-chart");

            // Chart Options
            var piechartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                responsiveAnimationDuration: 500,
                title: {
                    display: false,
                    text: '{{ ' '.$admin->name.' '.$admin->family }}'
                }
            };

            // Chart Data
            var piechartData = {
                labels: ["کارگاه", "پرداختی", "لغو شده", " جلسات موفق", " جلسات ثبتی","جلسه باز"],
                datasets: [{
                    label: "سری اطلاعات اول",
                    data: [{{ $wsh_num }}, {{ $payed_session->count() + $debt_session->count() }}, {{ $canceled_session_num }}, {{ $closed_session_num }}, {{ $registred_session }}, {{ $open_session_num }}],
                    backgroundColor: themeColors,
                }]
            };

            var pieChartconfig = {
                type: 'pie',

                // Chart Options
                options: piechartOptions,

                data: piechartData
            };

            // Create the chart
            var pieSimpleChart = new Chart(pieChartctx, pieChartconfig);

        });
    </script> --}}
    <!-- END: Page JS-->
<script>
    // window.print();
</script>

</html>
