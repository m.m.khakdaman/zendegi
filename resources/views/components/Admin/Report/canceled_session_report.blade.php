<x-base title="@t(گزارش روزانه)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(گزارش جلسات کنسل شده)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">@t(گزارش ها)</li>
                            <li class="breadcrumb-item active">@t(گزارش جلسات کنسل شده)</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                        
                        <div class="form-group">
                            <div class=" width-10-per d-inline-flex ">
                                <input type="text" class="form-control datepicer_date"
                                    name="end_date" value="{{  verta()->addDay()->format('Y/m/d') }}" id="end_date"
                                    required placeholder="تاریخ پایان">
                                <div class="form-control-position">
                                    <i class="bx bx-calendar">
                                    </i>
                                </div>
                            </div>
                            <form action="{{ route('canceled_session_print.report') }}" method="post">
                                @csrf
                                @method('post')
                            <button type="submit" class="btn btn-outline-secondary btn-icon"><i class="bx bx-chevron-right"></i>دیروز</button>
                            <button type="button" class="btn btn-outline-secondary">امروز</button>
                            <button class="btn btn-outline-secondary">فردا<i class="bx bx-chevron-left"></i></button>
                            </form>
                        </div>                          
                        <table class="table-hover table-bordered" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(تاریخ ثبت)</th>
                                    <th>@t(تاریخ جلسه)</th>
                                    <th>@t(تاریخ کنسل کردن)</th>
                                    <th>@t(علت کنسلی)</th>
                                    <th>@t(مراجع)</th>
                                    <th>@t(مشاور)</th>
                                    <th class="text-center">وضع</th>
                                    <th>لغو</th>
                                    <th class="text-center">@t(اپراتور)</th>
                                    <th>@t(قیمت)</th>
                                    <th>@t(تخفیف)</th>
                                    <th>@t(با تخفیف)</th>
                                    <th class="text-center">@t(پرداختی)</th>
                                    <th class="text-center">@t(بدهی)</th>
                                    <th><i class="bx bx-info-circle" data-toggle="tooltip" data-placement="top" title="@t(توضیحات)"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $tmp = 0;
                                @endphp
                                {{-- $session is comming from ReportController public function canceled_session_print(Request $request) --}}
                                @foreach ($sessions as $session)
                                <tr>
                                    <td class="text-center"><span data-toggle="tooltip" data-placement="top" title="{{ $session->id }}">{{ ++$tmp }}</span></td>
                                    <td>{{ verta($session->created_at)->format('Y/m/d') }}</td>
                                    <td>
                                        @if ($session->date < date('Y-m-d') and $session->session_status_id == 1)
                                            <span class="text-danger">{{ $session->day->date ?? '' }}</span>                                                        
                                        @else
                                            {{ $session->day->date ?? '' }}
                                        @endif
                                    </td>
                                    <td>{{ verta($session->updated_at)->format('Y/m/d') }}</td>
                                    <td>{{ $session->delete_comment }}</td>
                                    <td><span class="text-secondary pl-50">{{ ($session->customer->name ?? '').' '.($session->customer->family ?? '') }}</span></td>
                                    <td><span>{{ $session->advisor->name.' '.$session->advisor->family }}</span></td>
                                    <td class="text-center">
                                        @if ($session->session_status_id == 1)
                                            @if ($session->delete_type == 0)
                                                <i class="bx bx-lock-open text-danger" data-toggle="tooltip" data-placement="top" title="@t(این جلسه هنوز به پایان نرسیده و باز است)"></i>
                                            @endif
                                        @else
                                            <i class="bx bxs-lock text-success" data-toggle="tooltip" data-placement="top" title="@t(این جلسه به  پایان رسیده و بسته شده است)"></i>
                                        @endif
                                    </td>
                                    
                                    <td>
                                        @if ($session->delete_type)
                                            <i class="bx bx-x-circle text-danger" data-toggle="tooltip" data-placement="top" title="{{ setting($session->delete_type) }}"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <small>{{ $session->operator->name.' '.$session->operator->family }}</small>
                                    </td>
                                    <td><span class="currency">{{ $session->orginal_price ?? '' }}</span></td>
                                    <td class="text-center">
                                        @isset($session->discount)
                                            <span class="text-danger currency">{{ $session->discount ?? '' }}</span>
                                        @endisset
                                    </td>
                                    <td class="text-center"><span class="currency">{{ $session->total_price }}</span></td>
                                    <td class="text-center">
                                        <span>
                                            {{@ ($session->total_price + $session->transactions->amount_standing) ?? ''  }}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        @if(@ $session->transactions->amount_standing)
                                            <span class="badge badge-danger currency">{{ $session->transactions->amount_standing * -1 ?? '' }}</span>
                                        @else
                                            <span class="badge badge-light-success ">تسویه</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($session->comment or $session->delete_comment)
                                        <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="top" title="@t({{ $session->delete_comment.' '.$session->comment }})">
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                               
                            </tbody>
                        </table>
                        <br>
                        @php
                            $tmp = 0;
                        @endphp
                        <table class="table-hover table-bordered" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>عملیات</th>
                                    <th>مراجع</th>
                                    <th>مشاور</th>
                                    <th>اپراتور</th>
                                    <th>تاریخ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($canceleds as $item)
                                    <tr>
                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ ++$tmp }}</span></td>
                                        <td>{{ $item->subject }}</td>
                                        <td>{{ $item->customer->name ?? ''}}&nbsp;{{ $item->customer->family ?? ''}}</td>
                                        <td>{{ $item->advisor->name ?? '' }}&nbsp;{{ $item->advisor->family ?? ''}}</td>
                                        <td>{{ $item->user->name ?? ''}}&nbsp;{{ $item->user->family ?? ''}}</td>
                                        <td>{{ verta($item->created_at)->format('Y/m/d') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->
        

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
        <script>            
            $(document).ready(function() {

                $('.datepicer_date').datepicker({
                    dateFormat: "yy/mm/dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                });
            });
        </script>

    </x-slot>
</x-base>
