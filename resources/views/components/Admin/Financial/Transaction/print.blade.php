<x-base title="@t(پرینت تراکنش ها)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(عملکرد مالی هر مشاور)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                @t(مالی)
                            </li>
                            <li class="breadcrumb-item active">@t(عملکرد مالی هر مشاور)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="invoice-create-btn mb-1">
                            <h1 class="faq-title text-center mb-3">@t(عملکرد مالی هر مشاور)
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="" method="get">
                            <div class="row pl-5">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <input type="text" class="form-control datepicer_date" name="start_date"
                                                id="start_date" data-validation-pattern-message="فرمت فیلد معتبر نیست."
                                                required value="{{ verta()->format('Y/m/d') }}"
                                                placeholder="تاریخ شروع">
                                            <div class="form-control-position">
                                                <i class="bx bx-calendar">
                                                </i>
                                            </div>
                                            <label for="start_date">از تاریخ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <input type="text" class="form-control datepicer_date" name="end_date"
                                                value="{{ verta()->addDay()->format('Y/m/d') }}" id="end_date"
                                                required placeholder="تاریخ پایان">
                                            <div class="form-control-position">
                                                <i class="bx bx-calendar">
                                                </i>
                                            </div>
                                            <label for="end_date">تا تاریخ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            @if ($print_advisors)
                                                <select type="text" class="form-control select2" name="advisors"
                                                    value="{{ old('advisors') }}" id="advisors" required
                                                    placeholder="مشاور">
                                                    <option value="0">تمام مشاور ها</option>
                                                    @foreach ($advisors as $advisor)
                                                        <option value="{{ $advisor->id }}">
                                                            {{ $advisor->name . ' ' . $advisor->family }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <label for="advisors">مشاورین</label>
                                            @else
                                                <select type="text" class="form-control select2" name="operators"
                                                    value="{{ old('operators') }}" id="operators" required
                                                    placeholder="اپراتور">
                                                    <option value="0">تمام اپراتور ها</option>
                                                    @foreach ($operators as $operator)
                                                        <option value="{{ $operator->id }}">
                                                            {{ $operator->name . ' ' . $operator->family }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <label for="operators">اپراتور</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if (!$print_advisors)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <select type="text" class="form-control select2" name="cash"
                                                    value="{{ old('cash') }}" id="cash" required
                                                    placeholder="اپراتور">
                                                    <option value="0">تمام صندوق ها</option>
                                                    @foreach ($cashs as $cash)
                                                        <option value="{{ $cash->id }}">
                                                            {{ $cash->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <label for="cash">صندوق</label>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-12"></div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <input type="number" class="form-control" name="start_price"
                                                id="start_price" data-validation-pattern-message="فرمت فیلد معتبر نیست."
                                                required value="100000" placeholder="تاریخ شروع">
                                            <div class="form-control-position">
                                                <i class="bx bx-money">
                                                </i>
                                            </div>
                                            <label for="start_price">از مبلغ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <input type="number" class="form-control" name="end_price" value="200000"
                                                id="end_price" required placeholder="تاریخ پایان">
                                            <div class="form-control-position">
                                                <i class="bx bx-money">
                                                </i>
                                            </div>
                                            <label for="end_price">تا مبلغ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button type="submit"
                                        class="btn btn-icon rounded-circle btn-success glow mr-1 mb-3"><i
                                            class="bx bx-printer"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js">
        </script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js">
        </script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js">
        </script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js">
        </script>
        <script src="/assets/js/core/app-menu.js">
        </script>
        <script src="/assets/js/core/app.js">
        </script>
        <script src="/assets/js/scripts/components.js">
        </script>
        <script src="/assets/js/scripts/footer.js">
        </script>
        <script src="/assets/js/scripts/customizer.js">
        </script>
        <script src="/assets/js/scripts/forms/select/form-select2.js">
        </script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
        <script>
            $(document).ready(function() {

                $('.datepicer_date').datepicker({
                    dateFormat: "yy/mm/dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                });
            });
        </script>

    </x-slot>
</x-base>
