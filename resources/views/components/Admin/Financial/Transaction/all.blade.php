<x-base>
    <x-slot name="title">
        @t(لیست تراکنش ها)
    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">

        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/mobiscroll/mobiscroll.javascript.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/app-invoice.css">

    </x-slot>

    <section class="invoice-list-wrapper">
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(لیست تراکنش ها)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
                                            class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@t(مالی) </a>
                                </li>
                                <li class="breadcrumb-item active">@t(لیست تراکنش ها)
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <div class="action-dropdown-btn d-none">
            <div class="dropdown invoice-filter-action">
                <button class="btn border dropdown-toggle mr-1" type="button" id="invoice-filter-btn"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    فیلترها
                </button>
                <div class="dropdown-menu" aria-labelledby="invoice-filter-btn">
                    <a class="dropdown-item" href="#">دریافت شده</a>
                    <a class="dropdown-item" href="#">ارسال شده</a>
                    <a class="dropdown-item" href="#">پرداخت اقساطی</a>
                    <a class="dropdown-item" href="#">پرداخت شده</a>
                </div>
            </div>
            <div class="dropdown invoice-options">
                <button class="btn border dropdown-toggle mr-2" type="button" id="invoice-options-btn"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    اختیارات
                </button>
                <div class="dropdown-menu" aria-labelledby="invoice-options-btn">
                    <a class="dropdown-item" href="#">حذف</a>
                    <a class="dropdown-item" href="#">ویرایش</a>
                    <a class="dropdown-item" href="#">مشاهده</a>
                    <a class="dropdown-item" href="#">ارسال</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">

                
                <div class="table-responsive ">
                    <table class="table invoice-data-table nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>@t(شماره ثبت)</th>
                                <th>@t(نام و نام خانوادگی)</th>
                                <th>@t(مبلغ)</th>
                                <th>@t(باقی مانده)</th>
                                <th>@t(نوع تراکنش)</th>
                                <th>@t(حساب)</th>
                                <th>@t(تاریخ ثبت)</th>
                                <th>@t(اپراتور)</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>@t(شماره ثبت)</th>
                                <th>@t(نام و نام خانوادگی)</th>
                                <th>@t(مبلغ)</th>
                                <th>@t(باقی مانده)</th>
                                <th>@t(نوع تراکنش)</th>
                                <th>@t(حساب)</th>
                                <th>@t(تاریخ ثبت)</th>
                                <th>@t(اپراتور)</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/mobiscroll/mobiscroll.javascript.min.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.time.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>

        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script>
            $(document).ready(function() {

                $.fn.dataTable.pipeline = function(opts) {
                // Configuration options
                var conf = $.extend({
                    pages: 2, // number of pages to cache
                    url: '', // script url
                    data: null, // function or object with parameters to send to the server
                    method: 'GET' // Ajax HTTP method
                }, opts);

                // Private variables for storing the cache
                var cacheLower = -1;
                var cacheUpper = null;
                var cacheLastRequest = null;
                var cacheLastJson = null;

                return function(request, drawCallback, settings) {
                    var ajax = false;
                    var requestStart = request.start;
                    var drawStart = request.start;
                    var requestLength = request.length;
                    var requestEnd = requestStart + requestLength;

                    if (settings.clearCache) {
                        // API requested that the cache be cleared
                        ajax = true;
                        settings.clearCache = false;
                    } else if (cacheLower < 0 || requestStart < cacheLower || requestEnd >
                        cacheUpper) {
                        // outside cached data - need to make a request
                        ajax = true;
                    } else if (JSON.stringify(request.order) !== JSON.stringify(cacheLastRequest
                            .order) ||
                        JSON.stringify(request.columns) !== JSON.stringify(cacheLastRequest
                            .columns) ||
                        JSON.stringify(request.search) !== JSON.stringify(cacheLastRequest.search)
                    ) {
                        // properties changed (ordering, columns, searching)
                        ajax = true;
                    }

                    // Store the request for checking next time around
                    cacheLastRequest = $.extend(true, {}, request);

                    if (ajax) {
                        // Need data from the server
                        if (requestStart < cacheLower) {
                            requestStart = requestStart - (requestLength * (conf.pages - 1));

                            if (requestStart < 0) {
                                requestStart = 0;
                            }
                        }

                        cacheLower = requestStart;
                        cacheUpper = requestStart + (requestLength * conf.pages);

                        request.start = requestStart;
                        request.length = requestLength * conf.pages;

                        // Provide the same `data` options as DataTables.
                        if (typeof conf.data === 'function') {
                            // As a function it is executed with the data object as an arg
                            // for manipulation. If an object is returned, it is used as the
                            // data object to submit
                            var d = conf.data(request);
                            if (d) {
                                $.extend(request, d);
                            }
                        } else if ($.isPlainObject(conf.data)) {
                            // As an object, the data given extends the default
                            $.extend(request, conf.data);
                        }

                        return $.ajax({
                            "type": conf.method,
                            "url": conf.url,
                            "data": request,
                            "dataType": "json",
                            "cache": false,
                            "success": function(json) {
                                cacheLastJson = $.extend(true, {}, json);

                                if (cacheLower != drawStart) {
                                    json.data.splice(0, drawStart - cacheLower);
                                }
                                if (requestLength >= -1) {
                                    json.data.splice(requestLength, json.data.length);
                                }

                                drawCallback(json);
                            }
                        });
                    } else {
                        json = $.extend(true, {}, cacheLastJson);
                        json.draw = request.draw; // Update the echo for each response
                        json.data.splice(0, requestStart - cacheLower);
                        json.data.splice(requestLength, json.data.length);

                        drawCallback(json);
                    }
                }
            };

            // Register an API method that will empty the pipelined data, forcing an Ajax
            // fetch on the next draw (i.e. `table.clearPipeline().draw()`)
            $.fn.dataTable.Api.register('clearPipeline()', function() {
                return this.iterator('table', function(settings) {
                    settings.clearCache = true;
                });
            });
                // Setup - add a text input to each footer cell

                $('.invoice-data-table thead tr').clone(true).appendTo('.invoice-data-table thead');
                $('.invoice-data-table thead tr:eq(1) th').each(function(i) {
                    var title = $(this).text();
                    if (title != 'عملیات') {
                        $(this).html('<input type="text" placeholder="جست جو در ' + title +
                            '"  class="form-control form-control-sm" />');
                    }
                    if (title == 'حساب') {
                        $(this).html(
                            '<select class="form-control form-control-sm" placeholder="جست جو در ' +
                            title + '">' +
                            '<option value="" selected>همه ی صندوق ها</option>' +
                            @foreach ($cashes as $cash)
                                '<option value="{{ $cash->name }}">{{ $cash->name }}</option>'+
                            @endforeach '</select>'
                        );
                    }
                    if (title == 'اپراتور') {
                        $(this).html(
                            '<select class="form-control form-control-sm" placeholder="جست جو در ' +
                            title + '">' +
                            '<option value="" selected>همه ی اپراتور ها</option>' +
                            @foreach ($admins as $admin)
                                '<option value="{{ $admin->name . ' ' . $admin->family }}">{{ $admin->name . ' ' . $admin->family }}</option>'+
                            @endforeach '</select>'
                        );
                    }
                    if (title == 'نوع تراکنش') {
                        $(this).html(
                            '<select class="form-control form-control-sm" placeholder="جست جو در ' +
                            title + '">' +
                            '<option value="" selected>همه ی تراکنش ها</option>' +
                            @foreach ($transaction_types as $transaction_type)
                                '<option value="{{ $transaction_type->name }}">{{ $transaction_type->name }}</option>'+
                            @endforeach '</select>'
                        );
                    }
                });

                var table = $('.invoice-data-table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "orderCellsTop": true,
                    "order": [
                        [0, "desc"]
                    ],
                    "fixedHeader": true,
                    "ajax":  "{{ route('transaction.get_ajax') }}",
                    "language": persian,
                    "columns": [{
                            "data": "id",
                            "name": 'id'
                        },
                        {
                            data: null,
                            'name': 'fullname',
                            render: function(data, type, row) {
                                return data.user.name + ' ' +data.user.family;
                            },
                        },
                        {
                            data: null,
                            'name': 'amount',
                            render: function(data, type, row) {
                                return `
                                        <span dir="ltr">`+data.amount+`</span>
                                        <span class="badge badge-light-danger badge-pill">تومان</span>`;
                            },
                        },
                        {
                            data: null,
                            'name': 'amount_standing',
                            render: function(data, type, row) {
                                return `
                                        <span dir="ltr">`+data.amount_standing+`</span>
                                        <span class="badge badge-light-danger badge-pill">تومان</span>`;
                            },
                        },
                        {
                            "data": null,
                            "name": 'transaction_type',
                            render: function(data, type, row) {
                                n = " - ";
                                if (data.transaction_type) {
                                    n = data.transaction_type.name;
                                }
                                return `<span dir="ltr">`+n+`<span>`;
                            },
                        },
                        {
                            "data": null,
                            "name": 'cash',
                            render: function(data, type, row) {
                                n = " - ";
                                if (data.cash) {
                                    n = data.cash.name;
                                }
                                return `<span dir="ltr">`+n+`<span>`;
                            },
                        },
                        {
                            "data": "created_at",
                            "name": 'created_at'
                        },
                        {
                            data: null,
                            "name": 'operator',
                            render: function(data, type, row) {
                                return data.operator.name + ' ' +data.operator.family;
                            },
                        }
                    ],
                    "dom": '<"col-12"<"d-flex justify-content-between"lp><t><"d-flex justify-content-between"ip>>',
                    initComplete: function() {
                        // Apply the search
                        this.api().columns().every(function() {
                            var that = this;
                            $('input', $(
                                '.invoice-data-table thead tr:eq(1) th')[
                                this
                                .index()]).on('keyup change clear',
                                function() {
                                    if (that.search() !== this.value) {
                                        that
                                            .search(this.value)
                                            .draw();
                                    }
                                });
                            $('select', $(
                                '.invoice-data-table thead tr:eq(1) th'
                            )[this
                                .index()]).on('keyup change clear',
                                function() {
                                    if (that.search() !== this.value) {
                                        that
                                            .search(this.value)
                                            .draw();
                                    }
                                });
                        });
                        $('[data-toggle="tooltip"]').tooltip()
                    }
                });


                // $('.shamsi-datepicker-list').datepicker({
                //     dateFormat: "yy/mm/dd",
                //     showOtherMonths: true,
                //     selectOtherMonths: true,
                //     changeMonth: true,
                //     changeYear: true,
                //     showButtonPanel: true
                // });


                // mobiscroll.setOptions({
                //     locale: mobiscroll
                //         .localeFa, // Specify language like: locale: mobiscroll.localePl or omit setting to use default
                //     theme: 'ios', // Specify theme like: theme: 'ios' or omit setting to use default
                //     themeVariant: 'light' // More info about themeVariant: https://docs.mobiscroll.com/5-5-1/javascript/calendar#opt-themeVariant
                // });


                // mobiscroll.datepicker('#demo-date-def', {
                //     controls: [
                //         'calendar'
                //     ],
                //     selectMultiple: true
                // });
            });
            // $('#demo-date-def').on('keyup change clear', function() {
            //     x = this.value.replace(/,/g, ' |');
            //     x = x.replace(/ /g, '"');
            //     x = '"' + x + '"'
            //     x = "$('.invoice-data-table').DataTable().column(6).search(" + x + ").draw();";
            //     new Function(x)();
            // });
        </script>
        <!-- END: Page JS-->

    </x-slot>
</x-base>
