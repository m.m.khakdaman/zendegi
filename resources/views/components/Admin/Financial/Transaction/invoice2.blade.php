<x-base>
    <x-slot name="@t(ویرا)">

    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/app-invoice.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/animate/animate.css">
        <!-- END: Page CSS-->

    </x-slot>
    <section class="invoice-edit-wrapper">
        <div class="row">
            <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@t(صورتحساب){{ ' ' . $customer->name . ' ' . $customer->family }}</h4>
                </div>
                <div class="card-content">
                <div class="card-body">
                    <div class="row">
                            {{-- <p>بوت استرپ 6 طرح از پیش تعریف شده برای دکمه ها دارد که هرکدام کاربرد خاص خودشان را دارند.</p> --}}
                            <!-- basic buttons -->
                        <div class="col-2">
                            <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#naghdi">
                                <i class="bx bx-money"></i>
                                <span>پرداخت نقدی</span>
                            </button>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#kartkhan">
                                <i class="bx bx-hdd"></i>
                                <span class="text-nowrap">کارت خوان</span>
                            </button>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#kart_to_kart">
                                <i class="bx bx-credit-card"></i>
                                <span>کارت به کارت </span>
                            </button>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-light-secondary btn-block" data-toggle="modal" data-target="#internet">
                                <i class="bx bx-wifi"></i>
                                <span class="text-nowrap">پرداخت اینترنتی</span>
                            </button>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#pardakht_be_moraje">
                                <i class="bx bx-money"></i>
                                <span>عودت به مراجع</span>
                            </button>
                        </div>
                        <div class="col-2">
                            <div class="invoice-action-btn mb-1">
                                <button class="btn btn-primary btn-block">
                                    <i class="bx bx-send"></i>
                                    <small>@t(ارسال صورتحساب)</small>
                                </button>
                            </div>
                        </div>                            
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('transaction.offset_store') }}" method="POST" id="form_offset">
                                <div class="invoice-action-btn mb-1">
                                    @csrf
                                    <input type="hidden" name="sales_selected" id="offset_sales_selected">
                                    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                    <button class="btn btn-success btn-block" type="submit">
                                        <i class="bx bx-transfer"></i>
                                        <span>تهاتر</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div>
                                <table class="table-bordered table-hover text-secondary" style="width: 100%">
                                    <thead>
                                        <tr class=" text-center">
                                            <td>#</td>
                                            <td></td>
                                            <th>@t(نوع تراکنش)</th>
                                            <th>@t(محصول)</th>
                                            <th>@t(بدهکار)</th>
                                            <th>@t(بستانکار)</th>
                                            <th>@t(باقی مانده)</th>
                                            <th>@t(تاریخ)</th>
                                            <th>@t(اپراتور)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $tmp = 0;
                                        @endphp
                                        @foreach ($transactions as $item)
                                            @switch( $item->transaction_type_id )
                                                @case(1)
                                                    <tr>
                                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ $tmp++ }}</span></td>                                                        
                                                        <td class="text-center">                                                            
                                                            <input type="checkbox" name="checkff[]" value="1"
                                                                {{ $sale ? ($item->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                                id="{{ $item->id }}">
                                                            <label for="{{ $item->id }}"></label>
                                                        </td>                                                
                                                        <td>@t(پرداخت وجه نقد)</td>                                                
                                                        <td>{{ $item->sale->advisor->name ?? '' }}&nbsp;{{ $item->sale->advisor->family ?? '' }}</td>                                                
                                                        <td></td>
                                                        <td class="text-center">{{ $item->amount }}</td>                                                
                                                        <td>{{ $item->amount_standing }}</td>                                                
                                                        <td> {{ verta($item->created_at)->format('Y/m/d') }}</td>                                                
                                                        <td class="text-center">{{  $item->operator->name ?? '' }}&nbsp;{{  $item->operator->family ?? '' }}</td>

                                                    </tr>
                                                    @break
                                                @case(2)
                                                    <tr>                                                        
                                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ $tmp++ }}</span></td>                                                        
                                                        <td class="text-center">                                                            
                                                            <input type="checkbox" name="checkff[]" value="1"
                                                                {{ $sale ? ($item->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                                id="{{ $item->id }}">
                                                            <label for="{{ $item->id }}"></label>
                                                        </td>                                                
                                                        <td>@t(اینترنتی)</td>                                                
                                                        <td>{{ $item->sale->advisor->name ?? '' }}&nbsp;{{ $item->sale->advisor->family ?? '' }}</td>                                                
                                                        <td>{{ $item->amount }}</td>                                                
                                                        <td>{{ $item->amount_standing }}</td>                                                
                                                        <td> {{ verta($item->deposit_date)->format('Y/m/d') }}</td>                                                
                                                        <td class="text-center">{{  $item->operator->name ?? '' }}&nbsp;{{  $item->operator->family ?? '' }}</td>

                                                    </tr>
                                                    @break
                                                @case(3)
                                                    <tr>                                                        
                                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ $tmp++ }}</span></td>                                                        
                                                        <td class="text-center">                                                            
                                                            <input type="checkbox" name="checkff[]" value="1"
                                                                {{ $sale ? ($item->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                                id="{{ $item->id }}">
                                                            <label for="{{ $item->id }}"></label>
                                                        </td>                                                
                                                        <td>@t(ارائه یاری برگ)</td>                                                
                                                        <td>{{ $item->sale->advisor->name ?? '' }}&nbsp;{{ $item->sale->advisor->family ?? '' }}</td>                                                
                                                        <td></td>
                                                        <td class="currency text-center">{{ $item->amount }}</td>                                                
                                                        <td>{{ $item->amount_standing }}</td>                                                
                                                        <td> {{ verta($item->deposit_date)->format('Y/m/d') }}</td>                                                
                                                        <td class="text-center">{{  $item->operator->name ?? '' }}&nbsp;{{  $item->operator->family ?? '' }}</td>

                                                    </tr>
                                                    @break 
                                                @case(4)
                                                    <tr>                                                        
                                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ $tmp++ }}</span></td>                                                        
                                                        <td class="text-center">                                                            
                                                            <input type="checkbox" name="checkff[]" value="1"
                                                                {{ $sale ? ($item->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                                id="{{ $item->id }}">
                                                            <label for="{{ $item->id }}"></label>
                                                        </td>                                                
                                                        <td>@t(پرداخت کارت به کارتی)</td>   
                                                        <td>{{ $item->sale->advisor->name ?? '' }}&nbsp;{{ $item->sale->advisor->family ?? '' }}</td>   
                                                        <td></td>                                             
                                                        <td class="currency text-center">{{ $item->amount }}</td>                                                
                                                        <td>{{ $item->amount_standing }}</td>                                                
                                                        <td> {{ verta($item->deposit_date)->format('Y/m/d') }}</td>                                                
                                                        <td class="text-center">{{  $item->operator->name ?? '' }}&nbsp;{{  $item->operator->family ?? '' }}</td>

                                                    </tr>
                                                    @break 
                                                @case(5)
                                                    <tr>                                                        
                                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ $tmp++ }}</span></td>                                                        
                                                        <td class="text-center">                                                            
                                                            <input type="checkbox" name="checkff[]" value="1"
                                                                {{ $sale ? ($item->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                                id="{{ $item->id }}">
                                                            <label for="{{ $item->id }}"></label>
                                                        </td>                                                
                                                        <td>@t(شارژ حساب با کارت خوان)</td>   
                                                        <td>{{ $item->sale->advisor->name ?? '' }}&nbsp;{{ $item->sale->advisor->family ?? '' }}</td>   
                                                        <td></td>                                             
                                                        <td class="currency text-center">{{ $item->amount }}</td>                                                
                                                        <td>{{ $item->amount_standing }}</td>                                                
                                                        <td> {{ verta($item->deposit_date)->format('Y/m/d') }}</td>                                                
                                                        <td class="text-center">{{  $item->operator->name ?? '' }}&nbsp;{{  $item->operator->family ?? '' }}</td>
                                                    </tr>
                                                @break
                                                    @case(7)
                                                    <tr>                                                        
                                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ $tmp++ }}</span></td>                                                        
                                                        <td class="text-center">                                                            
                                                            <input type="checkbox" name="checkff[]" value="1"
                                                                {{ $sale ? ($item->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                                id="{{ $item->id }}">
                                                            <label for="{{ $item->id }}"></label>
                                                        </td>                                                
                                                        <td>@t(عودت وجه به مراجع)</td>   
                                                        <td>{{ $item->sale->advisor->name ?? '' }}&nbsp;{{ $item->sale->advisor->family ?? '' }}</td>   
                                                        <td></td>                                             
                                                        <td class="currency text-center">{{ $item->amount }}</td>                                                
                                                        <td>{{ $item->amount_standing }}</td>                                                
                                                        <td> {{ verta($item->deposit_date)->format('Y/m/d') }}</td>                                                
                                                        <td class="text-center">{{  $item->operator->name ?? '' }}&nbsp;{{  $item->operator->family ?? '' }}</td>
                                                    </tr>
                                                    @break                                                       
                                                @default
                                                <tr>                                                        
                                                    <td><span data-toggle="tooltip" data-placement="top" title="{{ $item->id }}">{{ $tmp++ }}</span></td>                                                        
                                                    <td class=" text-center">
                                                        <input type="checkbox" name="checkff[]" value="1"
                                                            {{ $sale ? ($item->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                            id="{{ $item->id }}">
                                                        <label for="{{ $item->id }}"></label>
                                                    </td>                                                
                                                    <td>
                                                        @t(خرید)
                                                        {{ $item->sale->products_type->product_type ?? '' }}
                                                    </td>                                                
                                                    <td>{{ $item->sale->advisor->name ?? '' }}&nbsp;{{ $item->sale->advisor->family ?? '' }}</td>                                                
                                                    <td class="text-center currency">{{ $item->amount * -1 }}</td> 
                                                    <td></td>                                               
                                                    <td>{{ $item->amount_standing }}</td>                                                
                                                    <td> {{ verta($item->deposit_date)->format('Y/m/d') }}</td>                                                
                                                    <td class="text-center">{{  $item->operator->name ?? '' }}&nbsp;{{  $item->operator->family ?? '' }}</td>
                                                </tr>
                                            @endswitch                                               
                                                                                            
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    <!--naghdi Modal start-->
        <div class="modal fade" id="naghdi" tabindex="-1" role="dialog" aria-labelledby="naghdiTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h4 class="modal-title" id="naghdiTitle">پرداخت نقدی صورتحساب
                            {{ $customer->name . ' ' . $customer->family }}
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('transaction.cash_payment_store') }}" method="POST">
                            @csrf
                            <input type="hidden" name="sales_selected" id="cash_payment_sales_selected">
                            <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                            <input type="hidden" name="transaction_type_id" value="1">

                            <div class="row">
                                @foreach ($accounts[1] as $account)
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-success radio-glow">
                                                <input type="radio" id="_{{ $account->id }}" name="account" required
                                                    value="{{ $account->id }}">
                                                <label
                                                    for="_{{ $account->id }}">{{ $account->name }}</label>
                                            </div>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <label>@t(مبلغ دریافتی) </label>
                            <div class="form-group col-12">
                                <input type="price" required name="price" id="cash_payment_price" placeholder="@t(مبلغ دریافتی)" class="form-control">
                            </div>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <label>@t(تاریخ دریافت وجه)</label>
                                <div class="form-group">
                                    <div class="controls form-label-group ">
                                        <input type="text" class="form-control datepicer_date" name="date" id="cash_payment_daet"
                                            data-validation-pattern-message="فرمت فیلد معتبر نیست." required
                                            value="{{ verta()->format('Y/m/d') }}" placeholder="تاریخ دریافت وجه">
                                        <div class="form-control-position">
                                            <i class="bx bx-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <label>@t(ساعت دریافت)</label>
                                <input type="time" id="cash_payment_time" name="time" placeholder="@t(ساعت دریافت وجه)" value="" class="form-control">
                            </div>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <label>@t(توضیحات)</label>
                                <textarea class="form-control" name="comment" rows="2" placeholder="@t(توضیحات)"></textarea>
                            </div>
                            <input type="submit" style="display: none;" id="submit_cash_payment_store">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">بستن</span>
                        </button>
                        <button type="submit" class="btn btn-success ml-1"  onclick="$('#submit_cash_payment_store').click()">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">ثبت</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <!--naghdi Modal end-->

    <!-- kartkhan Modal start-->
        <div class="modal fade text-left" id="kartkhan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('transaction.cash_payment_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="sales_selected" class="sales_selected">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <input type="hidden" name="transaction_type_id" value="5">
                        <div class="modal-header bg-warning">
                            <h4 class="modal-title" id="myModalLabel">
                                <span>@t(پرداخت صورت حساب)&nbsp;{{ $customer->name . ' ' . $customer->family }}&nbsp;@t(با دستگاه پوز)</span>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                @foreach ($accounts[5] as $account)
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-warning radio-glow">
                                                <input type="radio" id="{{ $account->id }}" name="account" required
                                                    value="{{ $account->id }}">
                                                <label
                                                    for="{{ $account->id }}">{{ $account->name ?? '' }}</label>
                                            </div>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            
                            <label class="pt-1">@t(مبلغی که)&nbsp;{{ $customer->name . ' ' . $customer->family }}&nbsp;@t(کارت کشیده است.) </label>
                            <div class="form-group  ">
                                <input type="text" placeholder="@t(مبلغ واریزی)" name="price" class="form-control">
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>@t(تاریخ کارت کشیدن)</label>
                                        <div class="controls form-label-group ">
                                            <input type="text" class="form-control datepicer_date" style="direction: ltr"
                                                    name="date" value="{{  verta()->format('Y/m/d') }}" id="kartkhan_date"
                                                    required placeholder="تاریخ کارت کشیدن)">
                                        </div>
                                    </div>
                            
                                </div>
                                <div class="col-6">
                                    <label>@t(ساعت کارت کشیدن)</label>
                                    <div class="form-group">
                                        <input type="time" placeholder="@t(ساعت کارت کشیدن)" class="form-control" name="time">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>@t(کد رهگیری)</label>
                                        <div class="controls form-label-group ">
                                            <input type="text" class="form-control" name="tracking_code" value="" style="direction: ltr" required>
                                        </div>
                                    </div>
                            
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="disabledSelect">تاریخ و ساعت ثبت </label>
                                        <input class="form-control" style="direction: ltr" id="disabledInput" type="text" value="{{ verta()->format("Y/m/d H:i") }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="comment" placeholder="@t(توضیحات)" rows="2"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">ثبت</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <!-- kart khan Modal end -->

    <!-- kartbekart Modal start-->
        <div class="modal fade text-left" id="kart_to_kart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('transaction.cash_payment_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="sales_selected" class="sales_selected">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <input type="hidden" name="transaction_type_id" value="4">

                        <div class="modal-header bg-warning">
                            <h4 class="modal-title" id="myModalLabel">کارت به کارت</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-warning">
                                <p>
                                    در صورت ثبت مبلغ کمتر باقیمانده بعنوان بدهی مراجع ثبت می شود.
                                </p>
                            </div>
                            <div class="row">
                                @foreach ($accounts[4] as $account)
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-success radio-glow">
                                                <input type="radio" id="_{{ $account->id }}" name="account" required
                                                    value="{{ $account->id }}">
                                                <label
                                                    for="_{{ $account->id }}">{{ $account->name . ': ' . $account->account_number }}</label>
                                            </div>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label for="disabledSelect">تاریخ و ساعت ثبت </label>
                                <input class="form-control" id="disabledInput" type="text"
                                    placeholder="{{ verta() }}" disabled>
                            </div>
                            <label for="_price">@t(مبلغ واریزی) </label>
                            <div class="form-group col-12">
                                <input type="text" id="_price" placeholder="@t(مبلغ واریزی)" name="price"
                                    class="form-control price">
                            </div>
                            <label for="_date">@t(تاریخ واریز وجه)</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <fieldset class="d-flex">
                                    <input type="text" class="form-control date pickadate" id="_date" name="date"
                                        placeholder="@t(تاریخ واریز وجه)" value="{{ verta()->format('Y/m/d') }}">
                                </fieldset>
                            </div>
                            <label for="_time">@t(ساعت واریز وجه)</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <input type="time" placeholder="@t(ساعت واریز وجه)" id="_time"
                                    class="form-control time" name="time">
                            </div>
                            <label for="_comment">توضیحات</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <textarea class="form-control" name="comment" rows="2"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-warning">ثبت</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
    <!-- kartbekart Modal end -->

    <!-- internet Modal start -->
        <div class="modal fade" id="internet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('transaction.internet_peyment') }}" method="POST">
                        @csrf
                        <input type="hidden" name="sales_selected" class="sales_selected">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <div class="modal-header bg-danger">
                            <h4 class="modal-title" id="myModalLabel">پرداخت اینترنتی
                                صورتحساب{{ ' ' . $customer->name . ' ' . $customer->family }}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger">در صورت دریافت مبلغ کمتر باقیمانده دبعنوان بدهی مراجع ثبت
                                می
                                شود..</div>
                            <label for="">@t(مبلغ پرداخت)</label>
                            <div class="form-group input-group">
                                <input type="tel" dir="ltr" name="price" class="form-control price">
                            </div>
                            <label>توضیحات</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <textarea class="form-control" name="comment" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">@t(ارسال لینک
                                پرداخت)</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <!-- internet Modal end -->

    <!--pardakht_be_moraje Modal start-->
        <div class="modal fade" id="pardakht_be_moraje" tabindex="-1" role="dialog" aria-labelledby="pardakht_be_morajeTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-secondary">
                        <h4 class="modal-title text-danger" id="pardakht_be_morajeTitle">پرداخت به مراجع
                            {{ $customer->name . ' ' . $customer->family }}
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('transaction.pardakht_be_moraje') }}" id="form_pardakht_be_moraje"
                            method="POST">
                            @csrf
                            <input type="hidden" name="sales_selected" id="pardakht_be_moraje_sales_selected">
                            <input type="hidden" name="customer_id" value="{{ $customer->id }}">

                            <div class="modal-body">
                                <div class="row">
                                    @foreach ($accounts[1] as $account)
                                        <div class="col-12">
                                            <fieldset>
                                                <div class="radio radio-success radio-glow">
                                                    <input type="radio" id="__{{ $account->id }}" name="accounts"
                                                        required value="{{ $account->id }}">
                                                    <label
                                                        for="__{{ $account->id }}">{{ $account->name . ' ' . $account->account_number }}</label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    @endforeach
                                </div>
                                <label>@t(مبلغ دریافتی) </label>
                                <div class="form-group col-12">
                                    <input type="price" required name="price" id="pardakht_be_moraje_price"
                                        placeholder="@t(مبلغ دریافتی)" class="form-control">
                                </div>

                                <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                    <label>@t(تاریخ پرداخت وجه)</label>
                                    <fieldset class="d-flex">
                                        <input type="text" name="pardakht_be_moraje_date"
                                            class="form-control pickadate" placeholder="@t(تاریخ دریافت وجه)"
                                            value="{{ verta()->format('Y/m/d') }}">
                                    </fieldset>
                                </div>
                                <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                    <label>@t(ساعت پرداخت)</label>
                                    <input type="time" id="pardakht_be_moraje_time" name="pardakht_be_moraje_time"
                                        placeholder="@t(ساعت دریافت وجه)" value="" class="form-control">
                                </div>
                                <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                    <label>@t(توضیحات)</label>
                                    <textarea class="form-control" name="comment" rows="2"
                                        placeholder="@t(توضیحات)"></textarea>
                                </div>
                                <input type="submit" style="display: none;" id="submit_form_pardakht_be_moraje">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">بستن</span>
                        </button>
                        <button type="submit" class="btn btn-secondary ml-1"
                            onclick="$('#submit_form_pardakht_be_moraje').click()">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">ثبت</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade text-left" id="pardakht_be_moraje" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                </div>
            </div>
        </div>
    <!--pardakht_be_moraje Modal end-->
      
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <script src="/assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js?FWEFWFW"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        {{-- <script src="/assets/js/scripts/pages/app-invoice.js"></script> --}}

        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>

        <!-- END: Page JS-->

        <script>
            $('.currency').each(function(i, o) {
                $(o).html(numeral($(o).text()).format('0,0'));
            })
        </script>

        <script>            
            $(document).ready(function() {
                $('.datepicer_date').datepicker({
                    dateFormat: "yy/mm/dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                });
            });
        </script>





        <script>
            function change_price() {
                val = 0;
                $("input[name^='checkff[']:checked").each(function(index, value) {
                    val = val + (parseInt(document.getElementById('price[' + value.id + ']').innerHTML) < 0 ?
                        parseInt(document.getElementById('price[' + value.id + ']').innerHTML) : 0)
                })
                return val;
            }

            function slected_sales() {
                val = [];
                $("input[name^='checkff[']:checked").each(function(index, value) {
                    val[index] = value.id;
                })
                return val;
            }
            $("input[name^='checkff[']").on('click', function() {
                $('#price_total').html(change_price());
                $('#cash_payment_price').val((-1) * parseInt(change_price()));
                $('#cash_payment_sales_selected').val(slected_sales);
                $('#pardakht_be_moraje_sales_selected').val(slected_sales);
                $('.price').val((-1) * parseInt(change_price()));
                $('#offset_sales_selected').val(slected_sales);
                $('.sales_selected').val(slected_sales);
            });
            $('#price_total').html(change_price());
            $('.price').val((-1) * parseInt(change_price()));
            $('.sales_selected').val(slected_sales);
            $('#cash_payment_price').val((-1) * parseInt(change_price()));
            $('#cash_payment_sales_selected').val(slected_sales);
            $('#pardakht_be_moraje_sales_selected').val(slected_sales);
            time = new Date();
            // cash_payment_sales_selected
            $('.time').val(time.getHours() + ':' + time.getMinutes());
            $('#cash_payment_time').val(time.getHours() + ':' + time.getMinutes());
            $('#pardakht_be_moraje_time').val(time.getHours() + ':' + time.getMinutes());
        </script>
        <script>
            $('.invoice-data-table #thead tr').clone(true).appendTo('.invoice-data-table #thead');
            $('.invoice-data-table thead:eq(0) tr:eq(1) th').each(function(i) {
                var title = $(this).text();
                // if (title != 'توضیحات' && title != 'جزئیات') {
                //     $(this).html('<input type="text" placeholder="جست جو در ' + title +
                //         '"  class="form-control form-control-sm" />');

                // }
                // if (title == 'نوع محصول') {
                //     console.log(this);
                //     $(this).html('<select class="form-control">' +
                //         '<option value="">جنسیت</option>' +
                //         '<option value="نامشخص">نامشخص</option>' +
                //         '<option value="مرد">مرد</option>' +
                //         '<option value="زن">زن</option>' +
                //         '</select>');
                // }
            });
            $('.invoice-data-table').DataTable({
                language: {
                    "sEmptyTable": "هیچ داده‌ای در جدول وجود ندارد",
                    "sInfo": "نمایش _START_ تا _END_ از _TOTAL_ ردیف",
                    "sInfoEmpty": "نمایش 0 تا 0 از 0 ردیف",
                    "sInfoFiltered": "(فیلتر شده از _MAX_ ردیف)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "نمایش _MENU_ ردیف",
                    "sLoadingRecords": "در حال بارگزاری...",
                    "sProcessing": "در حال پردازش...",
                    "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                    "oPaginate": {
                        "sFirst": "برگه‌ی نخست",
                        "sLast": "برگه‌ی آخر",
                        "sNext": "بعدی",
                        "sPrevious": "قبلی"
                    },
                    "oAria": {
                        "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                        "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                    },
                    "sSearch": "",
                    "sSearchPlaceholder": "جستجوی",
                },
                rowCallback: function(row, data) {
                    console.log(data[4]);
                    if (data[4] > 0 ) {
                        $(row).addClass('btn-light-success');
                    }else{
                        $(row).addClass('btn-light-danger');
                    }
                    /*
                    //if it is not the summation row the row should be selectable
                    if (data.cashflow.position !== 'L') {
                        $(row).addClass('selectRow');
                    }*/
                },

                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        if (column.header().childNodes[0].data == 'نوع محصول') {
                            var select = $('<select><option value=""></option></select>')
                                .appendTo($(column.header()).empty())
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function(d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        if (column.header().childNodes[0].data == 'مشاور' || column.header().childNodes[0]
                            .data == 'توسط') {
                            var select = $('<input list="browsers"><datalist id="browsers"></datalist>')
                                .appendTo($(column.header()).empty())
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, true)
                                        .draw();
                                });

                            column.data().unique().sort().each(function(d, j) {
                                $('#browsers').append('<option value="' + d + '">' + d +
                                    '</option>')
                            });
                        }


                        var that = this;
                        $('input', $('.invoice-data-table thead:eq(0) tr:eq(1) th')[this.index()]).on(
                            'keyup change clear',
                            function() {
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                        $('select', $('.invoice-data-table thead:eq(0) tr:eq(1) th')[this
                            .index()]).on('keyup change clear',
                            function() {
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                    });
                    // $('[data-toggle="tooltip"]').tooltip()
                }
            });
        </script>

        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
    </x-slot>
</x-base>
