<x-base title="@t(ویرایش تراکنش)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(ویرایش تراکنش)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('transaction.all') }}">
                                    تراکنش ها
                                </a>
                            </li>
                            <li class="breadcrumb-item active">@t(ویرایش تراکنش)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="invoice-create-btn mb-1">
                            <h1 class="faq-title text-center mb-3">@t(ویرایش تراکنش )
                                {{ $transaction->user->name . ' ' . $transaction->user->family }}</h1>
                            <ul>
                                <li>مبلغ {{ $transaction->amount_standing }} باقی مانده</li>
                                @foreach ($transaction->sources as $item)
                                    <li>انتقال مبلغ {{ $item->amount }} به تراکنش
                                        {{ $item->distinction_transaction_id }}
                                        <form href="{{ route('transaction.back_transaction',$item->id) }}" method="POST" id="_form_{{ $item->id }}">
                                            @csrf
                                            <input type="submit" value="بازگشت" formaction="{{ route('transaction.back_transaction',$item->id) }}">
                                        </form>
                                    </li> 
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action={{ route('transaction.update', $transaction->id) }}" method="post">
                            @csrf
                            <div class="row pl-5">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <input type="text" class="form-control datepicer_date" name="amount"
                                                id="amount" data-validation-pattern-message="فرمت فیلد معتبر نیست."
                                                required value="{{ $transaction->amount }}" placeholder="مبلغ">
                                            <div class="form-control-position">
                                                <i class="bx bx-money">
                                                </i>
                                            </div>
                                            <label for="amount">مبلغ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <select name="cash" id="cash" required class="select2">
                                                @foreach ($cashes as $cash)
                                                    <option value="{{ $cash->id }}"
                                                        {{ $cash->id == $transaction->cash_id ? 'selected' : null }}>
                                                        {{ $cash->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-control-position">
                                                <i class="bx bx-cash">
                                                </i>
                                            </div>
                                            <label for="cash">صندوق</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <textarea type="text" class="form-control datepicer_date" name="comment"
                                                id="comment" data-validation-pattern-message="فرمت فیلد معتبر نیست."
                                                required placeholder="توضیحات">{{ $transaction->comment }}</textarea>
                                            <div class="form-control-position">
                                                <i class="bx bx-money">
                                                </i>
                                            </div>
                                            <label for="comment">توضیحات</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button type="submit"
                                        class="btn btn-icon rounded-circle btn-success glow mr-1 mb-3"><i
                                            class="bx bx-printer"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js">
        </script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js">
        </script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js">
        </script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js">
        </script>
        <script src="/assets/js/core/app-menu.js">
        </script>
        <script src="/assets/js/core/app.js">
        </script>
        <script src="/assets/js/scripts/components.js">
        </script>
        <script src="/assets/js/scripts/footer.js">
        </script>
        <script src="/assets/js/scripts/customizer.js">
        </script>
        <script src="/assets/js/scripts/forms/select/form-select2.js">
        </script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
        <script>
            $(document).ready(function() {});
        </script>

    </x-slot>
</x-base>
