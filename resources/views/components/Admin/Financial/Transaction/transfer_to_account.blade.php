<x-base title="@t(جابجایی بین حساب ها)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(جابجایی بین حساب ها)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('transaction.all') }}">@t(مالی)</a></li>
                            <li class="breadcrumb-item active">@t(جابجایی بین حساب ها)</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                            <h1 class="faq-title text-center mb-3">@t(جابجایی بین حساب ها )
                    </div>
                    <div class="card-body">
                        <form action="{{ route('transaction.transfer_to_account.store') }}" method="POST">
                            @csrf
                            <div class="row pl-md-5 pl-sm-0 pr-md-5 pr-sm-0">
                                <div class="col-md-4">
                                    <div class="form-group">
                                            {{-- <label for="price">مبلغ</label> --}}
                                            <div class="controls form-label-group ">
                                            <input type="number" class="form-control" name="price" id="price" min="0"
                                                data-validation-min-message="خطا!"
                                                data-validation-pattern-message="فرمت فیلد معتبر نیست." required
                                                placeholder="مبلغ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{-- <label for="from_cash_id">از صندوق</label> --}}
                                        <div class="controls form-label-group ">
                                            <select type="text" class="form-control select2" name="from_cash_id"
                                                value="{{ old('from_cash_id') }}" id="from_cash_id" required>
                                                <option value="" disabled selected hidden>@t(از حساب)</option>
                                                @foreach ($cashes as $cashe)
                                                    <option value="{{ $cashe->id }}">
                                                        {{ $cashe->name . ' ' . $cashe->family }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{-- <label for="to_cash_id">به صندوق</label> --}}
                                        <div class="controls form-label-group ">
                                            <select type="text" class="form-control select2" name="to_cash_id"
                                                value="{{ old('to_cash_id') }}" id="to_cash_id" required >
                                                <option value="" disabled selected hidden>@t(به حساب)</option>
                                                @foreach ($cashes as $cashe)
                                                    <option value="{{ $cashe->id }}">
                                                        {{ $cashe->name . ' ' . $cashe->family }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row pl-md-5 pl-sm-0 pr-md-5 pr-sm-0">
                                <div class="col-md-4">
                                    <div class="form-group">
                                            {{-- <label for="fish_num">@t(شماره فیش بانکی)</label> --}}
                                            <div class="controls form-label-group ">
                                            <input type="text" class="form-control" name="fish_num" id="fish_num"
                                                data-validation-pattern-message="فرمت فیلد معتبر نیست." 
                                                placeholder="@t(شماره فیش بانکی)">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{-- <label for="fish_fate">@t(تاریخ فیش بانکی)</label> --}}
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" name="fish_date"  class="form-control  datepicer_date" placeholder="@t(تاریخ فیش بانکی)">
                                            <div class="form-control-position">
                                            <i class="bx bx-calendar"></i>
                                        </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <div class="controls form-label-group ">
                                            <textarea class="form-control" name="comment" id="comment"
                                                data-validation-pattern-message="فرمت فیلد معتبر نیست." 
                                                placeholder="توضیحات"></textarea>
                                            {{-- <label for="comment">توضیحات</label> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success float-right mr-sm-4">ثبت</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->
        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->
        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Theme JS-->
        <script>
            $(document).ready(function() {

                $('.datepicer_date').datepicker({
                    dateFormat: "yy/mm/dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                });
            });
        </script>
    </x-slot>
</x-base>
