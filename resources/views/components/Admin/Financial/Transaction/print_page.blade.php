<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
</head>

<body style="background-image:none; font-family: tahoma;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center">
            <p>
                <b>بسمه تعالی</b>
            </p>
            <p>
                <b>تراکنش ها از تاریخ {{ verta($start)->format('Y/m/d') }} تا تاریخ
                    {{ verta($end)->format('Y/m/d') }}</b>
            </p>
        </div>
        <div class="col-4">

            <div class="row text-center float-right">
                <div>
                    <img src="/assets/images/logo/zendegiaghelane.png" alt="مرکز مشاوره زندگی عاقلانه"
                        class="img-responsive" />
                    <p class="mr-2">مرکز مشاوره زندگی عاقلانه</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 mt-1 mb-2">
                    <h4>تراکنش ها</h4>
                    <hr>
                </div>
            </div>
            <table class="table-bordered" width='100%'>
                <thead>
                    <tr>
                        <th>ردیف</th>
                        @if ($print_advisor)
                            <th>مشاور</th>
                            <th>مدت جلسه</th>
                            <th>علت مراجعه</th>
                            <th>نام مرجع</th>
                            <th>تاریخ برگزاری جلسه</th>
                            <th>نرخ نامه</th>
                            <th>زمان نرخ نامه </th>
                            <th>درصد مشاور</th>
                            <th>سهم مشاور</th>
                            <th>درصد مرکز</th>
                            <th>سهم مرکز</th>
                        @else
                            <th>اپراتور</th>
                        @endif
                        <th>مبلغ جلسه</th>
                        <th>تاریخ ثبت جلسه</th>
                        @if (!$print_advisor)
                            <th>نوع تراکنش</th>
                            <th>صندوق</th>
                            <th>حساب</th>
                        @endif

                    </tr>
                </thead>
                <tbody>
                    @if ($print_advisor)
                        @php
                            $total_time = [];
                        @endphp
                    @endif
                    @foreach ($Transactions as $key => $Transaction)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            @if ($print_advisor)
                                <td><a
                                        href="/admin/Financial/transaction/pardakht_be_advisor?advisor={{ $Transaction->sale->advisor_id }}">{{ $Transaction->sale->advisor->name . ' ' . $Transaction->sale->advisor->family }}</a>
                                </td>
                                <td>{{ $Transaction->sale->session_length ?? '' }}</td>
                                <td>{{ $Transaction->sale->category->name ?? '' }}</td>
                                <td>{{ ($Transaction->sale->customer->name ?? '') . ' ' . ($Transaction->sale->customer->family ?? '') }}
                                </td>
                                @php
                                    $total_time[$Transaction->sale->session_length] = ($total_time[$Transaction->sale->session_length] ?? 0) + 1;
                                @endphp
                                @if ($Transaction->sale)
                                    <td> {{ $Transaction->sale->session_start }}
                                        {{ verta($Transaction->sale->date)->format('Y/m/d') ?? '' }}</td>
                                @endif
                                @if ($price_list_2 = $Transaction->sale->price_list)
                                    <td>{{ $price_list_2->total_price }}</td>
                                    <td>{{ $price_list_2->session_length }} دقیقه</td>
                                    <td>{{ $sahme_moshaver = (100 * $price_list_2->advisor_rate) / $price_list_2->total_price }}٪
                                    </td>
                                    @php
                                        $amount_sale = $Transaction->amount < 0 ? -$Transaction->amount : $Transaction->amount;
                                    @endphp
                                    <td><i class="currency">{{ ($amount_sale * $sahme_moshaver) / 100 }}</i>
                                    </td>
                                    <td>{{ $sahme_markaz = (100 * $price_list_2->center_rate) / $price_list_2->total_price }}٪
                                    </td>
                                    <td><i class="currency">{{ ($amount_sale * $sahme_markaz) / 100 }}</i>
                                    </td>
                                @endif
                            @else
                                <td>{{ $Transaction->operator->name . ' ' . $Transaction->operator->family }}</td>
                            @endif
                            @if ($print_advisor)
                                <td><i class="currency">{{ $Transaction->sale->orginal_price }}</i></td>
                            @else
                                <td><i
                                        class="currency">{{ $Transaction->amount < 0 ? -$Transaction->amount : $Transaction->amount }}</i>
                                </td>
                            @endif
                            <td>{{ $Transaction->created_at }}</td>
                            @if (!$print_advisor)
                                <td>{{ $Transaction->transaction_type->name ?? '-' }}</td>
                                <td>{{ $Transaction->cash->name ?? '-' }}</td>
                                <td>{{ $Transaction->user->name }} {{ $Transaction->user->family }}</td>
                            @endif
                        </tr>
                        @if (!$print_advisor)
                            @foreach ($Transaction->sources as $item)
                                <tr style="border: black 2px solid">
                                    <th>بابت:</th>
                                    <td>{{ $item->distinction ? $item->distinction->sale->products_type->product_type : null }}
                                        با
                                        {{ (($item->distinction ? $item->distinction->sale->advisor->name : null) ?? '') .' ' .(($item->distinction ? $item->distinction->sale->advisor->family : null) ?? '') }}
                                    </td>
                                    <th>مبلغ:</th>
                                    <td><i class="currency">{{ $item->amount }}</i></td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach

                    <tr>
                        <th>مجموع:</th>
                        <th><i class="currency">{{ $Transactions->sum('amount') * -1 }}</i> تومان</th>
                    </tr>
                    {{-- @foreach ($session->transactions->distinctions as $key => $item)
                        <tr class="text-center">
                            <td class='td_center'>{{ $key + 1 }}</td>
                            <td dir="ltr">{{ $item->amount }}</td>
                            <td class="text-center">-</td>
                            <td dir="ltr" class="text-center">{{ $item->source->comment ?? '-' }}</td>
                            <td dir="ltr">{{ verta($item->created_at)->format('Y/m/d H:i') }}</td>
                        </tr>
                    @endforeach
                    <tr class="text-center">
                        <td class='td_center'>مجموع</td>
                        <td dir="ltr">
                            {{ $session->transactions->amount_standing - $session->transactions->amount }}
                        </td>
                        <td dir="ltr">
                            {{ -$session->transactions->amount_standing }}
                        </td>
                        <td class="text-center">-</td>
                        <td dir="ltr">{{ verta($session->transactions->created_at)->format('Y/m/d H:i') }}</td>
                    </tr> --}}
                </tbody>
            </table>

            @if ($print_advisor)
                <table class="table table-bordered" width='100%'>
                    <thead>
                        <tr>
                            @if ($print_advisor)
                                <th>زمان جلسات</th>
                                <th>تعداد جلسات</th>
                            @endif

                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($total_time as $key => $time)
                            <tr>
                                <td>{{ $key }} دقیقه</td>
                                <td>{{ $time }}</td>
                            </tr>
                        @endforeach
                    </tbody>
            @endif
            </table>
        </div>

    </div>
</body>

<script>
    $('.currency').each(function(i, o) {
        $(o).html(numeral($(o).text()).format('0,0'));
    })
</script>

</html>
