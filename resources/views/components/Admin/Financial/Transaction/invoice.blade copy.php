<x-base>
    <x-slot name="@t(ویرا)">

    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/app-invoice.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/animate/animate.css">
        <!-- END: Page CSS-->

    </x-slot>
    <section class="invoice-edit-wrapper">
        <div class="row">
            <!-- invoice view page -->
            <div class="col-xl-9 col-md-8 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body pb-0 mx-25">
                            <!-- header section -->
                            <div class="row mx-0">
                                <div class="col-xl-6 col-md-12 d-flex align-items-center pl-0">
                                    <h6 class="invoice-number mr-75 mb-0">
                                        @t(صورتحساب){{ ' ' . $customer->name . ' ' . $customer->family }}</h6>
                                </div>
                                <div class="col-xl-6 col-md-12 px-0 pt-xl-0 pt-1">
                                    <div
                                        class="invoice-date-picker d-flex align-items-center justify-content-xl-end flex-wrap">
                                        <div class="d-flex align-items-center">
                                            <small class="text-muted mr-75">مبلغ کل: </small>
                                            <small class="text-muted mr-75 " dir="ltr" id="price_total"></small>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- table start -->
                            <div class="table-responsive">
                                <table
                                    class="table  table-bordered invoice-data-table dt-responsive nowrap table-hover  m-0 p-0"
                                    style="width:100%">
                                    <thead class="thead-light" id="thead">
                                        <tr>
                                            <th>~</th>
                                            <th>#</th>
                                            <th>@t(مشاور)</th>
                                            <th>@t(توسط)</th>
                                            <th>@t(نوع محصول)</th>
                                            <th>@t(عنوان)</th>
                                            <th>@t(مبلغ)</th>
                                            <th>@t(باقی مانده)</th>
                                            <th>@t(تاریخ)</th>
                                            <th>@t(توضیحات)</th>
                                            <th>@t(جزئیات)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $prudcot_type['1'] = setting('prudcot_type_1');
                                        @endphp
                                        @foreach ($transactions as $transaction)
                                            @php
                                                if (isset($transaction->sale->product->product_type_id) && empty($prudcot_type[$transaction->sale->product->product_type_id])) {
                                                    $prudcot_type[$transaction->sale->product->product_type_id] = setting('prudcot_type_' . $transaction->sale->product->product_type_id);
                                                }
                                            @endphp
                                            <tr>
                                                <td>
                                                    <div class="checkbox checkbox-warning checkbox-sm">
                                                        <input type="checkbox" name="checkff[]" value="1"
                                                            {{ $sale ? ($transaction->sale_id == $sale->id ? 'checked' : '') : '' }}
                                                            id="{{ $transaction->id }}">
                                                        <label for="{{ $transaction->id }}"></label>
                                                    </div>
                                                </td>
                                                {{-- {{ dd($transaction->transaction_type->name) }} --}}
                                                <td>{{ $transaction->id }}</td>
                                                <td>{{ $transaction->sale ? ($transaction->sale->advisor ? $transaction->sale->advisor->name : '-') : '-' }}
                                                    {{ $transaction->sale ? ($transaction->sale->advisor ? $transaction->sale->advisor->family : '-') : '-' }}
                                                </td>
                                                <td>{{ $transaction->operator->name }}
                                                    {{ $transaction->operator->family }}</td>
                                                <td>
                                                    {{ $transaction->sale ? $prudcot_type[$transaction->sale->product->product_type_id ?? '1'] : 'شارژ ' . $transaction->transaction_type->name }}
                                                </td>
                                                <td class="font-small-2">
                                                    {{ $transaction->sale ? $transaction->sale->product->product_name ?? 'ازاد' : 'شارژ حساب' }}
                                                </td>
                                                <td dir="ltr">{{ intval($transaction->amount) }}</td>
                                                <td dir="ltr" id="price[{{ $transaction->id }}]">
                                                    {{ $transaction->amount_standing }}</td>
                                                <td class="text-success" style="font-size: smaler">
                                                    {{ $transaction->created_at }}</td>
                                                <td>
                                                    <i class="bx bx-info-circle" style="font-size: 30px;"
                                                        data-toggle="tooltip" data-placement="right"
                                                        title="{{ $transaction->comment ?? '-' }}">
                                                    </i>
                                                </td>
                                                <td>
                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn" data-toggle="modal"
                                                        data-target="#_modal{{ $transaction->id }}">
                                                        <i class="bx bx-dots-horizontal" style="font-size: 30px;"
                                                            data-toggle="tooltip" data-placement="right"
                                                            title="نمایش جزئیات">
                                                        </i>
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="_modal{{ $transaction->id }}"
                                                        tabindex="-1" role="dialog"
                                                        aria-labelledby="_modal{{ $transaction->id }}Label"
                                                        aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"
                                                                        id="_modal{{ $transaction->id }}Label">
                                                                        جزئیات تراکنش ({{ $transaction->id }})</h5>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <table class="table table-bordered">
                                                                        <thead>
                                                                            <th>#</th>
                                                                            <th>{{ $transaction->amount > 0 ? 'به' : 'از' }}
                                                                            </th>
                                                                            <th>مبلغ</th>
                                                                            <th>تاریخ</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            @if ($transaction->amount > 0)
                                                                                @forelse($transaction->sources as $item)
                                                                                    <tr>
                                                                                        <td>{{ $item->id }}</td>
                                                                                        <td>{{ $item->distinction->id . ' ' . $item->distinction->date }}
                                                                                        </td>
                                                                                        <td>{{ $item->amount }}</td>
                                                                                        <td dir="ltr">
                                                                                            {{ verta($item->created_at)->format('Y-m-d H:i') }}
                                                                                        </td>
                                                                                    </tr>
                                                                                @empty
                                                                                    <tr>
                                                                                        <td>-</td>
                                                                                    </tr>
                                                                                @endforelse
                                                                            @else
                                                                                @forelse ($transaction->distinctions as $item)
                                                                                    <tr>
                                                                                        <td>{{ $item->id }}</td>
                                                                                        <td>{{ $item->source->id . ' ' . $item->source->date }}
                                                                                        </td>
                                                                                        <td>{{ $item->amount }}</td>
                                                                                        <td dir="ltr">
                                                                                            {{ verta($item->created_at)->format('Y-m-d H:i') }}
                                                                                        </td>
                                                                                    </tr>
                                                                                @empty
                                                                                    <tr>
                                                                                        <td>-</td>
                                                                                    </tr>
                                                                                @endforelse
                                                                            @endif

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">بستن</button>
                                                                    <button type="button"
                                                                        class="btn btn-primary">پرینت</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>~</th>
                                            <th>#</th>
                                            <th>@t(مشاور)</th>
                                            <th>@t(توسط)</th>
                                            <th>@t(نوع محصول)</th>
                                            <th>@t(عنوان)</th>
                                            <th>@t(مبلغ)</th>
                                            <th>@t(باقی مانده)</th>
                                            <th>@t(تاریخ)</th>
                                            <th>@t(توضیحات)</th>
                                            <th>@t(جزئیات)</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- table ends -->

                            <hr>


                        </div>
                        <div class="card-body pt-50">
                            <div class="form-group">
                                <div class="col p-0">
                                    <a class="btn btn-light-primary btn-sm"
                                        href="{{ route('user.transactions.index', $customer->id) }}" type="button">
                                        <i class="bx bx-task"></i>
                                        <span class="invoice-repeat-btn">مشاهده تمام تراکنش های
                                            {{ $customer->name . ' ' . $customer->family }}</span>
                                    </a>

                                    <a class="btn btn-light-primary btn-sm" type="button" onclick="goback();">
                                        <i class="bx bx-task"></i>
                                        <span class="invoice-repeat-btn">بازگشت به صفحه مدیریت جلسات</span>
                                    </a>
                                    <script>
                                        function goback() {
                                            if (getCookie('old_location') != '') {
                                                location.href = getCookie('old_location');
                                            } else {
                                                location.href = '/admin/product/sessions';
                                            }
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- invoice action  -->
            <div class="col-xl-3 col-md-4 col-12">
                <div class="card invoice-action-wrapper shadow-none border">
                    <div class="card-body">
                        <div class="invoice-action-btn mb-1">
                            <button class="btn btn-primary btn-block invoice-send-btn">
                                <i class="bx bx-send"></i>
                                <span>@t(ارسال صورتحساب)</span>
                            </button>
                        </div>
                        <hr>
                        <div class="invoice-action-btn mb-1">
                            <button type="button" class="btn btn-light-primary btn-block" data-toggle="modal"
                                data-target="#naghdi">
                                <i class="bx bx-money"></i>
                                <span>پرداخت نقدی</span>
                            </button>
                        </div>
                        <div class="invoice-action-btn mb-1 d-flex">
                            <button type="button" class="btn btn-light-success btn-block" data-toggle="modal"
                                data-target="#kartkhan">
                                <i class="bx bx-hdd"></i>
                                <span class="text-nowrap">پرداخت با کارتخوان</span>
                            </button>
                        </div>
                        <div class="invoice-action-btn mb-1">
                            <button type="button" class="btn btn-light-danger btn-block" data-toggle="modal"
                                data-target="#internet">
                                <i class="bx bx-wifi"></i>
                                <span class="text-nowrap">پرداخت اینترنتی</span>
                            </button>
                        </div>
                        <div class="invoice-action-btn mb-1">
                            <button type="button" class="btn btn-light-warning btn-block" data-toggle="modal"
                                data-target="#kart_to_kart">
                                <i class="bx bx-credit-card"></i>
                                <span>کارت به کارت </span>
                            </button>
                        </div>
                        <form action="{{ route('transaction.offset_store') }}" method="POST" id="form_offset">
                            <div class="invoice-action-btn mb-1">
                                @csrf
                                <input type="hidden" name="sales_selected" id="offset_sales_selected">
                                <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                <button class="btn btn-light-secondary btn-block" type="submit">
                                    <i class="bx bx-transfer"></i>
                                    <span>تهاتر</span>
                                </button>
                            </div>
                        </form>
                        <div class="invoice-action-btn mb-1">
                            <button class="btn btn-light-info btn-block" data-toggle="modal" data-target="#yaribarg">
                                <i class="bx bx-handicap"></i>
                                <span>یاری برگ</span>
                            </button>
                        </div>
                        <div class="invoice-action-btn mb-1">
                            <button type="button" class="btn btn-light-secondary btn-block" data-toggle="modal"
                                data-target="#pardakht_be_moraje">
                                <i class="bx bx-money"></i>
                                <span>پرداخت به مراجع</span>
                            </button>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!--naghdi Modal start-->

        <div class="modal fade" id="naghdi" tabindex="-1" role="dialog" aria-labelledby="naghdiTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h4 class="modal-title" id="naghdiTitle">پرداخت نقدی صورتحساب
                            {{ $customer->name . ' ' . $customer->family }}
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('transaction.cash_payment_store') }}" method="POST">
                            @csrf
                            <input type="hidden" name="sales_selected" id="cash_payment_sales_selected">
                            <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                            <input type="hidden" name="transaction_type_id" value="1">

                            <div class="row">
                                @foreach ($accounts[1] as $account)
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-success radio-glow">
                                                <input type="radio" id="_{{ $account->id }}" name="account" required
                                                    value="{{ $account->id }}">
                                                <label
                                                    for="_{{ $account->id }}">{{ $account->name . ' ' . $account->account_number }}</label>
                                            </div>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <label>@t(مبلغ دریافتی) </label>
                            <div class="form-group col-12">
                                <input type="price" required name="price" id="cash_payment_price"
                                    placeholder="@t(مبلغ دریافتی)" class="form-control">
                            </div>

                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <label>@t(تاریخ دریافت وجه)</label>
                                <fieldset class="d-flex">
                                    <input type="text" name="date" class="form-control pickadate"
                                        placeholder="@t(تاریخ دریافت وجه)" value="{{ verta()->format('Y/m/d') }}">
                                </fieldset>
                            </div>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <label>@t(ساعت دریافت)</label>
                                <input type="time" id="cash_payment_time" name="time" placeholder="@t(ساعت دریافت وجه)"
                                    value="" class="form-control">
                            </div>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <label>@t(توضیحات)</label>
                                <textarea class="form-control" name="comment" rows="2"
                                    placeholder="@t(توضیحات)"></textarea>
                            </div>
                            <input type="submit" style="display: none;" id="submit_cash_payment_store">
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">بستن</span>
                        </button>
                        <button type="submit" class="btn btn-primary ml-1"
                            onclick="$('#submit_cash_payment_store').click()">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">ثبت</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--naghdi Modal end-->
        <!--pardakht_be_moraje Modal start-->
        <div class="modal fade" id="pardakht_be_moraje" tabindex="-1" role="dialog"
            aria-labelledby="pardakht_be_morajeTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-secondary">
                        <h4 class="modal-title text-danger" id="pardakht_be_morajeTitle">پرداخت به مراجع
                            {{ $customer->name . ' ' . $customer->family }}
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('transaction.pardakht_be_moraje') }}" id="form_pardakht_be_moraje"
                            method="POST">
                            @csrf
                            <input type="hidden" name="sales_selected" id="pardakht_be_moraje_sales_selected">
                            <input type="hidden" name="customer_id" value="{{ $customer->id }}">

                            <div class="modal-body">
                                <div class="row">
                                    @foreach ($accounts[1] as $account)
                                        <div class="col-12">
                                            <fieldset>
                                                <div class="radio radio-success radio-glow">
                                                    <input type="radio" id="__{{ $account->id }}" name="accounts"
                                                        required value="{{ $account->id }}">
                                                    <label
                                                        for="__{{ $account->id }}">{{ $account->name . ' ' . $account->account_number }}</label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    @endforeach
                                </div>
                                <label>@t(مبلغ دریافتی) </label>
                                <div class="form-group col-12">
                                    <input type="price" required name="price" id="pardakht_be_moraje_price"
                                        placeholder="@t(مبلغ دریافتی)" class="form-control">
                                </div>

                                <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                    <label>@t(تاریخ پرداخت وجه)</label>
                                    <fieldset class="d-flex">
                                        <input type="text" name="pardakht_be_moraje_date" class="form-control pickadate"
                                            placeholder="@t(تاریخ دریافت وجه)"
                                            value="{{ verta()->format('Y/m/d') }}">
                                    </fieldset>
                                </div>
                                <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                    <label>@t(ساعت پرداخت)</label>
                                    <input type="time" id="pardakht_be_moraje_time" name="pardakht_be_moraje_time"
                                        placeholder="@t(ساعت دریافت وجه)" value="" class="form-control">
                                </div>
                                <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                    <label>@t(توضیحات)</label>
                                    <textarea class="form-control" name="comment" rows="2"
                                        placeholder="@t(توضیحات)"></textarea>
                                </div>
                                <input type="submit" style="display: none;" id="submit_form_pardakht_be_moraje">
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">بستن</span>
                        </button>
                        <button type="submit" class="btn btn-secondary ml-1"
                            onclick="$('#submit_form_pardakht_be_moraje').click()">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">ثبت</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade text-left" id="pardakht_be_moraje" tabindex="-1" role="dialog"
            aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                </div>
            </div>
        </div>
        <!--pardakht_be_moraje Modal start-->
        <!-- kartkhan Modal start-->
        <div class="modal fade text-left" id="kartkhan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('transaction.cash_payment_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="sales_selected" class="sales_selected">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <input type="hidden" name="transaction_type_id" value="5">
                        <div class="modal-header bg-success">
                            <h4 class="modal-title" id="myModalLabel">پرداخت با کارت خوان</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-success">
                                <p>
                                    در صورت ثبت مبلغ کمتر باقیمانده بعنوان بدهی مراجع ثبت می شود.
                                </p>
                            </div>
                            <div class="row">
                                @foreach ($accounts[5] as $account)
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-success radio-glow">
                                                <input type="radio" id="{{ $account->id }}" name="account" required
                                                    value="{{ $account->id }}">
                                                <label
                                                    for="{{ $account->id }}">{{ $account->name . ' ' . $account->account_number }}</label>
                                            </div>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label for="disabledSelect">تاریخ و ساعت ثبت </label>
                                <input class="form-control" id="disabledInput" type="text"
                                    placeholder="{{ verta() }}" disabled>
                            </div>
                            <label>@t(مبلغ واریزی) </label>
                            <div class="form-group col-12">
                                <input type="text" placeholder="@t(مبلغ واریزی)" name="price"
                                    class="form-control price">
                            </div>
                            <label>@t(تاریخ واریز وجه)</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <fieldset class="d-flex">
                                    <input type="text" class="form-control date pickadate" name="date"
                                        placeholder="@t(تاریخ واریز وجه)" value="{{ verta()->format('Y/m/d') }}">
                                </fieldset>
                            </div>
                            <label>@t(ساعت واریز وجه)</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <input type="time" placeholder="@t(ساعت واریز وجه)" class="form-control time"
                                    name="time">
                            </div>
                            <label>توضیحات</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <textarea class="form-control" name="comment" rows="2"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">ثبت</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- kart khan Modal end -->
        <!-- kartbekart Modal start-->
        <div class="modal fade text-left" id="kart_to_kart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('transaction.cash_payment_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="sales_selected" class="sales_selected">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <input type="hidden" name="transaction_type_id" value="4">

                        <div class="modal-header bg-warning">
                            <h4 class="modal-title" id="myModalLabel">کارت به کارت</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-warning">
                                <p>
                                    در صورت ثبت مبلغ کمتر باقیمانده بعنوان بدهی مراجع ثبت می شود.
                                </p>
                            </div>
                            <div class="row">
                                @foreach ($accounts[4] as $account)
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-success radio-glow">
                                                <input type="radio" id="_{{ $account->id }}" name="account" required
                                                    value="{{ $account->id }}">
                                                <label
                                                    for="_{{ $account->id }}">{{ $account->name . ': ' . $account->account_number }}</label>
                                            </div>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label for="disabledSelect">تاریخ و ساعت ثبت </label>
                                <input class="form-control" id="disabledInput" type="text"
                                    placeholder="{{ verta() }}" disabled>
                            </div>
                            <label for="_price">@t(مبلغ واریزی) </label>
                            <div class="form-group col-12">
                                <input type="text" id="_price" placeholder="@t(مبلغ واریزی)" name="price"
                                    class="form-control price">
                            </div>
                            <label for="_date">@t(تاریخ واریز وجه)</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <fieldset class="d-flex">
                                    <input type="text" class="form-control date pickadate" id="_date" name="date"
                                        placeholder="@t(تاریخ واریز وجه)" value="{{ verta()->format('Y/m/d') }}">
                                </fieldset>
                            </div>
                            <label for="_time">@t(ساعت واریز وجه)</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <input type="time" placeholder="@t(ساعت واریز وجه)" id="_time" class="form-control time"
                                    name="time">
                            </div>
                            <label for="_comment">توضیحات</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <textarea class="form-control" name="comment" rows="2"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-warning">ثبت</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- kartbekart Modal end -->
        <!-- internet Modal start -->
        <div class="modal fade" id="internet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('transaction.internet_peyment') }}" method="POST">
                        @csrf
                        <input type="hidden" name="sales_selected" class="sales_selected">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <div class="modal-header bg-danger">
                            <h4 class="modal-title" id="myModalLabel">پرداخت اینترنتی
                                صورتحساب{{ ' ' . $customer->name . ' ' . $customer->family }}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger">در صورت دریافت مبلغ کمتر باقیمانده دبعنوان بدهی مراجع ثبت
                                می
                                شود..</div>
                            <label for="">@t(مبلغ پرداخت)</label>
                            <div class="form-group input-group">
                                <input type="tel" dir="ltr" name="price" class="form-control price">
                            </div>
                            <label>توضیحات</label>
                            <div class="form-group col-sm-12 col-lg-12 col-xl-12">
                                <textarea class="form-control" name="comment" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">@t(ارسال لینک
                                پرداخت)</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- internet Modal end -->
        <!-- yaribarg Modal start-->
        <div class="modal fade text-left" id="yaribarg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="{{ route('transaction.cash_payment_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="sales_selected" class="sales_selected">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <input type="hidden" name="transaction_type_id" value="3">

                        <div class="modal-header bg-info">
                            <h4 class="modal-title" id="myModalLabel">@t(پرداخت با یاری برگ)</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info">
                                <p>@t(فقط هزینه جلسات با یاری برگ قابل تسویه می باشد.)</p>
                            </div>
                            <div class="row">
                                @foreach ($accounts[3] as $account)
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-success radio-glow">
                                                <input type="radio" id="_{{ $account->id }}" name="account" required
                                                    value="{{ $account->id }}">
                                                <label
                                                    for="_{{ $account->id }}">{{ $account->name . ' ' . $account->account_number }}</label>
                                            </div>
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label for="disabledSelect">@t(تاریخ و ساعت ثبت یاری برگ) </label>
                                <input class="form-control" id="disabledInput" type="text"
                                    placeholder="{{ verta() }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="_price">@t(مبلغ یاری برگ) </label>
                                <input type="price" required id="_price" placeholder="مبلغ یاری برگ" name="price"
                                    class="form-control">
                                <div class="form-group">
                                </div>
                                <div class="form-group">
                                    <label for="_date">@t(تاریخ دریافت یاری برگ)</label>
                                    <fieldset class="d-flex">
                                        <input type="text" class="form-control date pickadate" id="_date" name="date"
                                            placeholder="@t(تاریخ واریز وجه)" value="{{ verta()->format('Y/m/d') }}">
                                    </fieldset>
                                </div>
                                <div class="form-group">
                                    <label for="_time">@t(ساعت واریز وجه)</label>
                                    <input type="time" placeholder="@t(ساعت واریز وجه)" id="_time"
                                        class="form-control time" name="time">
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <fieldset>
                                            <div class="radio radio-success radio-glow">
                                                <select id="YaribargType" name="yaribarg_type_id" required class="form-control "">
                                                    @foreach ($YaribargType as $Type)
                                                        <option value="{{ $Type->id }}">{{ $Type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </fieldset>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="_comment">توضیحات</label>
                                    <textarea class="form-control" name="comment" rows="2"></textarea>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info">ثبت</button>
                            </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- yaribarg Modal end -->
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <script src="/assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js?FWEFWFW"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        {{-- <script src="/assets/js/scripts/pages/app-invoice.js"></script> --}}

        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page JS-->

        <script>
            function change_price() {
                val = 0;
                $("input[name^='checkff[']:checked").each(function(index, value) {
                    val = val + (parseInt(document.getElementById('price[' + value.id + ']').innerHTML) < 0 ?
                        parseInt(document.getElementById('price[' + value.id + ']').innerHTML) : 0)
                })
                return val;
            }

            function slected_sales() {
                val = [];
                $("input[name^='checkff[']:checked").each(function(index, value) {
                    val[index] = value.id;
                })
                return val;
            }
            $("input[name^='checkff[']").on('click', function() {
                $('#price_total').html(change_price());
                $('#cash_payment_price').val((-1) * parseInt(change_price()));
                $('#cash_payment_sales_selected').val(slected_sales);
                $('#pardakht_be_moraje_sales_selected').val(slected_sales);
                $('.price').val((-1) * parseInt(change_price()));
                $('#offset_sales_selected').val(slected_sales);
                $('.sales_selected').val(slected_sales);
            });
            $('#price_total').html(change_price());
            $('.price').val((-1) * parseInt(change_price()));
            $('.sales_selected').val(slected_sales);
            $('#cash_payment_price').val((-1) * parseInt(change_price()));
            $('#cash_payment_sales_selected').val(slected_sales);
            $('#pardakht_be_moraje_sales_selected').val(slected_sales);
            time = new Date();
            // cash_payment_sales_selected
            $('.time').val(time.getHours() + ':' + time.getMinutes());
            $('#cash_payment_time').val(time.getHours() + ':' + time.getMinutes());
            $('#pardakht_be_moraje_time').val(time.getHours() + ':' + time.getMinutes());
        </script>
        <script>
            $('.invoice-data-table #thead tr').clone(true).appendTo('.invoice-data-table #thead');
            $('.invoice-data-table thead:eq(0) tr:eq(1) th').each(function(i) {
                var title = $(this).text();
                // if (title != 'توضیحات' && title != 'جزئیات') {
                //     $(this).html('<input type="text" placeholder="جست جو در ' + title +
                //         '"  class="form-control form-control-sm" />');

                // }
                // if (title == 'نوع محصول') {
                //     console.log(this);
                //     $(this).html('<select class="form-control">' +
                //         '<option value="">جنسیت</option>' +
                //         '<option value="نامشخص">نامشخص</option>' +
                //         '<option value="مرد">مرد</option>' +
                //         '<option value="زن">زن</option>' +
                //         '</select>');
                // }
            });
            $('.invoice-data-table').DataTable({
                language: {
                    "sEmptyTable": "هیچ داده‌ای در جدول وجود ندارد",
                    "sInfo": "نمایش _START_ تا _END_ از _TOTAL_ ردیف",
                    "sInfoEmpty": "نمایش 0 تا 0 از 0 ردیف",
                    "sInfoFiltered": "(فیلتر شده از _MAX_ ردیف)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "نمایش _MENU_ ردیف",
                    "sLoadingRecords": "در حال بارگزاری...",
                    "sProcessing": "در حال پردازش...",
                    "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                    "oPaginate": {
                        "sFirst": "برگه‌ی نخست",
                        "sLast": "برگه‌ی آخر",
                        "sNext": "بعدی",
                        "sPrevious": "قبلی"
                    },
                    "oAria": {
                        "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                        "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                    },
                    "sSearch": "",
                    "sSearchPlaceholder": "جستجوی",
                },
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;
                        if (column.header().childNodes[0].data == 'نوع محصول') {
                            var select = $('<select><option value=""></option></select>')
                                .appendTo($(column.header()).empty())
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function(d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                        if (column.header().childNodes[0].data == 'مشاور' || column.header().childNodes[0]
                            .data == 'توسط') {
                            var select = $('<input list="browsers"><datalist id="browsers"></datalist>')
                                .appendTo($(column.header()).empty())
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, true)
                                        .draw();
                                });

                            column.data().unique().sort().each(function(d, j) {
                                $('#browsers').append('<option value="' + d + '">' + d +
                                    '</option>')
                            });
                        }


                        var that = this;
                        $('input', $('.invoice-data-table thead:eq(0) tr:eq(1) th')[this.index()]).on(
                            'keyup change clear',
                            function() {
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                        $('select', $('.invoice-data-table thead:eq(0) tr:eq(1) th')[this
                            .index()]).on('keyup change clear',
                            function() {
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                    });
                    // $('[data-toggle="tooltip"]').tooltip()
                }
            });
        </script>

        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
    </x-slot>
</x-base>
