<x-base>
    <x-slot name="title">@t(ویرایش نوع حساب)</x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <!-- // Basic multiple Column Form section start -->
    <section>
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(ویرایش نوع حساب)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                                <li class="breadcrumb-item">@t(تنظیمات)</li>
                                <li class="breadcrumb-item">@t(تنظیمات مالی)</li>
                                <li class="breadcrumb-item">@t(انواع حساب)</li>
                                <li class="breadcrumb-item active">@t(ویرایش نوع حساب)</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(ویرایش نوع حساب)</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form action="{{ route('cashtype.update', $type->id) }}" method="POST">
                                @csrf
                                @method('patch')
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4 col-12">
                                            <label for="name">@t(نوع حساب)</label>
                                            <input type="text" value="{{ $type->name }}" id="name" class="form-control" required placeholder="@t(نوع حساب)" name="name">
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label for="created_at">@t(تاریخ ویرایش)</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at"
                                                value="{{ ' ' . $date->formatdate() }}" readonly>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <label for="account_status">@t(وضعیت حساب)</label></br>
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-success">
                                                            <input type="radio" name="account_status" value="فعال"
                                                                id="colorRadio3"
                                                                @if ($type->status == 'فعال')
                                                                    checked
                                                                @endif >
                                                            <label for="colorRadio3">@t(فعال)</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-danger">
                                                            <input type="radio" name="account_status" value="غیرفعال"
                                                                id="colorRadio4"
                                                                @if ($type->status == 'غیرفعال')
                                                                    checked
                                                                @endif>
                                                            <label for="colorRadio4">@t(غیر فعال)</label>
                                                        </div>
                                                    </fieldset>
                                            </ul>
                                            {{-- <div class="custom-control custom-switch custom-control-inline mb-1">
                        <span>@t(غیر فعال)</span>
                        <input type="checkbox" name="account_status" class="custom-control-input" checked id="active" >
                        <label class="custom-control-label mr-1 ml-1" for="active" ></label>
                        <span>@t(فعال)</span>
                        </div> --}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-12 mt-2">
                                            <div class="form-label-group">
                                                <textarea class="form-control" id="note" name="note" rows="2"
                                                    placeholder="@t(توضیحات)">{{ $type->note }}</textarea>
                                                <label for="note">@t(توضیحات)</label>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-1 mb-1">ویرایش</button>
                                            <button type="reset"  class="btn btn-light-secondary mr-1 mb-1">پاک کردن</button>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- // Basic multiple Column Form section end -->

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
    </x-slot>
</x-base>
