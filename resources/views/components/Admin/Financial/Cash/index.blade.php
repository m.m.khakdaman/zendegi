<x-base>
    <x-slot name="title">
        @t(لیست حسابهای مرکز)
    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->
        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    </x-slot>
    <!-- table success start -->
    <section id="table-success">
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(حساب ها و صندوق)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.home') }}">
                                        <i class="bx bx-home-alt">
                                        </i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ route('cash.index') }}">@t(مالی) </a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{ route('cash.index') }}">@t(حساب ها و صندوق)
                                    </a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <div class="card">

            <div class="card-header">
                <!-- create invoice button-->

                @can('cash.create')
                    <div class="row">

                        <div class="invoice-create-btn mb-1">
                            <a class="btn btn-primary" href="{{ route('cash.create') }}" role="button">@t(تعریف حساب جدید)</a>
                        </div>
                    </div>
                @endcan

                <!-- datatable start -->
                <div class="table-responsive">
                    <table id="table-extended-success" class="table mb-0">
                        <thead>
                            <tr class="text-center">
                                <th class="text-left">@t(ردیف)</th>
                                <th class="text-left">@t(کد)</th>
                                <th class="text-left">@t(نام حساب)</th>
                                {{-- <th>@t(شماره حساب)</th> --}}
                                <th class="text-left">@t(مانده حساب)</th>
                                <th>@t(تاریخ افتتاح حساب)</th>
                                <th>@t(نوع حساب)</th>
                                <th>@t(وضعیت)</th>
                                <th>@t(سایر)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cash as $tmp => $cash)
                                <tr>
                                    <td class="text-bold-600 text-secondary pr-0">{{ ++$tmp }}</td>
                                    <td><small>{{ $cash->id }}</small></td>
                                    <td class="text-sexondary">{{ $cash->name }}</td>
                                    {{-- <td><small>{{ $cash->account_number }}</small></td> --}}
                                    <td class="currency text-secondary">{{ $cash->transactions_sum_amount_standing }}</td>
                                    <td class="text-center"><small>{{ verta($cash->created_at)->format('Y/m/d') }}</small></td>
                                    <td class="text-center">{{ $cash->cashe_type->name ?? '' }}</td>
                                    <td class="text-center">
                                        <span
                                            class="bullet {{ $cash->account_status == 'فعال' ? 'bullet-success' : 'bullet-danger' }} bullet-sm">

                                        </span>
                                        <small class="text-muted">{{ $cash->account_status }}</small>
                                    </td>
                                    
                                    <td>
                                        <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="right" title="{{ $cash->note ?? '-' }}">                                     </i>
                                        <i class="bx bx-dollar-circle" data-toggle="tooltip" data-placement="top" title="@t(شماره حساب:)&nbsp;{{ $cash->account_number ?? '-' }}">                                     </i>
                                        <a href="{{ route('cash.edit', $cash->id) }}" class="text-warning" data-toggle="toltip" title="ویرایش">
                                            <i class="bx bx-edit mr-1"></i>
                                        </a>
                                        {{-- <div data-target="#_{{ $cash->id }}" data-toggle="modal" class="display-inline">
                                        <button href="#" class="text-danger btn" data-toggle="toltip" title="حذف">
                                            <i class="bx bx-trash mr-1">
                                            </i>
                                        </button>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" id="_{{ $cash->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    آیا می خواهید -- {{ $cash->name }} -- را حذف نمایید ؟
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{ route('chat.destroy', $cash->id) }}"
                                                        method="post">
                                                        @method('delete')
                                                        @csrf
                                                        <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal">خیر</button>
                                                        <button type="submit" class="btn btn-success">بله</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- datatable ends -->
            </div>
    </section>

    <x-slot name="script">
    <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->
    <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->
        <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
        <script>
            $('.currency').each(function(i, o) {
                $(o).html(numeral($(o).text()).format('0,0'));
            })
        </script>
    <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
    <!-- END: Page JS-->
    </x-slot>
</x-base>
