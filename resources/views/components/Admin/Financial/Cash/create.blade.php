<x-base>
    <x-slot name="title">@t(افزودن حساب جدید)</x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <!-- // Basic multiple Column Form section start -->
    <section id="multiple-column-form">
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(تعریف حساب جدید)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
                                            class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@t(مالی) </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('cash.index') }}">@t(حساب ها و صندوق)
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a class="text-primary" href="#">@t(تعریف حساب جدید)</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(تعریف حساب جدید)</h4>
                        <small
                            class="text-bold-600">@t(توسط:)</small><small>{{ auth()->user()->name . ' ' . auth()->user()->family }}</small>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form action="{{ route('cash.store') }}" method="POST">
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="form-label-group">
                                                <input type="text" id="account_name" class="form-control" required
                                                    placeholder="@t(نام حساب)" name="account_name">
                                                <label for="account_name">@t(نام حساب)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-label-group">
                                                <input type="number" id="account_number" class="form-control"
                                                    placeholder="@t(شماره حساب)" name="account_number">
                                                <label for="account_number">@t(شماره حساب)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                                <label for="account_type">@t(نوع حساب)</label>
                                                <div class="form-label-group">
                                                <select name="cash_type" id="cash_type"  class="form-control" required>
                                                    @foreach ($cash_types as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                                <label for="admins">@t(کاربرها)</label>
                                                <div class="form-label-group">
                                                <select name="admins[]" id="admins"  class="form-control select2" required multiple>
                                                    @foreach ($admins as $admin)
                                                        <option value="{{ $admin->id }}">{{ $admin->fullname() }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-label-group">
                                                <input type="number" id="initial_amount" class="form-control" min="0"
                                                    required
                                                    value="0"
                                                    name="initial_amount" placeholder="@t(مبلغ اولیه حساب)">
                                                <label for="initial_amount">@t(مبلغ اولیه حساب)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label for="created_at">@t(تاریخ افتتاح حساب)</label>
                                            <input type="text" class="form-control" id="created_at" name="created_at"
                                                value="{{ ' ' . $date->formatdate() }}" readonly>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label for="account_status">@t(وضعیت حساب)</label></br>
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-success">
                                                            <input type="radio" name="account_status" value="1"
                                                                id="colorRadio3" checked>
                                                            <label for="colorRadio3">@t(فعال)</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-danger">
                                                            <input type="radio" name="account_status" value="0"
                                                                id="colorRadio4">
                                                            <label for="colorRadio4">@t(غیر فعال)</label>
                                                        </div>
                                                    </fieldset>
                                            </ul>
                                            {{-- <div class="custom-control custom-switch custom-control-inline mb-1">
                        <span>@t(غیر فعال)</span>
                        <input type="checkbox" name="account_status" class="custom-control-input" checked id="active" >
                        <label class="custom-control-label mr-1 ml-1" for="active" ></label>
                        <span>@t(فعال)</span>
                        </div> --}}
                                        </div>
                                        <div class="col-md-12 col-12">
                                            <div class="form-label-group">
                                                <textarea class="form-control" id="note" name="note" rows="2"
                                                    placeholder="@t(توضیحات)"></textarea>
                                                <label for="note">@t(توضیحات)</label>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-1 mb-1">ثبت</button>
                                            <button type="reset"
                                                class="btn btn-light-secondary mr-1 mb-1">بازنشانی</button>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- // Basic multiple Column Form section end -->

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->
    </x-slot>
</x-base>
