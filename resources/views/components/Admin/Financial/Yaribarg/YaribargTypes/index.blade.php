<x-base>
    <x-slot name="title">
        @t(انواع یاری برگ)
    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    </x-slot>
    <!-- table success start -->
<section id="table-success">
  <!-- breadcrumb start-->
  <div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1">@t(انواع یاری برگ)</h5>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
                                    class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#S">@t(مالی) </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#S">@t(یاری برگ) </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#S">@t(انواع یاری برگ) </a>
                        </li>                        
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb end-->
    <div class="card">
      <!-- datatable start -->
      <div class="table-responsive">
        <a class="btn btn-primary mt-50 ml-50" href="{{ route('yaribargtype.create') }}" role="button">@t(تعریف نوع یاری برگ)</a>
        @php
          $i = 0;
        @endphp
        <table id="table-extended-success" class="table mb-0">
            <thead>
                <tr class="text-center">   
                  <th>@t(ردیف)</th>               
                  <th>@t(نوع یاری برگ)</th>
                  <th>@t(تاریخ شروع)</th>
                  <th>@t(تاریخ اعتبار)</th>
                  <th>@t(تعداد مجاز)</th>
                  <th>@t(سایر)</th>
                </tr>
              </thead>
          <tbody>
            @foreach ($yaribargtype as $yaribargtype)
            <tr class="text-center">
              <td>{{ ++$i }}</td>
              <td class="text-bold-600 pr-0">{{ $yaribargtype->name ?? '' }}</td>
              <td>{{ $yaribargtype->start_date ?? '' }}</td>
              <td>{{ $yaribargtype->end_date ?? '' }}</td>
              <td>{{ $yaribargtype->number ?? '' }}</td>
              <td class="text-right">
                @if ($yaribargtype->status == 'فعال')
                  <i class="bx bx-check-circle text-success"  data-toggle="tooltip" data-placement="right" title="{{ $yaribargtype->status }}"></i>
                @else
                  <i class="bx bx-x-circle text-danger"  data-toggle="tooltip" data-placement="right" title="{{ $yaribargtype->status }}"></i>
                @endif
                @if ($yaribargtype->note)
                  <i class="bx bx-info-circle"  data-toggle="tooltip" data-placement="right" title="{{ $yaribargtype->note }}"></i>
                @endif
                <a href="{{ route('yaribargtype.edit',$yaribargtype->id) }}"><i class="bx bx-edit"  data-toggle="tooltip" data-placement="right" title="@t(ویرایش یاری برگ:){{ ' '.$yaribargtype->name }}"></i></a>
                <i class="bx bx-user-circle"  data-toggle="tooltip" data-placement="right" title="@t(اپراتور:){{ $yaribargtype->operator->name ?? '' }}&nbsp{{ $yaribargtype->operator->family ?? '' }}"></i>
                <i class="bx bx-calendar-event"  data-toggle="tooltip" data-placement="right" title="@t(تاریخ ثبت:){{ ' '.verta($yaribargtype->created_at)->format('Y/m/d') }}"></i>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>

      </div>
      <!-- datatable ends -->
    </div>
  </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
        <!-- END: Page JS-->

    </x-slot>
</x-base>