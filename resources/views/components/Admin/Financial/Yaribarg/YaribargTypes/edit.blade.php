<x-base title="@t(ویاریش نوع یاری برگ)">
    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <!-- END: Page CSS-->
        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(ویرایش نوع یاری برگ) «{{ $yaribargtype->name }}»</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">@t(مالی)</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">@t(یاری برگ)</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">@t(انواع یاری برگ)</a>
                            </li>
                            <li class="breadcrumb-item active">@t(ویرایش نوع یاری برگ) «{{ $yaribargtype->name }}»</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(ویرایش نوع یاری برگ) «{{ $yaribargtype->name }}»</h4>
                    </div>
                    <div class="card-content mt-2">
                        <div class="card-body">
                            @include('layouts.errors')
                            <form action="{{ route('yaribargtype.update',$yaribargtype->id) }}" method="POST" class="wizard-horizontal"
                                novalidate>
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="name" required name="name"
                                                    value="{{ $yaribargtype->name }}"
                                                    required
                                                    data-validation-required-message="@t(پر کردن فیلد عنوان نوع یاری برگ اجباری است.)"
                                                    placeholder="@t(عنوان نوع یاری برگ)">
                                                <label for="name">@t(عنوان نوع یاری برگ)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" name="start_date" value="{{ verta($yaribargtype->start_date)->format('Y/m/d') }}" class="form-control  date_of_birth" placeholder="تاریخ شروع">
                                            <div class="form-control-position">
                                                <i class="bx bx-calendar"></i>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" name="end_date" value="{{ verta($yaribargtype->end_date)->format('Y/m/d')  }}" class="form-control  date_of_birth" placeholder="تاریخ اعتبار">
                                            <div class="form-control-position">
                                                <i class="bx bx-calendar"></i>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="number" required name="number" value="{{ $yaribargtype->number }}"
                                                    value="{{ old('number') }}"
                                                    required
                                                    data-validation-required-message="پر کردن فیلد تعداد مجاز اجباری است."
                                                    placeholder="تعداد مجاز">
                                                <label for="number">تعداد مجاز</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="status">@t(وضعیت)</label>
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                  <fieldset>
                                                      <div class="radio radio-success">
                                                          <input type="radio" name="status" value="فعال" id="colorRadio3" {{ $yaribargtype->status == 'فعال'?'checked':'' }}>
                                                          <label for="colorRadio3">فعال</label>
                                                      </div>
                                                  </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                  <fieldset>
                                                    <div class="radio radio-danger">
                                                        <input type="radio" name="status" value="غیر فعال" {{ $yaribargtype->status == 'غیر فعال'?'checked':'' }} id="colorRadio4">
                                                        <label for="colorRadio4">غیر فعال</label>
                                                    </div>
                                                  </fieldset>
                                                </li>
                                              </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <textarea class="form-control" id="note" name="note"
                                                    placeholder='@t(توضیحات)'>{{ old('note') }}</textarea>
                                                <label for="note">@t(توضیحات)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group float-right">
                                            <input type="submit" class="form-control btn btn-success" value="ذخیره">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">



        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>        

        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.time.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/moment.min.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>

        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js">
        </script>
        <!-- END: Page JS-->
        <script>

            $('.date_of_birth').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
        </script>

    </x-slot>
</x-base>
