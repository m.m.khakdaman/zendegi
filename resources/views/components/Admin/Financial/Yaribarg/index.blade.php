<x-base>
    <x-slot name="title">
        @t(لیست یاری برگها)
    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/app-invoice.css">

    </x-slot>
    <section class="invoice-list-wrapper">
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(لیست یاری برگها)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
                                            class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@t(مالی) </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@t(یاری برگ) </a>
                                </li>
                                <li class="breadcrumb-item active">@t(لیست یاری برگها)
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <a class="btn btn-primary mb-25" href="{{ route('yaribarg.create') }}" role="button" data-toggle="modal" data-target="#new">@t(یاری برگ جدید)</a>
        <div class="action-dropdown-btn d-none">
            <div class="dropdown invoice-filter-action">
                <button class="btn border dropdown-toggle mr-1" type="button" id="invoice-filter-btn"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    فیلترها
                </button>
                <div class="dropdown-menu" aria-labelledby="invoice-filter-btn">
                    <a class="dropdown-item" href="#">دریافت شده</a>
                    <a class="dropdown-item" href="#">ارسال شده</a>
                    <a class="dropdown-item" href="#">پرداخت اقساطی</a>
                    <a class="dropdown-item" href="#">پرداخت شده</a>
                </div>
            </div>
            <div class="dropdown invoice-options">
                <button class="btn border dropdown-toggle mr-2" type="button" id="invoice-options-btn"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    اختیارات
                </button>
                <div class="dropdown-menu" aria-labelledby="invoice-options-btn">
                    <a class="dropdown-item" href="#">حذف</a>
                    <a class="dropdown-item" href="#">ویرایش</a>
                    <a class="dropdown-item" href="#">مشاهده</a>
                    <a class="dropdown-item" href="#">ارسال</a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table2 invoice-data-table dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>@t(ردیف)</th>
                        <th>@t(نام مراجع)</th>
                        <th><span class="align-middle">@t(سریال)</span></th>
                        <th>@t(تاریخ برگه)</th>
                        <th>@t(تاریخ ثبت)</th>
                        <th>@t(نوع یاری برگ)</th>
                        <th>@t(مبلغ)</th>
                        <th>@t(وضعیت)</th>
                        <th>@t(اپراتور)</th>
                        {{-- <th>@t(توضیحات)</th> --}}
                        <th>@t(سایر)</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $row = 0;
                    @endphp
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-center">{{ ++$row }}</td>
                            <td><span>{{ $transaction->user->name ?? '' }}&nbsp;{{ $transaction->user->family ?? ''}}</span></td>
                            <td class="text-center"><a href="app-invoice.html">{{ $transaction->yaribarg_serial }}</a></td>
                            <td class="text-center"><span class="font-small-2">{{ $transaction->yaribarg_date }}</span></td>
                            <td class="text-center">
                                <span class="font-small-2" data-toggle="tooltip" data-placement="top" title="@t(تاریخ دریافت:){{ ' '.$transaction->yaribarg_recived_date }}">
                                    {{ $transaction->yaribarg_reg_date }}
                                </span>
                            </td>
                            <td class="text-center">{{ $transaction->yaribargtype->name ?? '' }}</td>
                            <td class="text-center">
                                <span dir="ltr"  class="text-secondary currency">{{ $transaction->amount ?? '' }}</span>
                            </td>
                            @php
                                if ($transaction->yaribarg_status == 1) {
                                    $class = "badge badge-light-secondary badge-pill";
                                }elseif ($transaction->yaribarg_status == "2") {
                                    $class = "badge badge-warning badge-pill";
                                }elseif ($transaction->yaribarg_status == "3") {
                                    $class = "badge badge-success badge-pill";
                                }elseif ($transaction->yaribarg_status == "4") {
                                    $class = "badge badge-danger badge-pill";
                                }elseif ($transaction->yaribarg_status == "5") {
                                    $class = "badge badge-light-danger badge-pill";
                                }
                            @endphp
                            <td class="text-center"><small class="{{ $class }}">{{ $transaction->yaribargstatus->text_value ?? '' }}</small></td>
                            <td class="text-center"><small class="text-secondary">{{ $transaction->operator->name ?? '' }}&nbsp;{{ $transaction->operator->family ?? '' }}</small></td>
                            <td>
                                <div class="invoice-action">
                                    <a href="{{ route('admin.index') }}{{ '/'.auth()->user()->id }}" class="invoice-action-view mr-1" data-toggle="tooltip" data-placement="top" title="{{ $transaction->operator->name ?? '' }}&nbsp;{{ $transaction->operator->family ?? '' }}">
                                        <i class="bx bx-show-alt"></i>
                                    </a>
                                    <i  class="bx bx-info-circle" data-toggle="tooltip" data-placement="right" title="{{ $transaction->comment }}">
                                    </i>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
 <!-- newModal -->
 <div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="newTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title" id="newTitle">@t(یاری برگ جدید)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            @include('layouts.errors')
            <form action="{{ route('yaribarg.store') }}" method="POST" >
            @csrf
            <div class="modal-body line-height-2">
                    <label>تاریخ استفاده از یاری برگ</label>
                    <div class="form-group">
                        <input type="text" name="yaribarg_date_of_use" class="form-control date_of_birth">
                    </div>
                    <div>
                    <div class="form-group">
                        <label for="customer_id">نام مراجع</label>
                        <select type="text" id="customer_id" class=" form-control select2"
                        validitymsg='مراجع را انتخاب کنید.' style="width: 100%" name="customer_id"
                        placeholder="مراجع" required>
                        </select>
                    </div>
                    <div class="form-group form-label-group">
                        <input type="number" id="ammount" name="ammount" class="form-control" placeholder="مبلغ یاری برگ">
                        <label for="ammount">@t(مبلغ یاری برگ)</label>
                    </div>
                    <div class="form-group form-label-group">
                        <input type="text" name="tracking_code" id="tracking_code" class="form-control" placeholder="@t(شماره سریال یاری برگ)">
                        <label for="tracking_code">@t(شماره سریال یاری برگ)</label>
                    </div>

                    <div class="form-group">
                        <label>نوع یاری برگ</label>
                        <select class="form-control" name="yaribarg_type_id">
                            @foreach ($yaribargtype as $yaribargtype)
                            <option value="{{ $yaribargtype->id }}">{{ $yaribargtype->name }}</option>
                            @endforeach                            
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="controls form-label-group ">
                            <textarea class="form-control" id="note" name="note"
                                placeholder='@t(توضیحات)'>{{ old('note') }}</textarea>
                            <label for="note">@t(توضیحات)</label>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <div class="form-group float-right">
                    <input type="submit" class="form-control btn btn-success" value="ثبت" required>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" >انصراف</button>
                </div>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.newModal -->
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>        

        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.time.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/pages/app-invoice.js"></script>
        <script src="/assets/js/scripts/modal/components-modal.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->


        {{-- این اسکریپت اعداد مالی را سه رقم سه رقم جدا می کند --}}
        <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
        <script>
            $('.currency').each(function(i, o) {
                $(o).html(numeral($(o).text()).format('0,0'));
            })
        </script>
        {{-- این اسکریپت اعداد مالی را سه رقم سه رقم جدا می کند --}}

        <script>

            $('.date_of_birth').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });



            $('#customer_id').select2({
                    ajax: {
                        url: '{{ route('customer.get_all_ajax') }}',
                        dataType: 'json'
                        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                    }
                });

        </script>

    </x-slot>
</x-base>
