<x-base>
    <x-slot name="title">
        @t(لیست بدهکاران)
    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/app-invoice.css">

    </x-slot>
    <section class="invoice-list-wrapper">
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(لیست بدهکاران)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
                                            class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">@t(مالی) </a>
                                </li>
                                <li class="breadcrumb-item active">@t(لیست بدهکاران)
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <div class="action-dropdown-btn d-none">
            <div class="dropdown invoice-filter-action">
                <button class="btn border dropdown-toggle mr-1" type="button" id="invoice-filter-btn"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    فیلترها
                </button>
                <div class="dropdown-menu" aria-labelledby="invoice-filter-btn">
                    <a class="dropdown-item" href="#">دریافت شده</a>
                    <a class="dropdown-item" href="#">ارسال شده</a>
                    <a class="dropdown-item" href="#">پرداخت اقساطی</a>
                    <a class="dropdown-item" href="#">پرداخت شده</a>
                </div>
            </div>
            <div class="dropdown invoice-options">
                <button class="btn border dropdown-toggle mr-2" type="button" id="invoice-options-btn"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    اختیارات
                </button>
                <div class="dropdown-menu" aria-labelledby="invoice-options-btn">
                    <a class="dropdown-item" href="#">حذف</a>
                    <a class="dropdown-item" href="#">ویرایش</a>
                    <a class="dropdown-item" href="#">مشاهده</a>
                    <a class="dropdown-item" href="#">ارسال</a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>
                            <span class="align-middle">صورتحساب</span>
                        </th>
                        <th>@t(نام و نام خانوادگی)</th>
                        <th>@t(مبلغ)</th>
                        <th>@t(تاریخ)</th>
                        <th>@t(شماره تماس)</th>
                        <th>@t(عملیات)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <a>{{ $transaction->id }}</a>
                            </td>
                            <td>
                                <span>{{ ($transaction->user->name ?? '') . ' ' . ($transaction->user->family ?? '') }}</span>
                            </td>
                            <td>
                                <span dir="ltr">{{ $transaction->amount_standing . ' ' }}</span>
                                <span class="badge badge-light-danger badge-pill">تومان</span>
                            </td>
                            <td>
                                <span class="bx bx-calendar text-success font-small-3"></span>
                                <span>{{ $transaction->updated_at }}</span>
                            </td>
                            <td><span>{{ $transaction->user->mobile??'' }}</span></td>
                            <td>
                                <div class="invoice-action">
                                    <a href="app-invoice.html" class="invoice-action-view mr-1">
                                        <i class="bx bx-show-alt"></i>
                                    </a>
                                    <a href="app-invoice-edit.html" class="invoice-action-edit cursor-pointer">
                                        <i class="bx bx-message"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/pages/app-invoice.js"></script>
        <!-- END: Page JS-->

    </x-slot>
</x-base>
