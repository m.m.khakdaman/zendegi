<x-base title="@t(پیامک)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t( پیامک های پیش فرض)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item">@t(تنظیمات)</li>
                  <li class="breadcrumb-item">@t(پیامک ها)</li>
                  <li class="breadcrumb-item active">@t( پیامک های پیش فرض)</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>

<!-- Basic tabs start -->
<section id="basic-tabs-components">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          {{-- <h4>تب های پایه</h4> --}}
        </div>
      </div>
      <div class="card-content">
        <div class="card-body">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" aria-controls="home" role="tab" aria-selected="true">
                <i class="bx bx-home align-middle"></i>
                <span class="align-middle">@t(پیامک های عمومی)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="about-tab" data-toggle="tab" href="#about" aria-controls="about" role="tab" aria-selected="false">
                <i class="bx bx-message-square align-middle"></i>
                <span class="align-middle">@t(پیامک های کارگاهی)</span>
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('sms.create') }}" >
                  <i class="bx bx-user align-middle"></i>
                  <span class="align-middle">@t(ساخت پیامک جدید)</span>
                </a>
              </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="home" aria-labelledby="home-tab" role="tabpanel">
                @php
                    $tmp = 0;
                @endphp
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>@t(کد پیام)</th>
                        <th>@t(پیامک پیش فرض)</th>
                        <th>@t(اپراتور)</th>
                        <th>@t(وضعیت)</th>
                        <th>@t(عملیات)</th>
                    </thead>
                    @foreach ($sms as $sms)
                    <tr>
                        <td>{{ ++$tmp }}</td>
                        <td>{{ $sms->id }}</td>
                        <td>{{ $sms->value }}</td>
                        <td><span class="badge badge-light-secondary">{{ $sms->operator->name.' '.$sms->operator->family }}</span></td>
                        <td>
                            @if ($sms->status)
                                <a class="text-success" href="{{ route('sms.activation',$sms->id,5) }}" data-toggle="tooltip" data-placement="right" title="@t(برای غیرفعال کردن این پیامک کلیک کنید.)">
                                    <i class="bx bx-check-circle"></i>
                                </a>
                            @else
                                <a class="text-danger" href="{{ route('sms.activation',$sms->id, 4) }}" data-toggle="tooltip" data-placement="right" title="@t(برای فعال کردن این پیامک کلیک کنید.)">
                                    <i class="bx bx-x-circle"></i>
                                </a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('sms.edit', $sms->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ویرایش پیامک با کد){{ ' '.$sms->key}}">
                                <i class="bx bx-edit"></i>
                            </a>
                            <a href="#" data-toggle="modal" class="" data-target="#_{{ $sms->id }}" title="@t(پاک کردن پیامک با کد){{ ' '.$sms->key}}">
                                <i class="bx bx-trash"></i>
                            </a>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="_{{ $sms->id }}" tabindex="-1"
                                role="dialog" aria-labelledby="exampleModalCenterTitle"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            آیا می خواهید -- {{ $sms->id }} -- را حذف نمایید ؟
                                        </div>
                                        <div class="modal-footer">
                                            <form action="{{ route('sms.destroy', $sms->id) }}"
                                                method="post">
                                                @method('delete')
                                                @csrf
                                                <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">خیر</button>
                                                <button type="submit"
                                                    class="btn btn-success">بله</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            
            <div class="tab-pane" id="about" aria-labelledby="about-tab" role="tabpanel">
                @php
                    $tmp = 0;
                @endphp
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>@t(کد پیام)</th>
                        <th>@t(پیامک پیش فرض)</th>
                        <th>@t(اپراتور)</th>
                        <th>@t(وضعیت)</th>
                        <th>@t(عملیات)</th>
                    </thead>
                    @foreach ($sms_wsh as $sms_wsh)
                    <tr>
                        <td>{{ ++$tmp }}</td>
                        <td>{{ $sms_wsh->id }}</td>
                        <td>{{ $sms_wsh->value }}</td>
                        <td><span class="badge badge-light-secondary">{{ $sms_wsh->operator->name.' '.$sms_wsh->operator->family }}</span></td>
                        <td>
                            @if ($sms_wsh->status)
                                <a class="text-success" href="{{ route('sms.activation',$sms_wsh->id,5) }}" data-toggle="tooltip" data-placement="right" title="@t(برای غیرفعال کردن این پیامک کلیک کنید.)">
                                    <i class="bx bx-check-circle"></i>
                                </a>
                            @else
                                <a class="text-danger" href="{{ route('sms.activation',$sms_wsh->id, 4) }}" data-toggle="tooltip" data-placement="right" title="@t(برای فعال کردن این پیامک کلیک کنید.)">
                                    <i class="bx bx-x-circle"></i>
                                </a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('sms.edit', $sms_wsh->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ویرایش پیامک با کد){{ ' '.$sms_wsh->key}}">
                                <i class="bx bx-edit"></i>
                            </a>
                            <a href="#" data-toggle="modal" class="" data-target="#_{{ $sms_wsh->id }}" title="@t(پاک کردن پیامک با کد){{ ' '.$sms_wsh->key}}">
                                <i class="bx bx-trash"></i>
                            </a>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="_{{ $sms_wsh->id }}" tabindex="-1"
                                role="dialog" aria-labelledby="exampleModalCenterTitle"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            آیا می خواهید -- {{ $sms_wsh->id }} -- را حذف نمایید ؟
                                        </div>
                                        <div class="modal-footer">
                                            <form action="{{ route('sms.destroy', $sms_wsh->id) }}"
                                                method="post">
                                                @method('delete')
                                                @csrf
                                                <button type="button" class="btn btn-danger"
                                                    data-dismiss="modal">خیر</button>
                                                <button type="submit"
                                                    class="btn btn-success">بله</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Basic Tag Input end -->
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/navs/navs.js"></script>
        <!-- END: Page JS-->


    </x-slot>
</x-base>
