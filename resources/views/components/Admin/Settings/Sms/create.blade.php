<x-base title="@t(پیامک)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(ارسال پیامک)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item">@t(تنظیمات)
                </li>
                <li class="breadcrumb-item">@t(پیامک ها)
                </li>
                <li class="breadcrumb-item">@t(پیامک های پیش فرض)
                </li>
                <li class="breadcrumb-item active"><a href="{{ route('sms.create') }}" class="text-primary">@t(ساخت پیامک جدید)</a>
                </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section class="faq-search">
        <div class="row">
            <div class="col-12">
            <div class="card background-color">
                <div class="card-header">
                </div>
                <div class="card-content">
                    <div class="card-body p-0">
                        <form action="{{ route('sms.store') }}" 
                        class="wizard-validation" 
                        method="POST"
                        enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group  mr-2 ml-2">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <small>@t(دسته بندی پیامک)</small>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio"
                                                            class="custom-control-input bg-primary" value="sms_list{{  date('his') }}"
                                                            name="sms_type" id="man" checked>
                                                        <label class="custom-control-label"
                                                            for="man">@t(پیامک های عمومی)</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio"
                                                            class="custom-control-input bg-danger" value="s_m_s_wsh{{  date('his') }}"
                                                            name="sms_type" id="internet">
                                                        <label class="custom-control-label"
                                                            for="internet">@t(پیامک های کارگاهی)</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mr-2 ml-2">
                                        <label for="sms_text">
                                            @t(متن پیامک پیش فرض)
                                        </label>
                                        <textarea name="sms_text" id="sms_text" class="form-control"  rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mr-2 ml-2">  
                                        <input type="submit" id="submit_form" class="btn btn-success mt-75 mb-1 float-right" value="@t(درج)">
                                        <a href="{{ route('sms.index') }}" class="btn btn-warning mt-75 float-right mr-50 mb-1">@t(بازگشت)</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->


    </x-slot>
</x-base>
