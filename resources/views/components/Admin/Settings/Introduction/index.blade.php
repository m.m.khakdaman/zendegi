<x-base title="@t(سمت ها)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(نحوه آشنایی با مرکز)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item">@t(تنظیمات)</li>
                    <li class="breadcrumb-item">@t(تنظیمات مراجعین)</li>
                    <li class="breadcrumb-item active">@t(نحوه آشنایی با مرکز)</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="invoice-create-btn mb-1">
                            <a href="{{ route('introduction.create') }}" class="btn btn-primary glow invoice-create"
                                role="button" aria-pressed="true">@t(تعریف نحوه جدید آشنایی)</a>
                        </div>
                        <div class="table-responsive ">
                            <table class="table data-table nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@t(نحوه آشنایی با مرکز)</th>
                                        <th>@t(کاربر)</th>
                                        <th>@t(سایر)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($introductions as $method)
                                    <tr>
                                       <td>{{ $method->id }}</td>
                                       <td>{{ $method->Introduction_method }}</td>
                                       <td><small>{{ $method->operator->name.' '.$method->operator->family }}</small></td>
                                       <td>
                                        @if ($method->status != 0)
                                            <a href="{{ route('introduction.activation', $method->id) }}" class="text-success" data-toggle="tooltip" data-placement="top" title="@t(برای غیر فعال کردن کلیک کنید.)"><i class="bx bx-check-circle"></i></a>
                                        @else
                                            <a href="{{ route('introduction.activation', $method->id) }}" class="text-danger" data-toggle="tooltip" data-placement="top" title="@t(برای فعال کردن کلیک کنید.)"><i class="bx bxs-x-circle"></i></a>
                                        @endif 
                                        <a href="{{ route('introduction.edit', $method->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ویرایش)"><i class="bx bx-edit"></i></a>
                                        {{-- <a href="{{ route('sms.send', $user->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ارسال پیامک برای){{ ' '.$user->name.' '.$user->family }}"><i class="bx bx-message-dots"></i></a> --}}
                                        {{-- <a href="{{ route('sms.index', $user->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(بازنشانی گذرواژه برای){{ ' '.$user->name.' '.$user->family }}"><i class="bx bx-reset"></i></a> --}}
                                       </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->


    </x-slot>
</x-base>
