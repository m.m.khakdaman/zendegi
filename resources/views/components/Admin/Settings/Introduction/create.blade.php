<x-base title="@t(سمت ها)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(ساخت نحوه جدید آشنایی)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item">@t(تنظیمات)</li>
                    <li class="breadcrumb-item">@t(تنظیمات مراجعین)</li>
                    <li class="breadcrumb-item"><a href="{{ route('introduction.index') }}">@t(نحوه آشنایی)</a></li>
                    <li class="breadcrumb-item active">@t(ساخت نحوه جدید آشنایی)</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="invoice-create-btn mb-1">
                            <h5>@t(ساخت نحوه جدید آشنایی)</h5>
                        </div>
                        <div class="col-md-12">
                            <form action="{{ route('introduction.store') }}" 
                                class="wizard-validation" 
                                method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row align-items-center row">
                                    <div class="col-lg-2 col-3">
                                      <label class="col-form-label">@t(عنوان نحوه آشنایی)</label>
                                    </div>
                                    <div class="col-lg-10 col-9">
                                        <input type="text" id="introduction_method" class="form-control" name="introduction_method" placeholder="@t(عنوان نحوه آشنایی را در این قسمت وارد کنید.)">
                                    </div>
                                </div>
                                <div class="row float-right">
                                    <a type="button" class="btn btn-danger mr-50" href="{{ route('introduction.index') }}">@t(انصراف)</a>
                                    <input type="submit" class="btn btn-success mr-1" value="@t(ثبت)">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->


    </x-slot>
</x-base>
