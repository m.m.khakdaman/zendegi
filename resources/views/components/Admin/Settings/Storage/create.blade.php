<x-base title="@t(فرم ساخت اقلام مصرفی)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(ساخت اقلام مصرفی)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item">@t(تنظیمات)
                </li>
                <li class="breadcrumb-item">@t(تنظیمات انبار)
                </li>
                <li class="breadcrumb-item">@t(اقلام مصرفی)
                </li>
                <li class="breadcrumb-item active"><a href="#">@t(ساخت اقلام مصرفی)</a>
                </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section class="faq-search">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>@t(فرم تعریف کالای مصرفی جدید)</h5>
                    </div>
                    <div class="card-content">
                        <div class="card-body p-0">
                            @php
                            $tmp = 0;
                        @endphp
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>@t(کد پیام)</th>
                                <th>@t(پیامک پیش فرض)</th>
                                <th>@t(اپراتور)</th>
                                <th>@t(وضعیت)</th>
                                <th>@t(عملیات)</th>
                            </thead>
                            @foreach ($kala_types as $kala)
                            <tr>
                                <td>{{ ++$tmp }}</td>
                                <td>{{ $kala->id }}</td>
                                <td>{{ $kala->value }}</td>
                                <td>{{ $kala->operator_id }}</td>
                                <td>
                                    @if ($kala->status)
                                        <a class="text-success" href="#" data-toggle="tooltip" data-placement="right" title="@t(برای غیرفعال کردن این پیامک کلیک کنید.)">
                                            <i class="bx bx-check-circle"></i>
                                        </a>
                                    @else
                                        <a class="text-danger" href="#" data-toggle="tooltip" data-placement="right" title="@t(برای فعال کردن این پیامک کلیک کنید.)">
                                            <i class="bx bx-x-circle"></i>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="#" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ویرایش پیامک با کد){{ ' '.$kala->key}}">
                                        <i class="bx bx-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>@t(فرم تعریف کالای مصرفی جدید)</h5>
                    </div>
                    <div class="card-content">
                        <div class="card-body p-0">
                            <form action="{{ route('storage.kala_type_store') }}" 
                            class="wizard-validation" 
                            method="POST"
                            enctype="multipart/form-data">
                            @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mr-2 ml-2">
                                            <label for="sms_text">
                                                @t(متن پیامک پیش فرض)
                                            </label>
                                            <textarea name="sms_text" id="sms_text" class="form-control"  rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mr-2 ml-2">  
                                            <input type="hidden" name="key" value="sms_list{{  date('his') }}">                                      
                                            <input type="submit" id="submit_form" class="btn btn-success mt-75 mb-1 float-right" value="@t(درج)">
                                            <input type="reset" class="btn btn-warning mt-75 float-right mr-50 mb-1" value="@t(پاک کن)" name="" id="">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->


    </x-slot>
</x-base>
