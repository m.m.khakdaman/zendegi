<x-base title="@t(سخن بزرگان)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(سخن بزرگان)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item">@t(تنظیمات)</li>
                  <li class="breadcrumb-item">@t(تنظیمات عمومی)</li>
                  <li class="breadcrumb-item active">@t(سخن بزرگان)</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section class="faq-search">
            @include('partials.flash')
            <div class="row">
            <div class="col-12">
            <div class="card background-color">
                <div class="card-header">
                    <a href="{{ route('sentence.create') }}" class="btn btn-primary"><i class="bx bx-plus"></i>@t(درج جمله جدید)</a>
                </div>
                <div class="card-content">
                    <div class="card-body p-0">
                        @php
                            $tmp = 0;
                        @endphp
                        <table class="table-bordered table-hover m-1">
                            <thead>
                                <th>#</th>
                                <th>@t(گوینده)</th>
                                <th>@t(متن جمله)</th>
                                <th>@t(اپراتور)</th>
                                <th>@t(وضعیت)</th>
                                <th>@t(عملیات)</th>
                            </thead>
                            @foreach ($sentences as $sentence)
                            <tr style="height: 40px">
                                <td>{{ ++$tmp }}</td>
                                <td><span class="text-secondary">{{ $sentence->from }}</span></td>
                                <td>{{ $sentence->sentence }}</td>
                                <td><span class="badge badge-light-warning">{{ $sentence->operator->name.' '.$sentence->operator->family }}</span></td>
                                <td class="text-center">
                                    @if ($sentence->status)
                                        <a class="text-success" href="{{ route('sentence.activation',$sentence->id) }}" data-toggle="tooltip" data-placement="right" title="@t(برای غیرفعال کردن کلیک کنید.)">
                                            <i class="bx bx-check-circle"></i>
                                        </a>
                                    @else
                                        <a class="text-danger" href="{{ route('sentence.activation',$sentence->id) }}" data-toggle="tooltip" data-placement="right" title="@t(برای فعال کردن کلیک کنید.)">
                                            <i class="bx bx-x-circle"></i>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('sentence.edit', $sentence->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ویرایش)">
                                        <i class="bx bx-edit"></i>
                                    </a>
                                    <a href="#" data-toggle="modal" class="text-danger" data-target="#_{{ $sentence->id }}" title="@t(پاک کردن این جمله)">
                                        <i class="bx bx-trash"></i>
                                    </a>
                                    
                                    <!-- Modal -->
                                    <div class="modal fade" id="_{{ $sentence->id }}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalCenterTitle"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    آیا می خواهید جمله شماره {{ $sentence->id }}  را حذف نمایید ؟
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{ route('sentence.destroy', $sentence->id) }}"
                                                        method="post">
                                                        @method('delete')
                                                        @csrf
                                                        <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal">خیر</button>
                                                        <button type="submit"
                                                            class="btn btn-success">بله</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        
                       
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script>$('div.alert').delay(3000).slideUp(300);</script>
        <!-- END: Page JS-->


    </x-slot>
</x-base>
