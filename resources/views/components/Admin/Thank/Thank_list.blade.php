<x-base title="@t(شکایات و رضایتمندی ها)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">

        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(شکایات و رضایتمندی ها)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                            </li>

                            <li class="breadcrumb-item active">@t(شکایات و رضایتمندی ها)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <!-- Nav Justified Starts -->
    <section id="nav-justified">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">

                            <ul class="nav nav-tabs nav-justified" id="myTab2" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active btn-success" id="home-tab-justified" data-toggle="tab"
                                        href="#home-just" role="tab" aria-controls="home-just" aria-selected="true">
                                        @t(رضایتمندی ها)
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn-danger" id="profile-tab-justified" data-toggle="tab"
                                        href="#profile-just" role="tab" aria-controls="profile-just"
                                        aria-selected="true">
                                        @t(شکایات و نارضایتی ها)
                                    </a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="tab-pane active" id="home-just" role="tabpanel"
                                    aria-labelledby="home-tab-justified">
                                    <form action="{{ route('thank.store') }}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="user">مراجع</label>
                                                    <select type="text" class="form-control" id="user" validitymsg='مراجع را انتخاب کنید.'
                                                        style="width: 100%" name="user" placeholder="مراجع" required>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="audience1">@t(رضایت از)</label>
                                                    <select name="audience" id="audience1" style="width: 100%"
                                                        class="form-control select2" required>
                                                        <option value="5828">@t(مرکز مشاوره)</option>
                                                        @foreach ($admins->merge($advisors) as $admin)
                                                            <option value="{{ $admin->id }}">
                                                                {{ $admin->name ?? '' }}&nbsp;{{ $admin->family ?? '' }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reason_id2">@t(انتخاب دسته)</label>
                                                    <select name="reason_id" id="reason_id2" style="width: 100%"
                                                        class="form-control select2" required>
                                                        @foreach ($dislike_reasons as $reason)
                                                            <option value="{{ $reason->id }}">
                                                                {{ $reason->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <div class="form-group">
                                                    <div class="controls form-label-group ">
                                                        <textarea name="note" id="note" rows="4" for="note" class="form-control"
                                                            placeholder="@t(متن تشکر و رضایتمندی)"></textarea>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="flag" value="like">
                                                <input type="submit" class="btn btn-success float-right" value="ثبت">
                                            </div>
                                        </div>
                                    </form>
                                    <div class="table-responsive ">
                                        <table class="table data-table nowrap font-small-2" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>@t(ردیف)</th>
                                                    <th>@t(کاربر)</th>
                                                    <th>@t(مخاطب)</th>
                                                    <th>@t(متن)</th>
                                                    <th>@t(سایر)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $tmp = 0;
                                                @endphp
                                                @foreach ($likes as $like)
                                                    <tr>
                                                        <td>{{ ++$tmp }}</td>
                                                        <td>{{ $like->user->name ?? '' }}&nbsp;{{ $like->user->family ?? '' }}
                                                        </td>
                                                        <td>{{ $like->audience->name ?? '' }}&nbsp;{{ $like->audience->family ?? '' }}
                                                        </td>
                                                        <td>{{ $like->note }}</td>
                                                        <td>
                                                            <i class="bx bx-user" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="{{ $like->operator->name ?? '' }}&nbsp;{{ $like->operator->family ?? '' }}"></i>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="profile-just" role="tabpanel"
                                    aria-labelledby="profile-tab-justified">
                                    <form action="{{ route('thank.store') }}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="user2">مراجع</label>
                                                    <select id="user2" class="form-control" validitymsg='مراجع را انتخاب کنید.'
                                                        style="width: 100%" name="user" placeholder="مراجع" required>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="audience">@t(ناراضی از)</label>
                                                    <select name="audience" id="audience" style="width: 100%"
                                                        class="form-control select2" required>
                                                        <option value="5828">@t(مرکز مشاوره)</option>
                                                        @foreach ($admins->merge($advisors) as $admin)
                                                            <option value="{{ $admin->id }}">
                                                                {{ $admin->name ?? '' }}&nbsp;{{ $admin->family ?? '' }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reason_id">@t(انتخاب دسته)</label>
                                                    <select name="reason_id" id="reason_id" style="width: 100%"
                                                        class="form-control select2" required>
                                                        @foreach ($dislike_reasons as $reason)
                                                            <option value="{{ $reason->id }}">
                                                                {{ $reason->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <div class="form-group">
                                                    <div class="controls form-label-group ">
                                                        <textarea name="note" id="note" rows="4" for="note" class="form-control"
                                                            placeholder="@t(متن شکایت و نارضایتی)"></textarea>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="flag" value="dislike">
                                                <input type="submit" class="btn btn-danger float-right" value="ثبت">
                                            </div>
                                        </div>
                                    </form>
                                    <div class="table-responsive ">
                                        <table class="table data-table nowrap font-small-2" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>@t(ردیف)</th>
                                                    <th>@t(کاربر)</th>
                                                    <th>@t(مخاطب)</th>
                                                    <th>@t(متن)</th>
                                                    <th>@t(سایر)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($dislikes as $key => $dislike)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $dislike->user->name ?? '' }}&nbsp;{{ $dislike->user->family ?? '' }}
                                                        </td>
                                                        <td>{{ $dislike->audience->name ?? '' }}&nbsp;{{ $dislike->audience->family ?? '' }}
                                                        </td>
                                                        <td>{{ $dislike->note }}</td>
                                                        <td>
                                                            <i class="bx bx-user" data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="{{ $dislike->operator->name ?? '' }}&nbsp;{{ $dislike->operator->family ?? '' }}"></i>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Nav Justified Ends -->
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->
        <script>
            $('.select2').select2();
            $('#user,#user2').select2({
                ajax: {
                    url: '{{ route('customer.get_all_ajax') }}',
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });
        </script>

    </x-slot>
</x-base>
