<x-base>
    <x-slot name='title'>دسته بندی جدید</x-slot>
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <!-- END: Page CSS-->
        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(دسته جدید)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('education_level.index') }}">@t(تنظیمات)</a>
                            </li>
                            <li class="breadcrumb-item active">@t(دسته جدید شکایات و رضایتمندیها)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(دسته جدید)</h4>
                    </div>
                    <div class="card-content mt-2">
                        <div class="card-body">
                            @include('layouts.errors')
                            <form action="{{ route('Thanks_reason_update',$reason->id) }}" method="POST"
                                class="wizard-horizontal" novalidate>
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="title">عنوان</label>
                                                <input type="text" name="title" class="form-control" id="title" required 
                                                    value="{{ $reason->title }}"
                                                    data-validation-required-message="پر کردن فیلد عنوان اجباری است." 
                                                    placeholder="عنوان">
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row pt-1">
                                    <div class="col-12">
                                        <fieldset class="form-group">
                                            <label>@t(گروه)</label>
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-success radio-glow">
                                                            <input type="radio" id="like" name="flag" value="like"
                                                            @if ($reason->flag == 'like')
                                                                checked
                                                            @endif>
                                                            <label for="like">@t(رضایتمندی)</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-danger radio-glow">
                                                            <input type="radio" id="dislike" name="flag" value="dislike"
                                                            @if ($reason->flag == 'dislike')
                                                                checked
                                                            @endif>
                                                            <label for="dislike">@t(شکایات)</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                          </ul>
                                        </fieldset>                                       
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <fieldset class="form-group">
                                            <label>@t(وضعیت)</label>
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-success radio-glow">
                                                            <input type="radio" id="active" name="status" value="active"
                                                            @if ($reason->status == 'active')
                                                                checked
                                                            @endif>
                                                            <label for="active">@t(فعال)</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio radio-danger radio-glow">
                                                            <input type="radio" id="inactive" name="status" value="inactive"
                                                            @if ($reason->status == 'inactive')
                                                                checked
                                                            @endif>
                                                            <label for="inactive">@t(غیرفعال)</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                          </ul>
                                        </fieldset>                                       
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="comment">@t(توضیحات)</label>
                                            <textarea class="form-control" name="comment" id="comment" cols="30" rows="3" 
                                            placeholder="{{ $reason->comment }}"></textarea>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group float-right">
                                            <input type="submit" class="form-control btn btn-success" value="ثبت" required>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <!-- END: Page JS-->

    </x-slot>
</x-base>
