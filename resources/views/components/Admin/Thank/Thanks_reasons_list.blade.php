<x-base >
    <x-slot name='title'>دسته بندی شکایات ورضایتمندیها</x-slot>
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(دسته بندی شکایات و رضایتمندیها)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('education_level.index') }}">@t(تنظیمات)</a>
                            </li>
                            <li class="breadcrumb-item active">@t(دسته بندی شکایات و رضایتمندیها)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">        
                        <!-- create invoice button-->
                        {{-- @can('education_level_create') --}}
                        <div class="invoice-create-btn">
                            <a href="{{ route('Thanks_reason_create') }}" class="btn btn-primary" role="button" aria-pressed="true">
                                ایجاد دسته جدید</a>
                        </div>
                        {{-- @endcan --}}
                        @php
                            $likereasons = $reasons->where('flag', 'like');
                            $dislikereasons = $reasons->where('flag', 'dislike');
                        @endphp
                        <div class="card-content">
                            <div class="card-body">                              
                              <ul class="nav nav-tabs nav-justified" id="myTab2" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active btn-success" id="home-tab-justified" data-toggle="tab" href="#home-just" role="tab" aria-controls="home-just" aria-selected="true">
                                    @t(رضایتمندی ها)
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link btn-danger" id="profile-tab-justified" data-toggle="tab" href="#profile-just" role="tab" aria-controls="profile-just" aria-selected="true">
                                    @t(شکایات و نارضایتی ها)
                                  </a>
                                </li>                              
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div class="tab-pane active" id="home-just" role="tabpanel" aria-labelledby="home-tab-justified">
                                    <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>@t(ردیف)</th>
                                                <th>@t(عنوان)</th>
                                                <th>@t(وضعیت)</th>
                                                <th>@t(توضیحات)</th>
                                                <th>تنظیمات</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $counter = 1;
                                            @endphp
                                            @foreach ($likereasons as $reason)
                                                <tr>
                                                    <td><span data-toggle="tooltip" data-placement="top" title="{{ $reason->id }}">{{ $counter++ }}</span></td>
                                                    <td>{{ $reason->title }}</td>
                                                    <td>
                                                        @if ($reason->status == 'active')
                                                            <span class="text-success"><b>فعال</b></span>
                                                        @else
                                                            <span class="text-danger"><b>غیرفعال</b></span>
                                                        @endif                                                            
                                                    </td>
                                                    <td>{{ $reason->comment }}</td>
                                                    <td>
                                                        <div class="btn-group btn-group-sm">
                                                                <a href="{{ route('Thanks_reason_edit', $reason->id) }}"
                                                                    class="btn btn-warning text-white">ویرایش</a>
                                                                <button class="btn btn-danger text-white" data-toggle="modal"
                                                                    data-target="#_{{ $reason->id }}">حذف</button>
                                                        </div>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="_{{ $reason->id }}" tabindex="-1" reason="dialog"
                                                            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" reason="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        آیا می خواهید -- {{ $reason->title }} -- را حذف نمایید ؟
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <form action="{{ route('Thanks_reason_destroy', $reason->id) }}"
                                                                            method="GET">
                                                                            {{-- @method('post') --}}
                                                                            @csrf
                                                                            <button type="button" class="btn btn-danger"
                                                                                data-dismiss="modal">خیر</button>
                                                                            <button type="submit" class="btn btn-success">بله</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="profile-just" role="tabpanel" aria-labelledby="profile-tab-justified">
                                    <div class="table-responsive ">
                                        <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>@t(عنوان)</th>
                                                    <th>@t(وضعیت)</th>
                                                    <th>@t(توضیحات)</th>
                                                    <th>تنظیمات</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $counter = 1;
                                                @endphp
                                                @foreach ($dislikereasons as $reason)
                                                    <tr>
                                                        <td><span data-toggle="tooltip" data-placement="top" title="{{ $reason->id }}">{{ $counter++ }}</span></td>
                                                        <td>{{ $reason->title }}</td>
                                                        <td>
                                                            @if ($reason->status == 'active')
                                                            <span class="text-success"><b>فعال</b></span>
                                                        @else
                                                            <span class="text-danger"><b>غیرفعال</b></span>
                                                        @endif                                                             
                                                        </td>
                                                        <td>{{ $reason->comment }}</td>
                                                        <td>
                                                            <div class="btn-group btn-group-sm">
                                                                    <a href="{{ route('Thanks_reason_edit', $reason->id) }}"
                                                                        class="btn btn-warning text-white">ویرایش</a>
                                                                    <button class="btn btn-danger text-white" data-toggle="modal"
                                                                        data-target="#_{{ $reason->id }}">حذف</button>
                                                            </div>
                                                            <!-- Modal -->
                                                            <div class="modal fade" id="_{{ $reason->id }}" tabindex="-1" reason="dialog"
                                                                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" reason="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            آیا می خواهید -- {{ $reason->title }} -- را حذف نمایید ؟
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <form action="{{ route('Thanks_reason_destroy', $reason->id) }}"
                                                                                method="GET">
                                                                                {{-- @method('post') --}}
                                                                                @csrf
                                                                                <button type="button" class="btn btn-danger"
                                                                                    data-dismiss="modal">خیر</button>
                                                                                <button type="submit" class="btn btn-success">بله</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                              </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->

        <script>
            $(document).ready(function() {
                // Setup - add a text input to each footer cell
                $('.invoice-data-table tfoot th').each(function() {
                    var title = $(this).text();
                    if (title != 'تنظیمات') {
                        $(this).html('<input type="text" placeholder="جست جو در ' + title +
                            '"  class="form-control form-control-sm" />');

                    }
                });

                // DataTable
                var table = $('.invoice-data-table').DataTable({
                    language: {
                        "sEmptyTable": "هیچ داده‌ای در جدول وجود ندارد",
                        "sInfo": "نمایش _START_ تا _END_ از _TOTAL_ ردیف",
                        "sInfoEmpty": "نمایش 0 تا 0 از 0 ردیف",
                        "sInfoFiltered": "(فیلتر شده از _MAX_ ردیف)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "نمایش _MENU_ ردیف",
                        "sLoadingRecords": "در حال بارگزاری...",
                        "sProcessing": "در حال پردازش...",
                        "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                        "oPaginate": {
                            "sFirst": "برگه‌ی نخست",
                            "sLast": "برگه‌ی آخر",
                            "sNext": "بعدی",
                            "sPrevious": "قبلی"
                        },
                        "oAria": {
                            "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                            "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                        },
                        "sSearch": "",
                        "sSearchPlaceholder": "جستجوی صورتحساب",
                    },
                    dom: '<"top d-flex flex-wrap"><"clear">rt<"bottom"p>',
                    initComplete: function() {
                        // Apply the search
                        this.api().columns().every(function() {
                            var that = this;

                            $('input', this.footer()).on('keyup change clear', function() {
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                        });
                    }
                });

            });

        </script>

    </x-slot>
</x-base>
