<x-base name="title">
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <section>
        <!-- Notification Widget Starts -->
        <div class="col-lg-12">
            <div class="card widget-notification">
                <div class="card-header border-bottom">
                    <h4 class="card-title d-flex align-items-center">
                    <i class="bx bx-bell font-medium-4 mr-1"></i>@t(یادآوری ها)</h4>
                    <div class="heading-elements">
                        <button type="button" class="btn btn-sm btn-light-primary">@t(افزودن یادآوری جدید)</button>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body p-0">
                        <ul class="list-group list-group-flush">
                            {{-- <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                <div class="list-left d-flex align-items-center">
                                <div class="list-icon mr-1">
                                    <div class="avatar bg-rgba-primary m-0 p-25">
                                    <div class="avatar-content">
                                        <i class="bx bx-edit-alt text-primary font-medium-5"></i>
                                    </div>
                                    </div>
                                </div>
                                <div class="list-content mt-n25">
                                    <span class="list-title">گزارش جدید دریافت شد</span>
                                    <small class="text-muted d-block">چند ثانیه پیش</small>
                                </div>
                                </div>
                                <div class="readable-mark-icon" data-toggle="tooltip" data-placement="left" title="Mark as read">
                                <i class="bx bxs-circle text-light-primary font-medium-1"></i>
                                </div>
                            </li> --}}
                            @foreach ($reminders as $reminder)
                            <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                <div class="list-left d-flex align-items-center">
                                <div class="list-icon mr-1">
                                    <div class="avatar bg-rgba-danger m-0 p-25">
                                        <div class="avatar-content">
                                            <span class="font-medium-1 text-danger">03:44</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-content mt-n25">
                                    <span class="list-title">{{ $reminder->title }}</span>
                                    <small class="text-muted d-block">{{ $reminder->note }}</small>
                                </div>
                                </div>
                                <div class="readable-mark-icon" data-toggle="tooltip" data-placement="left" title="Mark as read">
                                <i class="bx bxs-circle text-light-primary font-medium-1"></i>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Notification Widget Ends -->
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
    </x-slot>
</x-base>