<x-base title="@t(انبار)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(لیست اموال)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item ">@t(انبار)
                    </li>
                    <li class="breadcrumb-item ">@t(اموال اداری)
                    </li>
                  <li class="breadcrumb-item active">@t(لیست اموال)
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Basic tabs start -->
    <section id="basic-tabs-components">
        <div class="card">
        <div class="card-content">
            <div class="card-body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" aria-controls="home" role="tab" aria-selected="true">
                        <i class="bx bx-list-ol align-middle"></i>
                        <span class="align-middle">@t(لیست کلیه اموال)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="now-tab" data-toggle="tab" href="#now" aria-controls="now" role="tab" aria-selected="false">
                        <i class="bx bx-user align-middle"></i>
                        <span class="align-middle">@t(موجودی فعلی)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="keeping-tab" data-toggle="tab" href="#keeping" aria-controls="keeping" role="tab" aria-selected="false">
                        <i class="bx bx-message-square align-middle"></i>
                        <span class="align-middle">@t(لیست اموال تحویلی)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="broken-tab" data-toggle="tab" href="#broken" aria-controls="broken" role="tab" aria-selected="false">
                        <i class="bx bx-message-square align-middle"></i>
                        <span class="align-middle">@t( اموال از کار افتاده)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-success" id="enter-tab" data-toggle="tab" href="#enter" aria-controls="enter" role="tab" aria-selected="false">
                        <i class="bx bx-message-square align-middle"></i>
                        <span class="align-middle">@t(ورود کالا به انبار)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn-warning" id="exit-tab" data-toggle="tab" href="#exit" aria-controls="exit" role="tab" aria-selected="false">
                        <i class="bx bx-message-square align-middle"></i>
                        <span class="align-middle">@t(خروج کالا از انبار)</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                {{-- شروع لیست کلیه اموال --}}
                <div class="tab-pane active" id="home" aria-labelledby="home-tab" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>@t(نام کالا)</th>
                                    <th>@t(برچسب اموال)</th>
                                    <th>@t(تاریخ خرید)</th>
                                    <th>@t(شماره فاکتور)</th>
                                    <th>@t(کاربر)</th>
                                    <th>@t(عملیات)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allitems as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td><span class="badge badge-pill badge-secondary" >{{ $item->label_code }}</span></td>
                                    <td><span class="badge badge-pill badge-warning">{{ $item->date_of_buy }}</span></td>
                                    <td><span class="badge badge-pill badge-primary">{{ $item->factor_number }}</span></td>
                                    <td>{{ $item->keeper->name.' '.$item->keeper->family }}</td>
                                    <td>
                                        <i class="bx bx-info-circle" data-toggle="tooltip" title="{{ $item->note }}" data-placement="right"></i>
                                        <a href="dd">
                                            @if ($item->status == 'اسقاطی')
                                                <i class="bx bx-dislike text-danger" data-toggle="tooltip" data-placement="top" title="{{ $item->status }}"></i>
                                            @elseif ($item->status == 'معیوب')
                                                <i class="bx bx-wrench text-warning" data-toggle="tooltip" data-placement="top" title="{{ $item->status }}"></i>
                                            @else
                                                <i class="bx bx-like text-success" data-toggle="tooltip" data-placement="top" title="{{ $item->status }}"></i>
                                            @endif
                                            
                                        </a>
                                    </td>
                                </tr>    
                                @endforeach                                   
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- پایان لیست کلیه اموال --}}
                {{-- شروع موجودی فعلی --}}
                <div class="tab-pane" id="now" aria-labelledby="now-tab" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>@t(نام کالا)</th>
                                    <th>@t(برچسب اموال)</th>
                                    <th>@t(تاریخ خرید)</th>
                                    <th>@t(شماره فاکتور)</th>
                                    <th>@t(کاربر)</th>
                                    <th>@t(سایر)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($nowitems as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td><span class="badge badge-pill badge-warning">{{ $item->label_code }}</span></td>
                                    <td><span class="badge badge-pill badge-secondary">{{ $item->date_of_buy }}</span></td>
                                    <td><span class="badge badge-pill">{{ $item->factor_number }}</span></td>
                                    <td><small>{{ $item->keeper->name.' '.$item->keeper->family }}</small></td>
                                    <td>
                                        <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="right" title="{{ $item->note }}"></i>
                                        <i class="bx bx-face" data-toggle="tooltip" data-placement="right" title="@t(فروشنده:){{ ' '.$item->seller }}"></i>
                                    </td>
                                </tr>    
                                @endforeach                                   
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- پایان موجودی فعلی --}}
                {{-- شروع لیست اموال تحویلی --}}
                <div class="tab-pane" id="keeping" aria-labelledby="keeping-tab" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>@t(نام کالا)</th>
                                    <th>@t(برچسب)</th>
                                    <th>@t(تاریخ خرید)</th>
                                    <th>@t(شماره فاکتور)</th>
                                    <th>@t(کاربر)</th>
                                    <th>حقوق</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($keepingitems as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->label_code }}</td>
                                    <td>{{ $item->date_of_buy }}</td>
                                    <td>{{ $item->factor_number }}</td>
                                    <td>{{ $item->keeper->name.' '.$item->keeper->family }}</td>
                                    <td>320,800</td>
                                </tr>    
                                @endforeach                                   
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- پایان لیست اموال تحویلی --}}
                {{-- شروع لیست اموال از کار افتاده --}}
                <div class="tab-pane" id="broken" aria-labelledby="broken-tab" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table zero-configuration">
                            <thead>
                                <tr>
                                    <th>@t(نام کالا)</th>
                                    <th>@t(برچسب)</th>
                                    <th>@t(تاریخ خرید)</th>
                                    <th>@t(شماره فاکتور)</th>
                                    <th>@t(کاربر)</th>
                                    <th>@t(عملیات)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($brokenitems as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->label_code }}</td>
                                    <td>{{ $item->date_of_buy }}</td>
                                    <td>{{ $item->factor_number }}</td>
                                    <td>{{ $item->keeper->name.' '.$item->keeper->family }}</td>
                                    <td>
                                        <a href="ddd"><i class="bx bx-info-circle"></i></a>
                                    </td>
                                </tr>    
                                @endforeach                                   
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- پایان لیست اموال از کار افتاده --}}
                {{-- شروع فرم ورود کالا به انبار --}}
                <div class="tab-pane" id="enter" aria-labelledby="enter-tab" role="tabpanel">
                    <h5>@t(فرم ورود کالا به انبار)</h5>
                    <form action="{{ route('storage.store') }}" 
                        class="wizard-validation" 
                        method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>
                                        @t(نام کالا)
                                    </label>
                                    <input type="text" class="form-control" name="product_name">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="label_code">
                                        @t(شماره برچسب اموال)
                                    </label>
                                    <input type="text" class="form-control" name="label_code">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="price">
                                        @t(قیمت خرید)
                                    </label>
                                    <input type="text" class="form-control" name="price">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="factor_number">
                                        @t(شماره فاکتور)
                                    </label>
                                    <input type="text" class="form-control" name="factor_number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3 form-group">
                                <label>@t(تاریخ خرید)</label>
                                <div class="controls form-label-group ">
                                    <input type="text" class="form-control" name="date_of_buy" id="date_sw"
                                        pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                        data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                        placeholder="انتخاب تاریخ" aria-invalid="false"
                                        onclick="change_date(this.value)">
                                    <div class="form-control-position">
                                        <i class="bx bx-calendar">
                                        </i>
                                    </div>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="factor_number">
                                        @t(مشخصات فروشنده)
                                    </label>
                                    <input type="text" class="form-control" name="seller">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="note">
                                        @t(توضیحات)
                                    </label>
                                    <textarea name="note" id="note" class="form-control"  rows="1"></textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="keeper">
                                        @t(تحویل به:)
                                    </label>
                                    <select class="select2 form-control" name="keeper_id" id="keeper" required>
                                        <option value="square">@t(تحویل گیرنده را انتخاب کنید...)</option>
                                        @foreach ($admins as $admin)
                                        <option value="{{ $admin->id }}">{{ $admin->name.' '.$admin->family }}</option>
                                        @endforeach
                                    </select>
                                </div>
                              </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">  
                                    <input type="hidden" name="status" value="سالم">                                      
                                    <input type="hidden" name="type" value="اداری">
                                    <input type="submit" id="submit_form" class="btn btn-success mt-75 mb-1 float-right" value="@t(درج)">
                                    <input type="reset" class="btn btn-warning mt-75 float-right mr-50 mb-1" value="@t(پاک کن)" name="">
                                </div>
                            </div>
                        </div>
                    </form>                
                </div>
                {{-- پایان فرم ورود کالا به انبار --}}
                <div class="tab-pane" id="exit" aria-labelledby="exit-tab" role="tabpanel">
                    <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف
                    </p>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Basic Tag Input end -->
 
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/datatables/datatable.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src='/assets/fullcalendar/main.min.js'></script>
        <!-- END: Page JS-->
        <script>
            function G2J(D) {
                dd = D.toLocaleDateString().split('/');

                var a = (
                    jd_to_persian(
                        gregorian_to_jd(
                            dd[2],
                            dd[0],
                            parseInt(dd[1])
                        )
                    )
                );
                return (a[0] + '/' + a[1] + '/' + a[2]);
            }

            function change_date(D) {
                dd = D.split('/');
                var a = (
                    jd_to_gregorian(
                        persian_to_jd(
                            parseInt(dd[0]),
                            parseInt(dd[1]),
                            parseInt(dd[2])
                        )
                    )
                );
                calendar.gotoDate(new Date(a[0] + '-' + a[1] + '-' + a[2]));
            }

            $('#date_sw').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
           
        </script>
    </x-slot>
</x-base>
