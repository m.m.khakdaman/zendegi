<x-base title="@t(پیامک)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(ورود کالا به انبار)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                  <li class="breadcrumb-item ">@t(انبار)</li>
                    <li class="breadcrumb-item ">@t(اموال اداری)</li>
                  <li class="breadcrumb-item active">@t(ورود کالا به انبار)
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="col-12">
            <div class="card background-color">
                <div class="card-header">
                    <a href="{{ route('storage.index') }}" class="btn btn-secondary">@t(لیست اموال)</a>
                </div>
                <div class="card-content">
                    <div class="card-body p-0">
                        <form action="{{ route('storage.store') }}" 
                            class="wizard-validation" 
                            method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group mr-2 ml-2">
                                        <label for="sms_text">
                                            @t(نام کالا)
                                        </label>
                                        <input type="text" class="form-control" name="title">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group mr-2 ml-2">
                                        <label for="label_code">
                                            @t(شماره برچسب اموال)
                                        </label>
                                        <input type="text" class="form-control" name="label_code">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group mr-2 ml-2">
                                        <label for="price">
                                            @t(قیمت خرید)
                                        </label>
                                        <input type="text" class="form-control" name="price">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group mr-2 ml-2">
                                        <label for="factor_number">
                                            @t(شماره فاکتور)
                                        </label>
                                        <input type="text" class="form-control" name="factor_number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group mr-2 ml-2">
                                        <label for="date_of_buy">
                                            @t(تاریخ خرید)
                                        </label>
                                        <input type="text" class="form-control" name="date_of_buy">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group mr-2 ml-2">
                                        <label for="note">
                                            @t(توضیحات)
                                        </label>
                                        <textarea name="note" id="note" class="form-control"  rows="1"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mr-2 ml-2">  
                                        <input type="hidden" name="key" value="sms_list{{  date('his') }}">                                      
                                        <input type="submit" id="submit_form" class="btn btn-success mt-75 mb-1 float-right" value="@t(درج)">
                                        <input type="reset" class="btn btn-warning mt-75 float-right mr-50 mb-1" value="@t(پاک کن)" name="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->


    </x-slot>
</x-base>
