<x-base title="@t(پیامک)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(فرم مصرف کالا)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item ">@t(انبار)
                    </li>
                    <li class="breadcrumb-item ">@t(اموال مصرفی)
                    </li>
                  <li class="breadcrumb-item active">@t(فرم مصرف کالا)
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section class="faq-search">
        <div class="row">
            <div class="col-12">
            <div class="card background-color">
                <div class="card-header">
                    <h5>@t(فرم مصرف کالا)</h5>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form action="{{ route('storage.store') }}" 
                            class="wizard-validation" 
                            method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label>@t(نام کالا)</label>
                                    <select name="kala_type_id" class="form-control select2">
                                            <option value="">@t(انتخاب کنید....)</option>
                                        @foreach ($kala_types as $kala)
                                            <option value="{{ $kala->id }}">{{ $kala->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="label_code">
                                            @t(تعداد)
                                        </label>
                                        <input type="text" class="form-control" name="label_code">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="price">
                                            @t(مورد مصرف)
                                        </label>
                                        <input type="text" class="form-control" name="price">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="date_sw">@t(تاریخ مصرف)</label>
                                        <div class="controls form-label-group ">
                                            <input type="text" class="form-control" name="date_of_buy" id="date_sw"
                                                pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                                data-validation-pattern-message="فرمت فیلد معتبر نیست." value="{{ verta()->format('Y/m/d') }}"
                                                placeholder="انتخاب تاریخ" aria-invalid="false"
                                                onclick="change_date(this.value)">
                                            <div class="form-control-position">
                                                <i class="bx bx-calendar">
                                                </i>
                                            </div>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="date_of_buy">
                                            @t(تاریخ ثبت)
                                        </label>
                                        <input type="text" class="form-control" name="date_of_register" value="{{ verta()->format('Y/m/d') }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="date_of_buy">
                                            @t(ساعت ثبت)
                                        </label>
                                        <input type="text" class="form-control" name="date_of_register" value="{{ verta()->format('H:i') }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="note">
                                            @t(توضیحات)
                                        </label>
                                        <textarea name="note" id="note" class="form-control"  rows="1"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mr-2 ml-2">  
                                        <input type="hidden" name="key" value="sms_list{{  date('his') }}">                                      
                                        <input type="submit" id="submit_form" class="btn btn-success mt-75 mb-1 float-right" value="@t(درج)">
                                        <input type="reset" class="btn btn-warning mt-75 float-right mr-50 mb-1" value="@t(پاک کن)" name="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src='/assets/fullcalendar/main.min.js'></script>
        <!-- END: Page JS-->
         <script>
            function G2J(D) {
                dd = D.toLocaleDateString().split('/');

                var a = (
                    jd_to_persian(
                        gregorian_to_jd(
                            dd[2],
                            dd[0],
                            parseInt(dd[1])
                        )
                    )
                );
                return (a[0] + '/' + a[1] + '/' + a[2]);
            }

            function change_date(D) {
                dd = D.split('/');
                var a = (
                    jd_to_gregorian(
                        persian_to_jd(
                            parseInt(dd[0]),
                            parseInt(dd[1]),
                            parseInt(dd[2])
                        )
                    )
                );
                calendar.gotoDate(new Date(a[0] + '-' + a[1] + '-' + a[2]));
            }

            $('#date_sw').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
        </script>
    </x-slot>
</x-base>

