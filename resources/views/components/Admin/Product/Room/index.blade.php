<x-base title="اتاق">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        {{-- <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.8/css/rowReorder.dataTables.min.css"> --}}
        <!-- END: Page CSS-->

    </x-slot>

    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="invoice-create-btn mb-1">
                            <a href="{{ route('room.create') }}" class="btn btn-primary glow invoice-create"
                                role="button" aria-pressed="true">تعریف اتاق جدید</a>
                        </div>

                        <div class="table-responsive ">
                            <table class=" table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>@t(ردیف)</th>
                                        <th>@t(چیدمان)</th>
                                        <th>@t(بازچینی)</th>
                                        <th>عنوان</th>
                                        <th>شعبه</th>
                                        <th>ساختمان</th>
                                        <th>تنظیمات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rooms as $counter=>$room)
                                        <tr>
                                            <td>{{ ++$counter }}</td>
                                            <td>{{ $room->order }}</td>
                                            <td>
                                                @if ($room->order == 1)
                                                    <button class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="top" title="@t(نمی توان ردیف اول را بالاتر برد.)"><i class="bx bx-minus-circle"></i></button>
                                                    {{-- <a href="{{ route('room.index') }}" class="btn btn-icon btn-danger" id="position-bottom-end"><i class="bx bx-minus-circle"></i></a> --}}
                                                @else
                                                    <a href="{{ route('room.order', [$room->id, 'up']) }}" class="btn btn-icon btn-success"data-toggle="tooltip" data-placement="top" title="@t(این اتاق یک ردیف بالاتر برود.)"><i class="bx bx-upvote"></i></a>
                                                @endif
                                                   
                                                @if ($room->order >= $rooms->count())
                                                    <button class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="top" title="@t(نمی توان ردیف آخر را پایین تر برد.)"><i class="bx bx-minus-circle"></i></button>
                                                @else
                                                    <a href="{{ route('room.order', [$room->id, 'down']) }}" class="btn btn-icon btn-secondary" data-toggle="tooltip" data-placement="top" title="@t(این اتاق یک ردیف پایین تر برود.)"><i class="bx bx-downvote"></i></a>    
                                                @endif
                                            </td>
                                                
                                                {{-- <td>{{ $room->id }}</td> --}}
                                            <td>{{ $room->name }}</td>
                                            <td>{{ $room->branch->name }}</td>
                                            <td>{{ $room->building->title }}</td>
                                            <td>
                                                <div class="btn-group btn-group-sm">
                                                    {{-- @can('room.update') --}}
                                                    <a href="{{ route('room.edit', $room->id) }}"
                                                        class="btn btn-warning text-white">ویرایش</a>
                                                    {{-- @endcan --}}
                                                    {{-- @can('room.destroy') --}}
                                                    <button class="btn btn-danger text-white" data-toggle="modal"
                                                        data-target="#_{{ $room->id }}">حذف</button>
                                                    {{-- @endcan --}}
                                                </div>
                                                <!-- Modal -->
                                                <div class="modal fade" id="_{{ $room->id }}" tabindex="-1"
                                                    role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                آیا می خواهید -- {{ $room->name }} -- را حذف نمایید ؟
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form action="{{ route('room.destroy', $room->id) }}"
                                                                    method="post">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">خیر</button>
                                                                    <button type="submit"
                                                                        class="btn btn-success">بله</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                {{-- <tfoot>
                                    <tr>
                                        <th>~</th>
                                        <th>~</th>
                                        <th>#</th>
                                        <th>عنوان</th>
                                        <th>شعبه</th>
                                        <th>تنظیمات</th>
                                    </tr>
                                </tfoot> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        {{-- <script src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.min.js"></script> --}}
        <!-- END: Page JS-->

        {{-- <script>
            $(document).ready(function() {
                // Setup - add a text input to each footer cell
                $('.invoice-data-table tfoot th').each(function() {
                    var title = $(this).text();
                    if (title != 'تنظیمات') {
                        $(this).html('<input type="text" placeholder="جست جو در ' + title +
                            '"  class="form-control form-control-sm" />');

                    }
                });

                // DataTable
                var table = $('.invoice-data-table').DataTable({
                    // "scrollY": 500,
                    "scrollX": true,
                    "autoWidth": false,
                    rowReorder: {
                        selector: 'tr'
                    },
                    columnDefs: [{
                        targets: 0,
                        visible: false
                    }],
                    // rowReorder: true,
                    language: {
                        "sEmptyTable": "هیچ داده‌ای در جدول وجود ندارد",
                        "sInfo": "نمایش _START_ تا _END_ از _TOTAL_ ردیف",
                        "sInfoEmpty": "نمایش 0 تا 0 از 0 ردیف",
                        "sInfoFiltered": "(فیلتر شده از _MAX_ ردیف)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "نمایش _MENU_ ردیف",
                        "sLoadingRecords": "در حال بارگزاری...",
                        "sProcessing": "در حال پردازش...",
                        "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                        "oPaginate": {
                            "sFirst": "برگه‌ی نخست",
                            "sLast": "برگه‌ی آخر",
                            "sNext": "بعدی",
                            "sPrevious": "قبلی"
                        },
                        "oAria": {
                            "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                            "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                        },
                        "sSearch": "",
                        "sSearchPlaceholder": "جستجوی",
                    },
                });

            });
        </script> --}}

    </x-slot>
</x-base>
