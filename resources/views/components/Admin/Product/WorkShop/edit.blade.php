<x-base>
    <x-slot name='title'>
        @t(ویرایش کارگاه)
    </x-slot>
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/wizard.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(ویرایش کارگاه)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('workshop.index') }}">کارگاه ها</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('workshop.index') }}">لیست کارگاه ها</a></li>
                            <li class="breadcrumb-item active">@t(ویرایش کارگاه){{ ' '.$wsh->product_name }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- vertical Wizard start-->
    <section id="vertical-wizard form-repeater-wrapper">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">@t(ویرایش کارگاه){{ ' '.$wsh->product_name }}</h4>
            </div>
            <div class="card-content">
            <div class="card-body">
                <form action="{{ route('workshop.update', $wsh->id) }}" class="wizard-vertical" id="idform"
                method="post"
                >
                @csrf
                @method('PUT')
                <!-- step 1 -->
                <h3>
                    <span class="fonticon-wrap mr-1">
                    <i class="livicon-evo" data-options="name:gear.svg; size: 50px; style:lines; strokeColor:#adb5bd;"></i>
                    </span>
                    <span class="icon-title">
                    <span class="d-block">@t(اطلاعات اولیه)</span>
                    <small class="text-muted">@t(در این بخش اطلاعات اولیه کارگاه را وارد می کنیم.)</small>
                    </span>
                </h3>
                <!-- step 1 end-->
                <!-- step 1 content -->
                <fieldset class="pt-0">
                    <h6 class="pb-50 mt-2 mt-md-0">@t(اطلاعات اولیه کارگاه را وارد کنید.)</h6>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="firstName12">@t(عنوان کارگاه)</label>
                                <input type="text" name="wsh_title" class="form-control" id="firstName12" value="{{ $wsh->product_name }}" placeholder="عنوان کارگاه را وارد کنید">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <ul class="list-unstyled mb-0">
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <small>@t(نوع کارگاه)</small>
                                            <div class="custom-control custom-radio">
                                                <input type="radio"
                                                    class="custom-control-input bg-primary" value="1"
                                                    name="holding" id="man" 
                                                    @if ($wsh->holding == 1)
                                                        checked
                                                    @endif>
                                                <label class="custom-control-label"
                                                    for="man">@t(حضوری)</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio"
                                                    class="custom-control-input bg-danger" value="0"
                                                    name="holding" id="woman"
                                                    @if ($wsh->holding == 2)
                                                        checked
                                                    @endif>
                                                <label class="custom-control-label"
                                                    for="woman">@t(اینترنتی)</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio"
                                                    class="custom-control-input bg-secondary" value="0"
                                                    name="holding" id="booth"
                                                    @if ($wsh->holding == 3)
                                                        checked
                                                    @endif>
                                                <label class="custom-control-label"
                                                    for="booth">@t(هردو)</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="firstName12">@t(مکان برگزاری)</label>
                                <input type="text" name="location" class="form-control" id="firstName12" value="{{ $wsh->location }}" placeholder="مکان برگزاری جلسات کارگاه">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@t(انتخاب مشاور)</label>
                                <div class="controls form-label-group ">
                                    <select class="form-control select2" id="wsh_advisor" required name="wsh_advisor"
                                        data-validation-required-message="@t(باید برای کارگاه مشاور تعریف کنید.)"
                                        placeholder='@t(مشاور)'>
                                        @foreach ($advisors as $advisor)
                                            <option value="{{ $advisor->id }}"
                                                @if ($advisor->id == $wsh->advisor_id)
                                                    selected
                                                @endif
                                                >{{ $advisor->name.' '.$advisor->family }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">@t(حیطه کارگاه)</label>
                                <div class="controls form-label-group ">
                                    <select class="form-control select2" id="wsh_category" required name="wsh_category"
                                        data-validation-required-message="@t(باید برای کارگاه حیطه تعریف کنید.)"
                                        placeholder='@t(حیطه)'>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}"
                                                @if ($category->id == $wsh->category_id)
                                                    selected
                                                @endif
                                                >{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="d-inline-block mb-1">
                                <label><i class="bx bx-time-five font-small-2 mr-25"></i>@t(جمع ساعت)</label>
                                <input type="number" name="sumtime" class="touchspin" value="{{ $wsh->hour_sum }}" data-validation-required-message="@t(نوشتن مجموع ساعت کارگاه الزامی است.)">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label><i class="bx bx-time font-small-2 mr-25"></i>@t(تعداد جلسات)</label>
                            <div class="d-inline-block mb-1">
                                <input type="number" name="s_number" class="touchspin-color " value="{{ $wsh->session_count }}" data-bts-button-down-class="btn btn-success" data-bts-button-up-class="btn btn-success">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="d-inline-block mb-1">
                                <label><i class="bx bxs-hourglass-bottom font-small-2 mr-25"></i>@t(زمان هر جلسه)</label>
                                <div class="input-group disabled-touchspin">
                                    <input type="number" name="s_time" class="touchspin" value="{{ $wsh->hour_sum/$wsh->session_count }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="d-inline-block mb-1">
                                <label><i class='bx bx-user-circle font-small-2 mr-25'></i>@t(ظرفیت کارگاه)</label>
                                <input type="number" name="wsh_capacity" class="touchspin-color " data-bts-postfix="نفر" value="{{ $wsh->capacity }}" data-bts-button-down-class="btn btn-danger" data-bts-button-up-class="btn btn-danger" data-validation-required-message="@t(نوشتن ظرفیت کارگاه الزامی است.)">
                            </div>
                        </div>

                    </div>
                </fieldset>
                <!-- step 1 content end-->
                <!-- step 2 -->
                <h3>
                    <span class="fonticon-wrap mr-1">
                    <i class="livicon-evo" data-options="name:location.svg; size: 50px; style:lines; strokeColor:#adb5bd;"></i>
                    </span>
                    <span class="icon-title">
                    <span class="d-block">@t(اطلاعات مالی کارگاه)</span>
                    <small class="text-muted">@t(اطلاعات مالی کارگاه را در این بخش وارد کنید.)</small>
                    </span>
                </h3>
                <!-- step 2 end-->
                <!-- step 2 content -->
                <fieldset class="pt-0">
                    <h6 class="pb-50 mt-2 mt-md-0">@t(اطلاعات مالی کارگاه)</h6>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label for="price">@t(هزینه کارگاه به تومان)</label>
                            <input type="text" name="price" value="{{ $wsh->price }}" class="form-control" id="price" placeholder="@t(هزینه کارگاه برای هر نفر را وارد کنید)">
                            </div>
                            
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@t(هزینه با تخفیف به تومان)</label>
                                <input type="text" name="" class="form-control" placeholder="هزینه کارگاه با بیشترین  تخفیف را وارد کنید">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@t(هزینه ثبت نام اینترنتی)</label>
                                <input type="text" name="internet_price" value="{{ $wsh->internet_price }}" class="form-control" placeholder="هزینه ثبت نام اینترنتی را وارد کنید">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@t(درصد بازاریاب)</label>
                                <input type="text" name="percent_of_marketer" class="form-control" placeholder="درصد بازاریاب را وارد کنید">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@t(سهم مشاور)</label>
                                <input type="text" name="sahm_moshaver_sabet" value="{{ $wsh->sahm_moshaver_sabet, $wsh->sahm_moshaver_darsad }}" class="form-control" placeholder="سهم مشاور را وارد کنید">
                            </div>
                        </div>
                        
                    </div>
                </fieldset>
                <!-- step 2 content end-->
                <!-- section 3 -->
                <h3>
                    <span class="fonticon-wrap mr-1">
                    <i class="livicon-evo" data-options="name:headphones.svg; size: 50px; style:lines; strokeColor:#adb5bd;"></i>
                    </span>
                    <span class="icon-title">
                    <span class="d-block">@t(ثبت نام اینترنتی)</span>
                    <small class="text-muted">@t(تنظیمات ثبت نام اینترنتی در این بخش است.)</small>
                    </span>
                </h3>
                <!-- section 3 end-->
                <!-- step 3 content -->
                <fieldset class="pt-0">
                    <h6 class="pb-50 mt-2 mt-md-0">@t(تنظیمات ثبت نام اینترنتی)</h6>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <ul class="list-unstyled mt-1">
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <small>@t(امکان ثبت نام اینترنتی)</small>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input bg-success" value="1" name="internet_register" id="enable"
                                                @if ($wsh->internet_access)
                                                    checked
                                                @endif >
                                                <label class="custom-control-label"
                                                    for="enable">@t(داریم)</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input bg-danger" value="0" name="internet_register" id="disable"
                                                @if (!$wsh->internet_access)
                                                    checked
                                                @endif
                                                >
                                                <label class="custom-control-label" for="disable">@t(نداریم)</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">@t(توضیحات برای نمایش در سایت)</label>
                                <textarea name="internet_note" class="form-control" id=""  rows="6">{{ $wsh->internet_note }}</textarea>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <!-- step 3 content end-->
                <!-- step 4 -->
                <h3>
                    <span class="fonticon-wrap mr-1">
                    <i class="livicon-evo" data-options="name:truck.svg; size: 50px; style:lines; strokeColor:#adb5bd;"></i>
                    </span>
                    <span class="icon-title">
                    <span class="d-block">@t(زمان بندی کارگاه)</span>
                    <small class="text-muted">@t(تنظیم تاریخ و ساعت جلسات کارگاه.)</small>
                    </span>
                </h3>
                <!-- step 4 end-->
                <!-- step 4 content -->
                <fieldset class="pt-0">
                    <h6 class="pb-50 mt-2 mt-md-0">@t(تاریخ و ساعت جلسات کارگاه را تنظیم کنید.)</h6>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>@t(تاریخ آغاز کارگاه)</label>
                                <div class="controls form-label-group ">
                                    <input type="text" class="form-control" name="date_of_start" id="date_sw"
                                        pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                        data-validation-pattern-message="فرمت فیلد معتبر نیست." value="{{ $wsh->start }}"
                                        placeholder="انتخاب تاریخ" aria-invalid="false"
                                        onclick="change_date(this.value)">
                                    <div class="form-control-position">
                                        <i class="bx bx-calendar">
                                        </i>
                                    </div>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>@t(تاریخ پایان کارگاه)</label>
                                <div class="controls form-label-group ">
                                    <input type="text" class="form-control" name="date_of_end" id="date_end"
                                        pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                        data-validation-pattern-message="فرمت فیلد معتبر نیست." value="{{ $wsh->end }}"
                                        placeholder="انتخاب تاریخ" aria-invalid="false"
                                        onclick="change_date(this.value)">
                                    <div class="form-control-position">
                                        <i class="bx bx-calendar">
                                        </i>
                                    </div>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider">
                        <div class="divider-text">@t(توضیحات)</div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="">@t(توضیحات)</label>
                                <textarea name="comment" class="form-control" id=""  rows="6">{{ $wsh->comment }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            {{-- <form class="form repeater-default">
                                <div data-repeater-list="group-a">
                                  <div data-repeater-item>
                                    <div class="row justify-content-between">
                                      <div class="col-md-3 col-sm-12">
                                        <div class="form-group">
                                            <label for="date_sw">@t(تاریخ جلسه)</label>
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" name="date_of_buy" id="date_sw"
                                                    pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                                    data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                                    placeholder="انتخاب تاریخ" aria-invalid="false"
                                                    onclick="change_date(this.value)">
                                                <div class="form-control-position">
                                                    <i class="bx bx-calendar">
                                                    </i>
                                                </div>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="col-md-3 col-sm-12 form-group">
                                        <label for="password">@t(ساعت شروع)</label>
                                        <input type="time" class="form-control" id="password" placeholder="رمز عبور را وارد کنید">
                                      </div>
                                      <div class="col-md-3 col-sm-12 form-group">
                                        <label for="gender">@t(ساعت پایان)</label>
                                        <input type="time" class="form-control" id="password" placeholder="رمز عبور را وارد کنید">
                                      </div>
                                      
                                      <div class="col-md-3 col-sm-12 form-group d-flex align-items-center pt-2">
                                        <button class="btn btn-danger text-nowrap px-1" data-repeater-delete type="button"> <i class="bx bx-x"></i>
                                          حذف
                                        </button>
                                      </div>
                                    </div>
                                    <hr>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col p-0">
                                    <button class="btn btn-primary" data-repeater-create type="button"><i class="bx bx-plus"></i>
                                      افزودن
                                    </button>
                                  </div>
                                </div>
                            </form> --}}
                        </div>
                    </div>
                </fieldset>
                <!-- step 4 content end-->
                </form>
            </div>
            </div>
        </div>
    </section>
    <!-- vertical Wizard end-->
    
    <x-slot name='script'>        

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>


        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/extensions/jquery.steps.min.js"></script>
        <script src="/assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
        <script src="/assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/wizard-steps.js"></script>
        <script src="/assets/js/scripts/forms/number-input.js"></script>
        <script src="/assets/js/scripts/forms/form-repeater.js"></script>
        <script src='/assets/fullcalendar/main.min.js'></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->
        <script>
            function G2J(D) {
                dd = D.toLocaleDateString().split('/');

                var a = (
                    jd_to_persian(
                        gregorian_to_jd(
                            dd[2],
                            dd[0],
                            parseInt(dd[1])
                        )
                    )
                );
                return (a[0] + '/' + a[1] + '/' + a[2]);
            }

            function change_date(D) {
                dd = D.split('/');
                var a = (
                    jd_to_gregorian(
                        persian_to_jd(
                            parseInt(dd[0]),
                            parseInt(dd[1]),
                            parseInt(dd[2])
                        )
                    )
                );
                calendar.gotoDate(new Date(a[0] + '-' + a[1] + '-' + a[2]));
            }

            $('#date_sw').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
            $('#date_end').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
        </script>


    </x-slot>
</x-base>