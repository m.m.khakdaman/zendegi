<x-base title="@t(مدیریت کارگاه)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(مدیریت جلسات کارگاه)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                            <li class="breadcrumb-item">@t(کارگاه ها)</li>
                            <li class="breadcrumb-item"><a href="{{ route('workshop.index') }}">@t(لیست کارگاه ها)</a>
                            </li>
                            <li class="breadcrumb-item">@t(مدیریت جلسات کارگاه)</li>
                            <li class="breadcrumb-item active">@t(ثبت نام مراجع در کارگاه)</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        {{-- افزودن جلسه --}}
        <div class="row">
            <div class="col-3">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="invoice-create-btn mb-1">
                            <h5>@t(ثبت نام مراجع در این کارگاه)</h5>
                        </div>
                        <form action="{{ route('workshop.add_member') }}" class="wizard-vertical" id="session_form"
                            method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-sm-12 form-group">
                                <label>@t(نام مراجع)</label>
                                <div class="controls form-label-group ">
                                    <select id="customer_id" class="form-control select2" style="width: 100%"
                                        name="customer_id" placeholder="مراجع" required>
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>@t(هزینه ثبت نام){{ ' ' }}<small>@t( به تومان)</small></label>
                                    <input type="number" name="price" class="form-control"
                                        value="{{ $wsh->price }}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>@t(توضیحات)</label>
                                    <textarea name="comment" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <input type="hidden" name="product_id" value="{{ $wsh->id }}">
                                <input type="hidden" name="orginal_price" value="{{ $wsh->price }}">
                                <input type="hidden" name="products_type_id" value="{{ $wsh->product_type_id }}">
                                <input type="hidden" name="advisor_id" value="{{ $wsh->advisor_id }}">
                                <input type="hidden" name="category_id" value="{{ $wsh->category_id }}">
                                <button type="submit" class="btn btn-success float-right">
                                    <i class="bx bx-plus"></i>@t(ثبت نام)
                                </button>
                                <a href="{{ route('workshop.index') }}" class="btn btn-warning"><i
                                        class="bx bx-undo"></i></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- لیست جلسات --}}
            <div class="col-9">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="invoice-create-btn mb-1">
                            <h5>@t(لیست اعضای کارگاه:)<small>{{ ' ' . $wsh->product_name }}</small></h5>
                        </div>
                        <div class="1table-responsive ">
                            <table class="1table" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@t(نام مراجع)</th>
                                        <th>@t(تاریخ ثبت نام)</th>
                                        {{-- <th>@t(قیمت)</th> --}}
                                        <th><span data-toggle="tooltip" data-placement="top" title="@t(مبلغ هزینه برای ثبت نام این کاربر است که ممکن است شامل تخفیف و... باشد.)">@t(هزینه ثبت نام)</span></th>
                                        <th>@t(باقیمانده)</th>
                                        <th class="text-center">@t(اپراتور)</th>
                                        <th class="text-center">@t(سایر)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $tmp = 0;
                                    @endphp
                                    @foreach ($members as $member)
                                        <tr>
                                            <td><span data-toggle="tooltip" data-placement="left" title="@t(شماره تراکنش:){{ ' '.@$member->transactions->id }}">{{ ++$tmp }}</span></td>
                                            <td>
                                                <a href="{{ route('customer.show', $member->customer->id) }}">                                                    
                                                    @if (!$member->status)
                                                        <span class = "text-danger" data-toggle="tooltip" data-placement="top" title="@t(در تاریخ){{ ' '.verta($member->updated_at)->format('Y/m/d').' '.$member->why_cancel->value }}">
                                                    @else 
                                                        <span data-toggle="tooltip" data-placement="top" title="{{ $member->customer->mobile }}">
                                                    @endif
                                                        {{ $member->customer->name . ' ' . $member->customer->family }}
                                                    </span>
                                                </a>
                                            </td>
                                            <td>
                                                <span class="badge badge-pill badge-warning" data-toggle="tooltip" data-placement="top" title="{{ $member->date }}">
                                                    {{ $member->date2 }}
                                                </span>
                                            </td>
                                            
                                            <td>
                                                <span class="badge badge-pill badge-secondary">{{ $member->total_price }}</span>
                                            </td>
                                            <td>
                                                <span class="badge badge-pill badge-danger">{{@ $member->transactions->amount_standing * -1 }}</span>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.show', $member->operator->id) }}">
                                                    <small data-toggle="tooltip" data-placement="top" title="{{ $member->operator->mobile }}">
                                                        {{ $member->operator->name . ' ' . $member->operator->family }}
                                                    </small>
                                                </a>
                                            </td>
                                            <td>
                                                @if ($member->status)
                                                    <a href="{{ route('workshop.cancel_member_form', [$member->customer->id,$member->id]) }}" data-toggle="tooltip" data-placement="right"
                                                        title="@t(برای کنسل کردن ){{ ' ' . $member->customer->name . ' ' . $member->customer->family . ' ' }}@t(از ادامه کارگاه، کلیک کنید.)">
                                                        <i class="bx bx-check-circle"></i>
                                                    </a>
                                                @else
                                                    <span data-toggle="tooltip" data-placement="right"
                                                        title="{{ $member->customer->name . ' ' . $member->customer->family . ' ' }}@t(از لیست این کارگاه حذف شده است.)">
                                                        <i class="bx bx-x-circle text-danger"></i>
                                                    </span>
                                                @endif
                                                <a href="{{ route('user.invoise', [$member->customer->id, 'sale=' . $member->id]) }}"
                                                    class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(تسویه حساب)">
                                                    <i class="bx bx-dollar-circle"></i>
                                                </a>
                                                <a href="{{ route('sms.send', $member->customer->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top"
                                                    title="@t(ارسال پیامک برای){{ ' ' . $member->customer->name . ' ' . $member->customer->family }}">
                                                    <i class="bx bx-message-dots"></i>
                                                </a>
                                                <span class="text-info" data-toggle="tooltip" data-placement="top" title="@t(توضیحات:){{ ' '.$member->comment }}">
                                                    <i class="bx bx-info-circle"></i>
                                                </span>
                                                <a href="{{ route('workshop.print_receipt', [$member->product->id, $member->customer->id]) }}" class="text-secondary" data-toggle="tooltip" data-placement="top"
                                                    title="@t(پرینت قبض ثبت نام){{ ' ' . $member->customer->name . ' ' . $member->customer->family }}">
                                                    <i class="bx bx-printer"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- افزودن جلسه --}}

        <!-- cancel workshop modal Modal -->
        {{-- <div class="modal fade" id="cancel_wsh_modal" tabindex="-1" role="dialog"
            aria-labelledby="cancel_wsh_modalTitle" aria-hidden="true">
            <form class="form form-horizontal" action="{{ route('workshop.cancel_member') }}" id="cancel_form"
                method="POST">
                @csrf
                <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
                    role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="cancel_wsh_modalTitle">@t(انصراف از کارگاه)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <ul class="list-unstyled mb-0">
                                <li class="d-inline-block mr-2 mb-1">
                                    @csrf
                                    <input type="hidden" name="sale_id" id="sale_id2">

                                    <fieldset>
                                        <div>
                                            <label>@t(علت انصراف):</label>
                                        </div>
                                    </fieldset>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <fieldset>
                                        <div class="radio radio-primary radio-glow">
                                            <input type="radio" id="radioGlow1" name="delete_type" checked="" value="1">
                                            <label for="radioGlow1">مشاور</label>
                                        </div>
                                    </fieldset>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <fieldset>
                                        <div class="radio radio-secondary radio-glow">
                                            <input type="radio" id="radioGlow2" name="delete_type" value="2">
                                            <label for="radioGlow2">مرکز</label>
                                        </div>
                                    </fieldset>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <fieldset>
                                        <div class="radio radio-success radio-glow">
                                            <input type="radio" id="radioGlow3" name="delete_type" value="3">
                                            <label for="radioGlow3">مراجع</label>
                                        </div>
                                    </fieldset>
                                </li>
                            </ul>
                            <div class="col-12 p-0">
                                <label for="payback">@t(مبلغ عودت داده شده به تومان)</label>
                                <input type="number" class="form-control" id="payback" name="payback">
                            </div>
                            <div class="col-12 p-0 mt-50">
                                <textarea class="form-control" id="basicTextarea" rows="3" name="comment"
                                    placeholder="توضیحات"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">بستن</span>
                            </button>
                            <button type="submit" class="btn btn-success ml-1">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">@t(ثبت)</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div> --}}


    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src='/assets/vendors/js/ui/jquery.sticky.js'></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src='/assets/fullcalendar/main.min.js'></script>
        <script src='/assets/js/scripts/modal/components-modal.js'></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->
        <script>
            $('#customer_id').select2({
                ajax: {
                    url: "{{ route('customer.get_all_ajax') }}",
                    dataType: 'json'
                }
            });
        </script>
        {{-- <script>
            function cancel_form() {

                data = {
                    _token: '{{ csrf_token() }}',
                    sale_id: $('#sale_id2').val(),
                    delete_type: $('input[name=delete_type]:checked').val(),
                    comment: $('[name=comment]').val()
                };

                $.post(
                    "{{ route('sale.cancel_wsh_member') }}", 
                    data,
                    function(data, status) {
                        $(function() {
                            $('#cancel_wsh_modal').modal('toggle');
                        });

                        $(function() {
                            $('[data-toggle="tooltip"]').tooltip()
                        })
                    }
                );

            }
        </script> --}}

    </x-slot>
</x-base>
