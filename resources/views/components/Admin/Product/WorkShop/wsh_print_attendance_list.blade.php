<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center mt-2">
            <p>
                <b>بسمه تعالی</b>
            </p>
            <p>
                <b>@t(لیست حضور و غیاب شرکت کنندگان در کارگاه)</b>
                <br>
                <span class="font-medium-3">{{ $wsh_name->product_name }}</span>
            </p>
        </div>
        <div class="col-4">

            <div class="row text-center float-right">
                <div class="mr-4 mt-2">
                    <img src="http://zendegiaghelane.sanamsoft.com/theme/images/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                    <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 mt-1 mb-2">
                    <h4>@t(لیست حضور و غیاب شرکت کنندگان در کارگاه){{ ' '.$wsh_name->product_name }}</h4>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>@t(نام مراجع)</th>
                        @foreach ($session_numbers as $session_number)
                            <th>{{ $session_number->session_number }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @php
                        $tmp = 0;
                    @endphp
                    @foreach ($members as $member)
                        <tr>
                            <td>{{ ++$tmp }}</td>
                            <td>{{ $member->customer->name . ' ' . $member->customer->family }}</td>
                            @foreach ($session_numbers as $session_number)
                                <td>
                                    @php
                                        $shenaseh = $member->customer->id . $member->product_id . $session_number->session_number;
                                    @endphp
                                    @switch($attendance_lists)
                                        @case($attendance_lists->contains('shenaseh', $shenaseh.'P'))
                                            <i class="bx bx-check" style="color: #00b33c; font-size: 20px; font-weight: bold"></i>
                                        @break
                                        @case($attendance_lists->contains('shenaseh', $shenaseh.'A'))
                                            <span class="text-danger" style="font-weight: bold; font-size:16px">@t(غ)</span>
                                        @break
                                        @default
                                            <i class="bx bx-checkbox" style="color: #747974"></i>
                                    @endswitch
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ sentence() }}
            <br>

            <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
        </div>
    </div>
</body>

<script>
    // window.print();
</script>

</html>
