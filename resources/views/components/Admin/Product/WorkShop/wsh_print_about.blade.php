<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center mt-2">
            <p>
                <b>بسمه تعالی</b>
            </p>
            <p>
                <span class="font-medium-3">@t(کارگاه){{ ' '.$wsh->product_name }}</span>
            </p>
        </div>
        <div class="col-4">

            <div class="row text-center float-right">
                <div class="mr-4 mt-2">
                    <img src="http://zendegiaghelane.sanamsoft.com/theme/images/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                    <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">@t(کارگاه){{ ' '.$wsh->product_name }}</h4>
                    <div class="card-body">
                        <div class="card-text line-height-2">
                        <dl class="row">
                            <dt class="col-sm-3">@t(نام کارگاه)</dt>
                            <dd class="col-sm-9">{{ $wsh->product_name }}</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">@t(مدرس)</dt>
                            <dd class="col-sm-9">{{ $wsh->advisor->name.' '.$wsh->advisor->family }}</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">@t(حیطه)</dt>
                            <dd class="col-sm-9 ml-auto">{{ $wsh->category->name }}</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">@t(تاریخ برگزاری)</dt>
                            <dd class="col-sm-9">@t(از تاریخ)
                            <span>{{ ' '.$wsh->start.' ' }}</span>
                            @t(تا تاریخ)
                            <span>{{ ' '.$wsh->end }}</span>
                        </dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3 text-truncate">@t(هزینه ثبت نام)</dt>
                            <dd class="col-sm-9">
                                @t(هزینه ثبت نام حضوری)
                                <span>{{ ' '.$wsh->price.' ' }}</span>@t(تومان)
                                @t(هزینه ثبت نام اینترنتی)
                                <span>{{ ' '.$wsh->internet_price.' ' }}</span>@t(تومان)
                            </dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3">@t(تعداد جلسات)</dt>
                            <dd class="col-sm-9">
                            <dl class="row">
                                <dt class="col-sm-4">{{ $wsh->session_count.' ' }}@t(جلسه)</dt>
                                <dd class="col-sm-8">@t(مجموعاً){{ ' '.$wsh->hour_sum.' ' }}@t(ساعت.)</dd>
                            </dl>
                            </dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-3 text-truncate">@t(توضیحات)</dt>
                            <dd class="col-sm-9">{{ $wsh->comment }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <h4 class="card-title">@t(لیست شرکت کنندگان)</h4>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>@t(ردیف)</th>
                                <th>@t(نام و نام خانوادگی)</th>
                            </tr>
                            @php
                                $tmp = 0;
                            @endphp
                            @foreach ($members as $member)
                                <tr>
                                    <td>{{ ++$tmp }}</td>
                                    <td>{{ $member->customer->name.' '.$member->customer->family }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            {{ sentence() }}
            <br>

            <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
        </div>
    </div>
</body>

<script>
    // window.print();
</script>

</html>
