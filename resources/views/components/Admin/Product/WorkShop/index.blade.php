<x-base>
    <x-slot name="title">
        @t(کارگاه)
    </x-slot>
    <x-slot name="css">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">

        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(لیست کارگاه ها)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item">@t(کارگاه ها)</li>
                    <li class="breadcrumb-item active">@t(لیست کارگاه ها)</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary mb-25" href="{{ route('workshop.create') }}" role="button">
                            <i class="bx bx-plus"></i>@t(کارگاه جدید)
                        </a>
                        <button class="btn btn-google mb-25" data-toggle="modal" data-target="#help">
                            <i class="bx bx-info-circle mr-25"></i>@t(راهنمایی)
                        </button>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table 1zero-configuration" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="text-center">@t(مشاور)</th>
                                            <th class="text-center">@t(عنوان کارگاه)</th>
                                            <th>@t(شروع)</th>
                                            <th>@t(پایان)</th>
                                            <th>@t(تعداد جلسات) </th>
                                            <th>@t(عملیات)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 0;
                                        @endphp
                                        @foreach ($workshops as $workshop)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>
                                                    <span data-toggle="tooltip" data-placement="top" title="{{ $workshop->advisor->mobile }}">{{ $workshop->advisor->name . ' ' . $workshop->advisor->family }}</span>
                                                </td>
                                                <td>
                                                    <a href="{{ route('workshop.sessions', $workshop->id) }}" class="text-secondary">
                                                        {{ $workshop->product_name }}
                                                    </a>
                                                </td>
                                                <td><span class="badge badge-success">{{ $workshop->start }}</span></td>
                                                <td><span class="badge badge-danger">{{ $workshop->end }}</span></td>
                                                <td><span class="badge badge-warning">{{ $workshop->session_count }}</span></td>
                                                <td>
                                                    <a href="{{ route('workshop.sessions', $workshop->id) }}">
                                                      <i class="bx bx-calendar-event text-secondary" data-toggle="tooltip" data-placement="top" title="@t(مدیریت کارگاه)"></i>
                                                    </a>
                                                    <a href="{{ route('workshop.members', $workshop->id) }}">
                                                        <i class="bx bx-user-plus" data-toggle="tooltip" data-placement="top" title="@t(ثبت نام مراجع در کارگاه)"></i>
                                                    </a>
                                                    <a href="{{ route('workshop.attendance_list', $workshop->id) }}">
                                                        <i class="bx bx-list-check" data-toggle="tooltip" data-placement="top" title="@t(لیست حضور و غیاب)"></i>
                                                    </a>
                                                    <a href="{{ route('workshop.about', $workshop->id) }}">
                                                        <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="top" title="@t(درباره کارگاه)"></i>
                                                    </a>
                                                    <a href="{{ route('workshop.edit', $workshop->id) }}">
                                                        <i class="bx bx-edit" data-toggle="tooltip" data-placement="top" title="@t(ویرایش کارگاه)"></i>
                                                    </a>
                                                    <i class="bx bxs-minus-circle text-danger"></i>
                                                    
                                                    
                                                </td>
                                            </tr>
                                        @endforeach

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- help workshop modal Modal -->
    <div class="modal fade" id="help" tabindex="-1" role="dialog"  aria-labelledby="helpTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h5 class="modal-title" id="helpTitle">@t(نحوه ایجاد کارگاه)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <div class="modal-body text-justify">
                    <ul>
                        <li>ابتدا با زدن دکمه کارگاه جدید و پرکردن فیلدهای آن یک کارگاه ایجاد کنید.
                            <b class="text-warning">نکته: در هنگام تعریف کارگاه  جدید انتخاب مشاور ضروری می باشد.</b>
                        </li>
                        <li>سپس با کلیک کردن روی دکمه مدیریت کارگاه، جلسات کارگاه را تعریف نمایید.</li>
                        <li>در مرحله بعد باید نسبت به ثبت نام مراجعین در کارگاه اقدام نمایید</li>
                        <li>و در پایان هر جلسه از کارگاه، لیست حضور و غیاب را تکمیل نمایید.</li>
                        <li>اگر کسی از ادامه دادن کارگاه منصرف شد، درمنوی ثبت نام مراجعین می توان با اعلام علت کنسل شدن این فرد، انصراف او را ثبت و نسبت به عودت تمام یا بخشی از وجه وی اقدام کرد.</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">@t(متوجه شدم)</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/datatables/datatable.js"></script>
        <script>
                $('#customer_id').select2({
                    ajax: {
                        url: "{{ route('customer.get_all_ajax') }}",
                        dataType: 'json'
                    }
                });


        </script>
        <!-- END: Page JS-->
    </x-slot>
</x-base>
