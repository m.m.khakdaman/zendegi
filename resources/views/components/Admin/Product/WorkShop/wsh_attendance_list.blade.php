<x-base title="@t(سمت ها)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(مدیریت جلسات کارگاه)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                    <li class="breadcrumb-item">@t(کارگاه ها)</li>
                    <li class="breadcrumb-item"><a href="{{ route('workshop.index') }}" >@t(لیست کارگاه ها)</a></li>
                    <li class="breadcrumb-item">@t(مدیریت جلسات کارگاه)</li>
                    <li class="breadcrumb-item active">@t(ثبت نام مراجع در کارگاه)</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        {{-- افزودن جلسه --}}
        <div class="row">
            
            {{-- لیست جلسات --}}
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="invoice-create-btn mb-1">
                            {{-- <h5>@t(لیست اعضای کارگاه:)<small>{{ ' '.$wsh->product_name }}</small></h5> --}}
                        </div>
                        <div class="table-responsive ">
                                    

{{-- {{ $collection->implode('product', ', ') }} --}}
                            <form action="#" method="POST" enctype="multipart/form-data">
                                @csrf
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@t(نام مراجع)</th>
                                            @foreach ($session_numbers as $session_number)
                                                <th>{{ $session_number->session_number }}</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $tmp = 0;
                                        @endphp
                                        @foreach ($members as $member)
                                            <tr>
                                                <td>{{ ++$tmp }}</td>
                                                <td>{{ $member->customer->name.' '.$member->customer->family }}</td>
                                                @foreach ($session_numbers as $session_number)
                                                
                                                    <td>
                                                        @php
                                                            $shenaseh = $member->customer->id.$member->product_id.$session_number->session_number;
                                                           
                                                        @endphp
                                                         
                                                        {{-- @switch($attendance_lists)
                                                            @case($attendance_lists->contains('shenaseh', $shenaseh.'P'))
                                                            <div class="dropdown">
                                                                <span class="text-success bx bx-check dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'P']) }}"><i class="bx bxs-check-circle text-success mr-1"></i> @t(حاضر)</a>
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'A']) }}"><i class="text-danger bx bx-x-circle mr-1"></i>@t(غیبت)</a>
                                                                </div>
                                                            </div>
                                                                @break
                                                            @case($attendance_lists->contains('shenaseh', $shenaseh.'A'))
                                                            <div class="dropdown">
                                                                <span class="text-danger dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">@t(غ)</span>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'P']) }}"><i class="bx bxs-check-circle text-success mr-1"></i> @t(حاضر)</a>
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'A']) }}"><i class="text-danger bx bx-x-circle mr-1"></i>@t(غیبت)</a>
                                                                </div>
                                                            </div>
                                                                @break
                                                        
                                                            @default
                                                            <div class="dropdown">
                                                                <span class="bx bx-checkbox text-light-secondary dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'P']) }}"><i class="bx bxs-check-circle text-success mr-1"></i> @t(حاضر)</a>
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'A']) }}"><i class="text-danger bx bx-x-circle mr-1"></i>@t(غیبت)</a>
                                                                </div>
                                                            </div>
                                                        @endswitch --}}
                                                        
                                                        
                                                            <div class="dropdown">
                                                                @switch($attendance_lists)
                                                                    @case($attendance_lists->contains('shenaseh', $shenaseh.'P'))
                                                                        <span class="bx bx-check text-success dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                        @break
                                                                    @case($attendance_lists->contains('shenaseh', $shenaseh.'A'))
                                                                        <span class="text-danger dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">@t(غ)</span>
                                                                        @break
                                                                    @default
                                                                        <span class="bx bx-checkbox text-light-secondary dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                                @endswitch
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'P']) }}"><i class="bx bxs-check-circle text-success mr-1"></i> @t(حاضر)</a>
                                                                    <a class="dropdown-item" href="{{ route('workshop.attendance', [$member->customer->id,$member->product_id,$session_number->session_number,'A']) }}"><i class="text-danger bx bx-x-circle mr-1"></i>@t(غیبت)</a>
                                                                </div>
                                                            </div>
                                                        
                                                        {{-- {!!Form::checkbox('seat_id[]',$session_number->id,null)!!} Number: {{$session_number->session_number}} 
                                                         --}}
                                                         {{-- <input type="checkbox" name="5"> --}}
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach                                        
                                    </tbody>
                                </table>
                                <input type="submit" class="btn btn-success float-right" value="@t(ثبت)">
                                <a href="{{ route('workshop.print_attendance_list', $session_number->product_id) }}" type="button" class="btn btn-primary float-right mr-1"><i class="bx bx-printer mr-50"></i>@t(پرینت)</a>
                                <a href="{{ route('workshop.index') }}" type="button" class="btn btn-warning float-right mr-1"><i class="bx bx-undo"></i>@t(بازگشت)</a>
                            </form>
                            {{-- {!!Form::model($screening,['method'=>'post', 'action'=> 
                            ['workshop.attendance',$screening->auditorium->id]])!!}

                            <input type="hidden" name="screening_id" value="{{$screening->id}}">

                            @foreach($seats as $seat)

                            <label class="checkbox-inline">

                            {!!Form::checkbox('seat_id[]',$seat->id,null)!!} Number: {{$seat->number}}  

                            </label>

                            @endforeach

                            <div class='form-group'>

                            {!!Form::submit('Create Post',['class'=>'btn btn-primary '])!!}

                            </div>

                            {!!Form::close()!!} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- افزودن جلسه --}}
        
       
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src='/assets/vendors/js/ui/jquery.sticky.js'></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src='/assets/fullcalendar/main.min.js'></script>
        <script src='/assets/js/scripts/modal/components-modal.js'></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->
       

    </x-slot>
</x-base>
