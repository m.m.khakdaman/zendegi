<x-base title="@t(جلسات کارگاه)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(مدیریت جلسات کارگاه)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item">@t(کارگاه ها)</li>
                    <li class="breadcrumb-item"><a href="{{ route('workshop.index') }}" >@t(لیست کارگاه ها)</a></li>
                    <li class="breadcrumb-item active">@t(مدیریت جلسات کارگاه:){{ ' '.$product_name->product_name }}</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        {{-- افزودن جلسه --}}
        <div class="row">
            <div class="col-3">
                <div class="row">
                    <div class="card">
                        <div class="card-header">
                            <!-- create invoice button-->
                            <div class="invoice-create-btn mb-1">
                                <h5>@t(افزودن جلسه)</h5>
                            </div>
                            <form action="{{ route('workshop.add_session') }}" class="wizard-vertical" id="session_form"
                            method="POST"
                            enctype="multipart/form-data">
                            @csrf
                                <div class="col-sm-12 form-group">
                                    <label>@t(تاریخ جلسه)</label>
                                    <div class="controls form-label-group ">
                                        <input type="text" class="form-control" name="date" id="date_sw"
                                            pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                            data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                            placeholder="انتخاب تاریخ" aria-invalid="false"
                                            onclick="change_date(this.value)">
                                        <div class="form-control-position">
                                            <i class="bx bx-calendar">
                                            </i>
                                        </div>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>@t(ساعت جلسه)</label>
                                        <input type="time" name="time" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <input type="hidden" name="product_id" value="{{ $product_id }}">
                                    <button type="submit" class="btn btn-success float-right">
                                        <i class="bx bx-plus"></i>@t(افزودن جلسه)
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="card">
                        <div class="card-header">
                            <!-- create invoice button-->
                            <div class="invoice-create-btn mb-1">
                                <h5>@t(بستن کارگاه)</h5>
                            </div>
                            <form action="#" class="wizard-vertical" id="session_form"
                            method="POST"
                            enctype="multipart/form-data">
                            @csrf
                                {{-- <div class="col-sm-12 form-group">
                                    <label>@t(تاریخ جلسه)</label>
                                    <div class="controls form-label-group ">
                                        <input type="text" class="form-control" name="date" id="date_sw"
                                            pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                            data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                            placeholder="انتخاب تاریخ" aria-invalid="false"
                                            onclick="change_date(this.value)">
                                        <div class="form-control-position">
                                            <i class="bx bx-calendar">
                                            </i>
                                        </div>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>@t(ساعت جلسه)</label>
                                        <input type="time" name="time" class="form-control">
                                    </div>
                                </div> --}}
                                <div class="card-header">
                                    <input type="hidden" name="product_id" value="{{ $product_id }}">
                                    <button type="submit" class="btn btn-danger float-right">
                                        <i class="bx bx-window-close mr-3"></i>@t(بستن کارگاه)
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            {{-- لیست جلسات --}}
            <div class="col-9">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="invoice-create-btn mb-1">
                            <h5>@t(لیست جلسات کارگاه:)<small>{{ ' '.$product_name->product_name }}</small></h5>
                        </div>
                        <div class="table-responsive ">
                            <table class="table data-table nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@t(عنوان کارگاه)</th>
                                        <th>@t(جلسه)</th>
                                        <th>@t(تاریخ)</th>
                                        <th>@t(ساعت)</th>
                                        <th>@t(سایر)</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($wsh_sessions as $wsh_session)
                                    <tr>
                                        <td>{{ $wsh_session->session_number }}</td>
                                        <td>{{ $wsh_session->wsh_name->product_name }}</td>
                                        <td>
                                            <div class="badge-circle badge-circle-sm badge-circle-warning">
                                                {{ $wsh_session->session_number }}
                                            </div>
                                            
                                        </td>
                                        <td>{{ $wsh_session->date }}</td>
                                        <td>{{ $wsh_session->time }}</td>
                                        <td>
                                        <a href="{{ route('workshop.edit_session',$wsh_session->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ویرایش جلسه)"><i class="bx bx-edit"></i></a>
                                        <a href="" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ارسال پیامک برای)" ><i class="bx bx-message-dots"></i></a>
                                        <a href="" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(پذیرایی این جلسه)" ><i class="bx bx-coffee"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- افزودن جلسه --}}
        
       
    </section>
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src='/assets/vendors/js/ui/jquery.sticky.js'></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src='/assets/fullcalendar/main.min.js'></script>
        <script src='/assets/js/scripts/modal/components-modal.js'></script>
        <!-- END: Page JS-->
        <script>
            function G2J(D) {
                dd = D.toLocaleDateString().split('/');

                var a = (
                    jd_to_persian(
                        gregorian_to_jd(
                            dd[2],
                            dd[0],
                            parseInt(dd[1])
                        )
                    )
                );
                return (a[0] + '/' + a[1] + '/' + a[2]);
            }

            function change_date(D) {
                dd = D.split('/');
                var a = (
                    jd_to_gregorian(
                        persian_to_jd(
                            parseInt(dd[0]),
                            parseInt(dd[1]),
                            parseInt(dd[2])
                        )
                    )
                );
                calendar.gotoDate(new Date(a[0] + '-' + a[1] + '-' + a[2]));
            }

            $('#date_sw').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
           
        </script>

    </x-slot>
</x-base>
