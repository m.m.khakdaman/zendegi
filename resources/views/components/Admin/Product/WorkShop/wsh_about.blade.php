<x-base>
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <!-- END: Page CSS-->
        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(درباره کارگاه)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('workshop.index') }}">@t(لیست کارگاه ها)</a>
                            </li>
                            <li class="breadcrumb-item active">@t(درباره کارگاه){{ ' '.$wsh->product_name }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('workshop.print_about', $wsh->id) }}" class="btn btn-yahoo float-right"><i class="bx bx-printer mr-1"></i>@t(چاپ)</a>
                        <h4 class="card-title">@t(کارگاه){{ ' '.$wsh->product_name }}</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="card-content">
                                <div class="card-body">
                                  <div class="card-text line-height-2">
                                    <dl class="row">
                                      <dt class="col-sm-3"><i class="bx bx-label mr-50"></i>@t(نام کارگاه)</dt>
                                      <dd class="col-sm-9">{{ $wsh->product_name }}</dd>
                                    </dl>
                                    <dl class="row">
                                      <dt class="col-sm-3"><i class="bx bx-face mr-50"></i>@t(مدرس)</dt>
                                      <dd class="col-sm-9">{{ $wsh->advisor->name.' '.$wsh->advisor->family }}</dd>
                                    </dl>
                                    <dl class="row">
                                      <dt class="col-sm-3"><i class="bx bx-hive mr-50"></i>@t(حیطه)</dt>
                                      <dd class="col-sm-9 ml-auto">{{ $wsh->category->name }}</dd>
                                    </dl>
                                    <dl class="row">
                                      <dt class="col-sm-3"><i class="bx bx-calendar-event mr-50"></i>@t(تاریخ برگزاری)</dt>
                                      <dd class="col-sm-9">@t(از تاریخ)
                                        <span>{{ ' '.$wsh->start.' ' }}</span>
                                        @t(تا تاریخ)
                                        <span>{{ ' '.$wsh->end }}</span>
                                    </dd>
                                    </dl>
                                    <dl class="row">
                                      <dt class="col-sm-3 text-truncate"><i class="bx bx-dollar-circle mr-50"></i>@t(هزینه ثبت نام)</dt>
                                      <dd class="col-sm-9">
                                          @t(هزینه ثبت نام حضوری)
                                          <span>{{ ' '.$wsh->price.' ' }}</span>@t(تومان)
                                          @t(هزینه ثبت نام اینترنتی)
                                          <span>{{ ' '.$wsh->internet_price.' ' }}</span>@t(تومان)
                                      </dd>
                                    </dl>
                                    <dl class="row">
                                      <dt class="col-sm-3"><i class="bx bx-list-ol mr-50"></i>@t(تعداد جلسات)</dt>
                                      <dd class="col-sm-9">
                                        <dl class="row">
                                          <dt class="col-sm-4">{{ $wsh->session_count.' ' }}@t(جلسه)</dt>
                                          <dd class="col-sm-8">@t(مجموعاً){{ ' '.$wsh->hour_sum.' ' }}@t(ساعت.)</dd>
                                        </dl>
                                      </dd>
                                    </dl>
                                    <dl class="row">
                                        <dt class="col-sm-3 text-truncate"><i class="bx bx-note mr-50"></i>@t(توضیحات)</dt>
                                        <dd class="col-sm-9">{{ $wsh->comment }}</dd>
                                      </dl>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(لیست شرکت کنندگان)</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(نام و نام خانوادگی)</th>
                                </tr>
                                @php
                                    $tmp = 0;
                                @endphp
                                @foreach ($members as $member)
                                    <tr>
                                        <td>{{ ++$tmp }}</td>
                                        <td>{{ $member->customer->name.' '.$member->customer->family }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <!-- END: Page JS-->

    </x-slot>
</x-base>
