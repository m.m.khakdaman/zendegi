<x-base>
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <!-- END: Page CSS-->

        <style>
            .blink_me {
                animation: blinker 1s linear infinite;
                }

            @keyframes blinker {
            50% {
                opacity: 0;
            }
                }
        </style>
  
        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(کنسل کردن کارگاه)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('workshop.index') }}">@t(لیست کارگاه ها)</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('workshop.members', $wsh->product_id) }}">@t(کارگاه){{ ' '.$wsh_name->product_name }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('workshop.members', $wsh->product_id) }}">@t(مدیریت جلسات کارگاه)</a>
                            </li>
                            <li class="breadcrumb-item active">@t(کنسل کردن کارگاه){{ ' '.$customer->name.' '.$customer->family }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(کنسل کردن کارگاه){{ ' '.$customer->name.' '.$customer->family }}</h4><br>
                        @if ($debt < 0)
                            <h6 class="blink_me text-danger">@t(نامبرده مبلغ:)<b>{{ ' '.$debt * -1 .' ' }}</b>@t(تومان بدهکار است.)</h6>
                        @endif
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="card-content">
                                <div class="card-body p-0">
                                    <form action="{{ route('workshop.cancel_member') }}" method="POST">
                                        @csrf
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div>
                                                        <label>@t(علت انصراف):</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio radio-primary radio-glow">
                                                        <input type="radio" id="radioGlow1" name="delete_type" value="1">
                                                        <label for="radioGlow1">مشاور</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio radio-secondary radio-glow">
                                                        <input type="radio" id="radioGlow2" name="delete_type" value="2">
                                                        <label for="radioGlow2">مرکز</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio radio-success radio-glow">
                                                        <input type="radio" id="radioGlow3" name="delete_type" checked value="3">
                                                        <label for="radioGlow3">مراجع</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <div class="col-12 p-0">
                                            <label for="payback">@t(مبلغ عودت داده شده به تومان)</label>
                                            <input type="number" class="form-control" id="payback" name="payback">
                                        </div>
                                        <div class="col-12 p-0 mt-50">
                                            <textarea class="form-control" id="basicTextarea" rows="3" name="comment"
                                                placeholder="توضیحات"></textarea>
                                        </div>
                                        <input type="hidden" name="sale_id" value="{{ $wsh->id }}">
                                        <input type="hidden" name="user_id" value="{{ $customer->id }}">
                                        <input type="hidden" name="wsh_num" value="{{ $wsh->product_id }}">
                                        <input type="submit" class="btn btn-success mt-1 mb-1 float-right" value="ثبت">
                                        <a type="button" href="{{ route('workshop.members', $wsh->product_id) }}" class="btn btn-danger mt-1 mr-50 mb-1 float-right" style="color: white">@t(برگشت)</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <!-- END: Page JS-->
       

    </x-slot>
</x-base>
