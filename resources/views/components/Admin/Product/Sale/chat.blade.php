<x-base2 title="@t(پیامک)">
    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/app-chat.css">
        <!-- END: Page CSS-->

    </x-slot>
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-area-wrapper">
            <div class="sidebar-left">
                <div class="sidebar">
                    <!-- app chat user profile left sidebar start -->
                    <div class="chat-user-profile">
                        <header class="chat-user-profile-header text-center border-bottom">
                            <span class="chat-profile-close">
                                <i class="bx bx-x"></i>
                            </span>
                            <div class="my-2">
                                <div class="avatar">
                                    <img src="../../assets/images/portrait/small/avatar-s-11.jpg" alt="user_avatar"
                                        height="100" width="100">
                                </div>
                                <h5 class="mb-0">تونی استارک</h5>
                                <span>طراح</span>
                            </div>
                        </header>
                        <div class="chat-user-profile-content">
                            <div class="chat-user-profile-scroll">
                                <h6 class="text-uppercase mb-1">درباره</h6>
                                <p class="mb-2">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                                    از طراحان گرافیک است.</p>
                                <h6>اطلاعات شخصی</h6>
                                <ul class="list-unstyled mb-2 line-height-2">
                                    <li class="mb-25">email@gmail.com</li>
                                    <li><span class="ltr-text">(+98) 123 456 789</span></li>
                                </ul>
                                <h6 class="text-uppercase mb-1">کانال ها</h6>
                                <ul class="list-unstyled mb-2 line-height-2">
                                    <li><a href="javascript:void(0);"># توسعه دهندگان</a></li>
                                    <li><a href="javascript:void(0);"># طراحان</a></li>
                                </ul>
                                <h6 class="text-uppercase mb-1">تنظیمات</h6>
                                <ul class="list-unstyled line-height-2">
                                    <li class="mb-50 "><a href="javascript:void(0);"
                                            class="d-flex align-items-center"><i class="bx bx-tag mr-50"></i> افزودن
                                            برچسب</a></li>
                                    <li class="mb-50 "><a href="javascript:void(0);"
                                            class="d-flex align-items-center"><i class="bx bx-star mr-50"></i>
                                            مخاطبین مهم</a>
                                    </li>
                                    <li class="mb-50 "><a href="javascript:void(0);"
                                            class="d-flex align-items-center"><i class="bx bx-image-alt mr-50"></i>
                                            اسناد اشتراک گذاری شده</a></li>
                                    <li class="mb-50 "><a href="javascript:void(0);"
                                            class="d-flex align-items-center"><i class="bx bx-trash-alt mr-50"></i>
                                            اسناد پاک شده</a></li>
                                    <li><a href="javascript:void(0);" class="d-flex align-items-center"><i
                                                class="bx bx-block mr-50"></i> مخاطبین مسدود شده</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- app chat user profile left sidebar ends -->
                    <!-- app chat sidebar start -->
                    <div class="chat-sidebar card">
                        <span class="chat-sidebar-close">
                            <i class="bx bx-x"></i>
                        </span>
                        <div class="chat-sidebar-search">
                            <div class="d-flex align-items-center">
                                <div class="chat-sidebar-profile-toggle">
                                    <div class="avatar">
                                        <img src="../../assets/images/portrait/small/avatar-s-11.jpg" alt="user_avatar"
                                            height="36" width="36">
                                    </div>
                                </div>
                                <fieldset class="form-group position-relative has-icon-left mx-75 mb-0">
                                    <input type="text" class="form-control round" id="chat-search" placeholder="جستجو">
                                    <div class="form-control-position">
                                        <i class="bx bx-search-alt text-dark"></i>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="chat-sidebar-list-wrapper pt-2">
                            <h6 class="px-2 pb-25 mb-0">کانال ها<i class="bx bx-plus float-right cursor-pointer"></i>
                            </h6>
                            <ul class="chat-sidebar-list">
                                <li>
                                    <h6 class="mb-0"># توسعه دهندگان</h6>
                                </li>
                                <li>
                                    <h6 class="mb-0"># طراحان</h6>
                                </li>
                            </ul>
                            <h6 class="px-2 pt-2 pb-25 mb-0">گفتگو ها</h6>
                            <ul class="chat-sidebar-list">
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-26.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-busy"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">جسیکا آلبا</h6><span class="text-muted">پخت کیک</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-7.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-online"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">کریستوفر نولان</h6><span class="text-muted">پودر ژله</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <h6 class="px-2 pt-2 pb-25 mb-0">
                                مخاطبین
                                <i class="bx bx-plus float-right cursor-pointer"></i>
                            </h6>
                            <ul class="chat-sidebar-list">
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-8.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-away"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">سارا لنس</h6><span class="text-muted"> آبلیمو ترش</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar bg-info m-0 mr-50">
                                            <span class="avatar-content">ج‌پ</span>
                                            <span class="avatar-status-offline"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">اولیور کوئین</h6><span class="text-muted">آبنبات
                                                چوبی</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-5.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-online"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">بری الن</h6><span class="text-muted">آدامس</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-9.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-busy"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">استیو راجرز</h6><span class="text-muted">تافی مغزدار</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50 bg-success">
                                            <span class="avatar-content">ج‌ا</span>
                                            <span class="avatar-status-offline"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">جان اسنو</h6><span class="text-muted">نان زنجبیلی</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-14.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-online"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">کریس رونالدو</h6><span class="text-muted">پودینگ</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-3.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-offline"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">لیونل مسی</h6><span class="text-muted">بستنی چوبی</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex align-items-center">
                                        <div class="avatar m-0 mr-50"><img
                                                src="../../assets/images/portrait/small/avatar-s-7.jpg" height="36"
                                                width="36" alt="sidebar user image">
                                            <span class="avatar-status-online"></span>
                                        </div>
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">کریستوفر نولان</h6><span class="text-muted">عناب</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- app chat sidebar ends -->
                </div>
            </div>
            <div class="content-right">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="content-header row">
                    </div>
                    <div class="content-body">
                        <!-- app chat overlay -->
                        <div class="chat-overlay"></div>
                        <!-- app chat window start -->
                        <section class="chat-window-wrapper">
                            <div class="chat-start">
                                <span
                                    class="bx bx-message chat-sidebar-toggle chat-start-icon font-large-3 p-3 mb-1"></span>
                                <h4 class="d-none d-lg-block py-50 text-bold-500">یک مخاطب را برای شروع گفتگو انتخاب
                                    کنید!</h4>
                                <button
                                    class="btn btn-light-primary chat-start-text chat-sidebar-toggle d-block d-lg-none py-50 px-1">شروع
                                    گفتگو!</button>
                            </div>
                            <div class="chat-area d-none">
                                <div class="chat-header">
                                    <header
                                        class="d-flex justify-content-between align-items-center border-bottom px-1 py-75">
                                        <div class="d-flex align-items-center">
                                            <div class="chat-sidebar-toggle d-block d-lg-none mr-1"><i
                                                    class="bx bx-menu font-large-1 cursor-pointer"></i>
                                            </div>
                                            <div class="avatar chat-profile-toggle m-0 mr-1">
                                                <img src="../../assets/images/portrait/small/avatar-s-26.jpg"
                                                    alt="avatar" height="36" width="36">
                                                <span class="avatar-status-busy"></span>
                                            </div>
                                            <h6 class="mb-0">جسیکا آلبا</h6>
                                        </div>
                                        <div class="chat-header-icons">
                                            <span class="chat-icon-favorite">
                                                <i class="bx bx-star font-medium-5 cursor-pointer"></i>
                                            </span>
                                            <span class="dropdown">
                                                <i class="bx bx-dots-vertical-rounded font-medium-4 ml-25 cursor-pointer dropdown-toggle nav-hide-arrow cursor-pointer"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false" role="menu">
                                                </i>
                                                <span class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="JavaScript:void(0);"><i
                                                            class="bx bx-pin mr-25"></i> سنجاق به بالا</a>
                                                    <a class="dropdown-item" href="JavaScript:void(0);"><i
                                                            class="bx bx-trash mr-25"></i> حذف گفتگو</a>
                                                    <a class="dropdown-item" href="JavaScript:void(0);"><i
                                                            class="bx bx-block mr-25"></i> مسدود کردن</a>
                                                </span>
                                            </span>
                                        </div>
                                    </header>
                                </div>
                                <!-- chat card start -->
                                <div class="card chat-wrapper shadow-none">
                                    <div class="card-content">
                                        <div class="card-body chat-container">
                                            <div class="chat-content">
                                                @php
                                                    $me = auth()->id();
                                                @endphp
                                                @foreach ($messages as $message)
                                                    
                                                <div class="chat {{ $message->sender_id != $me ? 'chat-left':'' }}">
                                                    <div class="chat-avatar">
                                                        <a class="avatar m-0">
                                                            <img src="/assets/images/profile/admin/avatar/{{ $message->sender->pic }}"
                                                                alt="avatar" height="36" width="36">
                                                        </a>
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-message">
                                                            <p>{{ $message->text }}</p>
                                                            <span class="chat-time">{{ verta($message->created_at)->format('H:i') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer chat-footer border-top px-2 pt-1 pb-0 mb-1">
                                        <form class="d-flex align-items-center" 
                                        {{-- onsubmit="chatMessagesSend();" --}}
                                        method="POST"
                                            action="{{ route('sale.send_message',$sale) }}"
                                            >
                                            @csrf
                                            <i class="bx bx-face cursor-pointer"></i>
                                            <i class="bx bx-paperclip ml-1 cursor-pointer"></i>
                                            <input type="text" class="form-control chat-message-send mx-1" name="text"
                                                placeholder="پیام خود را اینجا بنویسید ...">
                                            <button type="submit" class="btn btn-primary glow send d-lg-flex"><i
                                                    class="bx bx-paper-plane"></i>
                                                <span class="d-none d-lg-block ml-1">ارسال</span></button>
                                        </form>
                                    </div>
                                </div>
                                <!-- chat card ends -->
                            </div>
                        </section>
                        <!-- app chat window ends -->
                        <!-- app chat profile right sidebar start -->
                        <section class="chat-profile">
                            <header class="chat-profile-header text-center border-bottom">
                                <span class="chat-profile-close">
                                    <i class="bx bx-x"></i>
                                </span>
                                <div class="my-2">
                                    <div class="avatar">
                                        <img src="../../assets/images/portrait/small/avatar-s-26.jpg" alt="chat avatar"
                                            height="100" width="100">
                                    </div>
                                    <h5 class="app-chat-user-name mb-0">جسیکا آلبا</h5>
                                    <span>توسعه دهنده</span>
                                </div>
                            </header>
                            <div class="chat-profile-content p-2">
                                <h6 class="mt-1">درباره</h6>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک</p>
                                <h6 class="mt-2">اطلاعات شخصی</h6>
                                <ul class="list-unstyled">
                                    <li class="mb-25">email@gmail.com</li>
                                    <li>+1(789) 950-7654</li>
                                </ul>
                            </div>
                        </section>
                        <!-- app chat profile right sidebar ends -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <x-slot name="script">
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/assets/js/scripts/pages/app-chat.js"></script>
    <!-- END: Page JS-->


    </x-slot>
    </x-base>
