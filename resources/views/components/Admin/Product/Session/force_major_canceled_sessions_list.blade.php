<x-base title="@t(لیست جلسات کنسلی فورس ماژور)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(گزارش جلسات کنسل شده فورس ماژور)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                            <li class="breadcrumb-item">@t(گزارش ها)</li>
                            <li class="breadcrumb-item active">@t(گزارش جلسات کنسل شده فورس ماژور)</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        
        <div class="row">                               
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="invoice-create-btn">
                            <h3>@t(گزارش جلسات کنسل شده فورس ماژور)</h3>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        @include('partials.flash')
                        
                        <table class="table-bordered text-secondary" style="width: 100%">
                            <thead>
                                <th>@t(ردیف)</th>
                                <th>list</th>
                                <th>@t(مراجع)</th>
                                <th>@t(مشاور)</th>
                                <th>@t(اپراتور)</th>
                                <th>@t(کنسل کننده)</th>
                                <th>@t(تاریخ جلسه)</th>
                                <th class="text-center">@t(لغواز طرف)</th>
                                <th>@t(تاریخ لغو)</th>
                                <th>@t(علت)</th>
                                <th>@t(توضیحات)</th>
                                <th>@t(عملیات)</th>
                            </thead>
                            <tbody>
                                @foreach ($fm_sessions as $counter => $fm)
                                    <tr>
                                        <td>{{ ++$counter }}</td>
                                        <td>{{ $fm->id }}</td>
                                        <td>{{ $fm->customer->name ?? '' }}&nbsp;{{ $fm->customer->family ?? '' }}&nbsp;({{ $fm->customer->mobile ?? '' }})</td>
                                        <td>{{ $fm->advisor->name ?? '' }}&nbsp;{{ $fm->advisor->family ?? '' }}</td>
                                        <td>{{ $fm->operator->name ?? '' }}&nbsp;{{ $fm->operator->family ?? '' }}</td>
                                        <td>{{ $fm->cancel_operator->name ?? '' }}&nbsp;{{ $fm->cancel_operator->family ?? '' }}</td>
                                        <td>{{ verta($fm->date)->format('Y/m/d') }}&nbsp;{{ $fm->session_start ?? '' }}</td>
                                        <td class="text-center">
                                            @switch($fm->delete_type)
                                                @case(1)
                                                    <div class="chip chip-danger mr-1" data-toggle="tooltip" data-placemnt="top" title="{{ setting($fm->delete_type) }}">
                                                        <div class="chip-body">
                                                            <div class="avatar">
                                                            <i class="bx bxs-institution"></i>
                                                            </div>
                                                            <span class="chip-text">مرکز</span>
                                                        </div>
                                                    </div>
                                                    @break
                                                @case(2)
                                                    <div class="chip chip-warning mr-1" data-toggle="tooltip" data-placemnt="top" title="{{ setting($fm->delete_type) }}">
                                                        <div class="chip-body">
                                                            <div class="avatar">
                                                            <i class="bx bx-briefcase"></i>
                                                            </div>
                                                            <span class="chip-text">مشاور</span>
                                                        </div>
                                                    </div>
                                                    @break
                                                @default
                                                    <div class="chip chip-success mr-1" data-toggle="tooltip" data-placemnt="top" title="{{ setting($fm->delete_type) }}">
                                                        <div class="chip-body">
                                                            <div class="avatar">
                                                            <i class="bx bx-user"></i>
                                                            </div>
                                                            <span class="chip-text">مراجع</span>
                                                        </div>
                                                    </div>
                                            @endswitch
                                        </td>
                                        <td dir="ltr">{{ verta($fm->updated_at) }}</td>
                                        <td>{{ $fm->cancel_reason->title ?? '' }}</td>
                                        <td>{{ $fm->delete_comment }}</td>
                                        <td>
                                            {{-- <a href="#" data-toggle="tooltip" data-placemnt="top" title="@t(تایید کنسلی)">
                                                <i class="bx bx-check text-success" data-toggle="modal" data-target="#exampleModal{{ $fm->id }}"></i></a> --}}
                                            {{-- <a href="{{ route('force_major_result',[($fm->id),'rejected']) }}" data-toggle="tooltip" data-placemnt="top" title="@t()"><i class="bx bx-x text-danger" data-toggle="tooltip" data-placemnt="top" title="@t(تایید کنسلی)"></i></a> --}}
                                            <button type="button" class="btn btn-success btn-sm btn-icon" data-toggle="modal" data-target="#exampleModal{{ $fm->id }}">
                                                <i class=" bx bx-check" data-toggle="tooltip" data-placemnt="top" title="@t(تایید کنسلی)"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm btn-icon" data-toggle="modal" data-target="#rejectedModal{{ $fm->id }}">
                                                <i class=" bx bx-x" data-toggle="tooltip" data-placemnt="top" title="@t(رد درخواست کنسلی)"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--Accepted Modal -->
                                    <div class="modal fade" id="exampleModal{{ $fm->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel{{ $fm->id }}" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel{{ $fm->id }}">@t(موجه دانستن دلیل کنسل کردن جلسه )&nbsp;{{ $fm->customer->name ?? '' }}&nbsp;{{ $fm->customer->family ?? '' }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                @t(موجه دانستن دلیل کنسل کردن جلسه )&nbsp;{{ $fm->customer->name ?? '' }}&nbsp;{{ $fm->customer->family ?? '' }}
                                                <br>
                                                @t(هزینه جلسه به کیف پول مراجع نامبرده عودت داده می شود)
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                                            <a type="button" href="{{ route('force_major_result',[($fm->id),'accepted']) }}" class="btn btn-success">@t(تایید کنسلی)</a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    {{-- rejectedmodal --}}
                                    <div class="modal fade" id="rejectedModal{{ $fm->id }}" tabindex="-1" role="dialog" aria-labelledby="rejectedModalLabel{{ $fm->id }}" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="rejectedModalLabel{{ $fm->id }}">@t(غیرموجه بودن دلیل کنسل کردن جلسه )&nbsp;{{ $fm->customer->name ?? '' }}&nbsp;{{ $fm->customer->family ?? '' }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                @t(غیر موجه دانستن دلیل کنسل کردن جلسه )&nbsp;{{ $fm->customer->name ?? '' }}&nbsp;{{ $fm->customer->family ?? '' }}
                                                <br>
                                                @t(هزینه جلسه از مراجع نامبرده کسر می شود)
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                                            <a type="button" href="{{ route('force_major_result',[($fm->id),'rejected']) }}" class="btn btn-danger">@t(رد کردن کنسلی)</a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        

    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->
        <script>$('div.alert').delay(3000).slideUp(300);</script>
        

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
        <script>            
            $(document).ready(function() {

                $('.datepicer_date').datepicker({
                    dateFormat: "yy/mm/dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                });
            });
        </script>

    </x-slot>
</x-base>
