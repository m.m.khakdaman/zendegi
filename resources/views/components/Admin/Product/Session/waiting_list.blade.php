<x-base title="@t(لیست انتظار)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/animate/animate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/extensions/sweetalert2.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t( لیست انتظار)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a></li>
                  <li class="breadcrumb-item">@t(جلسات مشاوره)</li>
                  <li class="breadcrumb-item active">@t(لیست انتظار)</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="col-3">
                <div class="card background-color">
                    <div class="card-header">
                        {{-- <h6>@t(افزودن){{ ' '.$customer->name.' '.$customer->family.' ' }}(به لیست انتظار)</h6> --}}
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                          <form action="{{ route('wl.save') }}" class="wizard-vertical" 
                          method="POST"
                          enctype="multipart/form-data">
                          @csrf
                            <fieldset class="form-group">
                                <label for="customer_id">نام مراجع</label>
                                <select name="customer_id" id="customer_id" class="form-control select2">
                                    <option value="">@t(نام یا موبایل مراجع را وارد کنید)</option>
                                    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name.' '.$customer->family }}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="advisor_id">نام مشاور</label>
                                <select name="advisor_id" id="advisor_id" class="form-control select2">
                                    <option value="">@t(نام مشاور را وارد کنید)</option>
                                    @foreach ($advisors as $advisor)
                                        <option value="{{ $advisor->id }}">{{ $advisor->name.' '.$advisor->family }}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="category_id">علت مراجعه</label>
                                <select name="category_id" id="category_id" class="form-control select2">
                                    <option value="">@t(علت مراجعه را وارد کنید)</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                            <div class="form-group">
                                <label>@t(تاریخ درخواستی)</label>
                                <div class="controls form-label-group ">
                                    <input type="text" class="form-control" name="date" id="date_sw"
                                        pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                        data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                        placeholder="انتخاب تاریخ" aria-invalid="false"
                                        onclick="change_date(this.value)">
                                    <div class="form-control-position">
                                        <i class="bx bx-calendar">
                                        </i>
                                    </div>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <fieldset class="form-group">
                                <label for="time">@t(زمان درخواستی)</label>
                                <input type="time" name="time" class="form-control" id="time">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="note">@t(توضیحات)</label>
                                <textarea name="note" class="form-control" id="note" rows="1"></textarea>
                            </fieldset>
                            <fieldset>
                                <input type="hidden" value="0" name="id">
                                <input type="hidden" value="در انتظار تخصیص" name="status">
                                <input type="submit" class="btn btn-success float-right" value="ثبت">
                            </fieldset>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="card background-color">
                    <div class="card-header">
                        <h5>@t(لیست انتظار)</h5>
                    </div>
                    <div class="card-content">
                        <div class="card-body p-0">
                            @php
                                $tmp = 0;
                            @endphp
                            <table class="table">
                                <thead>
                                    <th>#</th>
                                    <th>@t(نام مراجع)</th>
                                    <th>@t(نام مشاور)</th>
                                    <th>@t(علت مراجعه)</th>
                                    <th>@t(تاریخ)</th>
                                    <th>@t(عملیات)</th>
                                </thead>
                                @foreach ($lists as $list)
                                <tr>
                                    <td>{{ ++$tmp }}</td>
                                    <td>
                                        <span data-toggle="tooltip" data-placement="top" title="@t({{ $list->customer->mobile }})">{{ $list->customer->name.' '.$list->customer->family }}</span>
                                    </td>
                                    <td>
                                        <span data-toggle="tooltip" data-placement="top" title="@t({{ $list->advisor->mobile }})">{{ $list->advisor->name.' '.$list->advisor->family }}</span>
                                    </td>
                                    <td><small>{{ $list->category->name }}</small></td>
                                    <td dir="ltr">
                                        <div class="badge badge-pill badge-success">{{ $list->date }}</div>
                                        <div class="badge badge-pill badge-warning">{{ $list->time }}</div>
                                    </td>
                                    <td>
                                        @switch($list->status)
                                            @case('در انتظار تخصیص')
                                            <a href="#" class="text-warning" data-toggle="tooltip" data-placement="top" title="@t(ثبت جلسه برای){{ ' '.$list->customer->name.' '.$list->customer->family}}">
                                                <i class="bx bx-alarm-add"></i>
                                            </a>
                                            @break
                                            @case('تخصیص یافته')
                                                <i class="bx bx-alarm text-secondary" data-toggle="tooltip" data-placement="top" title="@t({{ $list->status }})"></i>
                                            @break
                                            @case('کنسل شد')
                                                <i class="bx bx-alarm-off text-danger" data-toggle="tooltip" data-placement="top" title="@t({{ $list->status }})"></i>
                                            @break
                                        
                                            @default
                                                
                                        @endswitch
                                        
                                        @if ($list->note)
                                        <a href="#" class="text-primary" data-toggle="tooltip" data-placement="top" title="{{ $list->note}}">
                                            <i class="bx bx-info-circle"></i>
                                        </a>
                                        @else
                                        <a href="#" class="text-danger" data-toggle="tooltip" data-placement="top" title="@t(توضیحات ندارد)">
                                            <i class="bx bx-notification-off"></i>
                                        </a>
                                        @endif
                                        
                                        <a href="#" class="text-secondary" id="confirm-text" data-toggle="tooltip" data-placement="top" title="@t(پاک کردن از لیست)">
                                            <i class="bx bx-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
        <script src="/assets/vendors/js/extensions/polyfill.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src="/assets/js/scripts/extensions/sweet-alerts.js"></script>
        <!-- END: Page JS-->
        <script>
            function G2J(D) {
                dd = D.toLocaleDateString().split('/');

                var a = (
                    jd_to_persian(
                        gregorian_to_jd(
                            dd[2],
                            dd[0],
                            parseInt(dd[1])
                        )
                    )
                );
                return (a[0] + '/' + a[1] + '/' + a[2]);
            }

            function change_date(D) {
                dd = D.split('/');
                var a = (
                    jd_to_gregorian(
                        persian_to_jd(
                            parseInt(dd[0]),
                            parseInt(dd[1]),
                            parseInt(dd[2])
                        )
                    )
                );
                calendar.gotoDate(new Date(a[0] + '-' + a[1] + '-' + a[2]));
            }

            $('#date_sw').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });

            // $('#confirm-text').on('click', function () {
            //     Swal.fire({
            //         title: 'آیا مطمئنید؟',
            //         text: "این عمل قابل بازگشت نخواهد بود!",
            //         type: 'warning',
            //         showCancelButton: true,
            //         confirmButtonText: 'باشه',
            //         confirmButtonClass: 'btn btn-primary',
            //         cancelButtonClass: 'btn btn-danger ml-1',
            //         cancelButtonText: 'انصراف',
            //         buttonsStyling: false,
            //     }).then(function (result) {
            //         if (result.value) {
            //             Swal.fire({
            //                 type: "success",
            //                 title: 'حذف شد!',
            //                 text: 'فایل شما حذف شد.',
            //                 confirmButtonClass: 'btn btn-success',
            //                 confirmButtonText: 'باشه',
            //             });
            //         }
            //     });
            // });
        </script>


    </x-slot>
</x-base>
