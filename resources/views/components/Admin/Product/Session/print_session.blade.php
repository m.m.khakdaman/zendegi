<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <style>
        /* body {
            width: 230mm;
            height: 100%;
            margin: 0 auto;
            padding: 0;
            font-size: 12pt;
            background: rgb(204, 204, 204);
        } */

        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

        /* @media print {

            html,
            body {
                width: 210mm;
                height: 297mm;
            }

            .main-page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        } */

    </style>
</head>

<body style="background-image:none;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center">
            <p>
                <b>بسمه تعالی</b>
            </p>
            <p>
                <b>مرکز مشاوره زندگی عاقلانه</b>
            </p>
        </div>
        <div class="col-4">

            <div class="row text-center float-right">
                <div>
                    <img src="http://zendegiaghelane.sanamsoft.com/theme/images/zendegiaghelane.png" alt=""
                        class="img-responsive" />
                    <p>مرکز مشاوره زندگی عاقلانه</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="col-12 row">
                <div class="col-6">
                    <p>نام مراجع:</p>
                </div>
                <div class="col-6">
                    <p>{{ $session->customer->name }} {{ $session->customer->family }}</p>
                </div>
                <div class='col-6'>
                    <p>شماره پرونده: </p>
                </div>
                <div class='col-6'>
                    <p>{{ $session->id }}</p>
                </div>
                <div class='col-6'>
                    <p>همراه مراجع: </p>
                </div>
                <div class='col-6'>
                    <p>{{ $session->customer->mobile }}</p>
                </div>
                <div class='col-6'>
                    <p>ساعت حضور:</p>
                </div>
                <div class='col-6'>
                    <p>{{ verta($session->session_start)->format('H:i') }}</p>
                </div>
                <div class='col-6'>
                    <p>نام مشاور:</p>
                </div>
                <div class='col-6'>
                    <p>{{ $session->advisor->name . ' ' . $session->advisor->family }}</p>
                </div>
                <div class='col-6'>
                    <p>تاریخ جلسه:</p>
                </div>
                <div class='col-6'>
                    <p>{{ verta($session->date)->format('Y/m/d l ') }}</p>
                </div>
                <div class='col-6'>
                    <p>مدت جلسه:</p>
                </div>
                <div class='col-6'>
                    <p>{{ $session->session_length }}دقیقه</p>
                </div>
                <div class='col-6'>
                    <p>اپراتور:</p>
                </div>
                <div class='col-6'>
                    <p>{{ $session->operator->name . ' ' . $session->operator->family }}</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 mt-1 mb-2">
                    <h4>مبالغ پرداختی</h4>
                    <hr>
                </div>
            </div>
            <table class="table table-bordered" width='100%'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>مبلغ دریافتی</th>
                        <th>باقی مانده</th>
                        <th>شرح</th>
                        <th>تاریخ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($session->transactions->distinctions as $key => $item)
                        <tr class="text-center">
                            <td class='td_center'>{{ $key + 1 }}</td>
                            <td dir="ltr">{{ $item->amount }}</td>
                            <td class="text-center">-</td>
                            <td dir="ltr" class="text-center">{{ $item->source->comment ?? '-' }}</td>
                            <td dir="ltr">{{ verta($item->created_at)->format('Y/m/d H:i') }}</td>
                        </tr>
                    @endforeach
                    <tr class="text-center">
                        <td class='td_center'>مجموع</td>
                        <td dir="ltr">
                            {{ $session->transactions->amount_standing - $session->transactions->amount }}
                        </td>
                        <td dir="ltr">
                            {{ -$session->transactions->amount_standing }}
                        </td>
                        <td class="text-center">-</td>
                        <td dir="ltr">{{ verta($session->transactions->created_at)->format('Y/m/d H:i') }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 mt-1 mb-2">
                    <h4>توضیحات</h4>
                    <hr>
                    <p>سلام و درود بر شما مراجع عزیز مرکز مشاوره زندگی عاقلانه
                        خواهشمند است موارد ذیل را جهت بهره مندی از خدمات بهینه مرکز مطالعه و رعایت فرمایید.</p>
                </div>
                <div class="col-12" style="
                border: 1px solid;
                border-radius: 15px;
            ">
                    <ul class="line-height-2 mt-1 font-size-base">

                        <li>1- لطفا در انتخاب مشاور خود دقت لازم را مبذول فرمایید ، لذا هیچ گونه اعتراضی پس از جلسه
                            پذیرفته نمی شود.
                        </li>
                        <li>2 - خواهشمند است در ساعت مقرر در مرکز حاضر شوید و در صورت تاخیر ، تنها امکان استفاده از زمان
                            باقی مانده
                            جلسه خود را خواهید داشت .</li>
                        <li>3 - درصورت تمایل به کنسل نمودن جلسه مشاوره حداقل 48 ساعت قبل از تایم جلسه اقدام نمائید،در
                            غیر اینصورت
                            مشمول پرداخت هزینه جلسه مشاوره می باشید . (هر تایم مخصوص یک مراجع می باشد)</li>
                        <li>4 - در صورت عدم حضور در جلسه ، هزینه عودت داده نمیشود و در صورت غیبت و کنسل نمودن کمتر از 24
                            ساعت جلسه
                            توسط مشاور ، یک جلسه رایگان دریافت خواهید کرد .</li>
                        <li> 5 - ممکن است به دلیل نیاز اضطراری مراجع قبل ، جلسه مشاوره شما با اندکی تاخیر شروع شود ، لذا
                            پیشاپیش عذر
                            خواهی مارا پذیرا باشید .</li>
                        <li>6 - مدت زمان هر جلسه مشاوره 45 دقیقه میباشد .درصورت افزایش مدت جلسه ، هزینه مشاوره نیز به
                            نسبت افزایش
                            میابد.</li>
                        <li>7 - لطفا برگه رسید جلسه را هنگام مراجعه به مرکز همراه داشته باشید .</li>
                        <li>8 - لطفا برای رعایت مسائل بهداشتی حتما از ماسک استفاده نمایید .</li>
                        <li>9 - به دلیل احتمال اختلال در سامانه پیامکی مخابرات، امکان عدم ارسال پیامک یادآور وجود دارد. پس لطفا به ساعت و تاریخ قبض رسید دریافتی خود توجه کنید .</li>
                        <li>در زندگی عاقلانه همه چیز مفید است .</li>
                        <li>{{ $session->room->building->description }} .</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>

<script>
    // window.print();
</script>

</html>
