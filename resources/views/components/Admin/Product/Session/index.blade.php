<x-base>
    <x-slot name='title'>
        مشاوران
    </x-slot>

    <x-slot name='css'>


        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/file-uploaders/dropzone.css">
        <!-- END: Page CSS-->


        <link href='/assets/fullcalendar/main.min.css' rel='stylesheet' />
        <!-- END: Page CSS-->
        <style>
            .Container {
                width: 200px;
                overflow-y: auto;
            }

            .Content {
                width: 300px;
            }

            .Flipped,
            .Flipped .Content {
                transform: rotateX(180deg);
                -ms-transform: rotateX(180deg);
                /* IE 9 */
                -webkit-transform: rotateX(180deg);
                /* Safari and Chrome */
            }

        </style>
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(لیست مشاورین)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.index') }}">@t(لیست مشاورین)</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->

    <div class="widget-chat widget-chat-demo d-none">
        <div class="card mb-0">
            <div class="card-header border-bottom p-0">
                <div class="media m-75">
                    <a href="JavaScript:void(0);">
                        <div class="avatar mr-75">
                            <img src="" alt="avtar images" width="32" height="32">
                            <span class="avatar-status-online"></span>
                        </div>
                    </a>
                    <div class="media-body">
                        <h6 class="media-heading mb-0 mt-n25"><a href="javaScript:void(0);">گیرنده</a></h6>
                        <span class="text-muted font-small-3">فعال</span>
                    </div>
                    <i class="bx bx-x widget-chat-close float-right my-auto cursor-pointer"></i>
                </div>
            </div>
            <div class="card-body widget-chat-container widget-chat-demo-scroll ps ps--active-y">
                <div class="chat-content" id="chat">
                    <!--
                    <div class="badge badge-pill badge-light-secondary my-1">امروز</div>
                    <div class="chat">
                        <div class="chat-body">
                            <div class="chat-message">
                                <p>متن 2</p>
                                <span class="chat-time">7:45 ق.ظ</span>
                            </div>
                        </div>
                    </div>
                    <div class="chat chat-left">
                        <div class="chat-body">
                            <div class="chat-message">
                                <p>متن 2</p>
                                <span class="chat-time">7:50 ق.ظ</span>
                            </div>
                        </div>
                    </div>
                    !-->
                </div>
                <div class="ps__rail-x" style="left: 0px; bottom: -117px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 117px; left: 344px; height: 405px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 91px; height: 314px;"></div>
                </div>
            </div>
            <div class="card-footer border-top p-1">
                <form class="d-flex" onsubmit="widgetChatMessageDemo();" action="javascript:void(0);" autocomplete="off">
                    <input type="text" id='chat_sale_id' name="chat_sale_id" style='display:none;' autocomplete="off">
                    <input type="text" class="form-control chat-message-demo mr-75" id="body_message"
                        placeholder="اینجا بنویسید ..." autocomplete="off">
                    <button type="submit" class="btn btn-primary glow px-1" onclick="send_message()"><i
                            class="bx bx-paper-plane"></i></button>
                </form>
            </div>
        </div>
    </div>
    <script>
        function send_message() {
            $.post({
                dataType: "json",
                url: "/admin/product/sale/" + chat_sale_id.value + "/comment/send_comment",
                data: {
                    '_token': $("input[name='_token']").val(),
                    'text': body_message.value,
                    'sale_id': chat_sale_id.value,
                },
                success: function(data, status) {}
            });

        }

        function show_chat(sale_id) {
            auth_user_id = {{ auth()->id() }};
            $.ajax({
                dataType: "json",
                url: "/admin/product/sale/" + sale_id + "/comment",
                data: false,
                success: function(data, status) {

                    $('#chat').html(``);
                    $.each(data, function(index, value) {
                        if (auth_user_id == value.sender_id) {
                            $('#chat').append(`
                            <div class="chat chat-left">
                                <div class="chat-body">
                                    <div class="chat-message">
                                        <p>` + value.text + `</p>
                                        <span class="chat-time">` + value.created_at + `</span>
                                    </div>
                                </div>
                            </div>
                        `);
                        } else {
                            $('#chat').append(`
                            <div class="chat chat">
                                <div class="chat-body">
                                    <div class="chat-message">
                                        <p>` + value.sender.name + ' ' + value.sender.family + ':' + value.text + `</p>
                                        <span class="chat-time">` + value.created_at + `</span>
                                    </div>
                                </div>
                            </div>
                        `);
                        }
                    });
                }
            });


            $('#chat_sale_id').val(sale_id);
            $(".widget-chat-demo").toggleClass("d-block d-none");
        }
    </script>




    <!-- Cancl form Modal -->
    <button type="button" class="btn btn-outline-primary block" data-toggle="modal" data-target="#cancel_session_modal"></button>
    <div class="modal fade" id="cancel_session_modal" tabindex="-1" role="dialog"
        aria-labelledby="cancel_session_modalTitle" aria-hidden="true">
        <form class="form form-horizontal" id="cancel_form" method="POST" onsubmit="return false;">
            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cancel_session_modalTitle">لغو جلسه</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">                        
                        <div class="row">
                            <ul class="list-unstyled mb-0">
                                <li class="d-inline-block mr-2 mb-1">
                                    @csrf
                                    <input type="hidden" name="sale_id" id="sale_id2">
                                    <span class="font-medium-1">@t(لغو جلسه از طرف:)</span>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <div class="radio radio-primary radio-glow">
                                        <input type="radio" id="radioGlow1" name="delete_type" value="1">
                                        <label for="radioGlow1">مشاور</label>
                                    </div>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <div class="radio radio-secondary radio-glow">
                                        <input type="radio" id="radioGlow2" name="delete_type" value="2">
                                        <label for="radioGlow2">مرکز</label>
                                    </div>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <div class="radio radio-success radio-glow">
                                        <input type="radio" id="radioGlow3" name="delete_type" value="3">
                                        <label for="radioGlow3">مراجع</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-12 p-0">
                                <label for="reason">@t(علت لغو جلسه)</label>
                                <select name="reason" id="reason" class="form-control select2">
                                    <option value="0" selected disabled>@t(علت لغو جلسه را انتخاب کنید...)</option>
                                    @foreach ($cancel_reasons as $reason)
                                        <option value="{{ $reason->id }}">{{ $reason->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 p-0 mt-1">
                                <label for="basicTextarea">توضیحات</label>
                                <textarea class="form-control" id="basicTextarea" rows="2" name="comment"
                                    placeholder="توضیحات لغو جلسه"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">بستن</span>
                        </button>
                        <button type="button" onclick="cancel_form()" class="btn btn-warning ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">لغو جلسه</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- Cancel form end Modal -->

    <!-- force Major Cancl form Modal -->
    <button type="button" class="btn btn-outline-primary block" data-toggle="modal" data-target="#force_major_cancel_session_modal"></button>
    <div class="modal fade" id="force_major_cancel_session_modal" tabindex="-1" role="dialog"
        aria-labelledby="force_major_cancel_session_modalTitle" aria-hidden="true">
        <form class="form form-horizontal" id="force_major_cancel_form" method="POST" onsubmit="return false;">
            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="force_major_cancel_session_modalTitle">لغو فورس ماژور</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">    
                            <ul class="list-unstyled mb-0">
                                <li class="d-inline-block mr-2 mb-1">
                                    @csrf
                                    <input type="hidden" name="fm_sale_id" id="fm_sale_id">
                                    <span class="font-medium-1">@t(لغو جلسه از طرف:)</span>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <div class="radio radio-primary radio-glow">
                                        <input type="radio" id="fm_advisor" name="fm_delete_type" value="1" required>
                                        <label for="fm_advisor">مشاور</label>
                                    </div>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <div class="radio radio-secondary radio-glow">
                                        <input type="radio" id="fm_markaz" name="fm_delete_type" value="2" required>
                                        <label for="fm_markaz">مرکز</label>
                                    </div>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <div class="radio radio-success radio-glow">
                                        <input type="radio" id="fm_customer" name="fm_delete_type" value="3" required>
                                        <label for="fm_customer">مراجع</label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-12 p-0">
                                <label for="fm_reason">@t(علت لغو جلسه)</label>
                                <select name="fm_reason" id="fm_reason" class="form-control select2" required>
                                    <option value="0" selected disabled>@t(علت لغو جلسه را انتخاب کنید...)</option>
                                    @foreach ($cancel_reasons as $reason)
                                        <option value="{{ $reason->id }}">{{ $reason->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 p-0 mt-1">
                                <label for="fm_Textarea">توضیحات</label>
                                <textarea class="form-control" id="fm_Textarea" rows="2" name="fm_comment" required
                                    placeholder="با توجه به اینکه کنسل کردن جلسه زیر 48 ساعت، ممنوع بوده و فقط در شرایط فورس ماژور امکان پذیر است، و تشخیص موجه بودن علت کنسلی منوط به نظر مدیر مجموعه است، ذکر دقیق علت کنسل کردن جلسه ضروری می باشد."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">@t(انصراف)</span>
                        </button>
                        <button type="button" onclick="force_major_cancel_form()" class="btn btn-warning ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">@t(ارائه به مدیریت)</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
        
    </div>
    <!-- force Major Cancel form end Modal -->

    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="controls form-label-group ">
                                    <input type="text" class="form-control" name="date_sw" id="date_sw"
                                        pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                        data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                        placeholder="انتخاب تاریخ" aria-invalid="false"
                                        onchange="change_date(this.value)">
                                    <div class="form-control-position">
                                        <i class="bx bx-calendar">
                                        </i>
                                    </div>
                                    <label for="date_sw">انتخاب تاریخ</label>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                        <div id="calendar_tag"></div>
                    </div>

                </div>
            </div>
        </div>

        <!--Basic Modal -->

        <!--Basic Modal -->
        <div class="modal fade text-left " id="edit_jalase" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered" role="document">
                <form class="form form-horizontal" id="form" onsubmit="return false;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel1"> ثبت و ویرایش جلسه <span id="mandeh"></span></h3>


                            <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>

                        <div class="modal-body">

                            @csrf
                            <input type="text" name="product_id" readonly id="product_id" style="display: none"
                                value='0'>
                            <input type="text" name="date" readonly id="date" style="display: none">
                            <input type="text" name="Waitinglist" readonly id="Waitinglist" style="display: none">
                            <input type="text" name="sale_id" readonly id="sale_id" style="display: none">
                            <input type="text" readonly id="session_status_id" class=" form-control "
                                name="session_status_id" placeholder="وضعیت جلسه" style="display: none">
                            <div class="form-body">
                                <div class="row">



                                    <div class="col-md-4">
                                        <label>مراجع</label> <button type="button" class="btn  btn-light-secondary"
                                            id="wating_list" title="فراخوانی از لیست انتظار">
                                            <i class="bx bx-list-check"></i></button>

                                        <button type="button" class="btn  btn-light-secondary" id="add_user"
                                            title="تعریف مراجع سریع">
                                            <i class="bx bx-user-plus"></i></button>
                                    </div>
                                    <div class="col-md-8 form-group">

                                        <select type="text" id="customer_id" class2=" form-control select2"
                                            validitymsg='مراجع را انتخاب کنید.' style="width: 100%" name="customer_id"
                                            placeholder="مراجع" required>

                                        </select>
                                    </div>
                                    {{-- <div class="col-md-12">
                                        <label>مدیریت مراجعین</label>
                                    </div> --}}
                                    <div class="col-md-4">
                                        <label>مشاور</label> <button type="button" class="btn  btn-light-secondary"
                                            id="moshaver_print" title="چاپ برنامه کاری مشاور">
                                            <i class="bx bx-printer"></i></button>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <select type="text" id="advisor_id" onchange="change_advisor()"
                                            class=" form-control select2" style="width: 100%" name="advisor_id"
                                            placeholder="مشاور">
                                            <option> </option>
                                            @foreach ($advisors as $advisor)
                                                <option value="{{ $advisor->id }}">
                                                    {{ $advisor->name . ' ' . $advisor->family . ' (' . $advisor->mobile . ')' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-12">
                                        <label>مدیریت مشاورین</label>
                                    </div> --}}

                                    <div class="col-md-4">
                                        <label>نوع و علت جلسه</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select type="text" id="session_type_id" class="form-control"
                                            name="session_type_id" placeholder="نوع جلسه">
                                            @foreach ($session_types as $session_type)
                                                <option value="{{ $session_type->id }}">{{ $session_type->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <select type="text" id="category_id" class="form-control" name="category_id"
                                            placeholder="علت مراجعه">
                                            @foreach ($categories as $categorie)
                                                <option value="{{ $categorie->id }}">{{ $categorie->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>



                                    <div class="col-md-4">
                                        <label>نرخ نامه و محل</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select type="text" id="price_list_id" class="form-control " required
                                            validitymsg='تعیین نرخ نامه ضروری است' name="price_list_id"
                                            placeholder="نرخ نامه" onchange="set_price_list()">
                                        </select>
                                    </div>





                                    <div class="col-md-4 form-group">
                                        <select type="text" id="room_id" class="form-control" required
                                            validitymsg='اتاق را انتخاب کنید' name="room_id" placeholder="اتاق">
                                            @foreach ($rooms as $room)
                                                <option value="{{ $room->id }}">{{ $room->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="col-md-4">
                                        <label>زمان و تاریخ جلسه</label>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <input type="time" id="session_start" class="form-control" required
                                            validitymsg='ساعت شروع جلسه را وارد کنید' name="session_start"
                                            placeholder="ساعت شروع">
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <input type="text" id="date2" class="form-control date2" name="date2"
                                            placeholder="تاریخ">
                                    </div>


                                    <div class="col-md-4">
                                        <label>مدت و مبلغ اصلی جلسه</label>
                                    </div>

                                    <div class="col-md-4 form-group input-group ">

                                        <input max="100" min="1" type="number" id="session_length" class="form-control"
                                            placeholder="مدت زمان" aria-describedby="basic-addon2" value=""
                                            name="session_length" required='required' onchange="set_amount()"
                                            validitymsg='زمان جلسه را وارد کنید'>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">دقیقه</span>
                                        </div>
                                    </div>




                                    <div class="col-md-4 form-group input-group ">
                                        <input type="text" class="form-control" readonly placeholder="مبلغ جلسه"
                                            aria-describedby="basic-addon2" name="amount" id="amount">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">تومان</span>
                                        </div>


                                    </div>
                                    <div class="col-md-4">
                                        <label>تخفیف و مبلغ نهایی</label>
                                    </div>

                                    <div class="col-md-4 form-group input-group ">
                                        <input type="number" class="form-control" placeholder="تخفیف"
                                            aria-describedby="basic-addon2" name="discount" id="discount"
                                            onchange="set_amount()">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">تومان</span>
                                        </div>

                                    </div>
                                    <div class="col-md-2 form-group">
                                        <select type="text" id="discount_type" class="form-control"
                                            validitymsg='نوع تخفیف را انتخاب کنید' name="discount_type"
                                            placeholder="نوع تخفیف" title="نوع تخفیف" value="1">
                                            <option value="1">هردو</option>
                                            <option value="2">تنها مرکز</option>
                                            <option value="3">تنها مشاور</option>

                                        </select>
                                    </div>

                                    <div class="col-md-2 form-group input-group ">
                                        <input type="text" class="form-control" readonly placeholder="مبلغ نهایی"
                                            aria-describedby="basic-addon2" name="Totalamount" id="Totalamount">



                                    </div>



                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">




                            <button type="submit" class="btn btn-primary glow mr-1" id="botton_submit_form"
                                value='save_session'>
                                <i class="bx bx-check"></i><span class="align-middle ml-25">ثبت</span></button>

                            <button type="submit" id="botton_end" class="btn btn-success glow mr-1" value='end_session'
                                style="display: none">
                                <i class="bx bx-checkbox-checked"></i><span class="align-middle ml-25">اعلام پایان
                                    جلسه</span></button>


                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">بستن</span>
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>


    </section>
    <x-slot name="script">


        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js?JNKJNKJNKJNKJN"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>


        <script src='/assets/fullcalendar/main.min.js'></script>
        <script src="/assets/js/scripts/modal/components-modal.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>


        <!-- END: Theme JS-->
        <script>
            function G2J(D) {
                dd = D.toLocaleDateString().split('/');

                var a = (
                    jd_to_persian(
                        gregorian_to_jd(
                            dd[2],
                            dd[0],
                            parseInt(dd[1])
                        )
                    )
                );

                if (a[1] < 10) a[1] = '0' + a[1]
                if (a[2] < 10) a[2] = '0' + a[2]

                return (a[0] + '/' + a[1] + '/' + a[2]);
            }

            function J2G(D) {
                dd = D.split('/');
                var a = (
                    jd_to_gregorian(
                        persian_to_jd(
                            parseInt(dd[0]),
                            parseInt(dd[1]),
                            parseInt(dd[2])
                        )
                    )
                );
                return new Date(a[0] + '-' + a[1] + '-' + a[2]);
            }
            if (location.hash == "") {

                location.href = location.toString().split('#')[0] + '#' + G2J(new Date());

            }

            function change_date(D) {
                location.href = location.toString().split('#')[0] + '#' + D;
            }

            window.onhashchange = function() {
                if (typeof(calendar) != 'undefined') {
                    calendar.gotoDate(J2G(location.hash.replace('#', '')));
                }
            }
            window.onbeforeunload = function() {
                setCookie('old_location', location.href)
            }
            $('#date_sw').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
            $('.date2').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });

            document.addEventListener('DOMContentLoaded', function() {

                $('#moshaver_print').click(function() {
                    location.href = '/admin/product/advisor/' + $('#advisor_id').val() +
                        '/session/print?start_date=' + $('#date2').val();
                });
                $('#wating_list').click(function() {
                    location.href = '/admin/Waitinglist?advisor_id=' + $('#advisor_id').val() +
                        '&date=' + $('#date2').val(); //+'&time=' + $('#session_start').val() ;
                });
                $('#add_user').click(function() {
                    data = {};
                    $($('#form').serializeArray()).each(function(i, o) {
                        if (o.name != '_token')
                            data[o.name] = o.value
                    })
                    data.customer = {
                        name: $('#select2-advisor_id-container').text(),
                        family: "",
                        mobile: "",
                    };
                    setCookie('add_user', JSON.stringify(data))
                    setCookie('back_url', location.href);
                    location.href = '/admin/user/customer/simple/create';
                });

                var calendarEl = document.getElementById('calendar_tag');

                calendar = new FullCalendar.Calendar(calendarEl, {
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                    initialView: "{{ isset($paid) ? 'resourceTimeGridDay' : 'listWeek' }}",
                    direction: 'rtl',
                    allDaySlot: false,
                    editable: true,
                    selectable: true,
                    dayMaxEvents: true, // allow "more" link when too many events
                    dayMinWidth: 200,
                    initialDate: J2G(location.hash.replace('#', '')),
                    slotMinTime: '07:00:00',
                    slotMaxTime: '24:00:00',
                    slotDuration: '00:15:00',
                    height: 'auto',
                    buttonText: {
                        today: 'امروز',
                        month: 'ماهانه',
                        week: 'هفتگی',
                        day: 'روزانه',
                        list: 'لیست',
                    },
                    customButtons: {
                        myCustomButton: {
                            text: "{{ isset($paid) ? 'لیست پرداخت نشده ها' : 'لیست پرداخت شده ها' }}",
                            click: function() {
                                @if (isset($paid))
                                    return window.location.href = window.location.pathname + '/paid' + window.location.hash;
                                @else
                                    return window.location.href = '{{ route('session.index') }}'+window.location.hash
                                @endif



                                // calendar.show()
                                // makeTodaybtnActive();
                            }
                        }
                    },
                    locale: 'fa',
                    headerToolbar: {
                        right: 'prev,today,next',
                        center: 'title',
                        left: 'resourceTimeGridDay,resourceTimelineWeek,resourceTimelineMonth,listWeek,myCustomButton'
                    },
                    views: {
                        resourceTimeGridDay: { // name of view
                            titleFormat: {
                                month: 'long',
                                weekday: 'long',
                                year: 'numeric',
                                day: '2-digit',
                            }
                            // other view-specific options here
                        },
                    },
                    datesSet: function(a, b) {
                        location.href = location.toString().split('#')[0] + '#' + G2J(calendar.getDate());
                        $('#date_sw').val(G2J(calendar.getDate()));
                    },
                    //// uncomment this line to hide the all-day slot
                    //allDaySlot: false,

                    resources: {!! $rooms !!},
                    resourceOrder: 'order',
                    events: {
                        url: '{{ route('session.getevent', ['paid' => $paid ?? 0]) }}',
                        error: function() {
                            $('#script-warning').show();
                        },
                        done: function() {
                            $(function() {
                                $('[data-toggle="tooltip"]').tooltip()
                            })
                        }
                    },

                    select: function(arg, b) {
                        // console.log(arg.startStr);
                        var data = {
                            customer_id: null,
                            advisor_id: null,
                            price_list_id: null,
                            date: arg.startStr.split('T')[0],
                            session_start: arg.startStr.substr(11, 5),
                            room_id: arg.resource ? arg.resource.id : '(no resource)',
                            date2: G2J(arg.start)
                        };

                        all_event = calendar.getEvents();

                        all_event.forEach(function(i, b) {
                            if (
                                arg.startStr >= i.startStr &&
                                arg.endStr <= i.endStr &&
                                arg.resource.id == i._def.resourceIds[0]
                            ) {
                                data.advisor_id = i._def.extendedProps.advisor_id;
                                data.product_id = i._def.defId;
                            }
                        });
                        price_list_id = 0;

                        open_form(data);

                    },
                    eventClick: function(e, b) {

                        if (e.jsEvent.target.tagName != 'DIV') return;
                        var data = JSON.parse(JSON.stringify(e.event._def.extendedProps));
                        data.date = e.event.startStr.split('T')[0];
                        data.session_start = e.event.startStr.substr(11, 5);
                        data.date2 = G2J(e.event.start);
                        data.sale_id = e.event.id;
                        console.log(e.event);
                        // data.session_length = (((e.event.end - e.event.start)) / 60000);
                        data.room_id = e.event._def.resourceIds[0]

                        if (data.transactions)
                            data.amount = data.transactions.amount;

                        price_list_id = data.price_list_id;
                        $("#mandeh").text('مانده:' + data.transactions.amount_standing);
                        open_form(data);
                    },
                    dragRevertDuration: 0,
                    eventDrop: function(info) {
                        data = {
                            _token: $("input[name='_token']").val(),
                            id: info.event._def.publicId,
                            date: info.event.startStr,
                            room_id: info.event._def.resourceIds[0]
                        };
                        $.post(
                            "{{ route('sale.change_session_date') }}", data,
                            function(data, status) {
                                // console.log("Data: " + data + "\nStatus: " + status);

                                $(function() {
                                    $('[data-toggle="tooltip"]').tooltip()
                                })
                            });
                    },

                    eventResize: function(info) {
                        data = {
                            _token: $("input[name='_token']").val(),
                            id: info.event._def.publicId,
                            session_length: (((info.event.end - info.event.start)) / 60000),
                        };

                        $.post(
                            "{{ route('sale.change_session_length') }}", data,
                            function(data, status) {
                                // console.log("Data: " + data + "\nStatus: " + status);

                                $(function() {
                                    $('[data-toggle="tooltip"]').tooltip()
                                })
                            });
                    },

                    eventContent: function(a, b, c) {
                        Props = a.event._def.extendedProps;
                        mablagh = '1';
                        if (Props.transactions) {
                            mablagh = Props.transactions.amount + ' / ' + Props.transactions
                                .amount_standing;
                        }
                        advisor = customer = '';
                        if (Props.advisor) {
                            advisor = Props.advisor;
                        }
                        if (Props.customer) {
                            customer = Props.customer;
                        }
                        let o = document.createElement('div');
                        n = Props.session_start;

                        bb = '<button type="button" data-toggle="tooltip" title="پرینت" ' +
                            '" class="btn-small btn btn-icon rounded-circle btn-success"><a href="/admin/product/session/' +
                            a.event._def.publicId + '/print"><i class="bx bx-printer"></i></a></button>';
                        //bb="";

                        if (Props.session_status_id == 100) {
                            bb +=
                                '<button type="button" class="btn-small btn btn-icon rounded-circle btn-success"><i class="bx bx-comment-dots"></i><a data-toggle="tooltip" title="ارسال پیامک" href="/admin/product/session/' +
                                a.event._def.publicId +
                                '/send_sms_to_advisor" ></a></button>';
                        }

                        if (Props.session_status_id == 1) {
                            bb +=
                                '<button type="button" class="btn-small btn btn-icon rounded-circle btn-success"><a data-toggle="tooltip" title="ارسال پیامک کلی" href="/admin/sms/send/' +
                                customer.id + '" ><i class="bx bx-comment-dots"></i></a></button>';

                        }
                        bb +=
                            '<button type="button" class="btn-small btn btn-icon rounded-circle btn-success" data-toggle="tooltip" onclick="send_alert_to_costomer(' +
                            a.event._def.publicId +
                            ')" title="پیامک هشدار جلسه"><i class="bx bx-comment-dots"></i></button>';

                            if(this.location.hash.replace('#','') == "{{ verta()->format('Y/m/d') }}"){
                        bb +=
                            '<button type="button" data-toggle="tooltip" title="هشدار جلسه"  class="btn-small btn btn-icon rounded-circle btn-warning bx-tada" style="display:none;"><i class="bx bxs-bell-ring btn-icon-zangole"  time="' +
                            Props.session_start + ':' + Props.session_length +
                            '" status="' + Props.session_status_id + '" ></i></button>';
                            }


                        if (Props.messages && Props.messages.length > 0) {
                            bb += '<span class="badge badge-pill badge-danger badge-up">' + Props.messages
                                .length + '</span>';
                        }
                        bb +=
                            '<button type="button" data-toggle="tooltip" title="یادداشت"  class="btn-small btn btn-icon rounded-circle btn-warning " onclick="show_chat(' +
                            a.event._def.publicId + ')" ><i class="bx bx-chat"  ></i></button>';

                        if (Props.session_status_id != 2) {
                            if (Props.can_cancel == 1) {
                                bb +=
                                    '<span data-toggle="modal" data-target="#cancel_session_modal"><button data-toggle="tooltip" title="لغو جلسه"  onclick="sale_id2.value=' +
                                    a.event._def.publicId +
                                    '" type="button" class="btn-small btn btn-icon rounded-circle btn-danger"><i class="bx bx-x"></i></button></span>';
                            } else {
                                bb +=
                                    '<span data-toggle="modal" data-target="#force_major_cancel_session_modal"><i data-toggle="tooltip" title="لغو فورس ماژور"  onclick="fm_sale_id.value=' +
                                    a.event._def.publicId +
                                    '"  class="bx bxs-eraser text-danger"></i></button></span>';
                            }
                        }
                        // 55555555555555555555555


                        // $date1 = new DateTime(props.created_at);
                        // $date2 = new DateTime(date('Y-m-dTH:m:s'));

                        // $diff = $date2->diff($date1);

                        // $hours = $diff->h;
                        // $hours = $hours + ($diff->days*24);





                        o.innerHTML =
                            '<div class="fc-event-time" data-toggle="tooltip" data-html="true" data-placement="top" title="' +
                            'مشاور:&nbsp;' + advisor.name + ' ' + advisor.family + ' ' + advisor.mobile +
                            '<br/>' +
                            'مراجع: ' +
                            customer.name + ' ' + customer.family + customer.mobile + '<br/>' +
                            ' وضیعت پرداخت: ' + mablagh + '-' + a.timeText +
                            '">' + advisor.name + ' ' + advisor.family + ' ' + advisor.mobile + '</div>' +
                            '<div class="fc-event-title-container"><div class="fc-event-title fc-sticky">' +
                            a.event.title +
                            '<br><div class=""><button type="button" class="btn-small btn btn-icon rounded-circle btn-success"><a data-toggle="tooltip" title="صورت حساب" href="/admin/Financial/user/' +
                            Props.customer_id +
                            '/invoise?sale=' + a.event._def.publicId +
                            '" ><i class="bx bx-dollar"></i></a></button>' + bb + '</div></div></div>';

                        let arrayOfDomNodes = [o];




                        $(function() {
                            // start
                            function zangole(element) {
                                $(element).addClass('bx-tada');
                            }
                            time = new Date();

                            function parseTime(t, date) {
                                var d = new Date();
                                var time = t.match(/(\d+)(?::(\d\d))?\s*(p?)/);
                                d.setHours(parseInt(time[1]) + (time[3] ? 12 : 0));
                                d.setMinutes(parseInt(time[2]) || 0);
                                return d;
                            }
                            if (Props.customer) {
                                date = new Date();
                                if (date.getTime() < time.getTime()) {
                                    // console.log($('#dots_icon_' + a.event._def.publicId));
                                    zangole($('#dots_icon_' + a.event._def.publicId));
                                } else {
                                    // console.log(date.getTime() - time.getTime());
                                    // console.log(Props,a.event._instance.range.end,time.getTime(),a.event._instance.range.end.getTime()-time.getTime());
                                    setTimeout(zangole('#dots_icon_' + a.event._def.publicId), date
                                        .getTime() - time.getTime());
                                }
                            }
                            // end
                            $('[data-toggle="tooltip"]').tooltip()
                        })


                        if (a.event._def.ui.display != "background") {
                            return {
                                domNodes: arrayOfDomNodes
                            }
                        }

                        $(function() {
                            $('[data-toggle="tooltip"]').tooltip()
                        })
                    },

                    dateClick2: function(arg) {
                        // console.log(arg, 'dateClick');
                        /*console.log(
                          'dateClick',
                          arg.date,
                          arg.resource ? arg.resource.id : '(no resource)'
                        );*/
                    }
                });
                calendar.render();

                $(function() {
                    $('[data-toggle="tooltip"]').tooltip()
                })


                $('#customer_id').select2({
                    ajax: {
                        url: '{{ route('customer.get_all_ajax') }}',
                        dataType: 'json'
                        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                    }
                });




                const params = new URLSearchParams(window.location.search);


                if (params.has('Waitinglist')) {

                    data = JSON.parse(getCookie("Waitinglist"));

                    open_form({
                        advisor_id: data.advisor_id,
                        customer_id: parseInt(data.customer_id),
                        customer: {
                            name: data.customer.name,
                            family: data.customer.family,
                            mobile: data.customer.mobile,
                        },
                        category_id: data.category_id,
                        date: J2G(data.date).toISOString().slice(0, 10),
                        session_start: data.time,
                        date2: data.date,
                        Waitinglist: params.get('Waitinglist')
                    });
                }
                if (getCookie('add_user') != "") {
                    data = JSON.parse(getCookie("add_user"));
                    open_form(data);
                    setCookie('add_user', "");
                }

                $(function() {
                    $('[data-toggle="tooltip"]').tooltip()
                })


            });
        </script>
        <script>
            price_list = {!! $price_lists !!};
            price_lists = {};
            price_list_id = 0;

            function change_zangole() {
                $('.btn-icon-zangole').each(function(i, o) {
                    if ($(o).attr('status') == '1') {
                        time = $(o).attr('time'); //'09:45:90'
                        d = new Date();
                        d.setHours(time.substr(0, 2)); //'09'
                        d.setMinutes(parseInt(time.substr(3, 2)) + parseInt(time.substr(6, 2))); //45+90=135
                        time1 = d.toTimeString().substr(0, 5); //11:15
                        now = new Date().toTimeString().substr(0, 5); // now clock for example 10:00
                        if (time1 < now) {
                            $(o).parent().show();
                        }
                    }
                });
                setTimeout(function() {
                    change_zangole()
                }, 2000);
            }
            change_zangole();

            function open_form(data) {
                if (!data.discount_type) data.discount_type = 1;

                $('#price_list_id').find('option').remove().end();
                $('#form')[0].reset();


                $('#botton_submit_form').attr('onclick', 'store_form()');



                $('#customer_id').html('');
                $('#form').loadJSON(data);


                $('#botton_submit_form').show();
                $('#botton_end').hide();

                if (data.sale_id > 0) {
                    $('#botton_submit_form').attr('onclick', 'update_form()');
                    // $('#botton_end').show();

                    $('#botton_end').attr('onclick', 'update_form("end_session")');


                    if ($('#session_status_id').val() > 1) {
                        $('#botton_submit_form').hide();
                        $('#botton_end').hide();
                    } else {
                        $('#botton_submit_form').show();
                        $('#botton_end').show();
                    }
                }

                if (!data.customer_id) {
                    $('#customer_id').prop('disabled', false);
                }
                if (data.customer_id) {
                    var data2 = {
                        id: data.customer_id,
                        text: data.customer.name + ' ' + data.customer.family + ' «' + data.customer.mobile + '»'
                    };
                    var newOption = new Option(data2.text, data2.id, false, false);

                    $('#customer_id').append(newOption).trigger('change')
                    $('#customer_id').prop('disabled', true);
                }



                $('#edit_jalase').modal({
                    show: true
                });

            }

            function store_form() {
                if (!$('#form')[0].checkValidity()) {
                    return false
                }

                $(function() {
                    $('#edit_jalase').modal('toggle');
                });

                if ($('#advisor_id').val() != null) {

                    data = {
                        _token: '{{ csrf_token() }}',
                        advisor_id: $('#advisor_id').val(),
                        customer_id: $('#customer_id').val(),
                        date: $('#date').val(),
                        session_start: $('#session_start').val(),
                        session_length: $('#session_length').val(),
                        session_type_id: $('#session_type_id').val(),
                        category_id: $('#category_id').val(),
                        room_id: $('#room_id').val(),
                        product_id: $('#product_id').val(),
                        price_list_id: $('#price_list_id').val(),
                        session_length: $('#session_length').val(),
                        discount: $('#discount').val(),
                        Waitinglist: $('#Waitinglist').val(),
                        discount_type: $('#discount_type').val()
                    };
                    $('#advisor_id').val(null);

                    $.post(
                        "{{ route('sale.calender_store') }}", data,
                        function(data, status) {
                            calendar.refetchEvents()

                            $(function() {
                                $('[data-toggle="tooltip"]').tooltip()
                            })
                        });
                }
            }


            function update_form(command = 'save') {

                if (!$('#form')[0].checkValidity()) {
                    return false
                }
                $(function() {
                    $('#edit_jalase').modal('toggle');
                });

                if ($('#advisor_id').val() != null) {
                    data = {
                        _token: '{{ csrf_token() }}',
                        advisor_id: $('#advisor_id').val(),
                        sale_id: $('#sale_id').val(),
                        customer_id: $('#customer_id').val(),
                        date: $('#date').val(),
                        date2: $('#date2').val(),
                        session_start: $('#session_start').val(),
                        session_length: $('#session_length').val(),
                        session_type_id: $('#session_type_id').val(),
                        category_id: $('#category_id').val(),
                        room_id: $('#room_id').val(),
                        product_id: $('#product_id').val(),
                        price_list_id: $('#price_list_id').val(),
                        session_length: $('#session_length').val(),
                        discount: $('#discount').val(),
                        Waitinglist: $('#Waitinglist').val(),
                        discount_type: $('#discount_type').val(),
                        command: command
                    };

                    $('#advisor_id').val(null);
                    $.post(
                        "{{ route('sale.calender_sale_update') }}", data,
                        function(data, status) {
                            calendar.refetchEvents()

                            $(function() {
                                $('[data-toggle="tooltip"]').tooltip()
                            })
                        });
                }
            }

            function cancel_form() {

                data = {
                    _token: '{{ csrf_token() }}',
                    sale_id: $('#sale_id2').val(),
                    delete_type: $('input[name=delete_type]:checked').val(),
                    comment: $('[name=comment]').val(),
                    reason: $('[name=reason]').val()
                };

                $.post(
                    "{{ route('sale.calender_sale_delete') }}", data,
                    function(data, status) {
                        $(function() {
                            $('#cancel_session_modal').modal('toggle');
                        });
                        calendar.refetchEvents()

                        $(function() {
                            $('[data-toggle="tooltip"]').tooltip()
                        })
                    });

            }

            function force_major_cancel_form() {

                data = {
                    _token: '{{ csrf_token() }}',
                    sale_id: $('#fm_sale_id').val(),
                    delete_type: $('input[name=fm_delete_type]:checked').val(),
                    comment: $('[name=fm_comment]').val(),
                    reason: $('[name=fm_reason]').val()
                };

                $.post(
                    "{{ route('sale.force_major_calender_sale_delete') }}", data,
                    function(data, status) {
                        $(function() {
                            $('#force_major_cancel_session_modal').modal('toggle');
                        });
                        calendar.refetchEvents()

                        $(function() {
                            $('[data-toggle="tooltip"]').tooltip()
                        })
                    });

            }

            function set_price_list() {
                o = price_lists[$('#price_list_id').val()];
                if (o) {
                    if ($('#sale_id').val() == '') {
                        $('#session_length').val(
                            o.session_length
                        )
                    }
                    set_amount();
                }
            }

            function set_amount() {
                o = price_lists[$('#price_list_id').val()];
                if (o) {
                    $('#amount').val(
                        Math.round(
                            o.total_price * $('#session_length').val() / o.session_length / 1000
                        ) * 1000
                    );
                    $('#Totalamount').val($('#amount').val() - $('#discount').val());
                    $('#discount').attr('max', ($('#amount').val() * .2));
                }
            }



            function change_advisor() {
                if ($('#advisor_id').val() == '') {

                    return false;
                }
                $.ajax({
                    dataType: "json",
                    url: "/admin/user/advisor/" + $('#advisor_id').val() + "/price_list",
                    data: false,
                    success: function(data, status) {

                        $('#price_list_id').find('option').remove().end();

                        $.each(data, function(i, o) {
                            $('#price_list_id').append(new Option(o.text, o.id));
                        });

                        $('#price_list_id').val(price_list_id);

                        price_list = data;
                        $.each(price_list, function(i, o) {
                            price_lists[o.id] = o;
                        });
                        set_price_list();
                    }
                });
            }
            $('input,select').each(function(i, o) {
                o.oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity(e.target.attributes.validitymsg.value);
                    }
                };
                o.oninput = function(e) {
                    e.target.setCustomValidity("");
                };
            });



            (function($) {
                $.fn.loadJSON = function(options) {
                    form = this;
                    $.each(options, function(i, value, n) {
                        element = $(form).find("[name='" + i + "']")[0];
                        if (element == null)
                            return;
                        var type = element.type || element.tagName;

                        if (type == null)
                            return;
                        type = type.toLowerCase();
                        switch (type) {
                            case 'radio':
                                if (value.toString().toLowerCase() == element.value.toLowerCase())
                                    $(element).attr("checked", "checked");
                                break;
                            case 'checkbox':
                                if (value)
                                    $(element).attr("checked", "checked");
                                break;
                            case 'select-multiple':
                                var values = value.constructor == Array ? value : [value];
                                for (var i = 0; i < element.options.length; i++) {
                                    for (var j = 0; j < values.length; j++) {
                                        element.options[i].selected |= element.options[i].value ==
                                            values[j];
                                    }
                                }
                                break;
                            case 'select-one':
                                $(element).val(value);
                                if ($(element).hasClass('select2'))
                                    $(element).select2().trigger('change');
                                break;
                            default:
                                //element.value = value;
                                //$(element).attr("value", value);
                                $(element).val(value);
                                break;
                        }
                    });
                }
            })(jQuery);

            function send_alert_to_advisor(id) {
                if (confirm('پیامک تاریخ جلسه به مشاور ارسال شود؟')) {
                    $.ajax({
                        type: "POST",
                        url: '/admin/product/session/' + id + '/send_alert_sms_to_advisor',
                        success: function(data) {
                            if (data) {
                                alert('پیامک ارسال شد')
                            }
                        }
                    });
                }
            }

            function send_alert_to_costomer(id) {
                if (confirm('پیامک تاریخ جلسه به مراجع ارسال شود؟')) {
                    $.ajax({
                        type: "POST",
                        url: '/admin/product/session/' + id + '/send_alert_sms_to_costomer',
                        success: function(data) {
                            if (data) {
                                alert('پیامک ارسال شد')
                            }
                        }
                    });
                }
            }
        </script>
        <style>
            td.fc-resource {
                height: .5em;
            }

            br:before {
                content: "\A";
                white-space: pre-line
            }

            .fc-scroller {
                margin-bottom: 0px !important;
            }

            .fc-title,
            .fc-event-title {
                color: black;
            }

            .fc-event-time {
                color: black;
                font-weight: bold;
            }

            .fc-event-title {
                color: black;
                position: absolute;
                font-weight: bold;
                /* margin-top: -14px !important; */
            }

            .fc .fc-bg-event .fc-event-title {
                /* margin-top: -14px !important; */
            }

            .fc-timegrid-bg-harness {
                /* margin-top: 14px; */
            }

            .fc-sticky {
                position: inherit;
                color: #FFF;
                font-weight: bold;
            }

            .fc .fc-bg-event {
                opacity: .7;
                opacity: var(--fc-bg-event-opacity, .7);
            }

            select.select2 {
                width: 100% !important;
            }

            .blink_me {
                animation: blinker 1s linear infinite;
            }

            @keyframes blinker {
                50% {
                    opacity: .1;
                }
            }

            .btn.btn-icon {
                padding: .1rem .3rem;
                margin: 0px 1px;
            }





            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
                padding: 12px 16px;
                z-index: 1;
            }

            .dropdown:hover .dropdown-content {
                display: block;
            }

            .badge.badge-up {
                left: inherit;
                top: 11px;
                padding: 2px 7px;
                position: absolute;
            }

        </style>

        
        

    </x-slot>
</x-base>
