<x-base>
    <x-slot name='title'>
        مشاوران
    </x-slot>

    <x-slot name='css'>


        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/file-uploaders/dropzone.css">
        <!-- END: Page CSS-->


        <link href='/assets/fullcalendar/main.min.css' rel='stylesheet' />
        <!-- END: Page CSS-->
        <style>
            .Container {
                width: 200px;
                overflow-y: auto;
            }

            .Content {
                width: 300px;
            }

            .Flipped,
            .Flipped .Content {
                transform: rotateX(180deg);
                -ms-transform: rotateX(180deg);
                /* IE 9 */
                -webkit-transform: rotateX(180deg);
                /* Safari and Chrome */
            }

        </style>
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(لیست مشاورین)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.index') }}">@t(لیست مشاورین)</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->


    <!-- Vertically Centered modal Modal -->
    <button type="button" class="btn btn-outline-primary block" data-toggle="modal"
        data-target="#cancel_session_modal"></button>
    <div class="modal fade" id="cancel_session_modal" tabindex="-1" role="dialog"
        aria-labelledby="cancel_session_modalTitle" aria-hidden="true">
        <form class="form form-horizontal" id="cancel_form" method="POST" onsubmit="return false;">
            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
                role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cancel_session_modalTitle">لغو جلسه</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="بستن">
                            <i class="bx bx-x"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <ul class="list-unstyled mb-0">
                            <li class="d-inline-block mr-2 mb-1">
                                @csrf
                                <input type="hidden" name="sale_id" id="sale_id2">

                                <fieldset>
                                    <div>
                                        <label>علت لغو</label>
                                    </div>
                                </fieldset>
                            </li>
                            <li class="d-inline-block mr-2 mb-1">
                                <fieldset>
                                    <div class="radio radio-primary radio-glow">
                                        <input type="radio" id="radioGlow1" name="delete_type" checked="" value="1">
                                        <label for="radioGlow1">مشاور</label>
                                    </div>
                                </fieldset>
                            </li>
                            <li class="d-inline-block mr-2 mb-1">
                                <fieldset>
                                    <div class="radio radio-secondary radio-glow">
                                        <input type="radio" id="radioGlow2" name="delete_type" value="2">
                                        <label for="radioGlow2">مرکز</label>
                                    </div>
                                </fieldset>
                            </li>
                            <li class="d-inline-block mr-2 mb-1">
                                <fieldset>
                                    <div class="radio radio-success radio-glow">
                                        <input type="radio" id="radioGlow3" name="delete_type" value="3">
                                        <label for="radioGlow3">مراجع</label>
                                    </div>
                                </fieldset>
                            </li>
                        </ul>
                        <div class="col-12">
                            <fieldset class="form-group">
                                <textarea class="form-control" id="basicTextarea" rows="3" name="comment"
                                    placeholder="توضیحات لغو جلسه"></textarea>
                            </fieldset>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                            <i class="bx bx-x d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">بستن</span>
                        </button>
                        <button type="button" onclick="cancel_form()" class="btn btn-primary ml-1">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">لغو جلسه</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="controls form-label-group ">
                                    <input type="text" class="form-control" name="date_sw" id="date_sw"
                                        pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                        data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                        placeholder="انتخاب تاریخ" aria-invalid="false"
                                        onchange="change_date(this.value)">
                                    <div class="form-control-position">
                                        <i class="bx bx-calendar">
                                        </i>
                                    </div>
                                    <label for="date_sw">انتخاب تاریخ</label>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>

                        <div id="calendar_tag"></div>
                    </div>

                </div>
            </div>
        </div>

        <!--Basic Modal -->

        <!--Basic Modal -->
        <div class="modal fade text-left " id="edit_jalase" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered" role="document">
                <form class="form form-horizontal" id="form" onsubmit="return false;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel1"> ثبت و ویرایش جلسه</h3>
                            <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="بستن">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>

                        <div class="modal-body">

                            @csrf
                            <input type="hidden" name="product_id" id="product_id">
                            <input type="hidden" name="date" readonly id="date">
                            <input type="hidden" name="sale_id" id="sale_id" style="display: none">
                            <div class="form-body">
                                <div class="row">

                                    <div class="col-md-4">
                                        <label>وضعیت جلسه</label>
                                    </div>
                                    <div class="col-md-8 form-group">

                                        <input type="text" id="session_status_id" class=" form-control "
                                            style="width: 100%" name="session_status_id" placeholder="وضعیت جلسه">

                                    </div>

                                    <div class="col-md-4">
                                        <label>مراجع</label>
                                    </div>
                                    <div class="col-md-8 form-group">

                                        <select type="text" id="customer_id" class2=" form-control select2"
                                            validitymsg='مراجع را انتخاب کنید.' style="width: 100%" name="customer_id"
                                            placeholder="مراجع" required>

                                        </select>
                                    </div>
                                    {{-- <div class="col-md-12">
                                        <label>مدیریت مراجعین</label>
                                    </div> --}}
                                    <div class="col-md-4">
                                        <label>مشاور</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <select type="text" id="advisor_id" onchange="change_advisor()"
                                            class=" form-control select2" style="width: 100%" name="advisor_id"
                                            placeholder="مشاور">
                                            <option> </option>
                                            @foreach ($advisors as $advisor)
                                                <option value="{{ $advisor->id }}">
                                                    {{ $advisor->name . ' ' . $advisor->family . ' (' . $advisor->mobile . ')' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-12">
                                        <label>مدیریت مشاورین</label>
                                    </div> --}}

                                    <div class="col-md-4">
                                        <label>نوع و علت جلسه</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select type="text" id="session_type_id" class="form-control"
                                            name="session_type_id" placeholder="نوع جلسه">
                                            @foreach ($session_types as $session_type)
                                                <option value="{{ $session_type->id }}">{{ $session_type->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <select type="text" id="category_id" class="form-control" name="category_id"
                                            placeholder="علت مراجعه">
                                            @foreach ($categories as $categorie)
                                                <option value="{{ $categorie->id }}">{{ $categorie->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>



                                    <div class="col-md-4">
                                        <label>نرخ نامه و محل</label>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select type="text" id="price_list_id" class="form-control " required
                                            validitymsg='تعیین نرخ نامه ضروری است' name="price_list_id"
                                            placeholder="نرخ نامه" onchange="set_price_list()">
                                        </select>
                                    </div>





                                    <div class="col-md-4 form-group">
                                        <select type="text" id="room_id" class="form-control" required
                                            validitymsg='اتاق را انتخاب کنید' name="room_id" placeholder="اتاق">
                                            @foreach ($rooms as $room)
                                                <option value="{{ $room->id }}">{{ $room->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="col-md-4">
                                        <label>زمان و تاریخ جلسه</label>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <input type="time" id="session_start" class="form-control" required
                                            validitymsg='ساعت شروع جلسه را وارد کنید' name="session_start"
                                            placeholder="ساعت شروع">
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <input type="text" readonly id="date2" class="form-control" name="date2"
                                            placeholder="تاریخ">
                                    </div>


                                    <div class="col-md-4">
                                        <label>مدت و مبلغ جلسه</label>
                                    </div>

                                    <div class="col-md-4 form-group input-group ">

                                        <input type="number" id="session_length" class="form-control"
                                            placeholder="مدت زمان" aria-describedby="basic-addon2" value=""
                                            name="session_length" required='required' onchange="set_amount()"
                                            validitymsg='زمان جلسه را وارد کنید'>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">دقیقه</span>
                                        </div>
                                    </div>




                                    <div class="col-md-4 form-group input-group ">
                                        <input type="text" class="form-control" readonly placeholder="مبلغ جلسه"
                                            aria-describedby="basic-addon2" name="amount" id="amount">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">تومان</span>
                                        </div>

                                    </div>



                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">



                            <button class="btn btn-primary glow mr-1" onclick="store_form()" name='command'
                                id="submit_form" value='save_session'>
                                <i class="bx bx-check"></i><span class="align-middle ml-25">ثبت</span></button>

                            <button type="button" class="btn btn-success glow mr-1" name='command' value='end_session'>
                                <i class="bx bx-checkbox-checked"></i><span class="align-middle ml-25">اعلام پایان
                                    جلسه</span></button>

                            <button type="submit" class="btn btn-danger glow mr-1" name='command'
                                value='cancel_session'>
                                <i class="bx bx-x"></i><span class="align-middle ml-25">لغو جلسه</span></button>



                            <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">بستن</span>
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>


    </section>
    <x-slot name="script">





        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>


        <script src='/assets/fullcalendar/main.min.js'></script>
        <script src="/assets/js/scripts/modal/components-modal.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>


        <!-- END: Theme JS-->
        <script>
            function G2J(D) {
                dd = D.toLocaleDateString().split('/');

                var a = (
                    jd_to_persian(
                        gregorian_to_jd(
                            dd[2],
                            dd[0],
                            parseInt(dd[1])
                        )
                    )
                );
                return (a[0] + '/' + a[1] + '/' + a[2]);
            }

            function change_date(D) {
                dd = D.split('/');
                var a = (
                    jd_to_gregorian(
                        persian_to_jd(
                            parseInt(dd[0]),
                            parseInt(dd[1]),
                            parseInt(dd[2])
                        )
                    )
                );
                calendar.gotoDate(new Date(a[0] + '-' + a[1] + '-' + a[2]));
            }
            $('#date_sw').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });

            document.addEventListener('DOMContentLoaded', function() {

                var calendarEl = document.getElementById('calendar_tag');

                calendar = new FullCalendar.Calendar(calendarEl, {
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                    initialView: 'resourceTimeGridDay',
                    direction: 'rtl',
                    allDaySlot: false,
                    editable: true,
                    selectable: true,
                    dayMaxEvents: true, // allow "more" link when too many events
                    dayMinWidth: 200,
                    slotMinTime: '08:00:00',
                    slotMaxTime: '23:00:00',
                    slotDuration: '00:15:00',
                    height: 'auto',
                    buttonText: {
                        today: 'امروز',
                        month: 'ماهانه',
                        week: 'هفتگی',
                        day: 'روزانه',
                        list: 'لیست'
                    },
                    locale: 'fa',
                    headerToolbar: {
                        right: 'prev,today,next',
                        center: 'title',
                        left: 'resourceTimeGridDay,resourceTimelineWeek,resourceTimelineMonth,listWeek'
                    },
                    views: {
                        resourceTimeGridDay: { // name of view
                            titleFormat: {
                                month: 'long',
                                weekday: 'long',
                                year: 'numeric',
                                day: '2-digit',
                            }
                            // other view-specific options here
                        }
                    },
                    datesSet: function(a, b) {
                        a = $('.fc-toolbar-title').text();

                        $('#date_sw').val(G2J(calendar.getDate()));

                    },
                    //// uncomment this line to hide the all-day slot
                    //allDaySlot: false,

                    resources: {!! $rooms !!},
                    resourceOrder: 'order',
                    events: {
                        url: "{{ route('session.session_getevent_deleted') }}",
                        error: function() {
                            $('#script-warning').show();
                        }
                    },

                    select: function(arg, b) {

                        var data = {
                            customer_id: null,
                            advisor_id: null,
                            price_list_id: null,
                            date: arg.startStr.split('T')[0],
                            session_start: arg.startStr.split('T')[1].split('+')[0],
                            room_id: arg.resource ? arg.resource.id : '(no resource)',
                            date2: G2J(arg.start)
                        };

                        all_event = calendar.getEvents();

                        all_event.forEach(function(i, b) {
                            if (
                                arg.startStr >= i.startStr &&
                                arg.endStr <= i.endStr &&
                                arg.resource.id == i._def.resourceIds[0]
                            ) {
                                data.advisor_id = i._def.extendedProps.advisor_id;
                                data.product_id = i._def.defId;
                            }
                        });
                        price_list_id = 0;
                        open_form(data);

                    },
                    eventClick: function(e, b) {
                        console.log(e.jsEvent.target.tagName);
                        if (e.jsEvent.target.tagName != 'DIV') return;
                        var data = JSON.parse(JSON.stringify(e.event._def.extendedProps));
                        data.date = e.event.startStr.split('T')[0];
                        data.session_start = e.event.startStr.substr(11, 5);
                        data.date2 = G2J(e.event.start);
                        data.sale_id = e.event.id;
                        data.session_length = (((e.event.end - e.event.start)) / 60000);
                        data.room_id = e.event._def.resourceIds[0]

                        if (data.transactions)
                            data.amount = data.transactions.amount;

                        price_list_id = data.price_list_id;
                        open_form(data);
                    },
                    dragRevertDuration: 0,
                    eventDrop: function(info) {
                        data = {
                            _token: $("input[name='_token']").val(),
                            id: info.event._def.publicId,
                            date: info.event.startStr,
                            room_id: info.event._def.resourceIds[0]
                        };
                        $.post(
                            "{{ route('sale.change_session_date') }}", data,
                            function(data, status) {
                                console.log("Data: " + data + "\nStatus: " + status);
                            });
                    },

                    eventResize: function(info) {
                        data = {
                            _token: $("input[name='_token']").val(),
                            id: info.event._def.publicId,
                            session_length: (((info.event.end - info.event.start)) / 60000),
                        };

                        $.post(
                            "{{ route('sale.change_session_length') }}", data,
                            function(data, status) {
                                console.log("Data: " + data + "\nStatus: " + status);
                            });
                    },

                    eventContent: function(a, b, c) {
                        //console.log(a);
                        Props = a.event._def.extendedProps;
                        mablagh = '1';
                        if (Props.transactions) {
                            mablagh = Props.transactions.amount + ' / ' + Props.transactions
                                .amount_standing;
                        }
                        let o = document.createElement('div');
                        o.innerHTML = '<div class="fc-event-time">' + a.timeText + '</div>' +
                            '<div class="fc-event-title-container"><div class="fc-event-title fc-sticky">' +
                            a.event.title +
                            '<br><a href="/admin/Financial/user/' + Props.customer_id +
                            '/invoise?sale=' + a.event._def.publicId +
                            '" ><button type="button" title="' + mablagh +
                            '" class="btn btn-icon rounded-circle btn-success"><i class="bx bx-dollar"></i></button></a><a href="/admin/product/session/' +
                            a.event._def.publicId +
                            '/print" ><button type="button" class="btn btn-icon rounded-circle btn-success"><i class="bx bx-detail"></i></button></a><a href="/admin/product/session/' +
                            a.event._def.publicId +
                            '/send_sms_to_advisor" ><button type="button" class="btn btn-icon rounded-circle btn-success"><i class="bx bx-detail"></i></button></a>' +
                            '<button type="button" class="btn btn-icon rounded-circle btn-warning" time="' +
                            a.timeText.split('- ')[1] +
                            '"><i class="bx bxs-bell-ring"></i></button><button onclick="sale_id2.value=' +
                            a.event._def.publicId +
                            '" type="button" class="btn btn-icon rounded-circle btn-danger" data-toggle="modal" data-target="#cancel_session_modal"><i class="bx bx-x"></i></button></a></div></div>';

                        let arrayOfDomNodes = [o];
                        if (a.event._def.ui.display != "background") {
                            return {
                                domNodes: arrayOfDomNodes
                            }
                        }
                    },

                    dateClick2: function(arg) {
                        //console.log(arg, 'dateClick');
                        /*console.log(
                          'dateClick',
                          arg.date,
                          arg.resource ? arg.resource.id : '(no resource)'
                        );*/
                    }
                });
                calendar.render();

                const params = new URLSearchParams(window.location.search);
                if (params.has('date')) {
                    calendar.gotoDate(new Date(params.get('date')));
                }

            });
        </script>
        <script>
            price_list = {!! $price_lists !!};
            price_lists = {};
            price_list_id = 0;


            function open_form(data) {
                $('#price_list_id').find('option').remove().end();
                $('#form')[0].reset();
                $('#submit_form').attr('onclick', 'store_form()');

                $('#customer_id').html('');
                $('#form').loadJSON(data);
                if (data.sale_id > 0) {
                    $('#submit_form').attr('onclick', 'update_form()');


                    var data = {
                        id: data.customer_id,
                        text: data.customer.name + ' ' + data.customer.family + ' «' + data.customer.mobile + '»'
                    };
                    var newOption = new Option(data.text, data.id, false, false);
                    $('#customer_id').append(newOption).trigger('change');
                }

                $('#edit_jalase').modal({
                    show: true
                });

            }

            function store_form() {
                console.log('a');

                data = {
                    _token: '{{ csrf_token() }}',
                    advisor_id: $('#advisor_id').val(),
                    customer_id: $('#customer_id').val(),
                    date: $('#date').val(),
                    session_start: $('#session_start').val(),
                    session_length: $('#session_length').val(),
                    session_type_id: $('#session_type_id').val(),
                    category_id: $('#category_id').val(),
                    room_id: $('#room_id').val(),
                    product_id: $('#product_id').val(),
                    price_list_id: $('#price_list_id').val(),
                    session_length: $('#session_length').val(),
                };

                $.post(
                    "{{ route('sale.calender_store') }}", data,
                    function(data, status) {
                        $(function() {
                            $('#edit_jalase').modal('toggle');
                        });
                        calendar.refetchEvents()
                    });
            }


            function update_form() {
                console.log('update_form');
                data = {
                    _token: '{{ csrf_token() }}',
                    advisor_id: $('#advisor_id').val(),
                    sale_id: $('#sale_id').val(),
                    customer_id: $('#customer_id').val(),
                    date: $('#date').val(),
                    session_start: $('#session_start').val(),
                    session_length: $('#session_length').val(),
                    session_type_id: $('#session_type_id').val(),
                    category_id: $('#category_id').val(),
                    room_id: $('#room_id').val(),
                    product_id: $('#product_id').val(),
                    price_list_id: $('#price_list_id').val(),
                    session_length: $('#session_length').val(),
                };

                $.post(
                    "{{ route('sale.calender_sale_update') }}", data,
                    function(data, status) {
                        $(function() {
                            $('#edit_jalase').modal('toggle');
                        });
                        calendar.refetchEvents()
                    });
            }

            function cancel_form() {

                data = {
                    _token: '{{ csrf_token() }}',
                    sale_id: $('#sale_id2').val(),
                    delete_type: $('input[name=delete_type]:checked').val(),
                    comment: $('[name=comment]').val()
                };

                $.post(
                    "{{ route('sale.calender_sale_delete') }}", data,
                    function(data, status) {
                        $(function() {
                            $('#cancel_session_modal').modal('toggle');
                        });
                        calendar.refetchEvents()
                    });

            }

            function set_price_list() {
                o = price_lists[$('#price_list_id').val()];
                console.log(o)
                if ($('#sale_id').val() == '') {
                    $('#session_length').val(
                        o.session_lenght
                    )
                }
                set_amount();
            }

            function set_amount() {
                o = price_lists[$('#price_list_id').val()];
                $('#amount').val(
                    Math.round(
                        o.total_price * $('#session_length').val() / o.session_lenght / 1000
                    ) * 1000
                );
            }



            function change_advisor() {
                if ($('#advisor_id').val() == '') {

                    return false;
                }
                $.ajax({
                    dataType: "json",
                    url: "/admin/user/advisor/" + $('#advisor_id').val() + "/price_list",
                    data: false,
                    success: function(data, status) {

                        $('#price_list_id').find('option').remove().end();

                        $.each(data, function(i, o) {
                            $('#price_list_id').append(new Option(o.text, o.id));
                        });

                        $('#price_list_id').val(price_list_id);

                        price_list = data;
                        $.each(price_list, function(i, o) {
                            price_lists[o.id] = o;
                        });
                        set_price_list();
                    }
                });
            }
            $('input,select').each(function(i, o) {
                o.oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity(e.target.attributes.validitymsg.value);
                    }
                };
                o.oninput = function(e) {
                    e.target.setCustomValidity("");
                };
            });

            $('#customer_id').select2({
                ajax: {
                    url: '{{ route('customer.get_all_ajax') }}',
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });


            (function($) {
                $.fn.loadJSON = function(options) {
                    form = this;
                    $.each(options, function(i, value, n) {
                        element = $(form).find("[name='" + i + "']")[0];
                        if (element == null)
                            return;
                        var type = element.type || element.tagName;

                        if (type == null)
                            return;
                        type = type.toLowerCase();
                        switch (type) {
                            case 'radio':
                                if (value.toString().toLowerCase() == element.value.toLowerCase())
                                    $(element).attr("checked", "checked");
                                break;
                            case 'checkbox':
                                if (value)
                                    $(element).attr("checked", "checked");
                                break;
                            case 'select-multiple':
                                var values = value.constructor == Array ? value : [value];
                                for (var i = 0; i < element.options.length; i++) {
                                    for (var j = 0; j < values.length; j++) {
                                        element.options[i].selected |= element.options[i].value ==
                                            values[j];
                                    }
                                }
                                break;
                            case 'select-one':
                                $(element).val(value);
                                if ($(element).hasClass('select2'))
                                    $(element).select2().trigger('change');
                                break;
                            default:
                                //element.value = value;
                                //$(element).attr("value", value);
                                $(element).val(value);
                                break;
                        }
                    });
                }
            })(jQuery);
        </script>
        <style>
            td.fc-resource {
                height: .5em;
            }

            .fc-scroller {
                margin-bottom: 0px !important;
            }

            .fc-title,
            .fc-event-title {
                color: red;
            }

            .fc-event-title {
                color: red;
                position: absolute;
                top: -25px;
            }

            .fc .fc-bg-event {
                opacity: .7;
                opacity: var(--fc-bg-event-opacity, .7);
            }

            select.select2 {
                width: 100% !important;
            }

            .blink_me {
                animation: blinker 3s linear infinite;
            }

            @keyframes blinker {
                50% {
                    opacity: 0;
                }
            }

            .btn.btn-icon {
                padding: .2rem .4rem;
                margin: 0px 2px;
            }

        </style>

    </x-slot>
</x-base>
