<x-base title="@t(پیامک)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(ارسال پیامک)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">@t(ارسال پیامک)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="faq-search">
        <div class="row">
            <div class="col-12">
                <div class="card background-color">
                    <div class="card-header">
                    </div>
                    <div class="card-content">
                        <div class="card-body p-0">
                            <h1 class="faq-title text-center mb-3">@t(ارسال پیامک برای)
                                {{ $user->name . ' ' . $user->family }}</h1>
                            <form method="POST" action="{{ route('sms.send_sms') }}" id="sms_form">
                                @csrf
                                <input type="hidden" name="back_url" value="0" id="back_url">
                                <fieldset class="faq-search-width form-group position-relative w-50 mx-auto">

                                    <select name="customers" id="customers"
                                        class="form-control round form-control-lg shadow pl-2">
                                        <option value="">انتخاب مراجع</option>

                                    </select>
                                </fieldset>
                                <fieldset class="faq-search-width form-group position-relative w-50 mx-auto">

                                    <select name="sms_list" id="sms_list"
                                        class="form-control round form-control-lg shadow pl-2"
                                        onchange="change_to_text()">
                                        <option value="">@t(پیامک پیش فرض را انتخاب کنید...)</option>
                                        @foreach ($smses as $sms)
                                            <option value="{{ $sms->value }}">{{ $sms->value }}</option>
                                        @endforeach
                                    </select>
                                </fieldset>
                                <fieldset class="faq-search-width form-group position-relative w-75 mx-auto">

                                    <input type="text" class="form-control round form-control-lg shadow pl-2"
                                        id="searchbar" name="message" placeholder="@t(پیامک دلخواه خود را بنویسید...)">
                                    <button class="btn btn-primary round position-absolute d-none d-sm-block" type="button"
                                        data-toggle="modal" data-target="#_{{ $user->id }}">
                                        @t(ارسال پیامک)
                                    </button>
                                    <button class="btn btn-primary round position-absolute d-block d-sm-none"
                                        type="button">
                                        <i class="bx bx-search"></i>
                                    </button>
                                    <div class="modal fade" id="_{{ $user->id }}" tabindex="-1" user="dialog"
                                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" user="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    @t('آیا از ارسال پیامک اطمینان دارید؟');
                                                </div>
                                                <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger"
                                                            data-dismiss="modal">خیر</button>
                                                        <button type="submit" class="btn btn-success" onclick="sms_form.submit()">بله</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
        <script>
            
            if (getCookie('old_location') != '') {
                $('#back_url').val(getCookie('old_location'));
            }
            function change_to_text() {
                $('#searchbar').val($('#sms_list').val());
            }
            $('#customers').select2({
                ajax: {
                    url: '{{ route('customer.get_all_ajax') }}',
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });

            $(document).ready(function() {
                var data = {
                    id: {{ $user->id }},
                    text: '{{ $user->name }} {{ $user->family }} «{{ $user->mobile }}»'
                };
                var newOption = new Option(data.text, data.id, true, true);
                $('#customers').append(newOption).trigger('change');
                // $('#cusomters').val({{ $user->id }});
            });
        </script>

    </x-slot>
</x-base>
