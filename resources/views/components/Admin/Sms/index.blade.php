<x-base title="@t(پیامک)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">@t(ارسال پیامک)</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active">@t(ارسال پیامک)
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section class="faq-search">
        <div class="row">
            <div class="col-12">
            <div class="card background-color">
                <div class="card-header">
                </div>
                <div class="card-content">
                    <div class="card-body p-0">
                        <h1 class="faq-title text-center mb-3">@t(ارسال پیامک برای....)</h1>
                        <form>
                            <fieldset class="faq-search-width form-group position-relative w-50 mx-auto">

                                <select name="cars" id="cars" class="form-control round form-control-lg shadow pl-2">
                                    <option value="">انتخاب مراجع</option>
                                  
                                  </select>
                            </fieldset>
                        <fieldset class="faq-search-width form-group position-relative w-50 mx-auto">

                            <select name="sms_list" id="sms_list" class="form-control round form-control-lg shadow pl-2">
                                <option value="">@t(پیامک پیش فرض را انتخاب کنید...)</option>
                                @foreach ($sms as $sms)
                                <option value="{{ $sms->value }}">{{ $sms->value }}</option>
                                @endforeach
                            </select>
                        </fieldset>
                        <fieldset class="faq-search-width form-group position-relative w-75 mx-auto">
                            
                            <input type="text" class="form-control round form-control-lg shadow pl-2" id="searchbar" placeholder="@t(پیامک دلخواه خود را بنویسید...)">
                            <button class="btn btn-primary round position-absolute d-none d-sm-block" type="button">@t(ارسال پیامک)</button>
                            <button class="btn btn-primary round position-absolute d-block d-sm-none" type="button"><i class="bx bx-search"></i></button>
                        </fieldset>
                        </form>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->


    </x-slot>
</x-base>
