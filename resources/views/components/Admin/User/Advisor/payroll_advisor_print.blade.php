<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">

    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        @page {
            size: A4;
            margin: 0;
        }

    </style>
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
</head>

<body style="background-image:none;">        
    <div class="container p-0">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3">
                                <div class="row text-center">
                                    <div class="ml-2">
                                        @php
                                            $avatar = $advisor->pic ?? 'avatar.jpg';
                                            $gender = 'جناب آقای';
                                            if ($advisor->gender == 'زن')
                                                {
                                                    $avatar = $advisor->pic ?? 'avatar_woman.jpg';
                                                    $gender = 'سرکار خانم';
                                                };

                                            $education_level = '';
                                            
                                            if ($advisor->education_level_id == '4')
                                                {
                                                    $education_level = 'دکتر';
                                                };
                                        @endphp
                                       
                                        <img src="/assets/images/profile/advisor/avatar/{{ $avatar }}" width="120px"  alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                                        <p>{{ $advisor->name.' '.$advisor->family }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 text-center">
                                <p>
                                    <h3>@t(فیش حقوقی){{ ' '.$gender.' '.$education_level.' '.$advisor->name.' '.$advisor->family }}</h3>
                                    <span class="font-medium-3">@t(از تاریخ:){{ ' '.$payroll->start_date }} تا {{ $payroll->end_date }}</span>
                                </p>
                            </div>
                            <div class="col-3">
                                <div class="row text-center float-right">
                                    <div class="mr-2">
                                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table-bordered table-hover text-secondary" width='100%'>
                            <thead>
                                <tr class=" text-center">
                                    <th>ردیف</th>
                                    <th>مشاور</th>
                                    {{-- <th>تاریخ رزرو</th> --}}
                                    <th>تاریخ برگزاری جلسه</th>
                                    <th>نام مراجع</th>
                                    <th class="text-center">مدت جلسه</th>
                                    <th>نرخ نامه</th>
                                    <th>درصد مشاور</th>
                                    <th>مبلغ جلسه</th>
                                    <th>باقیمانده</th>
                                    <th>وضعیت</th>
                                    <th>سهم مشاور</th>
                                    <th>باقیمانده مشاور</th>
                                    <th></th>
                                </tr>
                            </thead>
                            @php
                                $advisor_payroll = $remaining_advisor_payroll = 0;
                            @endphp
                            <tbody>
                                @foreach ($sessions as $key => $session)
                                    <tr>
                                        <td data-toggle="tooltip" data-placement="top" title="{{ $session->id }}">{{ $key + 1 }}</td>
                                        <td class="pr-1">{{ ($session->advisor->name ?? '') . ' ' . ($session->advisor->family ?? '') }}</td>
                                        {{-- <td style="direction: ltr"><small>{{ verta($session->created_at)->format('Y/m/d H:i') }}</small></td> --}}
                                        <td style="direction: ltr" data-toggle="tooltip" data-placement="top" title="{{ 'تاریخ رزرو:' . ' ' . verta($session->created_at)->format('H:i Y/m/d') }}">
                                            <span class="pl-1">{{ verta($session->date)->format('Y/m/d') . ' ' }}<small class="pr-1">{{ $session->session_start }}</small></span>
                                        </td>
                                        <td>
                                            <span>
                                                {{ ($session->customer->name ?? '') . ' ' . ($session->customer->family ?? '') }}
                                            </span>
                                        </td>
                                        <td class="text-center">{{ $session->session_length ?? '' }}</td>
                                        <td class="text-center">{{ ($session->price_list->total_price??1) ?? '' }}</td>
                                        <td class="text-center"><span>{{ ($session->price_list->advisor_rate??1) * 100 / ($session->price_list->total_price??1).'%' }}</span></td>
                                        <td class="text-center"><span class="currency">{{ $session->total_price ?? ''}}</span></td>
                                        <td class="text-center"><span class="currency">{{ $session->transactions->amount_standing * -1 }}</span></td>
                                        <td>
                                            @if ($session->session_status_id == 1)
                                                <span class="badge badge-pill badge-danger">باز</span>
                                            @else
                                                {{-- <span class="badge badge-pill badge-success">بسته</span> --}}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <span class="currency">
                                                {{ $tmp = ($session->price_list->advisor_rate??1) * 100 / ($session->price_list->total_price??1) * $session->total_price / 100 }}
                                                @php
                                                     $advisor_payroll = $advisor_payroll + $tmp;
                                                @endphp
                                            </span>
                                        </td>                                        
                                        <td class="text-center">
                                            <span class="currency">
                                                {{ $remaining_tmp = ($session->price_list->advisor_rate??1) * 100 / ($session->price_list->total_price??1) * $session->transactions->amount_standing / 100 * -1 }}
                                                @php
                                                     $remaining_advisor_payroll = $remaining_advisor_payroll + $remaining_tmp;
                                                @endphp
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            @if ( isset($session->comment) )
                                                <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $session->comment }}"></i>
                                            @endif
                                        </td>
                                    </tr>                                    
                                @endforeach            
                                    <tr class="text-bold-500">
                                        <td></td>
                                        <td>مجموع</td>
                                        <td class="text-center"></td>                                    
                                        <td></td>
                                        <td class="text-center">{{ $sessions->sum('session_length') }}</td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center"><span class="currency">{{ $sessions->sum('total_price') }}</span></td>                                    
                                        <td class="text-center"><span class="currency">{{ $sessions->sum('transactions.amount_standing') * -1 }}</span></td>
                                        <td></td>
                                        <td class="text-center"><span class="currency">{{ $advisor_payroll }}</span></td>
                                        <td class="text-center"><span class="currency">{{ $remaining_advisor_payroll }}</span></td>
                                        <td></td>
                                    </tr>
                                    <tr class="text-bold-600">
                                        <td></td>
                                        <td colspan="2">مبلغ پرداختی به مشاور</td>
                                        <td class="text-center">
                                            <span class="currency">{{ $advisor_payroll - $remaining_advisor_payroll }}</span>
                                            <span class="pr-1">تومان</span>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{ sentence() }}
        
        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>


<!-- BEGIN: Theme JS-->
<script src="/assets/js/core/app-menu.js"></script>
<script src="/assets/js/core/app.js"></script>
<!-- END: Theme JS-->
<script>
    $('.currency').each(function(i, o) {
        $(o).html(numeral($(o).text()).format('0,0'));
    })
</script>

</html>


