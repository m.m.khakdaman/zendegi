<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/charts/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/charts/apexcharts.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    {{-- <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css"> --}}
    <link rel="stylesheet" type="text/css" href="/assets/css/pages/chart-chartist.css">
    <!-- END: Page CSS-->

    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">    
    <div class="container">
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-2">
                <p>
                    <b>بسمه تعالی</b>
                </p>
                <p>
                    <h4 class="card-title">@t(گزارش عملکرد){{ ' '.$advisor->name.' '.$advisor->family }}</h4>
                    <span>@t(از تاریخ)</span>&nbsp;<span>{{ $from }}</span>&nbsp;<span>@t(تا تاریخ)</span>&nbsp;<span>{{ $to }}</span>
                </p>
            </div>
            <div class="col-4">

                <div class="row text-center float-right">
                    <div class="mr-4 mt-2">
                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (!$successful_session)
        <div class="container">        
            <div class="card" style="height: 500px">
                <div class="card-body text-center">
                    <br>
                    <h3>
                        <b><i>{{ $advisor->name ?? ''}}&nbsp;{{ $advisor->family ?? ''}}</i></b>
                        <span>@t(در این بازه زمانی کارکردی نداشته است.)</span>
                    </h3>
                </div>
            </div>
            {{ sentence() }}
            <br>
            <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
        </div>
    @else
    @php
        $start = verta()->month(1)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(1)->endMonth()->formatGregorian('Y-m-d');
        $far_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $farvardin = 0;
        if ($far_sess->count())
        {
            $farvardin = $far_sess->count()/$far_sess->groupBy('customer_id')->count();
        };
        

        $start = verta()->month(2)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(2)->endMonth()->formatGregorian('Y-m-d');
        $ordi_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $ordibehesht = 0;
        if ($ordi_sess->count()) 
        {
            $ordibehesht = $ordi_sess->count()/$ordi_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(3)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(3)->endMonth()->formatGregorian('Y-m-d');
        $khor_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $khordad = 0;
        if ($khor_sess->count()) 
        {
            $khordad = $khor_sess->count()/$khor_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(4)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(4)->endMonth()->formatGregorian('Y-m-d');
        $tir_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $tir = 0;
        if ($tir_sess->count()) 
        {
            $tir = $tir_sess->count()/$tir_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(5)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(5)->endMonth()->formatGregorian('Y-m-d');
        $mor_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $mordad = 0;
        if ($mor_sess->count()) 
        {
            $mordad = $mor_sess->count()/$mor_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(6)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(6)->endMonth()->formatGregorian('Y-m-d');
        $sher_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $shahrivar = 0;
        if ($sher_sess->count()) 
        {
            $shahrivar = $sher_sess->count()/$sher_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(7)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(7)->endMonth()->formatGregorian('Y-m-d');
        $mehr_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $mehr = 0;
        if ($mehr_sess->count()) 
        {
            $mehrdad = $mehr_sess->count()/$mehr_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(8)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(8)->endMonth()->formatGregorian('Y-m-d');
        $aban_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $aban = 0;
        if ($aban_sess->count()) 
        {
            $aban = $aban_sess->count()/$aban_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(9)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(9)->endMonth()->formatGregorian('Y-m-d');
        $azar_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $azar = 0;
        if ($azar_sess->count()) 
        {
            $azar = $azar_sess->count()/$azar_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(10)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(10)->endMonth()->formatGregorian('Y-m-d');
        $dey_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $dey = 0;
        if ($dey_sess->count()) 
        {
            $dey = $dey_sess->count()/$dey_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(11)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(11)->endMonth()->formatGregorian('Y-m-d');
        $bahman_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $bahman = 0;
        if ($bahman_sess->count()) 
        {
            $bahman = $bahman_sess->count()/$bahman_sess->groupBy('customer_id')->count();
        };

        $start = verta()->month(12)->startMonth()->formatGregorian('Y-m-d');
        $end = verta()->month(12)->endMonth()->formatGregorian('Y-m-d');
        $esfand_sess = $all_sessions->where('date', '>=', $start)->where('date', '<=', $end);
        $esfand = 0;
        if ($esfand_sess->count()) 
        {
            $esfand = $esfand_sess->count()/$esfand_sess->groupBy('customer_id')->count();
        };
    @endphp
    <div class="container">        
        <div class="card">
            <div class="card-body">                            
                <div class="row">
                    <div class="col-8">
                        <h4 class="card-title">
                            @t(گزارش عملکرد){{ ' '.$advisor->name.' '.$advisor->family }}
                            <span>@t(از تاریخ)</span>&nbsp;<span>{{ $from }}</span>&nbsp;<span>@t(تا تاریخ)</span>&nbsp;<span>{{ $to }}</span>
                        </h4>
                            @php    
                                $customers_tmp = $customers->groupBy('customer_id');
                                $tmp = 1;
                            @endphp
                            <table class="table-hover table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>@t(عنوان)</th>
                                        <th class="text-center">@t(تعداد)</th>
                                        <th class="text-center">@t(جلسه)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>@t(تعداد جلسات :)</th>
                                        <td class="text-center">{{ $session.' ' }}</td>
                                        <td class="text-center">@t(جلسه)</td>
                                    </tr>
                                    <tr>
                                        <th>@t(تعداد جلسات موفق:)</th>
                                        <td class="text-center">{{ $successful_session.' ' }}</td>
                                        <td class="text-center">@t(جلسه)</td>
                                    </tr>
                                    <tr>
                                        <th>@t(تعداد جلسات آینده:)</th>
                                        <td class="text-center">{{ $waiting_session.' ' }}</td>
                                        <td class="text-center">@t(جلسه)</td>
                                    </tr>
                                    <tr>
                                        <th>@t(نرخ نگهداری:)</th>
                                        <td class="text-center">{{ $successful_session/$customers_tmp->count().' ' }}</td>
                                        <td class="text-center">@t(جلسه)</td>
                                    </tr>
                                    <tr>
                                        <th>@t(تعداد جلسات لغو شده با عودت وجه:)</th>
                                        <td class="text-center">{{ $cancel_payback_session.' ' }}</td>
                                        <td class="text-center">@t(جلسه)</td>
                                    </tr>
                                    <tr>
                                        <th>@t(تعداد جلسات لغو شده بدون عودت وجه:)</th>
                                        <td class="text-center">{{ $cancel_no_payback_session }}</td>
                                        <td class="text-center">@t(جلسه)</td>
                                    </tr>
                                </tbody>
                            </table>
                            {{-- فروردین:{{ $farvardin }}<br>
                            {{ $ordibehesht }}<br>
                            {{ $khordad }}<br>
                            {{ $tir }}<br>
                            {{ $mordad }}<br>
                            {{ $shahrivar }}<br>
                            {{ $mehr }}<br>
                            {{ $aban }}<br>
                            {{ $azar }}<br>
                            {{ $dey }}<br>
                            {{ $bahman }}<br>
                            {{ $esfand }}<br> --}}
                    </div>
                    <div class="col-4">
                        <h4 class="card-title">@t(لیست مراجعین این مشاور)</h4>
                            <table class="table-bordered table-hover">
                                <tbody>                                    
                                    <tr>
                                        <th>@t(ردیف)</th>
                                        <th>@t(نام و نام خانوادگی)</th>
                                        <th>@t(شماره همراه)</th>
                                        <th>@t(تعداد جلسات)</th>
                                    </tr>
                                    @foreach ($customers_tmp as $item => $customers)
                                        <tr>
                                            <td>{{ $tmp++ }}</td> 
                                            <td>
                                                <span data-toggle = "tooltip" data-placement="top" title="{{ $item }}">
                                                    {{ \App\Models\user::find($item)->name?? '' }}&nbsp;{{ \App\Models\user::find($item)->family ?? '' }}
                                                </span>
                                            </td>
                                            <td>
                                                <span data-toggle = "tooltip" data-placement="top" title="{{ $item }}">
                                                    {{ \App\Models\user::find($item)->mobile ?? '' }} 
                                                </span>
                                            </td>
                                            <td class="text-center">
                                                @php
                                                    $counter = 0;
                                                @endphp
                                                @foreach ($customers as $item2)
                                                    {{-- @if ($item == $item2->customer_id) --}}
                                                        @php
                                                            ++$counter;
                                                        @endphp
                                                    {{-- @endif --}}
                                                @endforeach
                                                {{ $counter }}
                                            </td>                                                    
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-12">
            <div class="card p-0">
                <div class="card-header">
                <h6 class="card-title">برچسب های چند خطی</h6>
                </div>
                <div class="card-content">
                <div class="card-body">
                    <div class="multi-line-chart ct-golden-section"></div>
                </div>
                </div>
            </div> 
        </div> --}}
          <!-- Column Chart -->
    <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">@t(نمودار ستونی نرخ نگهداری در سال جاری)</h4>
          </div>
          <div class="card-content">
            <div class="card-body">
              <div id="column-chart"></div>
            </div>
          </div>
        </div>
      </div>
        {{ sentence() }}
        <br>
        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>     
    @endif
       
</body>
<!-- BEGIN: Vendor JS-->
<script src="/assets/vendors/js/vendors.min.js"></script>
<script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
<script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->
<!-- BEGIN: Page Vendor JS-->
<script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="/assets/vendors/js/charts/chartist.min.js"></script>
<script src="/assets/vendors/js/charts/apexcharts.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
<script src="/assets/js/core/app-menu.js"></script>
<script src="/assets/js/core/app.js"></script>
<script src="/assets/js/scripts/components.js"></script>
<script src="/assets/js/scripts/footer.js"></script>
<script src="/assets/js/scripts/customizer.js"></script>
<!-- END: Theme JS-->

<script>
    // window.print();
</script>
{{-- <script>
    //Multi-line bar chart
	new Chartist.Bar('.multi-line-chart', {
		labels: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
		series: [
			[5, 4, 3, 7, 5, 10, 3, 4, 8, 10, 6, 8],
			[3, 2, 9, 5, 4, 6, 4, 6, 7, 8, 7, 4],
			[2, 1, 7, 3, 2, 4, 2, 3, 4, 3, 5, 1]
		]
	}, {
		seriesBarDistance: 10,
		axisX: {
			offset: 60
		},
		axisY: {
			offset: 80,
			labelInterpolationFnc: function (value) {
				return value
			},
			scaleMinSpace: 15
		}
	});
</script> --}}
{{-- <script>
    $(document).ready(function () {

    var $primary = '#5A8DEE',
        $success = '#39DA8A',
        $danger = '#FF5B5C',
        $warning = '#FDAC41',
        $info = '#00CFDD',
        $label_color = '#373D3F';
        $label_color_light = '#E6EAEE';

    var themeColors = [$primary, $warning, $danger, $success, $info];

    Apex.chart = {
        fontFamily: 'inherit',
        locales: [{
            "name": "fa",
            "options": {
                "months": ["ژانویه", "فوریه", "مارس", "آوریل", "می", "ژوئن", "جولای", "آگوست", "سپتامبر", "اکتبر", "نوامبر", "دسامبر"],
                "shortMonths": ["ژانویه", "فوریه", "مارس", "آوریل", "می", "ژوئن", "جولای", "آگوست", "سپتامبر", "اکتبر", "نوامبر", "دسامبر"],
                "days": ["یکشنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"],
                "shortDays": ["ی", "د", "س", "چ", "پ", "ج", "ش"],
                "toolbar": {
                    "exportToSVG": "دریافت SVG",
                    "exportToPNG": "دریافت PNG",
                    "menu": "فهرست",
                    "selection": "انتخاب",
                    "selectionZoom": "بزرگنمایی قسمت انتخاب شده",
                    "zoomIn": "بزرگ نمایی",
                    "zoomOut": "کوچک نمایی",
                    "pan": "جا به جایی",
                    "reset": "بازنشانی بزرگ نمایی"
                }
            }
        }],
        defaultLocale: "fa"
    }
    // Column Chart
	// ----------------------------------
	var columnChartOptions = {
		chart: {
			height: 350,
			type: 'bar',
		},
		colors: themeColors,
		plotOptions: {
			bar: {
				horizontal: false,
				endingShape: 'rounded',
				columnWidth: '55%',
			},
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		series: [{
			name: 'سود خالص',
			data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
		}, {
			name: 'درآمد',
			data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
		}, {
			name: 'جریان نقدینگی',
			data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
		}],
		legend: {
			offsetY: -10
		},
		xaxis: {
			categories: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر'],
		},
		yaxis: {
			title: {
				text: '(درآمد (هزار تومان'
			}
		},
		fill: {
			opacity: 1

		},
		tooltip: {
			y: {
				formatter: function (val) {
					return val + " هزار تومان"
				}
			}
		}
	}
	var columnChart = new ApexCharts(
		document.querySelector("#column-chart"),
		columnChartOptions
	);

	columnChart.render();
</script> --}}
<script>  
    $(document).ready(function () {
        var $primary = '#5A8DEE',
            $success = '#39DA8A',
            $danger = '#FF5B5C',
            $warning = '#FDAC41',
            $info = '#00CFDD',
            $label_color = '#373D3F';
            $label_color_light = '#E6EAEE';

        var themeColors = [$danger, $warning, $primary, $success, $info];

        Apex.chart = {
            fontFamily: 'inherit',
            locales: [{
                "name": "fa",
                "options": {
                    "months": ["ژانویه", "فوریه", "مارس", "آوریل", "می", "ژوئن", "جولای", "آگوست", "سپتامبر", "اکتبر", "نوامبر", "دسامبر"],
                    "shortMonths": ["ژانویه", "فوریه", "مارس", "آوریل", "می", "ژوئن", "جولای", "آگوست", "سپتامبر", "اکتبر", "نوامبر", "دسامبر"],
                    "days": ["یکشنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"],
                    "shortDays": ["ی", "د", "س", "چ", "پ", "ج", "ش"],
                    "toolbar": {
                        "exportToSVG": "دریافت SVG",
                        "exportToPNG": "دریافت PNG",
                        "menu": "فهرست",
                        "selection": "انتخاب",
                        "selectionZoom": "بزرگنمایی قسمت انتخاب شده",
                        "zoomIn": "بزرگ نمایی",
                        "zoomOut": "کوچک نمایی",
                        "pan": "جا به جایی",
                        "reset": "بازنشانی بزرگ نمایی"
                    }
                }
            }],
            defaultLocale: "fa"
        }
        // Column Chart
        // ----------------------------------
        var columnChartOptions = {
            chart: {
                height: 500,
                type: 'bar',
            },
            colors: themeColors,
            plotOptions: {
                bar: {
                    horizontal: false,
                    endingShape: 'rounded',
                    columnWidth: '50%',
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 5,
                colors: ['transparent']
            },
            series: [{
                name: '@t(نرخ نگهداری)',
                data: [{{ $farvardin ??null}}, {{ $ordibehesht ??null}}, {{ $khordad ??null}}, {{ $tir ??null}}, {{ $mordad ??null}}, {{ $shahrivar ??null}}, {{ $mehr ??null}}, {{ $aban ??null}}, {{ $azar ??null}}, {{ $dey ??null}}, {{ $bahman ??null}}, {{ $esfand ??null}}]
            }, {
                // name: 'سود خالص',
                // data: [3, 2, 1, 3, 2, 4, 3, 1, 2]
            }, {
                // name: 'جریان نقدینگی',
                // data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
            }],
            legend: {
                offsetY: -10
            },
            xaxis: {
                categories: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
            },
            yaxis: {
                title: {
                    text: '@t(نفر)'
                }
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val + " نفر"
                    }
                }
            }
        }
        var columnChart = new ApexCharts(
            document.querySelector("#column-chart"),
            columnChartOptions
        );

        columnChart.render();
    });
</script>
</html>
