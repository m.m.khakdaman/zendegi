
<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <style>

        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }
    </style>
</head>

<body style="background-image:none;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center">
            <p>
                <b>بسمه تعالی</b>
            </p>
            <p>
                <b>جلسات مشاور {{ $advisor->name . ' ' . $advisor->family }} «{{ $advisor->mobile }}»</b>
            </p>
        </div>
        <div class="col-4">

            <div class="row text-center float-right">
                <div>
                    <img src="http://zendegiaghelane.sanamsoft.com/theme/images/zendegiaghelane.png" alt=""
                        class="img-responsive" />
                    <p>مرکز مشاوره زندگی عاقلانه</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 mt-1 mb-2">
                    <h4>جلسات</h4>
                    <hr>
                </div>
            </div>
            <table class="table table-bordered" width='100%'>
                <thead>
                    <tr>
                        <th>تاریخ</th>
                        <th>مراجع</th>
                        <th>ساعت شروع</th>
                        <th>مدت جلسه</th>
                        <th>اتاق</th>
                    </tr>
                </thead>
                <tbody>
                    
        @foreach ($sales as $sale)
        <tr>
            <td>{{ verta($sale->date)->format('Y/m/d') }}</td>
            <td>{{ $sale->customer->name . ' ' . $sale->customer->family }}</td>
            <td>{{ \Carbon\Carbon::parse($sale->session_start)->format('H:i') }}</td>
            <td>{{ $sale->session_length }}</td>
            <td>{{ $sale->rooms->name }}</td>
        </tr>
    @endforeach
                    {{-- @foreach ($session->transactions->distinctions as $key => $item)
                        <tr class="text-center">
                            <td class='td_center'>{{ $key + 1 }}</td>
                            <td dir="ltr">{{ $item->amount }}</td>
                            <td class="text-center">-</td>
                            <td dir="ltr" class="text-center">{{ $item->source->comment ?? '-' }}</td>
                            <td dir="ltr">{{ verta($item->created_at)->format('Y/m/d H:i') }}</td>
                        </tr>
                    @endforeach
                    <tr class="text-center">
                        <td class='td_center'>مجموع</td>
                        <td dir="ltr">
                            {{ $session->transactions->amount_standing - $session->transactions->amount }}
                        </td>
                        <td dir="ltr">
                            {{ -$session->transactions->amount_standing }}
                        </td>
                        <td class="text-center">-</td>
                        <td dir="ltr">{{ verta($session->transactions->created_at)->format('Y/m/d H:i') }}</td>
                    </tr> --}}
                </tbody>
            </table>
        </div>

    </div>
</body>

<script>
    // window.print();
</script>

</html>
