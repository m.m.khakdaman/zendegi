<x-base>
    <x-slot name='title'>
        جلسات مشاور {{ $advisor->name . ' ' . $advisor->family }}
    </x-slot>

    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
        <base href="/">
    </x-slot>
    <!-- BEGIN: Content-->
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(برنامه کاری) {{ $advisor->name . ' '. $advisor->family }}</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.index') }}">ليست مشاورين</a>
                            </li>
                            <li class="breadcrumb-item active">
                                @t(برنامه کاری) {{ $advisor->name . ' '. $advisor->family }}
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">

                        <!-- create invoice button-->
                        {{-- @can('advisor_create') --}}
                        <div class="invoice-create-btn mb-1">
                            <a href="{{ route('advisor.session.create', $advisor->id) }}"
                                class="btn btn-primary glow invoice-create" role="button" aria-pressed="true">ایجاد جلسه
                                جدید</a>

                            <a href="{{ route('advisor.session.create', [$advisor->id,'type'=>'not_persent']) }}"
                                class="btn btn-primary glow invoice-create" role="button" aria-pressed="true">ثبت روز های عدم حضور</a>
                        </div>
                        {{-- @endcan --}}
                        <div class="table-responsive ">
                            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>عنوان</th>
                                        {{-- <th>نام اتاق</th> --}}
                                        <th>تاریخ شروع</th>
                                        <th>تاریخ پایان</th>
                                        <th>تنظیمات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sessions as $session)
                                        <tr>
                                            <td>{{ $session->id }}</td>
                                            <td>{{ $session->product_name }}</td>
                                            {{-- <td>{{ $session->room->name . ' -- ' . $session->room->branch->name }}</td> --}}
                                            <td>{{ $session->start }}</td>
                                            <td>{{ $session->end }}</td>
                                            <td>
                                                {{-- @can('session.advisor.update') --}}
                                                <a href="{{ route('session.edit', $session->id) }}"
                                                    data-toggle="tooltip" data-placement="top" title="ویرایش"
                                                    class="btn btn-icon rounded-circle btn-light-warning mr-1 mb-1">
                                                    <i class="bx bx-edit"></i>
                                                </a>

                                                {{-- @endcan --}}
                                                {{-- @can('session.advisor.destroy') --}}
                                                <button class="btn btn-icon rounded-circle btn-light-danger mr-1 mb-1"
                                                    data-toggle="modal" data-target="#_{{ $session->id }}">
                                                    <i class="bx bxs-trash" data-toggle="tooltip" data-placement="top"
                                                        title="حذف"></i>
                                                </button>
                                                {{-- @endcan --}}
                                                <!-- Modal -->
                                                <div class="modal fade" id="_{{ $session->id }}" tabindex="-1"
                                                    user="dialog" aria-labelledby="exampleModalCenterTitle"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" user="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                آیا می خواهید -- {{ $session->name }} -- را حذف
                                                                نمایید
                                                                ؟
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form
                                                                    action="{{ route('session.destroy', $session->id) }}"
                                                                    method="post">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">خیر</button>
                                                                    <button type="submit"
                                                                        class="btn btn-success">بله</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>عنوان</th>
                                        {{-- <th>نام اتاق</th> --}}
                                        <th>حیطه</th>
                                        <th>مبلغ</th>
                                        <th>تنظیمات</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->

        <script>
            $(document).ready(function() {
                // Setup - add a text input to each footer cell
                $('.invoice-data-table tfoot th').each(function() {
                    var title = $(this).text();

                    if (title != 'مبلغ') {
                        if (title != 'تنظیمات') {
                            $(this).html('<input type="text" placeholder="جست جو در ' + title +
                                '"  class="form-control form-control-sm" />');
                        }
                    } else {
                        $(this).html('<input type="tel" placeholder="جست جو در ' + title +
                            '"  class="form-control form-control-sm" />');
                    }

                });

                // DataTable
                var table = $('.invoice-data-table').DataTable({
                    language: {
                        "sEmptyTable": "هیچ داده‌ای در جدول وجود ندارد",
                        "sInfo": "نمایش _START_ تا _END_ از _TOTAL_ ردیف",
                        "sInfoEmpty": "نمایش 0 تا 0 از 0 ردیف",
                        "sInfoFiltered": "(فیلتر شده از _MAX_ ردیف)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "نمایش _MENU_ ردیف",
                        "sLoadingRecords": "در حال بارگزاری...",
                        "sProcessing": "در حال پردازش...",
                        "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                        "oPaginate": {
                            "sFirst": "برگه‌ی نخست",
                            "sLast": "برگه‌ی آخر",
                            "sNext": "بعدی",
                            "sPrevious": "قبلی"
                        },
                        "oAria": {
                            "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                            "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                        },
                        "sSearch": "",
                        "sSearchPlaceholder": "جستجوی صورتحساب",
                    },
                    dom: '<"top d-flex flex-wrap"><"clear">rt<"bottom"p>',
                    initComplete: function() {
                        // Apply the search
                        this.api().columns().every(function() {
                            var that = this;

                            $('input', this.footer()).on('keyup change clear', function() {
                                if (that.search() !== this.value) {
                                    that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                        });
                    }
                });

            });

        </script>

    </x-slot>
</x-base>
