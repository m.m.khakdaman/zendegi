<x-base>
    <x-slot name='title'>
        @t(تعریف برنامه کاری جدید)
    </x-slot>
    <x-slot name='css'>


        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/file-uploaders/dropzone.css">
        <!-- END: Page CSS-->

        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(برنامه کاری جدید)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.index') }}">
                                    لیست مشاورین
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.session.index', $advisor->id) }}">لیست برنامه کاری مشاور
                                    {{ $advisor->name . ' ' . $advisor->family }}</a>
                            </li>
                            <li class="breadcrumb-item active">@t(برنامه کاری جدید)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(برنامه کاری جدید) مشاور {{ $advisor->name . ' ' . $advisor->family }}
                        </h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            @include('layouts.errors')
                            <form action="{{ route('advisor.session.store', $advisor) }}" method="POST"
                                class="wizard-horizontal" novalidate>
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="title" required name="title"
                                                    data-validation-required-message="پر کردن فیلد عنوان اجباری است."
                                                    value="{{ old('title') }}" placeholder='@t(عنوان)'>
                                                <label for="title">@t(عنوان)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control datepicer_date" name="start_date"
                                                    id="start_date"
                                                    pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                                    data-validation-pattern-message="فرمت فیلد معتبر نیست." required
                                                    value="{{ old('start_date') }}" placeholder="تاریخ شروع">
                                                <div class="form-control-position">
                                                    <i class="bx bx-calendar">
                                                    </i>
                                                </div>
                                                <label for="start_date">تاریخ شروع</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control datepicer_date" name="end_date"
                                                    {{-- data-validation-pattern-message="فرمت فیلد معتبر نیست." --}} value="{{ old('end_date') }}" id="end_date"
                                                    required placeholder="تاریخ پایان">
                                                <div class="form-control-position">
                                                    <i class="bx bx-calendar">
                                                    </i>
                                                </div>
                                                <label for="end_date">تاریخ پایان</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 contact-repeater">
                                        <div data-repeater-list="contact">

                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-icon rounded-circle btn-primary"
                                                        type="button" data-repeater-create>
                                                        <i class="bx bx-plus"></i>
                                                    </button>
                                                    <span class="ml-1 font-weight-bold text-primary">افزودن جدید</span>
                                                </div>
                                                <div class="col-md-3 col-3 mb-50">
                                                    <label class="text-nowrap">روز</label>
                                                </div>
                                                <div class="col-md-2 col-2 mb-50">
                                                    <label class="text-nowrap">ساعت شروع</label>
                                                </div>
                                                <div class="col-md-2 col-2 mb-50">
                                                    <label class="text-nowrap">ساعت پایان</label>
                                                </div>
                                                <div class="col-md-2 col-2 mb-50">
                                                    <label class="text-nowrap">اتاق</label>
                                                </div>
                                                <div class="col-md-2 col-2 mb-50">
                                                    <label class="text-nowrap">رنگ</label>
                                                </div>
                                                <div class="col-md-1 col-1 mb-50">
                                                    <label class="text-nowrap">حذف</label>
                                                </div>
                                            </div>
                                            <div class="row justify-content-between" data-repeater-item>
                                                <div class="col-md-3 col-12 form-group d-flex align-items-center">
                                                    <i class="bx bx-menu mr-1"></i>
                                                    <select name="day" placeholder="روز" required class="form-control">
                                                        <option value="1">شنبه</option>
                                                        <option value="2">یک شنبه</option>
                                                        <option value="3">دو شنبه</option>
                                                        <option value="4">سه شنبه</option>
                                                        <option value="5">چهارشنبه</option>
                                                        <option value="6">پنج شنبه</option>
                                                        <option value="7">جمعه</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 col-12 form-group">
                                                    <div class="controls ">
                                                        <input type="time" name="start_time" required
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12 form-group" class="form-control">
                                                    <div class="controls ">
                                                        <input type="time" name="end_time" required
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="controls form-label-group ">
                                                            <select name="room" id="room" required class="form-control">
                                                                <option value=""></option>
                                                                @foreach ($rooms as $room)
                                                                    <option value="{{ $room->id }}">
                                                                        {{ $room->name }} --
                                                                        {{ $room->branch->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12 form-group" class="form-control">
                                                    <div class="controls ">
                                                        <input type="color" name="color" required class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-1 col-12 form-group">
                                                    <button class="btn btn-icon btn-danger rounded-circle" type="button"
                                                        data-repeater-delete>
                                                        <i class="bx bx-x"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group float-right">
                                            <input type="submit" class="form-control btn btn-success" value="ثبت"
                                                required>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.time.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/moment.min.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src="/assets/js/scripts/num2persian-min.js"></script>
        <!-- END: Page JS-->

        <script>
            $('.datepicer_date').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
            $('#category_id').select2({
                dropdownAutoWidth: true,
                width: '100%',
                language: "fa",
                placeholder: "حیطه",
            });
            // $('#room').select2({
            //     dropdownAutoWidth: true,
            //     width: '100%',
            //     language: "fa",
            //     placeholder: "اتاق را انتخاب کنید!",
            // });

            $('.datepicer_time').pickatime();
        </script>

        <script type="text/javascript" language="javascript">
            function autocomma(number_input) {
                $('.price_label.internet_price_label').html(Num2persian(number_input) + ' تومان');
                number_input += '';
                number_input = number_input.replace(',', '');
                number_input = number_input.replace(',', '');
                number_input = number_input.replace(',', '');
                number_input = number_input.replace(',', '');
                number_input = number_input.replace(',', '');
                number_input = number_input.replace(',', '');
                x = number_input.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1))
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                return x1 + x2;
            }
            $(document).ready(function() {
                // form repeater jquery
                $('.file-repeater, .contact-repeater, .repeater-default').repeater({
                    show: function() {
                        $(this).slideDown();
                        $('.datepicer_time').pickatime();

                    },
                    hide: function(deleteElement) {
                        if (confirm('آیا از حذف این آیتم مطمئن هستید؟')) {
                            $(this).slideUp(deleteElement);
                        }
                    }
                });

            });
        </script>
    </x-slot>
</x-base>
