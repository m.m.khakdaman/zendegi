<x-base title="@t(پیرینت برنام کاری)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(پرینت برنامه کاری)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.index') }}">
                                    مشاورین
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.show', $advisor->id) }}">
                                    {{ $advisor->name . ' ' . $advisor->family }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">@t(پرینت برنامه کاری)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="faq-search">
        <div class="row">
            <div class="col-12">
                <div class="card background-color">
                    <div class="card-header">
                    </div>
                    <div class="card-content">
                        <div class="card-body p-0 pb-3">
                            <h1 class="faq-title text-center mb-3">@t(پرینت برنامه کاری برای)
                                {{ $advisor->name . ' ' . $advisor->family }}</h1>
                            <form action="" method="get">

                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-8 row ">

                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="controls form-label-group ">
                                                    <input type="text" class="form-control datepicer_date"
                                                        name="start_date" id="start_date"
                                                        data-validation-pattern-message="فرمت فیلد معتبر نیست." required
                                                        value="{{ old('start_date') }}" placeholder="تاریخ شروع">
                                                    <div class="form-control-position">
                                                        <i class="bx bx-calendar">
                                                        </i>
                                                    </div>
                                                    <label for="start_date">از تاریخ</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="controls form-label-group ">
                                                    <input type="text" class="form-control datepicer_date"
                                                        name="end_date" value="{{ old('end_date') }}" id="end_date"
                                                        required placeholder="تاریخ پایان">
                                                    <div class="form-control-position">
                                                        <i class="bx bx-calendar">
                                                        </i>
                                                    </div>
                                                    <label for="end_date">تا تاریخ</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit"
                                                class="btn btn-icon rounded-circle btn-success glow mr-1 mb-1"><i
                                                    class="bx bx-printer"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-2"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js">
        </script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js">
        </script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js">
        </script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js">
        </script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js">
        </script>
        <script src="/assets/js/core/app-menu.js">
        </script>
        <script src="/assets/js/core/app.js">
        </script>
        <script src="/assets/js/scripts/components.js">
        </script>
        <script src="/assets/js/scripts/footer.js">
        </script>
        <script src="/assets/js/scripts/customizer.js">
        </script>
        <script src="/assets/js/scripts/forms/select/form-select2.js">
        </script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
        <script>
            $(document).ready(function() {

            $('.datepicer_date').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
            });
            });
        </script>

    </x-slot>
</x-base>
