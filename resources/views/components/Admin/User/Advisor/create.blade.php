<x-base>
    <x-slot name='title'>
        @t(تعریف مشاور جدید)
    </x-slot>
    <x-slot name='css'>


        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/file-uploaders/dropzone.css">
        <!-- END: Page CSS-->

        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(مشاور جدید)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('advisor.index') }}">لیست مشاورین</a>
                            </li>
                            <li class="breadcrumb-item active">@t(مشاور جدید)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(مشاور جدید)</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            @include('layouts.errors')
                            <form action="{{ route('advisor.store') }}" method="POST" class="wizard-horizontal"
                                novalidate>
                                @csrf
                                <div class="row">
                                    @include('components.Admin.User.create')
                                    <div class="form-group float-right">
                                        <input type="submit" class="form-control btn btn-success" value="ثبت" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/extensions/dropzone.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/moment.min.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->

        <script>
            $('#date_of_birth').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                maxDate: "+0D",
            });
            
            $('#education_level_id').select2({
                dropdownAutoWidth: true,
                width: '100%',
                language: "fa",
                placeholder: "مدرک تحصیلی",
            });
            $('#price_list').select2({
                dropdownAutoWidth: true,
                width: '100%',
                language: "fa",
                placeholder: "نرخ نامه ها",
            });
            Dropzone.options.dpzMultipleFiles = {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 100, // MB
                clickable: true
            }

        </script>
    </x-slot>
</x-base>
