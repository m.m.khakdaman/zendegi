<x-base>
    <x-slot name='title'>
        @t(پروفایل){{ ' '.$user->name . ' ' . $user->family }}
    </x-slot>
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/extensions/swiper.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/page-user-profile.css">
        <!-- END: Page CSS-->
    </x-slot>

    
  <section class="page-user-profile">
    <div class="content-header row">
      <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
              <div class="col-12">
                  <h5 class="content-header-title float-left pr-1">@t(پروفایل مشاور)</h5>
                  <div class="breadcrumb-wrapper">
                      <ol class="breadcrumb p-0 mb-0">
                          <li class="breadcrumb-item">
                              <a href="{{ route('admin.home') }}">
                                  <i class="bx bx-home-alt">
                                  </i>
                              </a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="{{ route('advisor.index') }}">@t(لیست مشاورین)</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="#"> @t(پروفایل مشاور)</a>
                          </li>
                          <li class="breadcrumb-item active">{{ $user->name . ' ' . $user->family }}</li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <!-- user profile heading section start -->
        <div class="card">
          <div class="card-content">
            <div style="background-color: #4971f5;">
                <!-- user profile image -->
                @php
                  $pic = "avatar.jpg";
                  if ($user->pic)
                  {
                    $pic = $user->pic;
                  }
                @endphp
                <img src="../../assets/images/profile/customer/avatar/{{ $pic }}" class="rounded mt-25 ml-25 mb-25" alt="user profile image" height="140" width="140">
            </div>
            <div class="user-profile-text">
              <h4 class="mb-0 text-bold-500 profile-text-color">{{ $user->name . ' ' . $user->family }}</h4>
              <small>@t(مشاور)</small>
            </div>
            <!-- user profile nav tabs start -->
            <div class="card-body px-0">
              <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 active align-items-center" id="profile-tab" data-toggle="tab" href="#profile" aria-controls="profile" role="tab" aria-selected="false"><i class="bx bx-copy-alt"></i><span class="d-none d-md-block">@t(پروفایل)</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="session-tab" data-toggle="tab" href="#session" aria-controls="session" role="tab" aria-selected="false"><i class="bx bx-conversation"></i><span class="d-none d-md-block">@t(جلسات)</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="workshop-tab" data-toggle="tab" href="#workshop" aria-controls="workshop" role="tab" aria-selected="false"><i class="bx bx-laptop"></i><span class="d-none d-md-block">@t(کارگاه ها)</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="product-tab" data-toggle="tab" href="#product" aria-controls="product" role="tab" aria-selected="false"><i class="bx bx-basket"></i><span class="d-none d-md-block">@t(خرید ها)</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="transactions-tab" data-toggle="tab" href="#transactions" aria-controls="transactions" role="tab" aria-selected="false"><i class="bx bx-dollar-circle"></i><span class="d-none d-md-block">@t(مالی)</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="price-list-tab" data-toggle="tab" href="#price-list" aria-controls="price-list" role="tab" aria-selected="false"><i class="bx bx-group"></i><span class="d-none d-md-block">@t(نرخ نامه)</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="doc-tab" data-toggle="tab" href="#doc" aria-controls="doc" role="tab" aria-selected="false"><i class="bx bx-file"></i><span class="d-none d-md-block">@t(مدارک)</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="folder-tab" data-toggle="tab" href="#folder" aria-controls="folder" role="tab" aria-selected="false"><i class="bx bx-folder-open"></i><span class="d-none d-md-block">@t(پرونده)</span></a>
                </li>
                
                <li class="nav-item pb-0 mr-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="comment-tab" data-toggle="tab" href="#comment" aria-controls="comment" role="tab" aria-selected="false"><i class="bx bx-comment-dots"></i><span class="d-none d-md-block">@t(نظرات)</span></a>
                </li>
              </ul>
            </div>
            <!-- user profile nav tabs ends -->
          </div>
        </div>
        <!-- user profile heading section ends -->
  
        <!-- user profile content section start -->
        <div class="row">
          <!-- user nav tabs content start -->
          <div class="col-lg-12">
            <div class="tab-content">
              <!-- user profile tabs profile start -->
             
              <div class="tab-pane active col-12" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                <div class="row">
                  <div class="col-lg-9 pl-0">
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-12">
                              <div class="row">
                                <div class="col-12 col-sm-3 text-center mb-1 mb-sm-0">
                                  <img src="assets/images/profile/customer/avatar/{{ $pic }}" class="rounded" alt="{{ $user->name . ' ' . $user->family }}" height="120" width="120">
                                </div>
                                <div class="col-12 col-sm-9">
                                  <div class="row">
                                    <div class="col-12 text-center text-sm-left">
                                        <h6 class="media-heading mb-0 primary-font">{{ $user->name . ' ' . $user->family }}<i class="cursor-pointer bx bxs-star text-warning ml-50 align-middle"></i></h6>
                                        <small class="text-muted align-top line-height-2">@t(علت مشاوره):{{ ' '.$user->categories_id . ' ' . $user->family }}</small>
                                    </div>
                                    <div class="col-12 text-center text-sm-left">
                                        <div class="mb-1 mt-50 line-height-2">
                                            {{-- <span class="mr-1">{{ $sessions->count() }} <small>@t(جلسه مشاوره داشته)</small></span> --}}
                                            {{-- <span class="mr-1">{{ $wshs->count() }} <small>@t(کارگاه داشته)</small></span> --}}
                                            <span class="mr-1"><small>@t(از)</small> {{ verta($user->created_at)->format('Y/m/d') }} <small>@t(با این مرکز ارتباط داشته)</small></span>
                                            
                                        </div>
                                        <p>{{ $user->note }}</p>
                                        <?php
                                          if ($user->gender == "مرد") {
                                            $icon = "bx bx-male";
                                            $gender = "جنسیت:مذکر";
                                          } else {
                                            $icon = "bx bx-female";
                                            $gender = "جنسیت:مونث";
                                          };

                                          if ($user->status == "فعال") {
                                            $statusicon = "bx bx-check-circle";
                                            $statustxt = "وضعیت این کاربر فعال است.";
                                          } else {
                                            $statusicon = "bx bx-x-circle";
                                            $statustxt = "وضعیت این کاربر غیر فعال است.";
                                          }
                                          
                                          ?>

                                        <div>
                                        <div class="badge badge-primary badge-round mr-1 mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $gender }}"><i class="cursor-pointer {{ $icon }}"></i>
                                        </div>
                                        <div class="badge badge-light-warning badge-round mr-1 mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $statustxt }}"><i class="{{ $statusicon }}"></i>
                                        </div>
                                        <div class="badge badge-light-success badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->mobile }}"><i class="cursor-pointer bx bx-mobile"></i>
                                        </div>
                                        <?php
                                        if ($user->mobile2) { ?>
                                        <div class="badge badge-light-danger badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->mobile2 }}"><i class="cursor-pointer bx bx-mobile-alt"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->phone) { ?>
                                        <div class="badge badge-light-info badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->phone }}"><i class="cursor-pointer bx bx-phone"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->instagram) { ?>
                                        <div class="badge badge-primary badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->instagram }}"><i class="cursor-pointer bx bxl-instagram"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->account_number) { ?>
                                        <div class="badge badge-light-primary badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="شماره حساب بانکی:{{ $user->account_number }}"><i class="cursor-pointer bx bx-credit-card"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->old_file_number) { ?>
                                        <div class="badge badge-light-warning badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="شماره پرونده قدیم:{{ $user->old_file_number }}"><i class="cursor-pointer bx bx-folder-open"></i>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                            <h5 class="card-title">@t(اطلاعات کامل){{ ' '.$user->name . ' ' . $user->family }}</h5>
                            <ul class="list-unstyled line-height-2">
                              <li class="mb-1"><i class="cursor-pointer bx bx-mobile align-middle mr-50" data-toggle="tooltip" data-placement="top" title="موبایل اول"></i>{{ $user->mobile }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-mobile-alt align-middle mr-50" data-toggle="tooltip" data-placement="top" title="موبایل دوم"></i>{{ $user->mobile2 }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-dollar align-middle mr-50" data-toggle="tooltip" data-placement="top" title="شماره حساب بانکی"></i>{{ $user->account_number }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-folder-open align-middle mr-50" data-toggle="tooltip" data-placement="top" title="شماره پرونده قدیم"></i>{{ $user->old_file_number }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-map align-middle mr-50" data-toggle="tooltip" data-placement="top" title="آدرس"></i>{{ $user->address }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-phone-call align-middle mr-50" data-toggle="tooltip" data-placement="top" title="تلفن ثابت"></i>{{ $user->phone }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-envelope align-middle mr-50" data-toggle="tooltip" data-placement="top" title="ایمیل"></i>{{ $user->email }}</li>
                            </ul>
                              
                            <div class="row">
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام)</small></h6>
                                <p><span class="ltr-text">{{ $user->name }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام خانوادگی) </small></h6>
                                <p><span class="ltr-text">{{ $user->family }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ تولد)</small></h6>
                                <p>{{ $user->date_of_birth }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام پدر)</small></h6>
                                <p>{{ $user->father_name }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(شماره شناسنامه)</small></h6>
                                <p>{{ $user->id_number }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(کد ملی)</small></h6>
                                <p>{{ $user->national_code }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(آدرس شبکه اجتماعی، اینستاگرام)</small></h6>
                                <p><span class="ltr-text">{{ $user->instagram }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(شماره حساب بانکی)</small></h6>
                                <p><span class="ltr-text">{{ $user->account_number }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(وضعیت تاهل)</small></h6>
                                <p>{{ $user->marital_status_id}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام همسر)</small></h6>
                                <p>{{ $user->spouse_name}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ ازدواج)</small></h6>
                                <p>{{ $user->date_of_marrige}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ جدایی)</small></h6>
                                <p>{{ $user->date_of_divorce}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ تولد همسر)</small></h6>
                                <p><span class="ltr-text">{{ $user->spouse_date_of_birth }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تعداد فرزند)</small></h6>
                                <p><span class="ltr-text">{{ $user->number_of_children }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(اطلاعات فرزندان)</small></h6>
                                <p><span class="ltr-text">{{ $user->children_information }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(شغل)</small></h6>
                                <p><span class="ltr-text">{{ $user->job }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(سطح در آمد)</small></h6>
                                <p><span class="ltr-text">{{ $user->Income_level }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(بهترین زمان تماس)</small></h6>
                                <p><span class="ltr-text">{{ $user->best_time_to_call }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(فکر می کنید در چه زمینه ای نیاز به مهارت و آموزش دارید؟ )</small></h6>
                                <p><span class="ltr-text">{{ $user->categories_id }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(چه ساعتی به فضای مجازی وصل می شوید؟)</small></h6>
                                <p><span class="ltr-text">{{ $user->time_to_use_internet }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(روش مناسب برای ارتباط)</small></h6>
                                <p><span class="ltr-text">{{ $user->connection_method_id }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نحوه آشنایی)</small></h6>
                                <p><span class="ltr-text">{{ $user->Introduction_method_id }}</span></p>
                              </div>
                              <div class="col-12">
                                <h6 class="mb-0"><small class="text-muted">@t(توضیحات)</small></h6>
                                <p>{{ $user->note }}</p>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 pr-0">
                    <!-- user profile right side content birthday card start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <div class="d-inline-flex align-items-center">
                            <div class="avatar mr-50">
                              <img src="assets/images/profile/customer/avatar/{{ $pic }}" alt="{{ $user->name . ' ' . $user->family }}" height="32" width="32">
                            </div>
                            <h6 class="mb-0 d-flex align-items-center"> امروز تولد {{ $user->name . ' ' . $user->family }} هست</h6>
                          </div>
                          <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                          <div class="user-profile-birthday-image text-center p-2">
                            <img class="img-fluid" src="../../assets/images/profile/pages/birthday.png" alt="image">
                          </div>
                          <div class="user-profile-birthday-footer text-center text-lg-left">
                            <p class="mb-0"><small>براش یه پیغام تبریک تولد بذار</small></p>
                            <a class="btn btn-sm btn-light-primary mt-50" href="JavaScript:void(0);">ارسال آرزو</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- user profile right side content birthday card ends -->
                    <!-- user profile right side content related groups start -->
                    <!-- user profile right side content related groups ends -->
                    <!-- user profile right side content gallery start -->
                    <!-- user profile right side content gallery ends -->
                  </div>
                </div>
              </div>
              <!-- user profile tabs profile ends -->
              <!-- user nav tabs session start -->
              <div class="tab-pane" id="session" aria-labelledby="session-tab" role="tabpanel">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <h5>{{ $user->name.' '.$user->family.' ' }}@t(در جلسات زیر شرکت داشته است.)</h5>
                      <div class="table-responsive">
                        <table class="table mb-0">
                          <thead>
                            <tr>
                              <th>@t(ردیف)</th>
                              <th class="text-center">@t(نام مشاور)</th>
                              <th>@t(تاریخ)</th>
                              <th>@t(زمان جلسه)</th>
                              <th>@t(قیمت)</th>
                              <th class="text-center">@t(اپراتور)</th>
                            </tr>
                          </thead>
                          <tbody>
                           
                          </tbody>
                        </table>
                      </div>
                      <div class="text-center">
                        {{-- <button class="btn btn-primary">مشاهده همه فعالیت ها</button> --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- user nav tabs session ends -->
              <!-- user nav workshop start -->
              <div class="tab-pane" id="workshop" aria-labelledby="workshop-tab" role="tabpanel">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <h5>{{ $user->name.' '.$user->family.' ' }}@t(در کارگاه های زیر شرکت داشته است.)</h5>
                      <div class="table-responsive">
                        <table class="table mb-0">
                          <thead>
                            <tr>
                              <th>@t(ردیف)</th>
                              <th>@t(نام کارگاه)</th>
                              <th class="text-center">@t(تاریخ ثبت نام)</th>
                              <th>@t(هزینه کارگاه)</th>
                              <th>@t(اپراتور)</th>
                            </tr>
                          </thead>
                          <tbody>
                            {{-- @php
                              $i = 0;
                            @endphp
                           @foreach ($wshs as $wsh)
                             <tr>
                              <td> {{ ++$i }}</td>
                              <td>
                                @if (!$wsh->status)
                                  <span class="text-danger">{{ $wsh->product->product_name }}<i class="text-primary">@t(: انصراف)</i></span>
                                @else
                                  {{ $wsh->product->product_name }}
                                @endif
                              </td>
                              <td>
                                <span class="badge badge-pill badge-warning" data-toggle="tooltip" data-placement="top" title="{{ $wsh->date }}">
                                  {{ $wsh->date2 }}
                                </span>
                              </td>
                              <td>
                                <span class="badge badge-pill badge-circle-light-danger">
                                  {{ $wsh->product->price }}
                                </span>
                              </td>
                              <td>{{ $wsh->operator->name.' '.$wsh->operator->family }}</td>
                             </tr>
                           @endforeach --}}
                          </tbody>
                        </table>
                      </div>
                      <div class="text-center">
                        {{-- <button class="btn btn-primary">مشاهده همه فعالیت ها</button> --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- user profile nav tabs workshop ends -->
              <!-- user nav product start -->
              <div class="tab-pane" id="product" aria-labelledby="product-tab" role="tabpanel">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="table-responsive">
                        <h5>[sajdhjsa]</h5>
                        <table class="table mb-0">
                          <thead>
                            <tr>
                              <th>@t(ردیف)</th>
                              <th>@t(نام مشاور)</th>
                              <th>@t(تاریخ)</th>
                              <th>@t(زمان جلسه)</th>
                              <th>@t(اپراتور)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>مقالات</td>
                              <td>خیر</td>
                              <td>بله</td>
                              <td>خیر</td>
                              <td>بله</td>
                            </tr>
                            <tr>
                              <td>کارمند</td>
                              <td>بله</td>
                              <td>بله</td>
                              <td>خیر</td>
                              <td>خیر</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="text-center">
                        <button class="btn btn-primary">مشاهده همه فعالیت ها</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- user profile nav tabs product ends -->
              <!-- user nav transactions start -->
              <div class="tab-pane" id="transactions" aria-labelledby="transactions-tab" role="tabpanel">
                <div class="card">
                  <div class="card-header">
                      <h4 class="card-title">@t(سوابق مالی){{ ' '.$user->name.' '.$user->family }}</h4>
                  </div>
                  <div class="card-content">
                      <div class="card-body card-dashboard">
                          <div class="table-responsive">
                              <table class="1table zero-configuration" style="width: 100%">
                                  <thead>
                                      <tr>
                                          <th>@t(کد)</th>
                                          <th>@t(تاریخ)</th>
                                          <th>@t(نوع تراکنش)</th>
                                          <th>@t(محصول)</th>
                                          <th>@t(قیمت)</th>
                                          <th>@t(پرداختی)</th>
                                          <th>@t(باقیمانده)</th>
                                          {{-- <th>@t(اپراتور)</th> --}}
                                          <th>@t(سایر)</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    {{-- @foreach ($transaction as $transaction)
                                      
                                      <tr>
                                          <td>{{ $transaction->id }}</td>
                                          <td>
                                            <span class="badge badge-pill badge-warning" data-toggle="tooltip" data-placement="top" title="$transaction->created_at">
                                              {{ $transaction->created_at }} 
                                            </span>
                                          </td>
                                          <td><span class="badge badge-pill badge-secondary">{{@ $transaction->transaction_type->name }}</span></td>
                                          @if($transaction->sale)
                                          <td>{{@ $transaction->sale->product->product_name }}</td>
                                          @else
                                          <td>-</td>
                                          @endif
                                          <td><span class="badge badge-pill badge-circle-light-danger">{{ -$transaction->amount }}</span></td>
                                          <td><span class="badge badge-pill badge-light-success">{{ $transaction->amount - $transaction->amount_standing }}</span></td>
                                          <td><span class="badge badge-pill badge-light-danger">{{ $transaction->amount_standing }}</span></td>
                                          <td>
                                            <i class="bx bx-user-circle" data-toggle="tooltip" data-placement="right" title="{{ $transaction->operator->name ?? '' }}&nbsp;{{ $transaction->operator->family ?? '' }}"></i>
                                            <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="right" title="{{ $transaction->comment }}"></i>
                                          </td>
                                      </tr>
                                    @endforeach --}}
                                      
                              </table>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              <!-- user profile nav tabs transactions ends -->
              <!-- user profile nav tabs price-list start -->
              <div class="tab-pane" id="price-list" aria-labelledby="price-list-tab" role="tabpanel">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <h5>@t(نرخ نامه های){{ ' '.$user->name.' '.$user->family }}</h5>
                        @foreach ($price_lists as $price_list)
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <fieldset>
                                    <div class="checkbox">
                                        <input name="price_list[]" value="{{ $price_list->id }}" type="checkbox" class="checkbox-input"
                                            id="price_list_{{ $price_list->id }}" @foreach ($user->PriceList as $item) {{ $item->id == $price_list->id ? 'checked' : '' }} @endforeach>
                                        <label for="price_list_{{ $price_list->id }}">{{ $price_list->name }}</label>
                                    </div>
                                </fieldset>
                                
                            </div>
                        @endforeach
                      <h5 class="mt-2">دوستان مشترک</h5>
                      <div class="row">
                        <div class="col-6">
                          <ul class="list-unstyled mb-0">
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-26.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-online"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">جکی چان</a></h6>
                                <small class="text-muted">شبکه</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-25.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-offline"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">لیونل مسی</a></h6>
                                <small class="text-muted">توسعه دهنده</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-24.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-busy"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">کریس رونالدو</a></h6>
                                <small class="text-muted">طراح</small>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div class="col-6">
                          <ul class="list-unstyled mb-0">
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-23.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-busy"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">جسیکا آلبا</a></h6>
                                <small class="text-muted">کارگر</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-22.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-away"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">بروس وین</a></h6>
                                <small class="text-muted">وکیل</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-21.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-offline"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">پیتر پارکر</a></h6>
                                <small class="text-muted">دانشجو</small>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- user profile nav tabs price-list ends -->
              <!-- user nav doc start -->
              <div class="tab-pane" id="doc" aria-labelledby="doc-tab" role="tabpanel">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                        <h5>@t(مدراک شناسایی){{ ' '.$user->name.' '.$user->family }}</h5>
                        





                        <div class="row">
                          <div class="col-md-4">
                            <div class="thumbnail">
                              <a href="https://learnsource.net\Uploads\Courses\HTML\lights.jpg" target="_blank">
                                <img src="https://learnsource.net\Uploads\Courses\HTML\lights.jpg" alt="Lights" style="width:100%">
                                <div class="caption">
                                  <p>مدرک لیسانس</p>
                                </div>
                              </a>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="thumbnail">
                              <a href="https://learnsource.net\Uploads\Courses\HTML\nature.jpg" target="_blank">
                                <img src="https://learnsource.net\Uploads\Courses\HTML\nature.jpg" alt="Nature" style="width:100%">
                                <div class="caption">
                                  <p>مدرک دیپلم نجاری صنعتی</p>
                                </div>
                              </a>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="thumbnail">
                              <a href="https://learnsource.net\Uploads\Courses\HTML\fjords.jpg" target="_blank">
                                <img src="https://learnsource.net\Uploads\Courses\HTML\fjords.jpg" alt="Fjords" style="width:100%">
                                <div class="caption">
                                  <p>فتوکپی شناسنامه</p>
                                </div>
                              </a>
                            </div>
                          </div>
                        </div>
                      
                      dsfkjsdjf
                    </div>
                  </div>
                </div>
              </div>
              <!-- user profile nav tabs doc ends -->
              <!-- user nav folder start -->
              <div class="tab-pane" id="folder" aria-labelledby="folder-tab" role="tabpanel">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                        <h5>@t(پرونده پزشکی){{ ' '.$user->name.' '.$user->family }}</h5>
                        
                      dsfkjsdjf
                    </div>
                  </div>
                </div>
              </div>
              <!-- user profile nav tabs folder ends -->
              <!-- user nav comment start -->
              <div class="tab-pane" id="comment" aria-labelledby="comment-tab" role="tabpanel">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                        <h5>@t(کامنت های ارائه شده توسط){{ ' '.$user->name.' '.$user->family }}</h5>
                        
                      dsfkjsdjf
                    </div>
                  </div>
                </div>
              </div>
              <!-- user profile nav tabs doc ends -->

              <div class="tab-pane " id="activity" aria-labelledby="activity-tab" role="tabpanel">
                <!-- user profile nav tabs activity start -->
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <!-- timeline widget start -->
                      <ul class="widget-timeline">
                        <li class="timeline-items timeline-icon-success active">
                          <div class="timeline-time">سه شنبه 8:17 ب.ظ</div>
                          <h6 class="timeline-title">امیلیا کلارک</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">لورم ایپسوم متن</a></p>
                          <div class="timeline-content">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-primary active">
                          <div class="timeline-time">5 روز پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن ساختگی با تولید</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">نام پروژه</a></p>
                          <div class="timeline-content">
                            <img src="../../assets/images/icon/sketch.png" alt="document" height="36" width="27" class="mr-50">پوشه اطلاعات
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-danger active">
                          <div class="timeline-time">7 ساعت پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن ساختگی</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">نام پروژه</a></p>
                          <div class="timeline-content">
                            <img src="../../assets/images/icon/pdf.png" alt="document" height="36" width="27" class="mr-50">Received.pdf
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-info active">
                          <div class="timeline-time">5 ساعت پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن ساختگی با تولید سادگی</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">لورم ایپسوم متن</a></p>
                          <div class="timeline-content">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-warning">
                          <div class="timeline-time">2 دقیقه پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن </h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">پسندیده شده</a></p>
                          <div class="timeline-content">
                            لورم ایپسوم متن
                          </div>
                        </li>
                      </ul>
                      <!-- timeline widget ends -->
                      <div class="text-center">
                        <button class="btn btn-primary">مشاهده همه فعالیت ها</button>
                      </div>
                    </div>
                  </div>
              </div>
              <!-- user profile nav tabs activity start -->
              </div>
            </div>
          </div>
          <!-- user nav tabs content ends -->
          <!-- user profile right side content start -->
          {{-- <div class="col-lg-3">
            <!-- user profile right side content birthday card start -->
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <div class="d-inline-flex align-items-center">
                    <div class="avatar mr-50">
                      <img src="../../assets/images/portrait/small/avatar-s-20.jpg" alt="avtar images" height="32" width="32">
                    </div>
                    <h6 class="mb-0 d-flex align-items-center"> امروز تولد جسیکا هست</h6>
                  </div>
                  <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                  <div class="user-profile-birthday-image text-center p-2">
                    <img class="img-fluid" src="../../assets/images/profile/pages/birthday.png" alt="image">
                  </div>
                  <div class="user-profile-birthday-footer text-center text-lg-left">
                    <p class="mb-0"><small>براش یه پیغام تبریک تولد بذار</small></p>
                    <a class="btn btn-sm btn-light-primary mt-50" href="JavaScript:void(0);">ارسال آرزو</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- user profile right side content birthday card ends -->
            <!-- user profile right side content related groups start -->
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <h5 class="card-title mb-1">گروه های مرتبط
                    <i class="cursor-pointer bx bx-dots-vertical-rounded align-top float-right"></i>
                  </h5>
                  <div class="media d-flex align-items-center mb-1">
                    <a href="JavaScript:void(0);">
                      <img src="../../assets/images/banner/banner-30.jpg" class="rounded" alt="group image" height="64" width="64">
                    </a>
                    <div class="media-body ml-1">
                      <h6 class="media-heading mb-0 mt-n25 primary-font"><small>لورم ایپسوم</small></h6><small class="text-muted">2.8k عضو (7 عضو جدید)</small>
                    </div>
                    <i class="cursor-pointer bx bx-plus-circle text-primary d-flex align-items-center "></i>
                  </div>
                  <div class="media d-flex align-items-center mb-1">
                    <a href="JavaScript:void(0);">
                      <img src="../../assets/images/banner/banner-31.jpg" class="rounded" alt="group image" height="64" width="64">
                    </a>
                    <div class="media-body ml-1">
                      <h6 class="media-heading mb-0 mt-n25 primary-font"><small>لورم ایپسوم متن</small></h6><small class="text-muted">9k عضو (7 عضو جدید)</small>
                    </div>
                    <i class="cursor-pointer bx bx-plus-circle text-primary d-flex align-items-center "></i>
                  </div>
                  <div class="media d-flex align-items-center">
                    <a href="JavaScript:void(0);">
                      <img src="../../assets/images/banner/banner-32.jpg" class="rounded" alt="group image" height="64" width="64">
                    </a>
                    <div class="media-body ml-1">
                      <h6 class="media-heading mb-0 mt-n25 primary-font"><small>لورم ایپسوم متن ساختگی</small></h6><small class="text-muted">7.6k عضو</small>
                    </div>
                    <i class="cursor-pointer bx bx-lock text-muted d-flex align-items-center"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- user profile right side content related groups ends -->
            <!-- user profile right side content gallery start -->
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <h5 class="card-title mb-1">گالری
                    <i class="cursor-pointer bx bx-dots-vertical-rounded align-top float-right"></i>
                  </h5>
                  <div class="row">
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-10.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-11.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-12.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-13.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-05.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-06.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-07.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-08.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-09.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- user profile right side content gallery ends -->
          </div> --}}
          <!-- user profile right side content ends -->
        </div>
        <!-- user profile content section start -->
      </div>
    </div>
  </section>

  
    <x-slot name='script'>
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/extensions/swiper.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>

        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/pages/page-user-profile.js"></script>
        <script src="/assets/js/scripts/datatables/datatable.js"></script>
        <!-- END: Page JS-->
    </x-slot>
</x-base>    