<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    {{-- <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css"> --}}
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A5;
            margin: 0;
        }

    </style>
</head>

    <body style="background-image:none;">        
        <div class="container p-0">
            <div class="row">
                <div class="col-4">
                </div>
                <div class="col-4 text-center mt-2">
                    <p>
                        <h3>@t(فیش حقوقی){{ ' '.$advisor->name.' '.$advisor->family }}</h3>
                        <span class="font-medium-3">@t(از تاریخ:){{ ' '.$payroll->start_date }} تا {{ $payroll->end_date }}</span>
                    </p>
                </div>
                <div class="col-4">
                    <div class="row text-center float-right">
                        <div class="mr-2 mt-2">
                            <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                            <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table-bordered" width='100%'>
                                <thead>
                                    <tr>
                                            <th>ردیف</th>
                                        @if ($print_advisor)
                                            <th>مشاور</th>
                                            <th>مدت جلسه</th>
                                            <th>علت مراجعه</th>
                                            <th>نام مرجع</th>
                                            <th>تاریخ برگزاری جلسه</th>
                                            <th>نرخ نامه</th>
                                            <th>زمان نرخ نامه </th>
                                            <th>درصد مشاور</th>
                                            <th>سهم مشاور</th>
                                            <th>درصد مرکز</th>
                                            <th>سهم مرکز</th>
                                        @else
                                            <th>اپراتور</th>
                                        @endif
                                        <th>مبلغ جلسه</th>
                                        <th>تاریخ ثبت جلسه</th>
                                        @if (!$print_advisor)
                                            <th>نوع تراکنش</th>
                                            <th>صندوق</th>
                                            <th>حساب</th>
                                        @endif
                
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($print_advisor)
                                        @php
                                            $total_time = [];
                                        @endphp
                                    @endif
                                    @foreach ($Transactions as $key => $Transaction)
                                        <tr>
                                                <td>{{ $key + 1 }}</td>
                                            @if ($print_advisor)
                                                {{-- <td><a
                                                        href="/admin/Financial/transaction/pardakht_be_advisor?advisor={{ $Transaction->sale->advisor_id }}">{{ $Transaction->sale->advisor->name . ' ' . $Transaction->sale->advisor->family }}</a> --}}
                                                </td>
                                                <td>{{ $Transaction->sale->session_length ?? '' }}</td>
                                                <td>{{ $Transaction->sale->category->name ?? '' }}</td>
                                                <td>{{ ($Transaction->sale->customer->name ?? '') . ' ' . ($Transaction->sale->customer->family ?? '') }}
                                                </td>
                                                @php
                                                    @ $total_time[$Transaction->sale->session_length] = ($total_time[$Transaction->sale->session_length] ?? 0) + 1;
                                                @endphp
                                                @if ($Transaction->sale)
                                                    <td> {{ $Transaction->sale->session_start }} {{ verta($Transaction->sale->date)->format('Y/m/d') ?? '' }}</td>
                                                @endif
                                                @if ($Transaction->sale->price_list)
                
                                                    <td>{{ $Transaction->sale->price_list->total_price }}</td>
                                                    <td>{{ $Transaction->sale->price_list->session_length }} دقیقه</td>
                                                    <td>{{ 100 / ($Transaction->sale->price_list->total_price / $Transaction->sale->price_list->advisor_rate) }}٪
                                                    </td>
                                                    <td><i
                                                            class="currency">{{ ($Transaction->amount < 0 ? -$Transaction->amount : $Transaction->amount) * (100 / 100 / ($Transaction->sale->price_list->total_price / $Transaction->sale->price_list->advisor_rate)) }}</i>
                                                    </td>
                                                    <td>{{ 100 / ($Transaction->sale->price_list->total_price / $Transaction->sale->price_list->center_rate) }}٪
                                                    </td>
                
                                                    <td><i class="currency">{{ ($Transaction->amount < 0 ? -$Transaction->amount : $Transaction->amount) * (100 / 100 / ($Transaction->sale->price_list->total_price / $Transaction->sale->price_list->center_rate)) }}</i>
                                                    </td>
                                                @endif
                                            @else
                                                <td>{{ $Transaction->operator->name . ' ' . $Transaction->operator->family }}</td>
                                            @endif
                                            @if ($print_advisor)
                                                <td><i class="currency">{{ $Transaction->sale->orginal_price }}</i></td>
                                            @else
                                                <td><i
                                                        class="currency">{{ $Transaction->amount < 0 ? -$Transaction->amount : $Transaction->amount }}</i>
                                                </td>
                                            @endif
                                            <td>{{ $Transaction->created_at }}</td>
                                            @if (!$print_advisor)
                                                <td>{{ $Transaction->transaction_type->name ?? '-' }}</td>
                                                <td>{{ $Transaction->cash->name ?? '-' }}</td>
                                                <td>{{ $Transaction->user->name }} {{ $Transaction->user->family }}</td>
                                            @endif
                                        </tr>
                                        @if (!$print_advisor)
                
                                            @foreach ($Transaction->sources as $item)
                                                <tr style="border: black 2px solid">
                                                    <th>بابت:</th>
                                                    <td>{{ $item->distinction->sale->products_type->product_type }} با
                                                        {{ ($item->distinction->sale->advisor->name ?? '') . ' ' . ($item->distinction->sale->advisor->family ?? '') }}
                                                    </td>
                                                    <th>مبلغ:</th>
                                                    <td><i class="currency">{{ $item->amount }}</i></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                
                                    <tr>
                                        <th>مجموع:</th>
                                        <th><i class="currency">{{ $Transactions->sum('amount') * -1 }}</i> تومان</th>
                                    </tr>
                                    {{-- @foreach ($session->transactions->distinctions as $key => $item)
                                        <tr class="text-center">
                                            <td class='td_center'>{{ $key + 1 }}</td>
                                            <td dir="ltr">{{ $item->amount }}</td>
                                            <td class="text-center">-</td>
                                            <td dir="ltr" class="text-center">{{ $item->source->comment ?? '-' }}</td>
                                            <td dir="ltr">{{ verta($item->created_at)->format('Y/m/d H:i') }}</td>
                                        </tr>
                                    @endforeach
                                    <tr class="text-center">
                                        <td class='td_center'>مجموع</td>
                                        <td dir="ltr">
                                            {{ $session->transactions->amount_standing - $session->transactions->amount }}
                                        </td>
                                        <td dir="ltr">
                                            {{ -$session->transactions->amount_standing }}
                                        </td>
                                        <td class="text-center">-</td>
                                        <td dir="ltr">{{ verta($session->transactions->created_at)->format('Y/m/d H:i') }}</td>
                                    </tr> --}}
                                </tbody>
                            </table>
                            {{-- <table  style="border-collapse: collapse; width: 100%; direction: rtl" border="1">
                                <tbody>
                                    <tr>
                                        <td class="text-center" style="width: 25%;">
                                            @php
                                            $avatar = "avatar.jpg";
                                            if (isset($advisor->pic)) {
                                                $avatar = $advisor->pic;
                                            };
                                        @endphp
                                            <img src="/assets/images/profile/admin/avatar/{{ $avatar }}" alt="@t(عکس پرسنلی)" width="150px">
                                        </td>
                                        <td style="width: 25%;" colspan="3">
                                            <span class="float-right" style="direction: rtl">
                                                @t(فیش حقوقی مربوط به){{ ' '.$payroll->month.' ' }}@t(ماه سال 1400)<br>
                                                @t(از){{ ' '.$payroll->start_jalali.' ' }}@t(تا){{ ' '.$payroll->end_jalali }}
                                            </span>
                                            <span>@t(مرکز مشاوره زندگی عاقلانه)</span>
                                        </td>
                                    </tr>
                                <tr class="text-center bg-light">
                                    <td style="width: 25%;">@t(مشخصات پرسنل)</td>
                                    <td style="width: 25%;">@t(وضعیت کارکرد)</td>
                                    <td style="width: 25%;">@t(شرح پرداخت ها)</td>
                                    <td style="width: 25%;">@t(شرح کسور)</td>
                                </tr>
                                <tr>
                                    <td style="width: 25%; vertical-align: top; padding-top: 10px;">
                                        <span>@t(کد پرسنلی:){{ ' '.$admin->id }}</span><br>
                                        <span>@t(نام:){{ ' '.$admin->name }}</span><br>
                                        <span>@t(نام خانوادگی:){{ ' '.$admin->family }}</span><br>
                                        <span>@t(کدملی:){{ ' '.$admin->national_code }}</span><br>
                                        <span>@t(شماره شناسنامه:){{ ' '.$admin->id_number }}</span><br>
                                        <span>@t(محل خدمت:){{ ' '.$admin->semat }}</span><br>
                                        <span>@t(شغل یا سمت:){{ ' '.$admin->semat }}</span><br>
                                        <span>@t(شماره حساب:){{ ' '.$admin->account_number }}</span><br>
                                        <span>@t(شماره بیمه:){{ ' '.$admin->account_number }}</span><br>
                                    </td>
                                    <td style="width: 25%; vertical-align: top;  padding-top: 10px;" rowspan="3">
                                        <span class="ml-50">@t(مزد شغل روزانه:)</span><span class="float-right mr-50">{{ $payroll->daily }}</span><hr>
                                        <span class="ml-50">@t(روزهای کارکرد:)</span><span class="float-right mr-50">{{ $payroll->working_days }}</span><hr>
                                        <span class="ml-50">@t(اضافه کار:)</span><span class="float-right mr-50">{{ $payroll->overtime_hourly }}</span><hr>
                                        <span class="ml-50">@t(کسرکار ساعتی:)</span><span class="float-right mr-50">{{ $payroll->low_time_hourly }}</span><hr>
                                    </td>
                                    <td style="width: 25%; vertical-align: top;" class="pt-1">
                                        <span class="ml-50">@t(حقوق ماهانه:)</span><span class="float-right mr-50">{{ $payroll->payroll }}</span><hr>
                                        <span class="ml-50">@t(حق اولاد:)</span><span class="float-right mr-50">{{ $payroll->child }}</span><hr>
                                        <span class="ml-50">@t(مسکن:)</span><span class="float-right mr-50">{{ $payroll->home }}</span><hr>
                                        <span class="ml-50">@t(بن:)</span><span class="float-right mr-50">{{ $payroll->bon }}</span><hr>
                                        <span class="ml-50">@t(ایاب و ذهاب:)</span><span class="float-right mr-50">{{ $payroll->transportation }}</span><hr>
                                        <span class="ml-50">@t(اضافه کار:)</span><span class="float-right mr-50">{{ $payroll->overtime }}</span><hr>
                                        <span class="ml-50">@t(فوق العاده کاری نوبت صبح و عصر:)</span><span class="float-right mr-50">{{ $payroll->morning }}</span><hr>
                                        <span class="ml-50">@t(فوق العاده نوبت کاری صبح عصر شب:)</span><span class="float-right mr-50">{{ $payroll->night }}</span><hr>
                                        <span class="ml-50">@t(3 درصد از فروش کارگاه عمومی:)</span><span class="float-right mr-50">{{ $payroll->public_wsh }}</span><hr>
                                        <span class="ml-50">@t(2 درصد از فروش کارگاه تخصصی:)</span><span class="float-right mr-50">{{ $payroll->special_wsh }}</span><hr>
                                    </td>
                                    <td style="width: 25%; vertical-align: top; padding-top: 10px;">
                                        <span class="ml-50">@t(مالیات حقوق:)</span><span class="float-right mr-50">{{ $payroll->tax }}</span><hr>
                                        <span class="ml-50">@t(بیمه:)</span><span class="float-right mr-50">{{ $payroll->insurance }}</span><hr>
                                        <span class="ml-50">@t(مساعده:)</span><span class="float-right mr-50">{{ $payroll->help }}</span><hr>
                                        <span class="ml-50">@t(کسر کار ساعتی:)</span><span class="float-right mr-50">{{ $payroll->low_time }}</span><hr>
                                        <span class="ml-50">@t(هزینه کارگاه:)</span><span class="float-right mr-50">{{ $payroll->wsh }}</span><hr>
                                        <span class="ml-50">@t(مفاصا حساب:)</span><span class="float-right mr-50">{{ $payroll->mofasa }}</span><hr>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;" class="bg-light text-center">@t(مانده وام)</td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(طلب از قبل:)</span><span class="float-right mr-50">{{ $payroll->credit }}</span>
                                    </td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(کسر رند ماه جاری:)</span><span class="float-right mr-50">{{ $payroll->rond }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(وام قرض الحسنه)</span><span class="float-right mr-50">{{ $payroll->rest_of_loan }}</span>
                                    </td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(جمع ناخالص:)</span><span class="float-right mr-50">{{ $payroll->nakhales }}</span>
                                    </td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(کسورات:)</span><span class="float-right mr-50">{{ $payroll->kosoorat }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;"></td>
                                    <td style="width: 25%;"></td>
                                    <td style="width: 25%;" class="bg-light">
                                        <span class="ml-50">@t(خالص ماه جاری:)</span><span class="float-right mr-50">{{ $payroll->khales }}</span>
                                    </td>
                                    <td style="width: 25%;" class="bg-light">
                                        <span class="ml-50">@t(مبلغ پرداختی:)</span><span class="float-right mr-50">{{ $payroll->paid }}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table> --}}
                        </div>
                    </div>
                </div>
            </div>
            {{ sentence() }}
            <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
        </div>
    </body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <script src="/assets/vendors/js/charts/chart.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    
    <!-- END: Page JS-->

</html>


