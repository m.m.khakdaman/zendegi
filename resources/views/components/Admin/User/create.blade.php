<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="text" class="form-control" id="name" required name="name"
                data-validation-required-message="پر کردن فیلد نام اجباری است." value="{{ old('name') }}"
                placeholder='@t(نام)'>
            <label for="name">@t(نام)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="text" class="form-control" id="family" required name="family"
                data-validation-required-message="پر کردن فیلد نام خانوادگی اجباری است." value="{{ old('family') }}"
                placeholder='@t(نام خانوادگی)'>
            <label for="family">@t(نام خانوادگی)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="text" class="form-control" id="father" name="father" value="{{ old('father') }}"
                placeholder="نام پدر">
            <label for="father">نام پدر</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="text" class="form-control" name="date_of_birth" id="date_of_birth"
                pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                data-validation-pattern-message="فرمت فیلد معتبر نیست." value="{{ old('date_of_birth') }}"
                placeholder="تاریخ تولد">
            <div class="form-control-position">
                <i class="bx bx-calendar">
                </i>
            </div>
            <label for="date_of_birth">تاریخ تولد</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="tel" class="form-control" id="national_code" name="national_code"
                pattern="[\u06F0-\u06F90-9]{10}" data-validation-pattern-message="فرمت فیلد معتبر نیست."
                value="{{ old('national_code') }}" placeholder='@t( کدملی)'>
            <label for="national_code">@t(کدملی)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group validate">
        <div class="controls form-label-group ">
            <select class="form-control " id="education_level_id" name="education_level_id"
                placeholder='@t( مدرک تحصیلی)'>
                <option value="">مدرک تحصیلی:</option>
                @foreach ($education_levels as $education_level)
                    <option value="{{ $education_level->id }}">
                        {{ $education_level->education_level }}
                    </option>
                @endforeach
            </select>
            <label for="education_level_id">@t(مدرک تحصیلی)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="text" class="form-control" id="field_of_study" name="field_of_study"
                value="{{ old('field_of_study') }}" placeholder='@t(رشته تحصیلی)'>
            <label for="field_of_study">@t(رشته تحصیلی)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="file" class="form-control" id="pic" name="pic" placeholder='@t(عکس پرسنلی)'>
            <label for="pic">@t(عکس پرسنلی)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="card-content">
        <p>جنسیت</p>
        <div class="card-body">
            <ul class="list-unstyled mb-0">
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-primary" value="1"
                                {{ old('gender') == 1 ? 'checked' : '' }} name="gender" id="man">
                            <label class="custom-control-label" for="man">مرد</label>
                        </div>
                    </fieldset>
                </li>
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" {{ old('gender') == 2 ? 'checked' : '' }}
                                class="custom-control-input bg-danger" value="2" name="gender" id="woman">
                            <label class="custom-control-label" for="woman">زن</label>
                        </div>
                    </fieldset>
                </li>
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" value="3" {{ old('gender') == 3 ? 'checked' : '' }}
                                class="custom-control-input bg-secondary" name="gender" id="unknow">
                            <label class="custom-control-label" for="unknow">نامشخص</label>
                        </div>
                    </fieldset>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="card-content">
        <p>@t(خلق و خوی)</p>
        <div class="card-body">
            <ul class="list-unstyled mb-0">
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-info" name="customRadioColored"
                                id="customColorRadio1" checked>
                            <label class="custom-control-label" for="customColorRadio1">@t(بسیار آرام)</label>
                        </div>
                    </fieldset>
                </li>
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-primary" name="customRadioColored"
                                id="customColorRadio2">
                            <label class="custom-control-label" for="customColorRadio2">@t(آرام)</label>
                        </div>
                    </fieldset>
                </li>
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-success" name="customRadioColored"
                                id="customColorRadio3">
                            <label class="custom-control-label" for="customColorRadio3">@t(معمولی)</label>
                        </div>
                    </fieldset>
                </li>
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-secondary" name="customRadioColored"
                                id="customColorRadio4">
                            <label class="custom-control-label" for="customColorRadio4">@t(گله مند)</label>
                        </div>
                    </fieldset>
                </li>
                <li class="d-inline-block mr-2 mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-warning" name="customRadioColored"
                                id="customColorRadio5">
                            <label class="custom-control-label" for="customColorRadio5">@t(پرخاشگر)</label>
                        </div>
                    </fieldset>
                </li>
                <li class="d-inline-block mb-1">
                    <fieldset>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-danger" name="customRadioColored"
                                id="customColorRadio6">
                            <label class="custom-control-label" for="customColorRadio6">@t(عصبانی)</label>
                        </div>
                    </fieldset>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="tel" class="form-control" id="mobile" required name="mobile" value="{{ old('mobile') }}"
                pattern="0[0-9]{10}"
                data-validation-pattern-message="این شماره فرمت معتبری نیست."
                data-validation-required-message="پر کردن فیلد شماره همراه اجباری است." placeholder='@t( شماره همراه)'>
            <label for="mobile">@t(شماره همراه)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="tel" class="form-control" id="mobile2" name="mobile2" value="{{ old('mobile2') }}"
                pattern="0[0-9]{10}"
                data-validation-pattern-message="این شماره فرمت معتبری نیست." placeholder='@t(شماره کاری)'>
            <label for="mobile2">@t(شماره کاری)</label>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <div class="controls form-label-group ">
            <input type="tel" class="form-control" id="phone" name="phone" pattern="0[\u06F0-\u06F90-9]{10}"
                value="{{ old('phone') }}" data-validation-pattern-message="این شماره فرمت معتبری نیست."
                placeholder='@t(شماره ثابت)'>
            <label for="phone">@t(شماره ثابت)</label>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <div class="controls form-label-group ">
            <textarea class="form-control" id="note" name="note"
                placeholder='@t(توضیحات)'>{{ old('note') }}</textarea>
            <label for="note">@t(توضیحات)</label>
        </div>
    </div>
</div>

<div class="col-12">
    <h3>نرخ نامه های مشاور</h3>
</div>
@foreach ($price_lists as $price_list)
    <div class="col-lg-3 col-md-4 col-sm-6">
        <fieldset>
            <div class="checkbox">
                <input name="price_list[]" value="{{ $price_list->id }}" type="checkbox" class="checkbox-input"
                    id="price_list_{{ $price_list->id }}">
                <label for="price_list_{{ $price_list->id }}">{{ $price_list->name }}</label>
            </div>
        </fieldset>
    </div>
@endforeach
<div class="col-12">
    <h3>تخصص های مشاور</h3>
</div>
@foreach ($categories as $category)
    <div class="col-lg-3 col-md-4 col-sm-6">
        <fieldset>
            <div class="checkbox">
                <input name="category[]" value="{{ $category->id }}" type="checkbox" class="checkbox-input"
                    id="category_{{ $category->id }}">
                <label for="category_{{ $category->id }}">{{ $category->name }}</label>
            </div>
        </fieldset>
    </div>
@endforeach
{{-- <form>

</form>

<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">آپلود فایل چندتایی</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <form action="{{ route('admin.dropzone_upload') }}" class="dropzone dropzone-area" method="POST"
                    id="dpz-multiple-files">
                    @csrf
                    <div class="dz-message">فایل های خود را برای ارسال به اینجا
                        بکشید</div>
                </form>
            </div>
        </div>
    </div>
</div> --}}
