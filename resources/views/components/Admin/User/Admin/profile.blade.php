<x-base>
    <x-slot name='title'>
        @t(پروفایل){{ ' '.$user->name . ' ' . $user->family }}
    </x-slot>
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/extensions/swiper.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/page-user-profile.css">
        <!-- END: Page CSS-->
    </x-slot>

    <div class="content-header row">
      <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
              <div class="col-12">
                  <h5 class="content-header-title float-left pr-1">@t(پروفایل کاربر)</h5>
                  <div class="breadcrumb-wrapper">
                      <ol class="breadcrumb p-0 mb-0">
                          <li class="breadcrumb-item">
                              <a href="{{ route('admin.home') }}">
                                  <i class="bx bx-home-alt">
                                  </i>
                              </a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="{{ route('admin.index') }}">@t(لیست کاربران)</a>
                          </li>
                          <li class="breadcrumb-item">
                              <a href="#"> @t(پروفایل کاربر)</a>
                          </li>
                          <li class="breadcrumb-item active">{{ $user->name . ' ' . $user->family }}</li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <section class="page-user-profile">
    <div class="row">
      <div class="col-12">
        <!-- user profile heading section start -->
        <div class="card">
          <div class="card-content">
            <div style="background-color: #4971f5;">
                <!-- user profile image -->
                @php
                  if ($user->pic){
                    $avatar = $user->pic;
                  }else{
                  $avatar = 'avatar.jpg';
                  };
                @endphp
                <img src="../../assets/images/profile/admin/avatar/{{ $avatar }}" class="rounded mt-25 ml-25 mb-25" alt="user profile image" height="140" width="140">
            </div>
            <div class="user-profile-text">
              <h4 class="mb-0 text-bold-500 profile-text-color">{{ $user->name . ' ' . $user->family }}</h4>
              <small>@t(کاربر)</small>
            </div>
            <!-- user profile nav tabs start -->
            <div class="card-body px-0">
              <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 active align-items-center" id="profile-tab" data-toggle="tab" href="#profile" aria-controls="profile" role="tab" aria-selected="true"><i class="bx bx-copy-alt"></i><span class="d-none d-md-block">پروفایل</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="activity-tab" data-toggle="tab" href="#activity" aria-controls="activity" role="tab" aria-selected="false"><i class="bx bx-user"></i><span class="d-none d-md-block">فعالیت</span></a>
                </li>
                <li class="nav-item pb-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="friends-tab" data-toggle="tab" href="#friends" aria-controls="friends" role="tab" aria-selected="false"><i class="bx bx-message-alt"></i><span class="d-none d-md-block">دوستان</span></a>
                </li>
                <li class="nav-item pb-0 mr-0">
                  <a class="nav-link d-flex px-1 align-items-center" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="false"><i class="bx bx-copy-alt"></i><span class="d-none d-md-block">خوراک</span></a>
                </li>
              </ul>
            </div>
            <!-- user profile nav tabs ends -->
          </div>
        </div>
        <!-- user profile heading section ends -->
  
        <!-- user profile content section start -->
        <div class="row">
          <!-- user profile nav tabs content start -->
          <div class="col-lg-9">
            <div class="tab-content">
                <div class="tab-pane active" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                    <!-- user profile nav tabs profile start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-12">
                              <div class="row">
                                <div class="col-12 col-sm-3 text-center mb-1 mb-sm-0">
                                  <img src="../../assets/images/profile/admin/avatar/{{ $avatar }}" class="rounded" alt="group image" height="120" width="120">
                                </div>
                                <div class="col-12 col-sm-9">
                                  <div class="row">
                                    <div class="col-12 text-center text-sm-left">
                                        <h6 class="media-heading mb-0 primary-font">{{ $user->name . ' ' . $user->family }}<i class="cursor-pointer bx bxs-star text-warning ml-50 align-middle"></i></h6>
                                        <small class="text-muted align-top line-height-2">@t(علت مراجعه):{{ ' '.$user->categories_id . ' ' . $user->family }}</small>
                                    </div>
                                    <div class="col-12 text-center text-sm-left">
                                        <div class="mb-1 mt-50 line-height-2">
                                            <span class="mr-1">122 <small>@t(جلسه مشاوره داشته)</small></span>
                                            <span class="mr-1">4.7k <small>@t(کارگاه داشته)</small></span>
                                            <span class="mr-1"><small>@t(از)</small> 1399/08/12 <small>@t(ارتباط داشته)</small></span>
                                            
                                        </div>
                                        <p>{{ $user->note }}</p>
                                        <?php
                                          if ($user->gender == "مرد") {
                                            $icon = "bx bx-male";
                                            $gender = "جنسیت:مذکر";
                                          } else {
                                            $icon = "bx bx-female";
                                            $gender = "جنسیت:مونث";
                                          };
  
                                          if ($user->status == "فعال") {
                                            $statusicon = "bx bx-check-circle";
                                            $statustxt = "وضعیت این کاربر فعال است.";
                                          } else {
                                            $statusicon = "bx bx-x-circle";
                                            $statustxt = "وضعیت این کاربر غیر فعال است.";
                                          }
                                          
                                          ?>

                                        <div>
                                        <div class="badge badge-primary badge-round mr-1 mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $gender }}"><i class="cursor-pointer {{ $icon }}"></i>
                                        </div>
                                        <div class="badge badge-light-warning badge-round mr-1 mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $statustxt }}"><i class="{{ $statusicon }}"></i>
                                        </div>
                                        <div class="badge badge-light-success badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->mobile }}"><i class="cursor-pointer bx bx-mobile"></i>
                                        </div>
                                        <?php
                                        if ($user->mobile2) { ?>
                                        <div class="badge badge-light-danger badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->mobile2 }}"><i class="cursor-pointer bx bx-mobile-alt"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->phone) { ?>
                                        <div class="badge badge-light-info badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->phone }}"><i class="cursor-pointer bx bx-phone"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->instagram) { ?>
                                        <div class="badge badge-primary badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="{{ $user->instagram }}"><i class="cursor-pointer bx bxl-instagram"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->account_number) { ?>
                                        <div class="badge badge-light-primary badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="شماره حساب بانکی:{{ $user->account_number }}"><i class="cursor-pointer bx bx-credit-card"></i>
                                        </div>
                                        <?php
                                        }
                                        if ($user->old_file_number) { ?>
                                        <div class="badge badge-light-warning badge-round mb-1" data-toggle="tooltip" data-placement="bottom" title="شماره پرونده قدیم:{{ $user->old_file_number }}"><i class="cursor-pointer bx bx-folder-open"></i>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                        
                                      </div>
                                      <button class="btn btn-sm d-none d-sm-block float-right btn-light-primary">
                                        <i class="cursor-pointer bx bx-edit font-small-3 mr-50"></i><span>ویرایش</span>
                                      </button>
                                      <button class="btn btn-sm d-block d-sm-none btn-block text-center btn-light-primary">
                                        <i class="cursor-pointer bx bx-edit font-small-3 mr-25"></i><span>ویرایش</span></button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                            <h5 class="card-title">@t(اطلاعات کامل)</h5>
                            <ul class="list-unstyled line-height-2">
                              <li class="mb-1"><i class="cursor-pointer bx bx-mobile align-middle mr-50"></i>{{ $user->mobile }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-mobile-alt align-middle mr-50"></i>{{ $user->mobile2 }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-folder-open align-middle mr-50" data-toggle="tooltip" data-placement="top" title="شماره پرونده قدیم"></i>{{ $user->old_file_number }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-map align-middle mr-50"></i>{{ $user->address }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-phone-call align-middle mr-50"></i>{{ $user->phone }}</li>
                              <li class="mb-1"><i class="cursor-pointer bx bx-envelope align-middle mr-50"></i>{{ $user->email }}</li>
                            </ul>
                            <div class="row">
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام)</small></h6>
                                <p><span class="ltr-text">{{ $user->name }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام خانوادگی) </small></h6>
                                <p><span class="ltr-text">{{ $user->family }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ تولد)</small></h6>
                                <p>{{ $user->date_of_birth }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام پدر)</small></h6>
                                <p>{{ $user->father_name }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(شماره شناسنامه)</small></h6>
                                <p>{{ $user->id_number }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(کد ملی)</small></h6>
                                <p>{{ $user->national_code }}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(آدرس شبکه اجتماعی، اینستاگرام)</small></h6>
                                <p><span class="ltr-text">{{ $user->instagram }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(شماره حساب بانکی)</small></h6>
                                <p><span class="ltr-text">{{ $user->account_number }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(وضعیت تاهل)</small></h6>
                                <p>{{ $user->marital_status_id}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نام همسر)</small></h6>
                                <p>{{ $user->spouse_name}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ ازدواج)</small></h6>
                                <p>{{ $user->date_of_marrige}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ جدایی)</small></h6>
                                <p>{{ $user->date_of_divorce}}</p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تاریخ تولد همسر)</small></h6>
                                <p><span class="ltr-text">{{ $user->spouse_date_of_birth }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(تعداد فرزند)</small></h6>
                                <p><span class="ltr-text">{{ $user->number_of_children }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(اطلاعات فرزندان)</small></h6>
                                <p><span class="ltr-text">{{ $user->children_information }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(شغل)</small></h6>
                                <p><span class="ltr-text">{{ $user->job }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(سطح در آمد)</small></h6>
                                <p><span class="ltr-text">{{ $user->Income_level }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(بهترین زمان تماس)</small></h6>
                                <p><span class="ltr-text">{{ $user->best_time_to_call }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(فکر می کنید در چه زمینه ای نیاز به مهارت و آموزش دارید؟ )</small></h6>
                                <p><span class="ltr-text">{{ $user->categories_id }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(چه ساعتی به فضای مجازی وصل می شوید؟)</small></h6>
                                <p><span class="ltr-text">{{ $user->time_to_use_internet }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(روش مناسب برای ارتباط)</small></h6>
                                <p><span class="ltr-text">{{ $user->connection_method_id }}</span></p>
                              </div>
                              <div class="col-4">
                                <h6 class="mb-0"><small class="text-muted">@t(نحوه آشنایی)</small></h6>
                                <p><span class="ltr-text">{{ $user->Introduction_method_id }}</span></p>
                              </div>
                              <div class="col-12">
                                <h6 class="mb-0"><small class="text-muted">@t(توضیحات)</small></h6>
                                <p>{{ $user->note }}</p>
                              </div>
                            </div>
                            <button class="btn btn-sm d-none d-sm-block float-right btn-light-primary mb-2">
                              <i class="cursor-pointer bx bx-edit font-small-3 mr-50"></i><span>ویرایش</span>
                            </button>
                            <button class="btn btn-sm d-block d-sm-none btn-block text-center btn-light-primary">
                              <i class="cursor-pointer bx bx-edit font-small-3 mr-25"></i><span>ویرایش</span></button>
                          </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs profile ends -->
                  </div>
              <div class="tab-pane" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                <!-- user profile nav tabs feed start -->
                <div class="row">
                  <!-- user profile nav tabs feed left section start -->
                  <div class="col-lg-4">
                    <!-- user profile nav tabs feed left section info card start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <h5 class="card-title mb-1">اطلاعات
                            <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                          </h5>
                          <ul class="list-unstyled mb-0 line-height-2">
                            <li class="d-flex align-items-center mb-25">
                              <i class="bx bx-briefcase mr-50 cursor-pointer"></i><span>طراح UX در <a href="JavaScript:void(0);">گوگل</a></span>
                            </li>
                            <li class="d-flex align-items-center mb-25">
                              <i class="bx bx-briefcase mr-50 cursor-pointer"></i> <span>طراح UI سابق در<a href="JavaScript:void(0);">مایکروسافت</a></span>
                            </li>
                            <li class="d-flex align-items-center mb-25">
                              <i class="bx bx-receipt mr-50 cursor-pointer"></i> <span>تحصیل کرده <a href="JavaScript:void(0);">علوم کامپیوتر</a> در<a href="JavaScript:void(0);">تورنتو</a></span>
                            </li>
                            <li class="d-flex align-items-center mb-25">
                              <i class="bx bx-receipt mr-50 cursor-pointer"></i><span>تحصیل کرده در <a href="JavaScript:void(0);">دانشگاه صنعتی شریف</a></span>
                            </li>
                            <li class="d-flex align-items-center">
                              <i class="bx bx-rss mr-50 cursor-pointer"></i> <span>دنبال شده توسط <a href="JavaScript:void(0);">338 نفر</a></span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed left section info card ends -->
                    <!-- user profile nav tabs feed left section trending card start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <h5 class="card-title mb-1">موضوعات داغ<i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i></h5>
                          <ul class="list-unstyled mb-0 line-height-2">
                            <li class="d-flex mb-25">
                              <i class="cursor-pointer bx bx-trending-up text-primary mr-50 mt-25"></i><span>
                                <a href="JavaScript:void(0);" class="mr-50">لورم ایپسوم</a> لورم ایپسوم متن ساختگی با
                              </span>
                            </li>
                            <li class="d-flex mb-25">
                              <i class="cursor-pointer bx bx-trending-up text-primary mr-50 mt-25"></i><span>
                                <a href="JavaScript:void(0);" class="mr-50">لورم ایپسوم</a> لورم ایپسوم متن ساختگی با</span>
                            </li>
                            <li class="d-flex mb-25">
                              <i class="cursor-pointer bx bx-trending-up text-primary mr-50 mt-25"></i><span>
                                <a href="JavaScript:void(0);" class="mr-50">لورم ایپسوم</a> لورم ایپسوم متن ساختگی با
                              </span>
                            </li>
                            <li class="d-flex mb-25">
                              <i class="cursor-pointer bx bx-trending-up text-primary mr-50 mt-25"></i><span>
                                <a href="JavaScript:void(0);" class="mr-50">لورم ایپسوم متن</a> لورم ایپسوم متن ساختگی با</span>
                            </li>
                            <li class="d-flex">
                              <i class="cursor-pointer bx bx-trending-up text-primary mr-50 mt-25"></i><span>
                                <a href="JavaScript:void(0);" class="mr-50">لورم ایپسوم</a> لورم ایپسوم متن ساختگی با</span>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed left section trending card ends -->
                    <!-- user profile nav tabs feed left section like page card start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <h6><img src="../../assets/images/profile/pages/pixinvent.jpg" class="mr-25 round" alt="logo" height="28">
                            پروفایل اجتماعی<span class="text-muted"> (صفحه)</span>
                            <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i></h6>
                          <div class="mb-1 font-small-2">
                            <i class="cursor-pointer bx bxs-star text-warning align-middle"></i>
                            <i class="cursor-pointer bx bxs-star text-warning align-middle"></i>
                            <i class="cursor-pointer bx bxs-star text-warning align-middle"></i>
                            <i class="cursor-pointer bx bxs-star text-warning align-middle"></i>
                            <i class="cursor-pointer bx bx-star text-muted align-middle"></i>
                            <span class="ml-50 text-muted text-bold-500 line-height-2 align-middle">4.6 (142 نقد و بررسی)</span>
                          </div>
                          <div class="d-flex">
                            <button class="btn btn-sm btn-light-primary d-flex mr-50"><i class="cursor-pointer bx bx-like font-small-3 mb-25 mr-sm-25"></i><span class="d-none d-sm-block">Like</span></button>
                            <button class="btn btn-sm btn-light-primary d-flex"><i class="cursor-pointer bx bx-share-alt font-small-3 mb-25 mr-sm-25"></i><span class="d-none d-sm-block">اشتراک گذاری</span></button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed left section like page card ends -->
                    <!-- user profile nav tabs feed left section today's events card start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <h5 class="card-title mb-1">رویداد های امروز<i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                          </h5>
                          <div class="user-profile-event">
                            <div class="pb-1 d-flex align-items-center">
                              <i class="cursor-pointer bx bx-radio-circle-marked text-primary mr-25"></i>
                              <small>10:00 ق.ظ</small>
                            </div>
                            <h6 class="text-bold-500 font-small-3 primary-font">لورم ایپسوم متن ساختگی با</h6>
                            <p class="text-muted font-small-2">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک</p>
                            <i class="cursor-pointer bx bx-map text-muted align-middle"></i>
                            <span class="text-muted"><small>تبریز / ایران</small></span>
                            <!-- user profile likes avatar start -->
                            <ul class="list-unstyled users-list d-flex align-items-center mt-1">
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="بری الن" class="avatar pull-up">
                                <img src="../../assets/images/portrait/small/avatar-s-21.jpg" alt="Avatar" height="30" width="30">
                              </li>
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                <img src="../../assets/images/portrait/small/avatar-s-22.jpg" alt="Avatar" height="30" width="30">
                              </li>
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                <img src="../../assets/images/portrait/small/avatar-s-23.jpg" alt="Avatar" height="30" width="30">
                              </li>
                              <li class="pl-50 text-muted font-small-3">
                                +10 بیشتر
                              </li>
                            </ul>
                            <!-- user profile likes avatar ends -->
                          </div>
                          <hr>
                          <div class="user-profile-event">
                            <div class="pb-1 d-flex align-items-center">
                              <i class="cursor-pointer bx bx-radio-circle-marked text-primary mr-25"></i>
                              <small>10:00 ب.ظ</small>
                            </div>
                            <h6 class="text-bold-500 font-small-3 primary-font">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم</h6>
                          </div>
                          <hr>
                          <div class="user-profile-event">
                            <div class="pb-1 d-flex align-items-center">
                              <i class="cursor-pointer bx bx-radio-circle-marked text-primary mr-25"></i>
                              <small>6:00 ق.ظ</small>
                            </div>
                            <div class="pb-1">
                              <h6 class="text-bold-500 font-small-3 primary-font">لورم ایپسوم متن ساختگی با</h6>
                              <i class="cursor-pointer bx bx-map text-muted align-middle"></i>
                              <span class="text-muted"><small>تبریز / ایران</small></span>
                            </div>
                          </div>
                          <button class="btn btn-block btn-secondary">بررسی همه رویداد ها</button>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed left section today's events card ends -->
                  </div>
                  <!-- user profile nav tabs feed left section ends -->
                  <!-- user profile nav tabs feed middle section start -->
                  <div class="col-lg-8">
                    <!-- user profile nav tabs feed middle section post card start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <!-- user profile middle section blogpost nav tabs card start -->
                          <ul class="nav nav-tabs justify-content-center justify-content-sm-start border-bottom-0" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active d-flex" id="user-status-tab" data-toggle="tab" href="#user-status" aria-controls="user-status" role="tab" aria-selected="true"><i class="bx bx-detail align-text-top"></i>
                                <span class="d-none d-md-block">وضعیت</span>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link d-flex" id="multimedia-tab" data-toggle="tab" href="#user-multimedia" aria-controls="user-multimedia" role="tab" aria-selected="false"><i class="bx bx-movie align-text-top"></i>
                                <span class="d-none d-md-block">چندرسانه ای</span>
                              </a>
                            </li>
                            <li class="nav-item mr-0">
                              <a class="nav-link d-flex" id="blog-tab" data-toggle="tab" href="#user-blog" aria-controls="user-blog" role="tab" aria-selected="false"><i class="bx bx-chat align-text-top"></i>
                                <span class="d-none d-md-block">مطلب بلاگ</span>
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content pl-0">
                            <div class="tab-pane active" id="user-status" aria-labelledby="user-status-tab" role="tabpanel">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group row">
                                    <div class="col-sm-1 col-2">
                                      <div class="avatar">
                                        <img src="../../assets/images/portrait/small/avatar-s-2.jpg" alt="user image" width="32" height="32">
                                        <span class="avatar-status-online"></span>
                                      </div>
                                    </div>
                                    <div class="col-sm-11 col-10">
                                      <textarea class="form-control border-0 shadow-none" id="user-post-textarea" rows="3" placeholder="هر چیزی که در ذهن دارید را به اشتراک بگذارید ..."></textarea>
                                    </div>
                                  </div>
                                  <hr>
                                  <div class="card-footer p-0">
                                    <i class="cursor-pointer bx bx-camera font-medium-5 text-muted mr-1 pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="ارسال تصویر"></i>
                                    <i class="cursor-pointer bx bx-face font-medium-5 text-muted mr-1 pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="تگ کردن دوستان"></i>
                                    <i class="cursor-pointer bx bx-map font-medium-5 text-muted pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="اشتراک گذاری مکان"></i>
                                    <span class=" float-sm-right d-flex flex-sm-row flex-column justify-content-end">
                                      <button class="btn btn-light-primary mr-0 my-1 my-sm-0 mr-sm-1">پیش‌نمایش</button>
                                      <button class="btn btn-primary">ارسال وضعیت</button>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="user-multimedia" aria-labelledby="multimedia-tab" role="tabpanel">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group row">
                                    <div class="col-sm-1 col-2">
                                      <div class="avatar">
                                        <img src="../../assets/images/portrait/small/avatar-s-2.jpg" alt="user image" width="32" height="32">
                                        <span class="avatar-status-online"></span>
                                      </div>
                                    </div>
                                    <div class="col-sm-11 col-10">
                                      <textarea class="form-control border-0 shadow-none" id="user-postmulti-textarea" rows="3" placeholder="هر چیزی که در ذهن دارید را به اشتراک بگذارید ..."></textarea>
                                    </div>
                                  </div>
                                  <hr>
                                  <div class="card-footer p-0">
                                    <i class="cursor-pointer bx bx-camera font-medium-5 text-muted mr-1 pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="ارسال تصویر"></i>
                                    <i class="cursor-pointer bx bx-face font-medium-5 text-muted mr-1 pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="تگ کردن دوستان"></i>
                                    <i class="cursor-pointer bx bx-map font-medium-5 text-muted pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="اشتراک گذاری مکان"></i>
                                    <span class=" float-sm-right d-flex flex-sm-row flex-column justify-content-end">
                                      <button class="btn btn-light-primary mr-0 my-1 my-sm-0 mr-sm-1">پیش‌نمایش</button>
                                      <button class="btn btn-primary">ارسال وضعیت</button>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="user-blog" aria-labelledby="blog-tab" role="tabpanel">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group row">
                                    <div class="col-sm-1 col-2">
                                      <div class="avatar">
                                        <img src="../../assets/images/portrait/small/avatar-s-2.jpg" alt="user image" width="32" height="32">
                                        <span class="avatar-status-online"></span>
                                      </div>
                                    </div>
                                    <div class="col-sm-11 col-10">
                                      <textarea class="form-control border-0 shadow-none" id="user-postblog-textarea" rows="3" placeholder="هر چیزی که در ذهن دارید را به اشتراک بگذارید ..."></textarea>
                                    </div>
                                  </div>
                                  <hr>
                                  <div class="card-footer p-0">
                                    <i class="cursor-pointer bx bx-camera font-medium-5 text-muted mr-1 pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="ارسال تصویر"></i>
                                    <i class="cursor-pointer bx bx-face font-medium-5 text-muted mr-1 pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="تگ کردن دوستان"></i>
                                    <i class="cursor-pointer bx bx-map font-medium-5 text-muted pt-50" data-toggle="tooltip" data-popup="tooltip-custom" data-placement="top" title="اشتراک گذاری مکان"></i>
                                    <span class=" float-sm-right d-flex flex-sm-row flex-column justify-content-end">
                                      <button class="btn btn-light-primary mr-0 my-1 my-sm-0 mr-sm-1">پیش‌نمایش</button>
                                      <button class="btn btn-primary">ارسال وضعیت</button>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- user profile middle section blogpost nav tabs card ends -->
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed middle section post card ends -->
                    <!-- user profile nav tabs feed middle section user-1 card starts -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-header user-profile-header">
                          <div class="avatar mr-50 align-top mt-50">
                            <img src="../../assets/images/portrait/small/avatar-s-10.jpg" alt="user avatar" width="32" height="32">
                            <span class="avatar-status-online"></span>
                          </div>
                          <div class="d-inline-block">
                            <h6 class="mb-0 text-bold-500 mb-n25 primary-font">امیلیا کلارک <span class="text-bold-400">یک <a href="JavaScript:void(0);">لینک</a> اشتراک گذاری کرد</span></h6>
                            <p class="text-muted"><small>7 ساعت پیش</small></p>
                          </div>
                          <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                        </div>
                        <div class="card-body py-0">
                          <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                          <div class="d-flex border rounded">
                            <div class="user-profile-images"><img src="../../assets/images/banner/banner-29.jpg" alt="post" class="img-fluid user-profile-card-image">
                            </div>
                            <div class="p-1">
                              <h5>لورم ایپسوم متن ساختگی</h5>
                              <p class="user-profile-ellipsis">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در</p>
                            </div>
                          </div>
                        </div>
                        <div class="card-footer d-flex justify-content-between pt-1">
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-heart user-profile-like font-medium-4"></i>
                            <p class="mb-0 ml-25">18</p>
                            <!-- user profile likes avatar start -->
                            <div class="d-none d-sm-block">
                              <ul class="list-unstyled users-list m-0 d-flex align-items-center ml-1">
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="بری الن" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-21.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-22.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-23.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li class="d-inline-block pl-50">
                                  <p class="text-muted mb-0 font-small-3">+10 بیشتر</p>
                                </li>
                              </ul>
                            </div>
                            <!-- user profile likes avatar ends -->
                          </div>
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-comment-dots font-medium-4"></i>
                            <span class="ml-25">52</span>
                            <i class="cursor-pointer bx bx-share-alt font-medium-4 ml-1"></i>
                            <span class="ml-25">22</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed middle section user-1 card ends -->
                    <!-- user profile nav tabs feed middle section story swiper start -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">
                          <h5 class="card-title mb-0">داستان ها</h5>
                          <div class="user-profile-stories swiper-container pt-1">
                            <div class="swiper-wrapper user-profile-images">
                              <div class="swiper-slide">
                                <img src="../../assets/images/profile/portraits/avatar-portrait-1.jpg" class="rounded user-profile-stories-image" alt="story image">
                                <div class="card-img-overlay bg-overlay">
                                  <div class="user-swiper-text">لورم ایپسوم</div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <img src="../../assets/images/profile/portraits/avatar-portrait-2.jpg" class="rounded user-profile-stories-image" alt="story image">
                                <div class="card-img-overlay bg-overlay">
                                  <div class="user-swiper-text">لورم ایپسوم</div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <img src="../../assets/images/profile/portraits/avatar-portrait-3.jpg" class="rounded user-profile-stories-image" alt="story image">
                                <div class="card-img-overlay bg-overlay">
                                  <div class="user-swiper-text">لورم ایپسوم متن</div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <img src="../../assets/images/profile/portraits/avatar-portrait-4.jpg" class="rounded user-profile-stories-image" alt="story image">
                                <div class="card-img-overlay bg-overlay">
                                  <div class="user-swiper-text">لورم ایپسوم</div>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <img src="../../assets/images/profile/portraits/avatar-portrait-5.jpg" class="rounded user-profile-stories-image" alt="story image">
                                <div class="card-img-overlay bg-overlay">
                                  <div class="user-swiper-text">لورم ایپسوم</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed middle section story swiper ends -->
                    <!-- user profile nav tabs feed middle section user-2 card starts -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-header user-profile-header">
                          <div class="avatar mr-50 align-top mt-50">
                            <img src="../../assets/images/portrait/small/avatar-s-11.jpg" alt="avtar image" width="32" height="32">
                            <span class="avatar-status-offline"></span>
                          </div>
                          <div class="d-inline-block">
                            <h6 class="mb-0 text-bold-500 mb-n25">جانی اینگلیش</h6>
                            <p class="text-muted"><small>2 ساعت پیش</small></p>
                          </div>
                          <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                        </div>
                        <div class="card-body py-0">
                          <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای</p>
                        </div>
                        <div class="card-footer d-flex justify-content-between pb-0">
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-heart user-profile-like font-medium-4"></i>
                            <p class="mb-0 ml-25">49</p>
                            <!-- user profile likes avatar start -->
                            <div class="d-none d-sm-block">
                              <ul class="list-unstyled users-list m-0 d-flex align-items-center ml-1">
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="بری الن" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-24.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-25.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-26.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li class="d-inline-block pl-50">
                                  <p class="text-muted mb-0 font-small-3">+10 بیشتر</p>
                                </li>
                              </ul>
                            </div>
                            <!-- user profile likes avatar ends -->
                          </div>
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-comment-dots font-medium-4"></i>
                            <span class="ml-25">45</span>
                            <i class="cursor-pointer bx bx-share-alt font-medium-4 ml-1"></i>
                            <span class="ml-25">1</span>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <!-- user profile comments start -->
                      <div class="card-header user-profile-header pt-0">
                        <div class="avatar mr-50 align-top mt-50">
                          <img src="../../assets/images/portrait/small/avatar-s-12.jpg" alt="avtar image" width="32" height="32">
                          <span class="avatar-status-away"></span>
                        </div>
                        <div class="d-inline-block">
                          <h6 class="mb-0 text-bold-500 font-small-3 mb-n25">آنجلینا جولی</h6>
                          <p class="text-muted"><small>24 دقیقه پیش</small></p>
                        </div>
                        <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                      </div>
                      <div class="card-body py-0">
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان</p>
                      </div>
                      <div class="card-footer user-comment-footer pb-0">
                        <i class="cursor-pointer bx bx-heart user-profile-like font-medium-4 align-middle"></i>
                        <span class="ml-25">30</span>
                        <span class="ml-1">پاسخ</span>
                      </div>
                      <hr>
                      <div class="card-header user-profile-header pt-0">
                        <div class="avatar mr-50 align-top mt-50">
                          <img src="../../assets/images/portrait/small/avatar-s-13.jpg" alt="avtar images" width="32" height="32">
                          <span class="avatar-status-busy"></span>
                        </div>
                        <div class="d-inline-block">
                          <h6 class="mb-0 text-bold-500 font-small-3 mb-n25">پیتر پارکر</h6>
                          <p class="text-muted"><small>1 ساعت پیش</small></p>
                        </div>
                        <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                      </div>
                      <div class="card-body py-0">
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                      </div>
                      <div class="card-footer user-comment-footer pb-0">
                        <i class="cursor-pointer bx bx-heart user-profile-like font-medium-4 align-middle"></i>
                        <span class="ml-25">80</span>
                        <span class="ml-1">پاسخ</span>
                      </div>
                      <hr>
                      <div class="form-group row align-items-center px-1">
                        <div class="col-2 col-sm-1">
                          <div class="avatar">
                            <img src="../../assets/images/portrait/small/avatar-s-2.jpg" alt="avtar images" width="32" height="32">
                            <span class="avatar-status-online"></span>
                          </div>
                        </div>
                        <div class="col-sm-11 col-10">
                          <textarea class="form-control" id="user-comment-textarea" rows="1" placeholder="دیدگاه ..."></textarea>
                        </div>
                      </div>
                      <!-- user profile comments ends -->
                    </div>
                    <!-- user profile nav tabs feed middle section user-2 card ends -->
                    <!-- user profile nav tabs feed middle section user-3 card starts -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-header user-profile-header">
                          <div class="avatar mr-50 align-top mt-50">
                            <img src="../../assets/images/portrait/small/avatar-s-14.jpg" alt="avtar images" width="32" height="32">
                            <span class="avatar-status-online"></span>
                          </div>
                          <div class="d-inline-block">
                            <h6 class="mb-0 text-bold-500 mb-n25">استیو راجرز</h6>
                            <p class="text-muted"><small>7 ساعت پیش</small></p>
                          </div>
                          <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                        </div>
                        <div class="card-body py-0">
                          <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون</p>
                          <img src="../../assets/images/profile/post-media/2.jpg" alt="user image" class="img-fluid">
                        </div>
                        <div class="card-footer d-flex justify-content-between pt-1">
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-heart user-profile-like font-medium-4"></i>
                            <p class="mb-0 ml-25">77</p>
                            <!-- user profile likes avatar start -->
                            <div class="d-none d-sm-block">
                              <ul class="list-unstyled users-list m-0 d-flex align-items-center ml-1">
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="بری الن" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-11.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-12.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" title="اولیور کویین" class="avatar pull-up">
                                  <img src="../../assets/images/portrait/small/avatar-s-13.jpg" alt="Avatar" height="30" width="30">
                                </li>
                                <li class="d-inline-block pl-50">
                                  <p class="text-muted mb-0 font-small-3">+10 بیشتر</p>
                                </li>
                              </ul>
                            </div>
                            <!-- user profile likes avatar ends -->
                          </div>
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-comment-dots font-medium-4"></i>
                            <span class="ml-25">12</span>
                            <i class="cursor-pointer bx bx-share-alt font-medium-4 ml-1"></i>
                            <span class="ml-25">12</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed middle section user-3 card ends -->
                    <!-- user profile nav tabs feed middle section user-4 card starts -->
                    <div class="card">
                      <div class="card-content">
                        <div class="card-header user-profile-header">
                          <div class="avatar mr-50 align-top mt-50">
                            <img src="../../assets/images/portrait/small/avatar-s-18.jpg" alt="avtar images" width="32" height="32">
                            <span class="avatar-status-online"></span>
                          </div>
                          <div class="d-inline-block">
                            <h6 class="mb-0 text-bold-500 mb-n25">استیو راجرز</h6>
                            <p class="text-muted"><small>21 ساعت پیش</small></p>
                          </div>
                          <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                        </div>
                        <div class="card-body py-0">
                          <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای</p>
                        </div>
                        <div class="card-footer d-flex justify-content-between pt-1">
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-heart user-profile-like font-medium-4"></i>
                            <p class="mb-0 ml-25">0</p>
                          </div>
                          <div class="d-flex align-items-center">
                            <i class="cursor-pointer bx bx-comment-dots font-medium-4"></i>
                            <span class="ml-25">0</span>
                            <i class="cursor-pointer bx bx-share-alt font-medium-4 ml-1"></i>
                            <span class="ml-25">2</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- user profile nav tabs feed middle section user-4 card ends -->
                  </div>
                  <!-- user profile nav tabs feed middle section ends -->
                </div>
                <!-- user profile nav tabs feed ends -->
              </div>
              <div class="tab-pane " id="activity" aria-labelledby="activity-tab" role="tabpanel">
                <!-- user profile nav tabs activity start -->
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <!-- timeline widget start -->
                      <ul class="widget-timeline">
                        <li class="timeline-items timeline-icon-success active">
                          <div class="timeline-time">سه شنبه 8:17 ب.ظ</div>
                          <h6 class="timeline-title">امیلیا کلارک</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">لورم ایپسوم متن</a></p>
                          <div class="timeline-content">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-primary active">
                          <div class="timeline-time">5 روز پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن ساختگی با تولید</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">نام پروژه</a></p>
                          <div class="timeline-content">
                            <img src="../../assets/images/icon/sketch.png" alt="document" height="36" width="27" class="mr-50">پوشه اطلاعات
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-danger active">
                          <div class="timeline-time">7 ساعت پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن ساختگی</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">نام پروژه</a></p>
                          <div class="timeline-content">
                            <img src="../../assets/images/icon/pdf.png" alt="document" height="36" width="27" class="mr-50">Received.pdf
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-info active">
                          <div class="timeline-time">5 ساعت پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن ساختگی با تولید سادگی</h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">لورم ایپسوم متن</a></p>
                          <div class="timeline-content">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان
                          </div>
                        </li>
                        <li class="timeline-items timeline-icon-warning">
                          <div class="timeline-time">2 دقیقه پیش</div>
                          <h6 class="timeline-title">لورم ایپسوم متن </h6>
                          <p class="timeline-text">در <a href="JavaScript:void(0);">پسندیده شده</a></p>
                          <div class="timeline-content">
                            لورم ایپسوم متن
                          </div>
                        </li>
                      </ul>
                      <!-- timeline widget ends -->
                      <div class="text-center">
                        <button class="btn btn-primary">مشاهده همه فعالیت ها</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- user profile nav tabs activity start -->
              </div>
              <div class="tab-pane" id="friends" aria-labelledby="friends-tab" role="tabpanel">
                <!-- user profile nav tabs friends start -->
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <h5>دوستان</h5>
                      <div class="row">
                        <div class="col-6">
                          <ul class="list-unstyled mb-0">
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-2.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-online"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">استیو راجرز</a></h6>
                                <small class="text-muted">گرافیست</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-3.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-offline"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">اولیور کویین</a></h6>
                                <small class="text-muted">توسعه دهنده</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-4.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-busy"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">جان دیگل</a></h6>
                                <small class="text-muted">طراح</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-5.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-busy"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">استیو راجرز</a></h6>
                                <small class="text-muted">کارگر</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-5.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-away"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">لورل لنس</a></h6>
                                <small class="text-muted">وکیل</small>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div class="col-6">
                          <ul class="list-unstyled mb-0">
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-16.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-offline"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">پیتر پارکر</a></h6>
                                <small class="text-muted">دانشجو</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-7.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-busy"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">تونی استارک</a></h6>
                                <small class="text-muted">پروفسور</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-8.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-online"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">جان اسنو</a></h6>
                                <small class="text-muted">لورم ایپسوم</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-2.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-online"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">استیو راجرز</a></h6>
                                <small class="text-muted">گرافیست</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-3.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-offline"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">اولیور کویین</a></h6>
                                <small class="text-muted">توسعه دهنده</small>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <h5 class="mt-2">دوستان مشترک</h5>
                      <div class="row">
                        <div class="col-6">
                          <ul class="list-unstyled mb-0">
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-26.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-online"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">جکی چان</a></h6>
                                <small class="text-muted">شبکه</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-25.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-offline"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">لیونل مسی</a></h6>
                                <small class="text-muted">توسعه دهنده</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-24.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-busy"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">کریس رونالدو</a></h6>
                                <small class="text-muted">طراح</small>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div class="col-6">
                          <ul class="list-unstyled mb-0">
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-23.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-busy"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">جسیکا آلبا</a></h6>
                                <small class="text-muted">کارگر</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-22.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-away"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">بروس وین</a></h6>
                                <small class="text-muted">وکیل</small>
                              </div>
                            </li>
                            <li class="media my-2">
                              <a href="JavaScript:void(0);">
                                <div class="avatar mr-1">
                                  <img src="../../assets/images/portrait/small/avatar-s-21.jpg" alt="avtar images" width="32" height="32">
                                  <span class="avatar-status-offline"></span>
                                </div>
                              </a>
                              <div class="media-body">
                                <h6 class="media-heading mb-0 mt-n25 primary-font"><a href="javaScript:void(0);">پیتر پارکر</a></h6>
                                <small class="text-muted">دانشجو</small>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- user profile nav tabs friends ends -->
              </div>
              
            </div>
          </div>
          <!-- user profile nav tabs content ends -->
          <!-- user profile right side content start -->
          <div class="col-lg-3">
            <!-- user profile right side content birthday card start -->
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                    <div class="d-inline-flex align-items-center">
                      <div class="avatar mr-50">
                        <img src="../../assets/images/portrait/small/avatar-s-20.jpg" alt="avtar images" height="32" width="32">
                      </div>
                      <h6 class="mb-0 d-flex align-items-center"> امروز تولد جسیکا هست</h6>
                    </div>
                    <i class="cursor-pointer bx bx-dots-vertical-rounded float-right"></i>
                    <div class="user-profile-birthday-image text-center p-2">
                      <img class="img-fluid" src="../../assets/images/profile/pages/birthday.png" alt="image">
                    </div>
                    <div class="user-profile-birthday-footer text-center text-lg-left">
                      <p class="mb-0"><small>براش یه پیغام تبریک تولد بذار</small></p>
                      <a class="btn btn-sm btn-light-primary mt-50" href="JavaScript:void(0);">ارسال آرزو</a>
                    </div>
                  </div>
            </div>
            <!-- user profile right side content birthday card ends -->
            <!-- user profile right side content related groups start -->
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <h5 class="card-title mb-1">گروه های مرتبط
                    <i class="cursor-pointer bx bx-dots-vertical-rounded align-top float-right"></i>
                  </h5>
                  <div class="media d-flex align-items-center mb-1">
                    <a href="JavaScript:void(0);">
                      <img src="../../assets/images/banner/banner-30.jpg" class="rounded" alt="group image" height="64" width="64">
                    </a>
                    <div class="media-body ml-1">
                      <h6 class="media-heading mb-0 mt-n25 primary-font"><small>لورم ایپسوم</small></h6><small class="text-muted">2.8k عضو (7 عضو جدید)</small>
                    </div>
                    <i class="cursor-pointer bx bx-plus-circle text-primary d-flex align-items-center "></i>
                  </div>
                  <div class="media d-flex align-items-center mb-1">
                    <a href="JavaScript:void(0);">
                      <img src="../../assets/images/banner/banner-31.jpg" class="rounded" alt="group image" height="64" width="64">
                    </a>
                    <div class="media-body ml-1">
                      <h6 class="media-heading mb-0 mt-n25 primary-font"><small>لورم ایپسوم متن</small></h6><small class="text-muted">9k عضو (7 عضو جدید)</small>
                    </div>
                    <i class="cursor-pointer bx bx-plus-circle text-primary d-flex align-items-center "></i>
                  </div>
                  <div class="media d-flex align-items-center">
                    <a href="JavaScript:void(0);">
                      <img src="../../assets/images/banner/banner-32.jpg" class="rounded" alt="group image" height="64" width="64">
                    </a>
                    <div class="media-body ml-1">
                      <h6 class="media-heading mb-0 mt-n25 primary-font"><small>لورم ایپسوم متن ساختگی</small></h6><small class="text-muted">7.6k عضو</small>
                    </div>
                    <i class="cursor-pointer bx bx-lock text-muted d-flex align-items-center"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- user profile right side content related groups ends -->
            <!-- user profile right side content gallery start -->
            <div class="card">
              <div class="card-content">
                <div class="card-body">
                  <h5 class="card-title mb-1">گالری
                    <i class="cursor-pointer bx bx-dots-vertical-rounded align-top float-right"></i>
                  </h5>
                  <div class="row">
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-10.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-11.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-12.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-13.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-05.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-06.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-07.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-08.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                    <div class="col-md-4 col-6 pl-25 pr-0 pb-25">
                      <img src="../../assets/images/profile/user-uploads/user-09.jpg" class="img-fluid" alt="gallery avtar img">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- user profile right side content gallery ends -->
          </div>
          <!-- user profile right side content ends -->
        </div>
        <!-- user profile content section start -->
      </div>
    </div>
  </section>
    <x-slot name='script'>
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/extensions/swiper.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/pages/page-user-profile.js"></script>
        <!-- END: Page JS-->
    </x-slot>
</x-base>    