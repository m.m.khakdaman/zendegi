<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="container">
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-2">
                <p>
                    <b>بسمه تعالی</b>
                </p>
                <p>
                    <span class="font-medium-3">
                        @t(گزارش عملکرد){{ ' '.($admin->name ?? '').' '.($admin->family ?? '') }}<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>

                </p>
            </div>
            <div class="col-4">

                <div class="row text-center float-right">
                    <div class="mr-4 mt-2">
                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h4 class="card-title">@t(گزارش عملکرد){{ ' '.($admin->name ?? '').' '.($admin->family ?? '') }}</h4>
                <div class="card-body text-secondary">
                    <div class="card-text line-height-2">
                    <dl class="row">
                        <dt class="col-sm-5">
                            <span data-toggle="tooltip" data-placement="top" title="@t(تعداد جلسات مشاوره ای که توسط این کاربر رزرو شده است.)">
                                @t(تعداد جلسات ثبت کرده:)
                            </span>
                        </dt>
                        <dd class="col-sm-7">
                            <span  data-toggle="tooltip" data-placement="top" title="@t(کل مبلغی که می بایست بابت جلسات رزرو شده از مراجعین دریافت شود.)">
                                {{ $registred_session ?? '' }}<span class="pl-50">@t(جلسه)</span>
                            </span>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">
                            <span data-toggle="tooltip" data-placement="top" title="جلساتی که با موفقیت برگزار شده اند و به پایان رسیده اند ">
                                @t(تعداد جلساتی که بسته:)
                            </span>
                        </dt>
                        <dd class="col-sm-7">
                            <span>{{ $closed_all_session_num ?? '' }}<span class="pl-50">@t(جلسه)</span></span><br>
                            <span data-toggle="tooltip" data-placement="top" title="@t(جلساتی که این کاربر خودش رزرو کرده و خودش هم بسته)" >{{ $closed_session_num . ' ' }}@t(جلسه رزروی این کاربر)</span><br>
                            <span data-toggle="tooltip" data-placement="top" title="@t(جلساتی که دیگر کاربران رزرو کرده اند و این کاربر بسته است.)">{{ $closed_all_session_num - $closed_session_num.' ' }}@t( جلسه رزروی دیگران)</span>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">@t(تعداد جلسات لغو شده:)</dt>
                        <dd class="col-sm-7">{{ $canceled_session_num }}<span class="pl-50">@t(جلسه)</span></dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">@t(تعداد جلسات باز:)</dt>
                        <dd class="col-sm-7">
                            <span data-toggle="tooltip" data-placement="top" title="تعداد جلسات باز">{{ $open_session_num ?? ''}}<span class="pl-50">@t(جلسه)</span>
                            </span><br>

                                <span data-toggle="tooltip" data-placement="top" title=" جلسات رزرو شده ای که تاریخ برگزاری آنها هنوز فرا نرسیده است.">
                                    {{ $open_good_session_num}}<span class="pl-50">@t(جلسه)</span>
                                </span><br>
                                <span data-toggle="tooltip" data-placement="top" title="تعداد جلساتی که می بایست تا امروز بسته می شدند که ممکن است به جهت عدم تسویه حساب و یا فراموشی اپراتور باز مانده باشند. بهترین حالت صفر بودن این عدد است.">
                                    {{ $open_bad_session->count()}}<span class="pl-50">@t(جلسه)</span>
                                </span>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">
                            <span>@t(تعداد جلسات پرداختی:)</span>
                        </dt>
                        <dd class="col-sm-7">
                            <span>{{ $payed_session->count().' ' }}@t(جلسه)</span>&nbsp;<small>@t(که کل مبلغ جلسه تسویه شده است.)</small><br>
                            <span>{{ $debt_session->count().' ' }}@t(جلسه)</span>&nbsp;<small>@t(که دارای بدهکاری هسستند.)</small><br>
                            {{-- <span>{{ $payed_session_num.' ' }}@t(جلسه)</span>&nbsp;<small>@t(کل مبلغ جلسه پرداخت و تسویه شده است.)</small> --}}
                        </dd>
                    </dl>
                    {{-- <dl class="row">
                        <dt class="col-sm-5">@t(مبلغ کل فروش جلسات:)</dt>
                        <dd class="col-sm-7">{{ $session_total_amount }}</dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">@t(کل مبلغ پرداخت شده بابت جلسات:)</dt>
                        <dd class="col-sm-7">{{ $payed_session_amount }}</dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">@t(کل مبلغ تخفیفی بابت جلسات:)</dt>
                        <dd class="col-sm-7">{{ $session_discount }}</dd>
                    </dl> --}}
                    <dl class="row">
                        <dt class="col-sm-5">@t(ثبت نام مراجع در کارگاه:)</dt>
                        <dd class="col-sm-7">{{ $wsh_num.' ' }}@t(نفر، جمعاً به قیمت:){{ ' '.$wsh_amount }}</dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">@t(کل مبلغ تخفیفی بابت کارگاه:)</dt>
                        <dd class="col-sm-7">{{ $wsh_discount }}</dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">@t(تعداد تماس ثبت شده)</dt>
                        <dd class="col-sm-7 ml-auto"></dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">@t(تعداد پیگیری انجام شده)</dt>
                        <dd class="col-sm-7 ml-auto"></dd>
                    </dl>
                    {{-- <dl class="row">
                        <dt class="col-sm-5">@t(مبلغ کل فروش)</dt>
                        <dd class="col-sm-7 ml-auto">{{ $amount.' ' }}@t(تومان)</dd>
                    </dl> --}}
                    {{-- <dl class="row">
                        <dt class="col-sm-5 text-truncate">@t(توضیحات)</dt>
                        <dd class="col-sm-7"></dd>
                        </dl> --}}
                    </div>
                </div>
            </div>
            <div class="col-6">
                <h4 class="card-title">@t(نمودار دایره ای عملکرد){{ ' '.($admin->name ?? '').' '.($admin->family ?? '') }}</h4>
                <div class="card">
                    {{-- <div class="card-header">
                        <h4 class="card-title"></h4>
                    </div> --}}
                    <div class="card-content">
                        <div class="card-body">
                            <div class="height-300">
                                <canvas id="simple-pie-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>کارگاه ها
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                            
                        <h4 class="card-title">@t(جزئیات)</h4>
                            <div class="table-responsive">
                                    <table class="table-hover table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>@t(ردیف)</th>
                                                <th>@t(تاریخ ثبت)</th>
                                                <th>@t(مراجع)</th>
                                                <th>@t(مشاور)</th>
                                                <th class="text-center">وضع</th>
                                                <th>@t(تاریخ جلسه)</th>
                                                <th>لغو</th>
                                                <th class="text-center">@t(نوع)</th>
                                                <th>@t(قیمت)</th>
                                                <th>@t(تخفیف)</th>
                                                <th>@t(با تخفیف)</th>
                                                <th class="text-center">@t(پرداختی)</th>
                                                <th class="text-center">@t(بدهی)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $tmp = 0;
                                            @endphp
                                            @foreach ($lists as $list)
                                            <tr>
                                                <td class="text-center"><span data-toggle="tooltip" data-placement="top" title="{{ $list->id }}">{{ ++$tmp }}</span></td>
                                                <td>{{ verta($list->created_at)->format('Y/m/d') }}</td>
                                                <td><span class="text-secondary pl-50">{{ ($list->customer->name ?? '').' '.($list->customer->family ?? '') }}</span></td>
                                                <td>{{ $list->advisor->name.' '.$list->advisor->family }}</td>
                                                <td class="text-center">
                                                    @if ($list->session_status_id == 1)
                                                        <span class="badge badge-success">باز</span>
                                                    @else
                                                        <span class="badge badge-warning">بسته</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($list->date < date('Y-m-d') and $list->session_status_id == 1)
                                                        <span class="text-danger">{{ $list->day->date ?? '' }}</span>                                                        
                                                    @else
                                                        {{ $list->day->date ?? '' }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($list->delete_type)
                                                        <span class="badge badge-pill badge-danger">لغو</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($list->products_type_id == 1)
                                                        <span class="badge badge-light-secondary">جلسه</span>
                                                    @else                                        
                                                        <span class="badge badge-secondary">کارگاه</span>
                                                    @endif
                                                </td>
                                                <td><span class="currency">{{ $list->orginal_price ?? '' }}</span></td>
                                                <td class="text-center">
                                                    @isset($list->discount)
                                                        <span class="text-danger currency">{{ $list->discount * -1 ?? '' }}</span>
                                                    @endisset
                                                </td>
                                                <td class="text-center"><span class="currency">{{ $list->total_price }}</span></td>
                                                <td class="text-center">
                                                    <span>
                                                        {{@ ($list->total_price + $list->transactions->amount_standing) ?? ''  }}
                                                    </span>
                                                </td>
                                                <td class="text-center">
                                                    @if(@ $list->transactions->amount_standing)
                                                        <span class="badge badge-danger currency">{{ $list->transactions->amount_standing * -1 ?? '' }}</span>
                                                    @else
                                                        <span class="badge badge-light-success ">تسویه</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr class="text-bold-600 text-secondary text-center" style="height: 40px">
                                                <td></td>
                                                <td>مجموع</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    {{-- محاسبه جمع قیمت جلساتی که رزرو شده البته کنسلی ها از آن کم شده --}}
                                                    <span class="currency">
                                                        {{ $price = $lists->where('delete_type' ,0)->sum('orginal_price') ?? '' }}
                                                    </span>
                                                </td>
                                                <td>
                                                    {{-- جمع تخفیف داده شده  --}}
                                                    <span class="currency">
                                                        {{ $discount = $lists->where('delete_type' ,0)->sum('discount') * -1 ?? '' }}
                                                    </span>
                                                </td>
                                                <td>
                                                    {{--  جمع قیمت جلسات رزرو شده پس از اعمال تخفیف --}}
                                                    <span class="currency">
                                                        {{ $total_price = ($price - $discount) ?? '' }}
                                                    </span>
                                                </td>
                                                <td>
                                                    {{--  جمع قیمت پرداخت شده پس از اعمال تخفیف --}}
                                                    <span class="btn btn-success p-50 currency">
                                                        {{ ($total_price + $lists->where('delete_type' ,0)->sum('transactions.amount_standing')) ?? '' }}
                                                    </span>
                                                </td>
                                                <td>
                                                    {{--  جمع بدهی --}}
                                                    <span class="btn btn-danger p-50 currency">
                                                        {{ (($lists->where('delete_type' ,0)->sum('transactions.amount_standing')) * -1) ?? '' }}
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                        
                                    </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        {{ sentence() }}
        <br>

        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <!-- END: Page Vendor JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
     <!-- END: Page Vendor JS-->
     <script src="/assets/vendors/js/charts/chart.min.js"></script>

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="/assets/js/scripts/charts/admin_report.js"></script> --}}
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
    <script>
        $('.currency').each(function(i, o) {
            $(o).html(numeral($(o).text()).format('0,0'));
        })
    </script>
    <script>
        $(window).on("load", function() {

            Chart.defaults.global.defaultFontFamily = '"primary-font", "segoe ui", "tahoma"';

            var $primary = '#5A8DEE',
                $success = '#39DA8A',
                $danger = '#FF5B5C',
                $warning = '#FDAC41',
                $info = '#00CFDD',
                $label_color = '#475F7B',
                grid_line_color = '#dae1e7',
                scatter_grid_color = '#f3f3f3',
                $scatter_point_light = '#E6EAEE',
                $scatter_point_dark = '#5A8DEE',
                $white = '#fff',
                $black = '#000';

            var themeColors = [$primary, $warning, $danger, $success, $info, $label_color];


            // Pie Chart
            // --------------------------------
            //Get the context of the Chart canvas element we want to select
            var pieChartctx = $("#simple-pie-chart");

            // Chart Options
            var piechartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                responsiveAnimationDuration: 500,
                title: {
                    display: false,
                    text: '{{ ' '.$admin->name.' '.$admin->family }}'
                }
            };

            // Chart Data
            var piechartData = {
                labels: ["کارگاه", "پرداختی", "لغو شده", " جلسات موفق", " جلسات ثبتی","جلسه باز"],
                datasets: [{
                    label: "سری اطلاعات اول",
                    data: [{{ $wsh_num }}, {{ $payed_session->count() + $debt_session->count() }}, {{ $canceled_session_num }}, {{ $closed_session_num }}, {{ $registred_session }}, {{ $open_session_num }}],
                    backgroundColor: themeColors,
                }]
            };

            var pieChartconfig = {
                type: 'pie',

                // Chart Options
                options: piechartOptions,

                data: piechartData
            };

            // Create the chart
            var pieSimpleChart = new Chart(pieChartctx, pieChartconfig);

        });
    </script>
    <!-- END: Page JS-->
<script>
    // window.print();
</script>

</html>
