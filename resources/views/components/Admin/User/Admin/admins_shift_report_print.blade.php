<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="container">
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-2">
                <p>
                    <b>بسمه تعالی</b>
                </p>
                <p>
                    <span class="font-medium-3">
                        @t(گزارش تحویل شیفت کلیه کارکنان)<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>

                </p>
            </div>
            <div class="col-4">

                <div class="row text-center float-right">
                    <div class="mr-4 mt-2">
                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                            
                        <table class="table-hover table-bordered text-secondary" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>@t(ردیف)</th>
                                    <th>@t(اپراتور)</th>
                                    <th>@t(مجموع نقد)</th>
                                    <th>@t(مجموع کارتخوان)</th>
                                    <th>@t(مجموع کارت به کارت)</th>
                                    <th>@t(مجموع یاری برگ)</th>
                                    <th>@t(مجموع اینترنتی)</th>
                                    <th>@t(مجموع عودت وجه)</th>
                                    <th>@t(تاریخ شروع)</th>
                                    <th>@t(تاریخ پایان)</th>                                                
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @php
                                    $transactions = $transactions->groupBy('operator_id');
                                    $tmp = 1;
                                    $tmp1 = $tmp2 = $tmp3 = $tmp4 = $tmp5 = $tmp7 = 0;
                                @endphp
                                @foreach ($transactions as $item => $transactions)
                                    <tr>
                                        <td>{{ $tmp++ }}</td>
                                        <td>
                                            <span data-toggle = "tooltip" data-placement="top" title="{{ $item }}">
                                                {{ \App\Models\user::find($item)->name?? '' }}&nbsp;{{ \App\Models\user::find($item)->family ?? '' }}
                                            </span>
                                        </td>
                                        <td class="currency">{{ $transactions->where('transaction_type_id', '1')->sum('amount') }}</td>
                                        <td class="currency">{{ $transactions->where('transaction_type_id', '5')->sum('amount') }}</td>
                                        <td class="currency">{{ $transactions->where('transaction_type_id', '4')->sum('amount') }}</td>
                                        <td class="currency">{{ $transactions->where('transaction_type_id', '3')->sum('amount') }}</td>
                                        <td class="currency">{{ $transactions->where('transaction_type_id', '2')->sum('amount') }}</td>
                                        <td class="currency">{{ $transactions->where('transaction_type_id', '7')->sum('amount') }}</td>
                                        <td><small>{{ $start_date }}</small></td>
                                        <td><small>{{ $end_date }}</small></td>
                                    </tr>
                                    @php
                                        $tmp1 = $tmp1 + $transactions->where('transaction_type_id', '1')->sum('amount');
                                        $tmp5 = $tmp5 + $transactions->where('transaction_type_id', '5')->sum('amount');
                                        $tmp4 = $tmp4 + $transactions->where('transaction_type_id', '4')->sum('amount');
                                        $tmp3 = $tmp3 + $transactions->where('transaction_type_id', '3')->sum('amount');
                                        $tmp2 = $tmp2 + $transactions->where('transaction_type_id', '2')->sum('amount');
                                        $tmp7 = $tmp7 + $transactions->where('transaction_type_id', '7')->sum('amount');
                                    @endphp
                                    
                                @endforeach
                                <tr>
                                    <td><b>@t(جمع)</b></td>
                                    <td>@t(کارکنان)</td>
                                    <td class="currency">{{ $tmp1 }}</td>
                                    <td class="currency">{{ $tmp5 }}</td>
                                    <td class="currency">{{ $tmp4 }}</td>
                                    <td class="currency">{{ $tmp3 }}</td>
                                    <td class="currency">{{ $tmp2 }}</td>
                                    <td class="currency">{{ $tmp7 }}</td>
                                    <td><small>{{ $start_date }}</small></td>
                                    <td><small>{{ $end_date }}</small></td>
                                </tr>
                            </tbody>
                        </table>                                    
                        <div class="mt-3 mb-3">
                            <span class="mt-4">@t(امضای تحویل دهنده)</span>
                            <span class="float-right">@t(امضای تحویل گیرنده)</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        {{ sentence() }}
        <br>

        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <!-- END: Page Vendor JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
     <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="/assets/js/scripts/charts/admin_report.js"></script> --}}
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
    <script>
        $('.currency').each(function(i, o) {
            $(o).html(numeral($(o).text()).format('0,0'));
        })
    </script>
    
    <!-- END: Page JS-->
<script>
    // window.print();
</script>

</html>
