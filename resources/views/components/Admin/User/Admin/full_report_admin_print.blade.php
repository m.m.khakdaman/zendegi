<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center mt-2">
            {{-- <p>
                <b>بسمه تعالی</b>
            </p> --}}
            <p>
                @if ($admin == 'all')
                    <span class="font-medium-3">
                        @t(گزارش عملکرد کلیه کارکنان)<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>
                @else
                    <span class="font-medium-3">
                        @t(گزارش عملکرد){{ ' '.$admin->name.' '.$admin->family }}<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>
                @endif
            </p>
        </div>
        {{-- <div class="col-4">
            <div class="row text-center float-right">
                <div class="mr-4 mt-2">
                    <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)">
                    <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                </div>
            </div>
        </div> --}}
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                            
                        @if ($admin == 'all')
                        <h4 class="card-title">@t(گزارش عملکرد کلیه کارکنان)</h4>
                        @else
                        <h4 class="card-title">@t(گزارش عملکرد){{ ' '.$admin->name.' '.$admin->family }}</h4>
                        @endif
                        <div>
                            <table class="table-hover table-bordered" style="width: 100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>@t(ردیف)</th>
                                        <th>@t(نام و نام خانوادگی)</th>
                                        <th>@t(تاریخ عملکرد)</th>
                                        <th>@t(عملکرد)</th>
                                        <th>@t(کنترلر)</th>
                                        <th class="text-center">مراجع</th>
                                        <th>@t(مشاور)</th>
                                        <th>@t(متد)</th>
                                        <th>IP</th>
                                        <th>@t(توضیحات)</th>
                                        <th class="text-center">@t(مرورگر)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $tmp = 0;
                                    @endphp
                                    @foreach ($logs as $log)
                                    <tr>
                                        <td class="text-center"><span data-toggle="tooltip" data-placement="top" title="{{ $log->id }}">{{ ++$tmp }}</span></td>
                                        <td><small>{{ $log->user->name.' '.$log->user->family }}</small></td>
                                        <td><span class="badge badge-light-black" style="direction: ltr">{{ verta($log->created_at)->format('Y/m/d H:i') }}</td>
                                        <td><small>{{ $log->subject }}</small></td>
                                        <td class="text-right"><small>{{ $log->url }}</small></td>
                                        <td class="text-center"><small>{{ $log->customer->name ?? '' }}&nbsp;{{ $log->customer->family ?? '' }}</small></td>
                                        <td class="text-center"><small>{{ $log->advisor->name ?? '' }}&nbsp;{{ $log->advisor->family ?? '' }}</small></td>
                                        <td class="text-center"><small>{{ $log->method }}</small></td>
                                        <td><small>{{ $log->ip }}</small></td>
                                        <td><small>{{ $log->comment }}</small></td>
                                        <td class="text-center">
                                            <i class="bx bx-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $log->agent }}"></i>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        {{ sentence() }}
        <br>
        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <!-- END: Page Vendor JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
     <!-- END: Page Vendor JS-->
     <script src="/assets/vendors/js/charts/chart.min.js"></script>

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="/assets/js/scripts/charts/admin_report.js"></script> --}}
    {{-- <script>
        $(window).on("load", function() {

            Chart.defaults.global.defaultFontFamily = '"primary-font", "segoe ui", "tahoma"';

            var $primary = '#5A8DEE',
                $success = '#39DA8A',
                $danger = '#FF5B5C',
                $warning = '#FDAC41',
                $info = '#00CFDD',
                $label_color = '#475F7B',
                grid_line_color = '#dae1e7',
                scatter_grid_color = '#f3f3f3',
                $scatter_point_light = '#E6EAEE',
                $scatter_point_dark = '#5A8DEE',
                $white = '#fff',
                $black = '#000';

            var themeColors = [$primary, $warning, $danger, $success, $info, $label_color];


            // Pie Chart
            // --------------------------------
            //Get the context of the Chart canvas element we want to select
            var pieChartctx = $("#simple-pie-chart");

            // Chart Options
            var piechartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                responsiveAnimationDuration: 500,
                title: {
                    display: false,
                    text: '{{ ' '.$admin->name.' '.$admin->family }}'
                }
            };

            // Chart Data
            var piechartData = {
                labels: ["کارگاه", "پرداختی", "لغو شده", " جلسات موفق", " جلسات ثبتی","جلسه باز"],
                datasets: [{
                    label: "سری اطلاعات اول",
                    data: [{{ $wsh_num }}, {{ $payed_session_num }}, {{ $canceled_session_num }}, {{ $closed_session_num }}, {{ $registred_session }}, {{ $open_session_num }}],
                    backgroundColor: themeColors,
                }]
            };

            var pieChartconfig = {
                type: 'pie',

                // Chart Options
                options: piechartOptions,

                data: piechartData
            };

            // Create the chart
            var pieSimpleChart = new Chart(pieChartctx, pieChartconfig);

        });
    </script> --}}
    <!-- END: Page JS-->
<script>
    // window.print();
</script>

</html>
