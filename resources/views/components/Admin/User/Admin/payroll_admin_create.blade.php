<x-base title="@t(صدور فیش حقوقی)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(صدور فیش حقوقی کارکنان)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">@t(واریز حقوق)</li>
                            <li class="breadcrumb-item">@t(صدور فیش حقوقی کارکنان)</li>
                            <li class="breadcrumb-item active">@t(فیش حقوقی:){{ ' '.$admin->name.' '.$admin->family }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="invoice-create-btn mb-1">
                            <h3 class="mb-3">@t(صدور فیش حقوقی برای:){{ ' '.$admin->name.' '.$admin->family }}</h3>
                        </div>
                    </div>
                    <div class="card-body">                       
                        <form action="{{ route('payroll_admin_print') }}" method="post">
                            @csrf
                            @method('post')
                            <table  style="border-collapse: collapse; width: 100%; direction: rtl" border="1">
                                <tbody>
                                    <tr>
                                        <td class="text-center" style="width: 25%;">
                                            @php
                                                $avatar = "avatar.jpg";
                                                if (isset($admin->pic)) {
                                                    $avatar = $admin->pic;
                                                };
                                            @endphp
                                            <img src="/assets/images/profile/admin/avatar/{{ $avatar }}" alt="@t(عکس پرسنلی)" width="150px">
                                        </td>
                                        <td style="width: 25%;" colspan="3">
                                            <span class="float-right" style="direction: rtl">
                                                @t(فیش حقوقی مربوط به )
                                                <input type="text" name="month" size="5">
                                                @t(ماه سال 1400)<br>
                                                @t(از){{ ' '.$start_jalali.' ' }}@t(تا){{ ' '.$end_jalali }}
                                            </span>
                                            <span>@t(مرکز مشاوره زندگی عاقلانه)</span>
                                        </td>
                                    </tr>
                                    <tr class="text-center bg-light">
                                        <td style="width: 25%;">@t(مشخصات پرسنل)</td>
                                        <td style="width: 25%;">@t(وضعیت کارکرد)</td>
                                        <td style="width: 25%;">@t(شرح پرداخت ها)</td>
                                        <td style="width: 25%;">@t(شرح کسور)</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%; vertical-align: top; padding-top: 10px;">
                                            <span>@t(کد پرسنلی:){{ ' '.$admin->id }}</span><br>
                                            <span>@t(نام:){{ ' '.$admin->name }}</span><br>
                                            <span>@t(نام خانوادگی:){{ ' '.$admin->family }}</span><br>
                                            <span>@t(کدملی:){{ ' '.$admin->national_code }}</span><br>
                                            <span>@t(شماره شناسنامه:){{ ' '.$admin->id_number }}</span><br>
                                            <span>@t(محل خدمت:){{ ' '.$admin->semat }}</span><br>
                                            <span>@t(شغل یا سمت:){{ ' '.$admin->semat }}</span><br>
                                            <span>@t(شماره حساب:){{ ' '.$admin->account_number }}</span><br>
                                            <span>@t(شماره بیمه:){{ ' '.$admin->account_number }}</span><br>
                                        </td>
                                        <td style="width: 25%; vertical-align: top;  padding-top: 10px;" rowspan="3">
                                            <span class="ml-50">@t(مزد شغل روزانه:)</span>
                                            <span class="float-right mr-50"><input type="text" name="daily" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(روزهای کارکرد:)</span>
                                            <span class="float-right mr-50"><input type="text" name="working_days" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(اضافه کار:)</span>
                                            <span class="float-right mr-50"><input type="text" name="overtime_hourly" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(کسرکار ساعتی:)</span>
                                            <span class="float-right mr-50"><input type="text" name="low_time_hourly" value="30" style="direction: ltr"></span><hr>
                                        </td>
                                        <td style="width: 25%; vertical-align: top;" class="pt-1">
                                            <span class="ml-50">@t(حقوق ماهانه:)</span>
                                            <span class="float-right mr-50"><input type="text" name="payroll" value="11.112.690" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(حق اولاد:)</span>
                                            <span class="float-right mr-50"><input type="text" name="child" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(مسکن:)</span>
                                            <span class="float-right mr-50"><input type="text" name="home" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(بن:)</span>
                                            <span class="float-right mr-50"><input type="text" name="bon" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(ایاب و ذهاب:)</span>
                                            <span class="float-right mr-50"><input type="text" name="transportation" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(اضافه کار:)</span>
                                            <span class="float-right mr-50"><input type="text" name="overtime" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(فوق العاده کاری نوبت صبح و عصر:)</span>
                                            <span class="float-right mr-50"><input type="text" name="morning" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(فوق العاده نوبت کاری صبح عصر شب:)</span>
                                            <span class="float-right mr-50"><input type="text" name="night" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(3 درصد از فروش کارگاه عمومی:)</span>
                                            <span class="float-right mr-50"><input type="text" name="public_wsh" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(2 درصد از فروش کارگاه تخصصی:)</span>
                                            <span class="float-right mr-50"><input type="text" name="special_wsh" value="30" style="direction: ltr"></span>
                                        </td>
                                        <td style="width: 25%; vertical-align: top; padding-top: 10px;">
                                            <span class="ml-50">@t(مالیات حقوق:)</span>
                                            <span class="float-right mr-50"><input type="text" name="tax" value="16260" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(بیمه:)</span>
                                            <span class="float-right mr-50"><input type="text" name="insurance" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(مساعده:)</span>
                                            <span class="float-right mr-50"><input type="text" name="help" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(کسر کار ساعتی:)</span>
                                            <span class="float-right mr-50"><input type="text" name="low_time" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(هزینه کارگاه:)</span>
                                            <span class="float-right mr-50"><input type="text" name="wsh" value="30" style="direction: ltr"></span><hr>
                                            <span class="ml-50">@t(مفاصا حساب:)</span>
                                            <span class="float-right mr-50"><input type="text" name="mofasa" value="30" style="direction: ltr"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%;" class="bg-light text-center">@t(مانده وام)</td>
                                        <td style="width: 25%;">
                                            <span class="ml-50">@t(طلب از قبل:)</span>
                                            <span class="float-right mr-50"><input type="text" name="credit" value="30" style="direction: ltr"></span>
                                        </td>
                                        <td style="width: 25%;">
                                            <span class="ml-50">@t(کسر رند ماه جاری:)</span>
                                            <span class="float-right mr-50"><input type="text" name="rond" value="30" style="direction: ltr"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%;">
                                            <span>@t(وام قرض الحسنه)</span>
                                            <span class="float-right mr-50"><input type="text" name="rest_of_loan" value="30" style="direction: ltr"></span>
                                        </td>
                                        <td style="width: 25%;">
                                            <span class="ml-50">@t(جمع ناخالص:)</span>
                                            <span class="float-right mr-50"><input type="text" name="nakhales" value="30" style="direction: ltr"></span>
                                        </td>
                                        <td style="width: 25%;">
                                            <span class="ml-50">@t(جمع کسور:)</span>
                                            <span class="float-right mr-50"><input type="text" name="kosoorat" value="30" style="direction: ltr"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%;"></td>
                                        <td style="width: 25%;"></td>
                                        <td style="width: 25%;" class="bg-light">
                                            <span class="ml-50">@t(خالص ماه جاری:)</span>
                                            <span class="float-right mr-50"><input type="text" name="khales" value="30" style="direction: ltr"></span>
                                        </td>
                                        <td style="width: 25%;" class="bg-light">
                                            <span class="ml-50">@t(مبلغ پرداختی:)</span>
                                            <span class="float-right mr-50"><input type="text" name="paid" value="30" style="direction: ltr"></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="admin_id" value="{{ $admin->id }}">
                            <input type="hidden" name="start_jalali" value="{{ $start_jalali }}">
                            <input type="hidden" name="end_jalali" value="{{ $end_jalali }}">
                            <input type="submit" class="btn btn-success float-right mt-50" value="@t(صدور فیش حقوق)">
                            <a href="" class="btn btn-danger float-right mt-50 mr-50">@t(برگشت)</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->
      

    </x-slot>
</x-base>
