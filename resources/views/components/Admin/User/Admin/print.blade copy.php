<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center mt-2">
            <p>
                <b>بسمه تعالی</b>
            </p>
            <p>
                <span class="font-medium-3">@t(گزارش عملکرد کارکنان )</span>
            </p>
        </div>
        <div class="col-4">

            <div class="row text-center float-right">
                <div class="mr-4 mt-2">
                    <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                    <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h4 class="card-title">@t(گزارش عملکرد){{ ' '.$admin->name.' '.$admin->family }}</h4>
                    <div class="card-body">
                        <div class="card-text line-height-2">
                        <dl class="row">
                            <dt class="col-sm-5">@t(تعداد جلسات ثبت کرده:)</dt>
                            <dd class="col-sm-7">{{ $session.' ' }}@t(جلسه جمعاً به قیمت:){{ ' '.$session_amount }}</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5">@t(تعداد جلسات بسته شده:)</dt>
                            <dd class="col-sm-7">{{ $closed_session.' ' }}@t(جلسه جمعاً به قیمت:){{ ' '.$session_amount }}</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5">@t(تعداد جلسات لغو شده:)</dt>
                            <dd class="col-sm-7">{{ $canceled_session.' ' }}@t(جلسه جمعاً به قیمت:){{ ' '.$session_amount }}</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5">@t(تعداد جلسات پرداخت شده:)</dt>
                            <dd class="col-sm-7">{{ $session.' ' }}@t(جلسه جمعاً به قیمت:){{ ' '.$session_amount }}</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5">@t(ثبت نام مراجع در کارگاه:)</dt>
                            <dd class="col-sm-7">{{ $wsh.' ' }}@t(نفر)</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5">@t(تعداد تماس ثبت شده)</dt>
                            <dd class="col-sm-7 ml-auto"></dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5">@t(تعداد پیگیری انجام شده)</dt>
                            <dd class="col-sm-7 ml-auto"></dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5">@t(مبلغ کل فروش)</dt>
                            <dd class="col-sm-7 ml-auto">{{ $amount.' ' }}@t(تومان)</dd>
                        </dl>
                        <dl class="row">
                            <dt class="col-sm-5 text-truncate">@t(توضیحات)</dt>
                            <dd class="col-sm-7"></dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <h4 class="card-title">@t(لیست  )</h4>
                    <div class="card-body">
                        <table class="1table table-bordered">
                            <tr>
                                <th>@t(ردیف)</th>
                                <th>@t(مراجع)</th>
                                <th>@t(مشاور)</th>
                                <th>وضع</th>
                                <th>@t(تاریخ)</th>
                                <th>لغو</th>
                                <th>products_type_id</th>
                            </tr>
                            @php
                                $tmp = 0;
                            @endphp
                            @foreach ($members as $member)
                                <tr>
                                    <td>{{ ++$tmp }}</td>
                                    <td>{{ $member->customer->name.' '.$member->customer->family }}</td>
                                    <td>{{ $member->advisor->name.' '.$member->advisor->family }}</td>
                                    <td>{{ $member->session_status_id }}</td>
                                    <td>{{ $member->day->date }}</td>
                                    <td>{{ $member->delete_type }}</td>
                                    <td>{{ $member->products_type_id }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h4 class="card-title">@t(لیست  )</h4>
                    <div class="card-body">
                        <table class="1table table-bordered">
                            <tr>
                                <th>@t(ردیف)</th>
                                <th>@t(تاریخ)</th>
                                <th>@t(مراجع)</th>
                                <th>@t(مشاور)</th>
                                <th>وضع</th>
                                <th>لغو</th>
                                <th>products_type_id</th>
                                <th>@t(قیمت)</th>
                                <th>@t(تخفیف)</th>
                                <th>@t(با تخفیف)</th>
                            </tr>
                            @php
                                $tmp = 0;
                            @endphp
                            @foreach ($lists as $list)
                                <tr>
                                    <td>{{ ++$tmp }}</td>
                                    <td>{{ $list->day->date }}</td>
                                    <td>{{ $list->customer->name.' '.$list->customer->family }}</td>
                                    <td>{{ $list->advisor->name.' '.$list->advisor->family }}</td>
                                    <td>{{ $list->session_status_id }}</td>
                                    <td>{{ $list->delete_type }}</td>
                                    <td>{{ $list->products_type_id }}</td>
                                    <td>{{ $list->orginal_price }}</td>
                                    <td>{{ $list->discount }}</td>
                                    <td>{{ $list->total_price }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            {{ sentence() }}
            <br>

            <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
        </div>
    </div>
</body>

<script>
    // window.print();
</script>

</html>
