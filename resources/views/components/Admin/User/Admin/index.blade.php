<x-base >
    <x-slot name='title'>
        @t(کارکنان)
    </x-slot>

    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(لیست کارکنان)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.index') }}">@t(لیست کارکنان)</a>
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">        
                        <!-- create invoice button-->
                        @can('advisor_create')
                        <div class="invoice-create-btn mb-1">
                            <a href="{{ route('admin.create') }}" class="btn btn-primary glow invoice-create" role="button"
                                aria-pressed="true">@t(کاربر جدید)</a>
                        </div>
                        @endcan
                        <div class="table-responsive">
                            @include('partials.flash')
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>@t(کد)</th>
                                        <th>@t(آواتار)</th>
                                        <th>@t(پیشوند)</th>
                                        <th>@t(نام)</th>
                                        <th>@t(نام خانوادگی)</th>
                                        <th>@t(موبایل)</th>
                                        <th>@t(سمت)</th>
                                        <th>@t(جنسیت)</th>
                                        <th>@t(وضعیت)</th>
                                        <th>@t(عملیات)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>
                                                <div class="avatar mr-1">
                                                    @php
                                                    $tmp = 'avatar.jpg';
                                                    if ($user->pic) {
                                                        $tmp = $user->pic;
                                                    }
                                                    @endphp
                                                    <img src="../../assets/images/profile/admin/avatar/{{ $tmp }}" alt="group image" height="32" width="32" data-toggle="tooltip" data-placement="top" title="{{ $user->name.' '.$user->family }}">
                                                </div>
                                            </td>
                                            <td>
                                                @php
                                                $tmp = 'آقای';
                                                    if ($user->education_level_id == '4') {
                                                        $tmp = 'دکتر';
                                                    }elseif ($user->gender == 'زن') {
                                                        $tmp = 'خانم';
                                                    } 
                                                @endphp
                                                {{ $tmp }}
                                            </td>
                                            <td><a href="{{ route('admin.show', $user->id) }}">{{ $user->name }}</a></td>
                                            <td><a href="{{ route('admin.show', $user->id) }}">{{ $user->family }}</a></td>
                                            <td><span data-toggle="tooltip" data-placement="top" title="{{ $user->mobile2 }}">{{ $user->mobile }}</span></td>
                                            <td>{{ $user->semat }}</td>
                                            <td>{{ $user->gender }}</td>
                                            <td>
                                                @if ($user->status == 'فعال')
                                                    <a href="{{ route('admin_activation', $user->id) }}" data-toggle="tooltip" data-placement="top" title="@t(برای غیر فعال کردن این کاربر کلیک کنید.)">
                                                        <span class="text-success">{{ $user->status }}</span>
                                                    </a>
                                                @else
                                                    <a href="{{ route('admin_activation', $user->id) }}" data-toggle="tooltip" data-placement="top" title="@t(برای فعال کردن این کاربر کلیک کنید.)">
                                                        <span class="text-danger">{{ $user->status }}</span>
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                    {{-- @can('user.admin.update') --}}
                                                        <a href="{{ route('admin.edit', $user->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ویرایش){{ ' '.$user->name.' '.$user->family }}"><i class="bx bx-edit"></i></a>
                                                        <a href="{{ route('sms.send', $user->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(ارسال پیامک برای){{ ' '.$user->name.' '.$user->family }}"><i class="bx bx-message-dots"></i></a>
                                                        <a href="{{ route('sms.index', $user->id) }}" class="text-secondary" data-toggle="tooltip" data-placement="top" title="@t(بازنشانی گذرواژه برای){{ ' '.$user->name.' '.$user->family }}"><i class="bx bx-reset"></i></a>
                                                    {{-- @endcan --}}
                                                    {{-- @can('user.admin.destroy') --}}
                                                        {{-- <button class="btn btn-danger text-white" data-toggle="modal"
                                                            data-target="#_{{ $user->id }}">حذف</button> --}}
                                                    {{-- @endcan --}}
                                                <!-- Modal -->
                                                {{-- <div class="modal fade" id="_{{ $user->id }}" tabindex="-1" user="dialog"
                                                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" user="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                آیا می خواهید -- {{ $user->name }} -- را حذف نمایید ؟
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form action="{{ route('advisor.destroy', $user->id) }}"
                                                                    method="post">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">خیر</button>
                                                                    <button type="submit" class="btn btn-success">بله</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/datatables/datatable.js"></script>
        <script>$('div.alert').delay(3000).slideUp(300);</script>
        <!-- END: Page JS-->
    </x-slot>
</x-base>
