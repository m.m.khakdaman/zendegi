<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A4;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="container">
        <div class="row">
            <div class="col-4">
            </div>
            <div class="col-4 text-center mt-2">
                <p>
                    <b>بسمه تعالی</b>
                </p>
                <p>
                    <span class="font-medium-3">
                        @t(گزارش تحویل شیفت){{ ' '.($admin->name ?? '').' '.($admin->family ?? '') }}<br>
                        <small>{{ $start_date.' ' }}@t(الی){{ ' '.$end_date }}</small>
                    </span>

                </p>
            </div>
            <div class="col-4">

                <div class="row text-center float-right">
                    <div class="mr-4 mt-2">
                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                        <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">                            
                        <h4 class="card-title">@t(گزارش تحویل شیفت){{ ' '.($admin->name ?? '').' '.($admin->family ?? '') }}</h4>
                        <div class="table-responsive">
                            <table class="table-hover table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>@t(ردیف)</th>
                                        <th>@t(زمان ثبت)</th>
                                        <th>@t(مراجع)</th>
                                        <th>@t(نقدی)</th>
                                        <th>@t(کارتخوان)</th>
                                        <th>@t(کارت به کارت)</th>
                                        <th>@t(یاری برگ)</th>
                                        <th>@t(اینترنتی)</th>
                                        <th>@t(عودت وجه)</th>
                                        <th>@t(توضیحات)</th>
                                        <th>@t(کدرهگیری)</th>
                                        <th>@t(صندوق)</th>
                                        <th>@t(تاریخ شروع)</th>
                                        <th>@t(تاریخ پایان)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $tmp = $tmp1 = $tmp2 = $tmp3 = $tmp4 = $tmp5 = $tmp7 = 0;
                                    @endphp
                                    @foreach ($transactions as $transaction)
                                    <tr>
                                        <td class="text-center"><span data-toggle="tooltip" data-placement="top" title="{{ $transaction->id }}">{{ ++$tmp }}</span></td>
                                        <td style="direction: ltr">{{ $transaction->created_at }}</td>   
                                        <td>
                                            <span data-toggle="tooltip" data-placement="top" title="{{ $transaction->user->id }}">
                                                {{ $transaction->user->name ?? '' }}&nbsp;{{ $transaction->user->family ?? '' }}
                                            </span>
                                        </td>                                             
                                        <td>
                                            @if ($transaction->transaction_type_id == 1)
                                                <span class="currency">{{ $transaction->amount ?? '' }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($transaction->transaction_type_id == 5)
                                                <span class="currency">{{ $transaction->amount ?? '' }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($transaction->transaction_type_id == 4)
                                                <span class="currency">{{ $transaction->amount ?? '' }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($transaction->transaction_type_id == 3)
                                                <span class="currency">{{ $transaction->amount ?? '' }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($transaction->transaction_type_id == 2)
                                                <span class="currency">{{ $transaction->amount ?? '' }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($transaction->transaction_type_id == 7)
                                                <span class="currency">{{ $transaction->amount ?? '' }}</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <small>{{ $transaction->comment ?? '' }}</small>
                                        </td>
                                        <td class="text-center"><small>{{ $transaction->tracking_code ?? '' }}</small></td>                                        
                                        <td class="text-center"><small>{{ $transaction->cash->name ?? '' }}</small></td>   
                                        <td><small>{{ $start_date }}</small></td>                                     
                                        <td><small>{{ $end_date }}</small></td>
                                    </tr>
                                    @php
                                        $tmp1 = $transactions->where('transaction_type_id', '1')->sum('amount');
                                        $tmp5 = $transactions->where('transaction_type_id', '5')->sum('amount');
                                        $tmp4 = $transactions->where('transaction_type_id', '4')->sum('amount');
                                        $tmp3 = $transactions->where('transaction_type_id', '3')->sum('amount');
                                        $tmp2 = $transactions->where('transaction_type_id', '2')->sum('amount');
                                        $tmp7 = $transactions->where('transaction_type_id', '7')->sum('amount');
                                    @endphp
                                    @endforeach
                                    <tr class="text-bold-600 text-secondary text-center" style="height: 40px">
                                        <td></td>
                                        <td>مجموع</td>
                                        <td></td>
                                        <td>
                                            @if ($tmp1)
                                                <span class="currency">{{ $tmp1 }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($tmp5)
                                                <span class="currency">{{ $tmp5 }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($tmp4)
                                                <span class="currency">{{ $tmp4 }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($tmp3)
                                                <span class="currency">{{ $tmp3 }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($tmp2)
                                                <span class="currency">{{ $tmp2 }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($tmp7)
                                                <span class="currency">{{ $tmp7 }}</span>
                                            @endif
                                        </td>
                                        <td></td>
                                        <td>@t(جمع کل)</td>
                                        <td class="currency">{{ $tmp1 + $tmp2 + $tmp3 + $tmp4 + $tmp5 - $tmp7  }}</td>
                                        <td><small>{{ $start_date }}</small></td>                                     
                                        <td><small>{{ $end_date }}</small></td>
                                    </tr>
                                </tbody>
                                
                            </table>
                    </div>
                            {{-- <div class="table-responsive"> --}}
                                    {{-- <table class="table-hover table-bordered" style="width: 100%"> --}}
                                        {{-- <thead>
                                            <tr>
                                                <th>@t(ردیف)</th>
                                                <th>@t(تاریخ ثبت)</th>
                                                <th>@t(مراجع)</th>
                                                <th>@t(مشاور)</th>
                                                <th class="text-center">وضع</th>
                                                <th>@t(تاریخ جلسه)</th>
                                                <th>لغو</th>
                                                <th class="text-center">@t(نوع)</th>
                                                <th>@t(قیمت)</th>
                                                <th>@t(تخفیف)</th>
                                                <th>@t(با تخفیف)</th>
                                                <th class="text-center">@t(پرداختی)</th>
                                                <th class="text-center">@t(بدهی)</th>
                                            </tr>
                                        </thead> --}}
                                        {{-- <tbody> --}}
                                            {{-- @php
                                            $tmp = 0;
                                            @endphp --}}
                                            {{-- @foreach ($sessions as $session)
                                            <tr>
                                                <td class="text-center"><span data-toggle="tooltip" data-placement="top" title="{{ $session->id }}">{{ ++$tmp }}</span></td>
                                                <td>{{ verta($session->created_at)->format('Y/m/d') }}</td>                                                
                                                <td>
                                                    <span class="text-secondary pl-50">
                                                        {{ $session->products_type->product_type ?? '' }}
                                                        {{ ($session->customer->name ?? '') }} {{ ($session->customer->family ?? '') }}
                                                        با
                                                        {{ $session->advisor->name ?? '' }} {{ $session->advisor->family ?? '' }}</span></td>
                                                <td>
                                                    {{ $session->transactions->transaction_type_id ?? '' }}
                                                </td>
                                                <td class="text-center">
                                                    @if ($session->session_status_id == 1)
                                                        <span class="badge badge-success">باز</span>
                                                    @else
                                                        <span class="badge badge-warning">بسته</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($session->date < date('Y-m-d') and $session->session_status_id == 1)
                                                        <span class="text-danger">{{ $session->day->date ?? '' }}</span>                                                        
                                                    @else
                                                        {{ $session->day->date ?? '' }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($session->delete_type)
                                                        <span class="badge badge-pill badge-danger">لغو</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    
                                                </td>
                                                <td><span class="currency">{{ $session->orginal_price ?? '' }}</span></td>
                                                <td class="text-center">
                                                    @isset($session->discount)
                                                        <span class="text-danger currency">{{ $session->discount * -1 ?? '' }}</span>
                                                    @endisset
                                                </td>
                                                <td class="text-center"><span class="currency">{{ $session->total_price }}</span></td>
                                                <td class="text-center">
                                                    <span>
                                                        {{@ ($session->total_price + $session->transactions->amount_standing) ?? ''  }}
                                                    </span>
                                                </td>
                                                <td class="text-center">
                                                    @if(@ $session->transactions->amount_standing)
                                                        <span class="badge badge-danger currency">{{ $session->transactions->amount_standing * -1 ?? '' }}</span>
                                                    @else
                                                        <span class="badge badge-light-success ">تسویه</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach --}}
                                            
                                        {{-- </tbody> --}}
                                        
                                    {{-- </table> --}}
                                    
                            {{-- </div> --}}
                            <div class="mt-3 mb-3">
                                <span class="mt-4">@t(امضای تحویل دهنده)</span>
                                <span class="float-right">@t(امضای تحویل گیرنده)</span>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        
        {{ sentence() }}
        <br>

        <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <!-- END: Page Vendor JS-->
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
     <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
     <!-- END: Page Vendor JS-->
     <script src="/assets/vendors/js/charts/chart.min.js"></script>

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    {{-- <script src="/assets/js/scripts/charts/admin_report.js"></script> --}}
    <script src="/assets/vendors/js/extensions/numeral/numeral.js"></script>
    <script>
        $('.currency').each(function(i, o) {
            $(o).html(numeral($(o).text()).format('0,0'));
        })
    </script>
    
    <!-- END: Page JS-->
<script>
    // window.print();
</script>

</html>
