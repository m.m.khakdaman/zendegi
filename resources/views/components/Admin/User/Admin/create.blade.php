<x-base>
    <x-slot name='title'>
        @t(تعریف کاربر جدید)
    </x-slot>
    <x-slot name='css'>


        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/file-uploaders/dropzone.css">
        <!-- END: Page CSS-->

        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(کاربر جدید)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
                                        class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">لیست کارکنان</a>
                            </li>
                            <li class="breadcrumb-item active">@t(کاربر جدید)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(کاربر جدید)</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <p>@t(پر کردن فیلدهای ستاره دار الزامی است.)</p>
                            {{-- @include('layouts.errors') --}}
                            <form action="{{ route('admin.store') }}" method="POST" enctype="multipart/form-data" class="wizard-horizontal"
                                novalidate>
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="name"  name="name" required
                                                    data-validation-required-message="پر کردن فیلد نام اجباری است."
                                                    placeholder='@t(نام)'>
                                                <label for="name">@t(نام)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="family" required
                                                    name="family"
                                                    data-validation-required-message="پر کردن فیلد نام خانوادگی اجباری است."
                                                    placeholder='@t(نام خانوادگی)'>
                                                <label for="family">@t(نام خانوادگی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="father_name"
                                                    name="father_name"
                                                    placeholder="نام پدر">
                                                <label for="father_name">نام پدر</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1 form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" name="birthday" id="birthday" class="form-control shamsi-datepicker-list"
                                                    placeholder="تاریخ تولد">
                                                    <label for="birthday">تاریخ تولد</label>
                                                <div class="form-control-position">
                                                    <i class="bx bx-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="national_code" 
                                                    name="national_code"
                                                    placeholder='@t(کدملی)'>
                                                <label for="national_code">@t(کدملی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <select class="form-control " id="field_of_study"  name="field_of_study" 
                                                    placeholder='@t(مدرک تحصیلی)'>
                                                    @foreach ($education_levels as $education_level)
                                                        <option value="{{ $education_level->id }}">{{ $education_level->education_level }}</option>
                                                    @endforeach
                                                </select>
                                                <label for="field_of_study">@t(مدرک تحصیلی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="field_of_study"  name="field_of_study"
                                                    placeholder='@t(رشته تحصیلی)'>
                                                <label for="field_of_study">@t(رشته تحصیلی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label for="basicInputFile">@t(عکس پرسنلی)</label>
                                            <div class="custom-file">
                                                <input id="file" type="file" class="form-control @error('file') is-invalid @enderror" name="file">
                                                @error('file')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </fieldset>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="card-content">
                                            <p>جنسیت</p>
                                            <div class="card-body">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-primary" value="1"
                                                                    name="gender" id="man" value="مرد" checked>
                                                                <label class="custom-control-label"
                                                                    for="man">مرد</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-danger" value="2"
                                                                    name="gender" id="woman" value="زن">
                                                                <label class="custom-control-label"
                                                                    for="woman">زن</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="card-content">
                                            <p>@t(وضعیت)</p>
                                            <div class="card-body">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-success" value="1"
                                                                    name="status" id="active" value="1" checked>
                                                                <label class="custom-control-label"
                                                                    for="active">@t(فعال)</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-secondary" value="2"
                                                                    name="status" id="inactive" value="2">
                                                                <label class="custom-control-label"
                                                                    for="inactive">@t(غیر فعال)</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card-content">
                                            <p>@t(خلق و خوی)</p>
                                            <div class="card-body">
                                                <ul class="list-unstyled mb-0">
                                                    @foreach ($moods as $mood )
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input {{ $mood->color }}"
                                                                    name="moods_id" id="customColorRadio{{ $mood->id }}"
                                                                    value="{{ $mood->id }}">
                                                                <label class="custom-control-label"
                                                                    for="customColorRadio{{ $mood->id }}">{{ $mood->mood }}</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>

                                                        
                                                    @endforeach
                                                    
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="mobile" 
                                                    name="mobile"
                                                    data-validation-pattern-message="این شماره فرمت معتبری نیست."
                                                    data-validation-required-message="پر کردن فیلد شماره همراه اجباری است."
                                                    placeholder='@t(شماره همراه)'>
                                                <label for="mobile">@t(شماره همراه)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="mobile2" 
                                                    name="mobile2"
                                                    data-validation-pattern-message="این شماره فرمت معتبری نیست."
                                                    placeholder='@t(شماره کاری)'>
                                                <label for="mobile2">@t(شماره کاری)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="phone"
                                                    name="phone"
                                                    pattern="[0-9]+"
                                                    data-validation-pattern-message="این شماره فرمت معتبری نیست."
                                                    placeholder='@t(شماره ثابت)'>
                                                <label for="phone">@t(شماره ثابت)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="password" class="form-control" id="password"
                                                    name="password"
                                                    required
                                                    placeholder='@t(رمز عبور)'>
                                                <label for="password">@t(رمز عبور)</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <textarea class="form-control" id="note" name="note"
                                                    placeholder='@t(توضیحات)'></textarea>
                                                <label for="note">@t(توضیحات)</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    @can('admin.edit_role')
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="roles">@t(نقش ها)</label>
                                            <select name="roles[]" id="roles[]" multiple class="form-control select2">
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}" >{{ $role->title . '--' .$role->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endcan
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">@t(آپلود مدارک کاربر) موقتا غیر فعال شده</h4>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body">
                                                    {{-- <form action="{{ route('admin.dropzone_upload') }}" class="dropzone dropzone-area" method="POST"
                                                        id="dpz-multiple-files">
                                                        @csrf
                                                        <div class="dz-message">فایل های خود را برای ارسال به اینجا
                                                            بکشید</div>
                                                    </form> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-11"></div>
                                    <div class="col-1">
                                        <input type="submit" class="form-control btn btn-success" value="ثبت" required>
                                    </div>
                                   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/extensions/dropzone.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/moment.min.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->

        <script>
            $('#date_of_birth').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                maxDate: "+0D",
            });
            $('#madrak').select2({
                dropdownAutoWidth: true,
                width: '100%',
                language: "fa",
                placeholder: "مدرک تحصیلی",
            });
            
            Dropzone.options.dpzMultipleFiles = {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 100, // MB
                clickable: true
            }

        </script>
    </x-slot>  
</x-base>
