<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- BEGIN: Vendor CSS-->
    {{-- <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css"> --}}
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <!-- END: Theme CSS-->
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }


        @page {
            size: A5;
            margin: 0;
        }

    </style>
</head>

<body style="background-image:none;">
    <div class="row ">
        <div class="col-4">
        </div>
        <div class="col-4 text-center mt-2">
            <p>
            
                                <h3>@t(فیش حقوقی){{ ' '.$admin->name.' '.$admin->family }}</h3>
                                <span class="font-medium-3">@t(از تاریخ:){{ ' '.$payroll->start_jalali }} تا {{ $payroll->end_jalali }}</span>
            </p>
        </div>
        <div class="col-4">
            <div class="row text-center float-right">
                <div class="mr-4 mt-2">
                    <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" />
                    <p>@t(مرکز مشاوره زندگی عاقلانه)</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table  style="border-collapse: collapse; width: 100%; direction: rtl" border="1">
                                <tbody>
                                    <tr>
                                        <td class="text-center" style="width: 25%;">
                                            @php
                                            $avatar = "avatar.jpg";
                                            if (isset($admin->pic)) {
                                                $avatar = $admin->pic;
                                            };
                                        @endphp
                                            <img src="/assets/images/profile/admin/avatar/{{ $avatar }}" alt="@t(عکس پرسنلی)" width="150px">
                                        </td>
                                        <td style="width: 25%;" colspan="3">
                                            <span class="float-right" style="direction: rtl">
                                                @t(فیش حقوقی مربوط به){{ ' '.$payroll->month.' ' }}@t(ماه سال 1400)<br>
                                                @t(از){{ ' '.$payroll->start_jalali.' ' }}@t(تا){{ ' '.$payroll->end_jalali }}
                                            </span>
                                            <span>@t(مرکز مشاوره زندگی عاقلانه)</span>
                                        </td>
                                    </tr>
                                <tr class="text-center bg-light">
                                    <td style="width: 25%;">@t(مشخصات پرسنل)</td>
                                    <td style="width: 25%;">@t(وضعیت کارکرد)</td>
                                    <td style="width: 25%;">@t(شرح پرداخت ها)</td>
                                    <td style="width: 25%;">@t(شرح کسور)</td>
                                </tr>
                                <tr>
                                    <td style="width: 25%; vertical-align: top; padding-top: 10px;">
                                        <span>@t(کد پرسنلی:){{ ' '.$admin->id }}</span><br>
                                        <span>@t(نام:){{ ' '.$admin->name }}</span><br>
                                        <span>@t(نام خانوادگی:){{ ' '.$admin->family }}</span><br>
                                        <span>@t(کدملی:){{ ' '.$admin->national_code }}</span><br>
                                        <span>@t(شماره شناسنامه:){{ ' '.$admin->id_number }}</span><br>
                                        <span>@t(محل خدمت:){{ ' '.$admin->semat }}</span><br>
                                        <span>@t(شغل یا سمت:){{ ' '.$admin->semat }}</span><br>
                                        <span>@t(شماره حساب:){{ ' '.$admin->account_number }}</span><br>
                                        <span>@t(شماره بیمه:){{ ' '.$admin->account_number }}</span><br>
                                    </td>
                                    <td style="width: 25%; vertical-align: top;  padding-top: 10px;" rowspan="3">
                                        <span class="ml-50">@t(مزد شغل روزانه:)</span><span class="float-right mr-50">{{ $payroll->daily }}</span><hr>
                                        <span class="ml-50">@t(روزهای کارکرد:)</span><span class="float-right mr-50">{{ $payroll->working_days }}</span><hr>
                                        <span class="ml-50">@t(اضافه کار:)</span><span class="float-right mr-50">{{ $payroll->overtime_hourly }}</span><hr>
                                        <span class="ml-50">@t(کسرکار ساعتی:)</span><span class="float-right mr-50">{{ $payroll->low_time_hourly }}</span><hr>
                                    </td>
                                    <td style="width: 25%; vertical-align: top;" class="pt-1">
                                        <span class="ml-50">@t(حقوق ماهانه:)</span><span class="float-right mr-50">{{ $payroll->payroll }}</span><hr>
                                        <span class="ml-50">@t(حق اولاد:)</span><span class="float-right mr-50">{{ $payroll->child }}</span><hr>
                                        <span class="ml-50">@t(مسکن:)</span><span class="float-right mr-50">{{ $payroll->home }}</span><hr>
                                        <span class="ml-50">@t(بن:)</span><span class="float-right mr-50">{{ $payroll->bon }}</span><hr>
                                        <span class="ml-50">@t(ایاب و ذهاب:)</span><span class="float-right mr-50">{{ $payroll->transportation }}</span><hr>
                                        <span class="ml-50">@t(اضافه کار:)</span><span class="float-right mr-50">{{ $payroll->overtime }}</span><hr>
                                        <span class="ml-50">@t(فوق العاده کاری نوبت صبح و عصر:)</span><span class="float-right mr-50">{{ $payroll->morning }}</span><hr>
                                        <span class="ml-50">@t(فوق العاده نوبت کاری صبح عصر شب:)</span><span class="float-right mr-50">{{ $payroll->night }}</span><hr>
                                        <span class="ml-50">@t(3 درصد از فروش کارگاه عمومی:)</span><span class="float-right mr-50">{{ $payroll->public_wsh }}</span><hr>
                                        <span class="ml-50">@t(2 درصد از فروش کارگاه تخصصی:)</span><span class="float-right mr-50">{{ $payroll->special_wsh }}</span><hr>
                                    </td>
                                    <td style="width: 25%; vertical-align: top; padding-top: 10px;">
                                        <span class="ml-50">@t(مالیات حقوق:)</span><span class="float-right mr-50">{{ $payroll->tax }}</span><hr>
                                        <span class="ml-50">@t(بیمه:)</span><span class="float-right mr-50">{{ $payroll->insurance }}</span><hr>
                                        <span class="ml-50">@t(مساعده:)</span><span class="float-right mr-50">{{ $payroll->help }}</span><hr>
                                        <span class="ml-50">@t(کسر کار ساعتی:)</span><span class="float-right mr-50">{{ $payroll->low_time }}</span><hr>
                                        <span class="ml-50">@t(هزینه کارگاه:)</span><span class="float-right mr-50">{{ $payroll->wsh }}</span><hr>
                                        <span class="ml-50">@t(مفاصا حساب:)</span><span class="float-right mr-50">{{ $payroll->mofasa }}</span><hr>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;" class="bg-light text-center">@t(مانده وام)</td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(طلب از قبل:)</span><span class="float-right mr-50">{{ $payroll->credit }}</span>
                                    </td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(کسر رند ماه جاری:)</span><span class="float-right mr-50">{{ $payroll->rond }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(وام قرض الحسنه)</span><span class="float-right mr-50">{{ $payroll->rest_of_loan }}</span>
                                    </td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(جمع ناخالص:)</span><span class="float-right mr-50">{{ $payroll->nakhales }}</span>
                                    </td>
                                    <td style="width: 25%;">
                                        <span class="ml-50">@t(کسورات:)</span><span class="float-right mr-50">{{ $payroll->kosoorat }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;"></td>
                                    <td style="width: 25%;"></td>
                                    <td style="width: 25%;" class="bg-light">
                                        <span class="ml-50">@t(خالص ماه جاری:)</span><span class="float-right mr-50">{{ $payroll->khales }}</span>
                                    </td>
                                    <td style="width: 25%;" class="bg-light">
                                        <span class="ml-50">@t(مبلغ پرداختی:)</span><span class="float-right mr-50">{{ $payroll->paid }}</span>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
            {{ sentence() }}
            <span class="float-right mb-1 mt-1">@t(مرکز مشاوره زندگی عاقلانه)</span>
        </div>
    </div>
</body>
    <!-- BEGIN: Vendor JS-->
    <script src="/assets/vendors/js/vendors.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="/assets/vendors/js/ui/jquery.sticky.js"></script> --}}
    <script src="/assets/vendors/js/charts/chart.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
    <script src="/assets/js/core/app-menu.js"></script>
    <script src="/assets/js/core/app.js"></script>
    <script src="/assets/js/scripts/components.js"></script>
    <script src="/assets/js/scripts/footer.js"></script>
    <script src="/assets/js/scripts/customizer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    
    <!-- END: Page JS-->

</html>


