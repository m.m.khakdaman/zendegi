<x-base>
	<x-slot name='title'>
		@t(تعریف مراجع جدید)
	</x-slot>
	<x-slot name='css'>


		<!-- BEGIN: Vendor CSS-->
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
		<!-- END: Vendor CSS-->



		<!-- BEGIN: Theme CSS-->
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/components.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
		<!-- END: Theme CSS-->

		<!-- BEGIN: Page CSS-->
		<link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/wizard.css">
		<!-- END: Page CSS-->

		<base href="/">
	</x-slot>
	<div class="content-header row">
		<div class="content-header-left col-12 mb-2 mt-1">
			<div class="row breadcrumbs-top">
				<div class="col-12">
					<h5 class="content-header-title float-left pr-1">@t(مراجع جدید)</h5>
					<div class="breadcrumb-wrapper">
						<ol class="breadcrumb p-0 mb-0">
							<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i
										class="bx bx-home-alt"></i></a>
							</li>
							<li class="breadcrumb-item"><a href="{{ route('customer.index') }}">لیست مراجعین</a>
							</li>
							<li class="breadcrumb-item active">@t(مراجع جدید)
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Form wizard with step validation section start -->
	<section id="validation">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header pb-0">
						<h4 class="card-title">@t(فرم پرسونا)
					</div>
					@include('layouts.errors')
					<div class="card-content">
						<div class="card-body">
							<form action="{{ route('customer.store') }}" class="wizard-validation" method="POST" enctype="multipart/form-data">
								@csrf
								<div id="div_submit_form">
									<button type="submit" id="submit_form" class="btn btn-success"">ثبت</button>
								</div>
								<!-- Step 1 -->
								<h6>
									<i class="step-icon"></i>
									<span>@t(اطلاعات پایه)</span>
								</h6>
								<!-- Step 1 -->
								<!-- body content of step 1 -->
								<fieldset>
									<div class="row">
										<div class="col-md-3">											
											<div class="form-group control">
												<label for="firstName">@t(نام) </label>
												<input type="text" class="form-control " id="firstName"
													value="{{ old('firstName') }}" name="firstName" required
													placeholder="@t(نام را وارد کنید)" maxlength="40">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group control">
												<label for="lastName">@t(نام خانوادگی)</label>
												<input type="text" class="form-control " id="lastName" name="lastName"
													value="{{ old('lastName') }}" required
													placeholder="@t(نام خانوادگی)" maxlength="50">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group control">
												<label for="mobile">@t(موبایل) </label>
												<input type="tel" class="form-control" id="mobile" name="Fmobile"
												onchange="check_mobile()"
													value="{{ old('Fmobile') }}" required
													placeholder="@t(تلفن همراه.)">
													<small id="error_mobile_repart"></small>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group control">
												<label for="account_number">@t(شماره حساب بانکی)</label>
												<input type="number" class="form-control" id="account_number" name="account_number"
														value="{{ old('account_number') }}" placeholder='@t(شماره حساب بانکی)'>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<div class="select-box mr-1">
													<label for="select2-users-name">@t(پیش مشاوره) </label>
													<select class="select2 form-control" id="select2-users-name" name="resiver_id" required>
														<option value=""></option>
														@foreach ($admins as $admin)
														<option value="{{ $admin->id }}">{{ $admin->name.' '.$admin->family }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label for="mobile2">@t(موبایل دوم)</label>
												<input type="tel" class="form-control" id="mobile2" name="Smobile"
													value="{{ old('Smobile') }}"
													placeholder="@t(در صورت وجود شماره تلفن همراه دوم را وارد کنید)">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="phone">@t(تلفن ثابت)</label>
												<input type="tel" class="form-control " id="phone" name="phone"
													value="{{ old('phone') }}"
													placeholder="@t(شماره تلفن ثابت را وارد کنید)">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="id_number">@t(شماره شناسنامه)</label>
												<input type="tel" class="form-control " id="id_number" name="id_number"
													value="{{ old('id_number') }}"
													placeholder="@t(شماره شناسنامه را وارد کنید)">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="national_code">@t(کد ملی)</label>
												<input type="tel" class="form-control " id="national_code"
													name="national_code" value="{{ old('national_code') }}"
													placeholder="@t(کد ملی را وارد کنید)">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="father_name">@t(نام پدر) </label>
												<input type="text" class="form-control " id="father_name"
													value="{{ old('father_name') }}" name="father_name"
													placeholder="@t(نام پدر را وارد کنید)" maxlength="40">
											</div>
										</div>
										<div class="col-md-2">
											<label class="d-block">جنسیت</label>
												<div class="custom-control-inline">
													<div class="radio mr-1">
														<input type="radio" name="gender" id="male" checked
															value="{{ setting('male_id') }}">
														<label for="male">مرد</label>
													</div>
													<div class="radio mr-1">
														<input type="radio" name="gender" id="female" value="{{ setting('female_id') }}">
														<label for="female">زن</label>
													</div>
												</div>
										
											{{-- <div class="form-group">
												<label for="spouse_name">@t(نام همسر) </label>
												<input type="text" class="form-control " id="spouse_name"
													value="{{ old('spouse_name') }}" name="spouse_name"
													placeholder="@t(نام همسر را وارد کنید)" maxlength="40">
											</div> --}}
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="location">@t(نحوه آشنایی با مرکز)</label>
												<select class="select2 form-control" id="location"
													name="Introduction_method">
													@foreach ($introduction_methods as $introduction_method)

														<option value="{{ $introduction_method->id }}">
															{{ $introduction_method->Introduction_method }}</option>
													@endforeach

												</select>
											</div>
										</div>
										<div class="col-md-4">
											<fieldset class="form-group">
												<label for="basicInputFile">@t(عکس پرسنلی)</label>
												<div class="custom-file">
													<input type="file" class="custom-file-input" id="inputGroupFile01" name="file">
													<label class="custom-file-label" for="inputGroupFile01">انتخاب فایل</label>
												</div>
											</fieldset>
										</div>


										<div class="col-md-4">
											<div class="form-group">
												<label for="note">@t(توضیحات)</label>
												<textarea name="note" class="form-control"></textarea>
											</div>
										</div>
									</div>
								</fieldset>
								<!-- body content of step 1 end -->
								<!-- Step 2 -->
								<h6>
									<i class="step-icon"></i>
									<span>@t(مشخصات)</span>
								</h6>
								<!-- step 2 -->
								<!-- body content of step 2  -->
								<fieldset>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="birthday">
													@t(تاریخ تولد)
												</label>
												<input type="date" class="form-control" id="birthday" name="birthday"
													placeholder="@t(تاریخ تولد را وارد کنید)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="education_level">
													@t(مدرک تحصیلی)
												</label>
												<select name="education_level" class="custom-select form-control"
													id="education_level" name="education_level">
													@foreach ($education_levels as $education_level)

														<option value="{{ $education_level->id }}">
															{{ $education_level->education_level }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="field_of_study">
													@t(رشته تحصیلی)
												</label>
												<input type="text" class="form-control" id="field_of_study"
													name="field_of_study" placeholder="@t(رشته تحصیلی را وارد کنید)">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label class="d-block">@t(وضعیت تاهل)</label>
												<div class="custom-control-inline">
													<div class="radio">
														<input type="radio" name="marital_status" id="single"
															value="{{ setting('single') }}" checked>
														<label for="single">@t(مجرد)</label>
													</div>
													<div class="radio">
														<input type="radio" name="marital_status" id="married"
															value="{{ setting('married') }}">
														<label for="married">@t(متاهل)</label>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="old_file_number">@t(شماره پرونده قدیم)</label>
												<input type="number" class="form-control" id="old_file_number" name="old_file_number"
														value="{{ old('old_file_number') }}" placeholder='@t(شماره پرونده قدیم)'>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="date_of_marrige">
													@t(تاریخ ازدواج)
												</label>
												<input type="date" class="form-control" id="date_of_marrige"
													name="date_of_marrige">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="date_of_divorce">
													@t(تاریخ جدایی)
												</label>
												<input type="date" class="form-control" id="date_of_divorce"
													name="date_of_divorce">
											</div>
										</div>

										<div class="d-inline col-md-4">
											<label for="number_of_children">
												@t(تعداد فرزند)
											</label>
											<input type="number" name="number_of_children" id="number_of_children"
												value="1">
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="jobTitle">
													@t(شغل)
												</label>
												<input type="text" class="form-control" id="jobTitle" name="job"
													placeholder="عنوان شغل باید دقیق باشد">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="Income_level">
													@t(سطح درآمد)
												</label>
												<input type="num" class="form-control" id="Income_level"
													name="Income_level" placeholder="سطح درآمد حتی المکان دقیق باشد">
											</div>
										</div>

										<hr>
										<div class="col-md-3">
											<div class="form-group">
												<label for="connection_method">@t(روش مناسب ارتباط)</label>
												<select name="connection_method" id="connection_method"
													class="select2 form-control select-light-warning" multiple>
													@foreach ($connection_methods as $connection_method)
														<option value="{{ $connection_method->id }}">
															{{ $connection_method->connection_method }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="best_time_to_call">@t(بهترین زمان تماس)</label>
												<input type="text" name="best_time_to_call" class="form-control"
													id="best_time_to_call" name="best_time_to_call"
													placeholder="@t(بهترین زمان تماس)">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="time_to_use_internet">@t(زمان اتصال به اینترنت)</label>
												<input name="time_to_use_internet" type="text" class="form-control"
													id="time_to_use_internet" placeholder="@t(زمان اتصال به اینترنت)">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="categories">@t(فکر می کنید در چه زمینه ای نیاز به مهارت و
													آموزش
													دارید؟)</label>
												<select name="categories"
													class="select2 form-control select-light-success" id="categories"
													multiple>
													@foreach ($categories as $categories)
														<option value="{{ $categories->id }}">
															{{ $categories->name }}</option>
													@endforeach

												</select>
											</div>
										</div>
									</div>

								</fieldset>
								<!-- body content of step 2 end -->
								<!-- Step 3 -->
								<h6>
									<i class="step-icon"></i>
									<span>@t(سلیقه ها)</span>
								</h6>
								<!-- step 3 end -->
								<!-- body content of step 3 -->
								<fieldset>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="email">
													@t(آدرس ایمیل)
												</label>
												<input type="email" class="form-control " id="email" name="email"
													placeholder="@t(آدرس ایمیل را وارد کنید)">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="instagram">
													@t(آدرس اینستاگرام)
												</label>
												<input type="text" class="form-control " id="instagram" name="instagram"
													placeholder="@t(آدرس اینستاگرام را وارد کنید)">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="mr-2">@t(از چه منابع آموزشی استفاده می کنید؟)</label>
												<div class="c-inputs-stacked">
													<div class="d-inline-block mr-2">
														<fieldset>
															<div class="checkbox">
																<input name="educational_resources" type="checkbox"
																	class="checkbox__input" id="checkbox1">
																<label for="checkbox1">@t(صوتی و تصویری)</label>
															</div>
														</fieldset>
													</div>
													<div class="d-inline-block">
														<fieldset>
															<div class="checkbox">
																<input name="educational_resources" type="checkbox"
																	name="book" value="1" class="checkbox__input"
																	id="checkbox2">
																<label for="checkbox2">@t(کتاب)</label>
															</div>
														</fieldset>
													</div>
													<div class="d-inline-block">
														<fieldset>
															<div class="checkbox">
																<input name="educational_resources" type="checkbox"
																	class="checkbox__input" id="checkbox3">
																<label for="checkbox3">@t(کلاس آموزشی)</label>
															</div>
														</fieldset>
													</div>
													<div class="d-inline-block">
														<fieldset>
															<div class="checkbox">
																<input name="educational_resources" type="checkbox"
																	class="checkbox__input" id="checkbox4">
																<label for="checkbox4">@t(فضای مجازی و سایت)</label>
															</div>
														</fieldset>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="book_and_mag">
													@t(کتابها و مجلات مورد علاقه)
												</label>
												<textarea class="form-control" name="book_and_mag"
													id="book_and_mag"></textarea>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="brands">
													@t(برندهای مورد علاقه)
												</label>
												<textarea class="form-control" name="brands" id=""></textarea>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="entertainment">
													@t(سرگرمی های مراجع)
												</label>
												<textarea class="form-control" name="entertainment"
													id="entertainment"></textarea>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="address">
													@t(آدرس)
												</label>
												<textarea class="form-control" name="address" id="address"></textarea>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="postal_code">
													@t(کد پستی)
												</label>
												<input type="text" class="form-control " id="postal_code"
													name="postal_code" placeholder="@t(کدپستی را وارد کنید)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="advertising_way">@t(به کدام راه تبلیغاتی توجه می
													کنید؟)</label>
												<select name="advertising_way"
													class="select2 form-control select-light-danger"
													id="advertising_way" multiple>
													<option value="square">@t(تراکت)</option>
													<option value="rectangle" selected>@t(بیلبورد)</option>
													<option value="rombo">@t(رادیو و تلویزیون)</option>
													<option value="romboid">@t(فضای مجازی)</option>
													<option value="trapeze">@t(سایت)</option>
													<option value="traible" selected>@t(تماس تلفنی)</option>
												</select>
											</div>
										</div>
									</div>
								</fieldset>
								<!-- body content of step 3 end -->
								<!-- step 4 -->
								<h6>
									<i class="step-icon"></i>
									<span>@t(روزمره)</span>
								</h6>
								<!-- step 4 end -->
								<!-- body content of step 4  -->
								<fieldset>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="everyday_events">
													@t(در زندگی روزمره شما چه اتفاقاتی جریان دارد؟)
												</label>
												<textarea class="form-control" name="everyday_events"
													id="everyday_events"></textarea>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="best_gift">
													@t(بهترین هدیه ای که در زندگی گرفته اید چیست؟)
												</label>
												<input type="text" class="form-control " id="best_gift" name="best_gift"
													placeholder="@t(بهترین هدیه ای که در زندگی گرفته اید چیست؟)">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="color">
													@t(چه رنگی را دوست دارید؟)
												</label>
												<input type="text" class="form-control " id="color" name="color"
													placeholder="@t(چه رنگی  را دوست دارید؟)">
											</div>
										</div>

									</div>
								</fieldset>
								<!-- body content of step 4 end -->
								<!-- step 5 -->
								<h6>
									<i class="step-icon"></i>
									<span>@t(ارجاع به مشاور)</span>
								</h6>
								<!-- step 5 end -->
								<!-- body content of step 5 -->
								<fieldset>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_advisor1">
													@t(پیشنهاد ارجاع به مشاور،اولویت 1)
												</label>
												<select class="select2 form-control" id="suggested_advisor1" name="suggested_advisor1">
													<option value="">@t(اولویت اول)</option>
													@foreach ($advisors as $advisor)
													<option value="{{ $advisor->name.' '.$advisor->family }}">{{ $advisor->name.' '.$advisor->family }}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_advisor2">
													@t(پیشنهاد ارجاع به مشاور،اولویت 2)
												</label>
												<select class="select2 form-control" id="suggested_advisor2" name="suggested_advisor2">
													<option value="">@t(اولویت دوم)</option>
													@foreach ($advisors as $advisor)
													<option value="{{ $advisor->name.' '.$advisor->family }}">{{ $advisor->name.' '.$advisor->family }}</option>
													@endforeach
												</select>
											</div>
										</div>
									
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_advisor3">
													@t(پیشنهاد ارجاع به مشاور،اولویت 3)
												</label>
												<select class="select2 form-control" id="suggested_advisor3" name="suggested_advisor3">
													<option value="">@t(اولویت سوم)</option>
													@foreach ($advisors as $advisor)
													<option value="{{ $advisor->name.' '.$advisor->family }}">{{ $advisor->name.' '.$advisor->family }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									{{-- <div class="row"> --}}
										{{-- <div class="col-md-3">
											<div class="form-group">
												<label for="session_follow_up">
													@t(مسئول پیگیری)
												</label>
												<select class="select2 form-control " id="session_follow_up" name="session_follow_up">
													<option value="">@t(مسئول پیگیری)</option>
													@foreach ($admins as $admin)
													<option value="{{ $admin->id }}">{{ $admin->name.' '.$admin->family }}</option>
													@endforeach
												</select>
											</div>
										</div> --}}
										{{-- <div class="col-md-6">
											<div class="form-group">
												<label for="note">@t(توضیحات)</label>
												<textarea name="note" class="form-control"></textarea>
											</div>
										</div> --}}
									{{-- </div> --}}
									<input type="hidden" name="history_title" value="@t(نوبت پیش مشاوره)">
									<input type="hidden" name="session_note" value="@t(با سلام. مشخصات مراجع فوق در سیستم ثبت شده و جهت مصاحبه و تکمیل فرم پرسونا به حضور معرفی می گردد.با تشکر)">
								</fieldset>
								<!-- body content of step 5 end -->
								<!-- step 6 -->
								<h6>
									<i class="step-icon"></i>
									<span>@t(ارجاع به کارگاه)</span>
								</h6>
								<!-- step 6 end -->
								<!-- body content of step 6 -->
								<fieldset>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_workshop1">
													@t(پیشنهادارجاع به کارگاه، اولویت1)
												</label>
												<input type="text" class="form-control " id="suggested_workshop1" name="suggested_workshop1"
													placeholder="@t(اولویت1)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_workshop2">
													@t(پیشنهادارجاع به کارگاه، اولویت2)
												</label>
												<input type="text" class="form-control " id="suggested_workshop2" name="suggested_workshop2"
													placeholder="@t(اولویت2)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_workshop3">
													@t(پیشنهادارجاع به کارگاه، اولویت3)
												</label>
												<input type="text" class="form-control " id="suggested_workshop3" name="suggested_workshop3"
													placeholder="@t(اولویت3)">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_workshop4">
													@t(پیشنهادارجاع به کارگاه، اولویت4)
												</label>
												<input type="text" class="form-control " id="suggested_workshop4" name="suggested_workshop4"
													placeholder="@t(اولویت4)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_workshop5">
													@t(پیشنهادارجاع به کارگاه، اولویت5)
												</label>
												<input type="text" class="form-control " id="suggested_workshop5" name="suggested_workshop5"
													placeholder="@t(اولویت5)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="workshop_follow_up">
													@t(مسئول پیگیری)
												</label>
												<select class="select2 form-control " id="workshop_follow_up" name="workshop_follow_up">
													<option value="">@t(مسئول پیگیری)</option>
													@foreach ($admins as $admin)
													<option value="{{ $admin->id }}">{{ $admin->name.' '.$admin->family }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<input type="hidden" name="workshop_history_title" value="@t(پیشنهاد کارگاه برای)">
									<input type="hidden" name="workshop_note" value="@t(با سلام. مشخصات مراجع فوق در سیستم ثبت شده و کارگاه های زیر به ایشان پیشنهاد می گردد در صورت موافقت، پرونده را به مسئول کارگاه ها باز بفرستید و درغیر این صورت جهت تکمیل فرم پرسونا روی دکمه پرسونا کلیک نموده و فرم را تکمیل نمایید.با تشکر)">
								</fieldset>
								<!-- body content of step 6 end -->
								<!-- step 7 -->
								<h6>
									<i class="step-icon"></i>
									<span>@t(پیشنهاد محصولات)</span>
								</h6>
								<!-- step 7 end -->
								<!-- body content of step 7 -->
								<fieldset>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_product1">
													@t(پیشنهاد محصولات، اولویت1)
												</label>
												<input type="text" class="form-control " id="suggested_product1" name="suggested_product1"
													placeholder="@t(اولویت اول)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_product2">
													@t(پیشنهاد محصولات، اولویت2)
												</label>
												<input type="text" class="form-control " id="suggested_product2" name="suggested_product2"
													placeholder="@t(اولویت دوم)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_product3">
													@t(پیشنهاد محصولات، اولویت3)
												</label>
												<input type="text" class="form-control " id="suggested_product3" name="suggested_product3"
													placeholder="@t(اولویت سوم)">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_product4">
													@t(پیشنهاد محصولات، اولویت4)
												</label>
												<input type="text" class="form-control " id="suggested_product4" name="suggested_product4"
													placeholder="@t(اولویت چهارم)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="suggested_product5">
													@t(پیشنهاد محصولات، اولویت5)
												</label>
												<input type="text" class="form-control " id="suggested_product5" name="suggested_product5"
													placeholder="@t(اولویت پنجم)">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="product_follow_up">
													@t(مسئول پیگیری)
												</label>
												<select class="select2 form-control " id="product_follow_up" name="product_follow_up">
													<option value="">@t(مسئول پیگیری)</option>
													@foreach ($admins as $admin)
													<option value="{{ $admin->id }}">{{ $admin->name.' '.$admin->family }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<input type="hidden" name="product_history_title" value="@t(پیشنهاد محصول برای)">
									<input type="hidden" name="product_note" value="@t(با سلام. مشخصات مراجع فوق در سیستم ثبت شده و محصولات زیر به ایشان پیشنهاد می گردد در صورت موافقت، پرونده را به مسئول فروش محصولات باز بفرستید و درغیر این صورت جهت تکمیل فرم پرسونا روی دکمه پرسونا کلیک نموده و فرم را تکمیل نمایید.با تشکر)">
								</fieldset>
								<!-- body content of step 7 end -->

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Form wizard with step validation section end -->
	<x-slot name="script">

		<!-- BEGIN: Vendor JS-->
		<script src="/assets/vendors/js/vendors.min.js"></script>
		<script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
		<script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
		<script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
		<!-- BEGIN Vendor JS-->

		<!-- BEGIN: Page Vendor JS-->
		<script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
		<script src="/assets/vendors/js/extensions/jquery.steps.min.js"></script>
		<script src="/assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
		<script src="/assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
		<script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
		<!-- END: Page Vendor JS-->

		<!-- BEGIN: Theme JS-->
		<script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
		<script src="/assets/js/core/app-menu.js"></script>
		<script src="/assets/js/core/app.js"></script>
		<script src="/assets/js/scripts/components.js"></script>
		<script src="/assets/js/scripts/footer.js"></script>
		<script src="/assets/js/scripts/customizer.js"></script>
		<!-- END: Theme JS-->



		<script>
			//       Validate steps wizard //
			// -----------------------------
			// Show form
			var stepsValidation = $(".wizard-validation");
			var form = stepsValidation.show();

			stepsValidation.steps({
				headerTag: "h6",
				bodyTag: "fieldset",
				transitionEffect: "fade",
				titleTemplate: '<span class="step">#index#</span> #title#',
				labels: {
					cancel: "انصراف",
					current: "قدم کنونی:",
					pagination: "صفحه بندی",
					finish: "ثبت",
					next: "بعدی",
					previous: "قبلی",
					loading: "در حال بارگذاری ..."
				},
				onStepChanging: function(event, currentIndex, newIndex) {
					// Allways allow previous action even if the current form is not valid!
					if (currentIndex > newIndex) {
						return true;
					}
					form.validate().settings.ignore = ":disabled,:hidden";
					return form.valid();
				},
				onFinishing: function(event, currentIndex) {
					form.validate().settings.ignore = ":disabled";
					return form.valid();
				},
				onFinished: function(event, currentIndex) {
					$('#submit_form').click();
				}
			});

			// Initialize validation

			jQuery.extend(jQuery.validator.messages, {
				required: "وارد کردن این فیلد الزامی است.",
				remote: "لطفا این فیلد را اصلاح کنید.",
				email: "لطفا یک آدرس ایمیل معتبر وارد کنید.",
				url: "لطفا یک URL معتبر وارد کنید.",
				date: "لطفا یک تاریخ معتبر وارد کنید.",
				dateISO: "لطفا یک تاریخ معتبر وارد کنید (ISO).",
				number: "لطفا یک شماره معتبر وارد کنید.",
				digits: "لطفا فقط عدد وارد کنید.",
				creditcard: "لطفا یک شماره کارت معتبر وارد کنید.",
				equalTo: "لطفا همان مقدار را دوباره وارد کنید.",
				accept: "لطفا یک مقدار با پسوند معتبر وارد کنید.",
				maxlength: jQuery.validator.format("لطفا بیشتر از {0} حرف وارد نکنید."),
				minlength: jQuery.validator.format("لطفا حداقل {0} حرف وارد کنید."),
				rangelength: jQuery.validator.format("لطفا مقداری به طول بین {0} و {1} وارد کنید."),
				range: jQuery.validator.format("لطفا مقداری بین {0} و {1} وارد کنید."),
				max: jQuery.validator.format("لطفا مقداری کمتر یا مساوی {0} وارد کنید."),
				min: jQuery.validator.format("لطفا مقداری بیشتر یا مساوی {0} وارد کنید.")
			});

			stepsValidation.validate({
				ignore: 'input[type=hidden]', // ignore hidden fields
				errorClass: 'danger line-height-2',
				successClass: 'success line-height-2',
				highlight: function(element, errorClass) {
					$(element).removeClass(errorClass);
				},
				unhighlight: function(element, errorClass) {
					$(element).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.insertAfter(element);
				},
				rules: {
					emailAddress: {
						required: true,
						email: true
					}
				},
				messages: {
					emailAddress: {
						required: "وارد کردن ایمیل الزامی است.",
						email: "لطفا یک ایمیل معتبر وارد نمایید."
					}
				}
			});

			// live Icon color change on state change
			$(document).ready(function() {
				$(".current").find(".step-icon").addClass("bx bx-time-five");
				$(".current").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
					strokeColor: '#5A8DEE'
				});
			});

			// Icon change on state

			// if click on next button icon change
			$(".actions [href='#next']").click(function() {
				$(".done").find(".step-icon").removeClass("bx bx-time-five").addClass("bx bx-check-circle");
				$(".current").find(".step-icon").removeClass("bx bx-check-circle").addClass("bx bx-time-five");
				// live icon color change on next button's on click
				$(".current").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
					strokeColor: '#5A8DEE'
				});
				$(".current").prev("li").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
					strokeColor: '#39DA8A'
				});
			});

			$(".actions [href='#previous']").click(function() {
				// live icon color change on next button's on click
				$(".current").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
					strokeColor: '#5A8DEE'
				});
				$(".current").next("li").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
					strokeColor: '#adb5bd'
				});
			});

			// if click on  submit   button icon change
			$(".actions [href='#finish']").click(function() {
				$(".done").find(".step-icon").removeClass("bx-time-five").addClass("bx bx-check-circle");
				$(".last.current.done").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
					strokeColor: '#39DA8A'
				});
			});

			// add primary btn class
			$('.actions a[role="menuitem"]').addClass("btn btn-primary");
			$('.icon-tab [role="menuitem"]').addClass("glow ");
			$('.wizard-vertical [role="menuitem"]').removeClass("btn-primary").addClass("btn-light-primary");


			$('#number_of_children').TouchSpin();

		</script>
		<!-- BEGIN: Page JS-->
		<script src="/assets/js/scripts/forms/select/form-select2.js"></script>
		<!-- END: Page JS-->
		<script>
			function check_mobile(){
				var mobile = $('#mobile').val();
				jQuery.ajax({
						type: 'POST',
						url: 'check_mobile',
						data: 'mobile='+ mobile,
						cache: false,
						success: function(response){
							if(response == 0){
								

								$('#div_submit_form').html(`<button type="submit" id="submit_form" class="btn btn-success">ثبت</button>`);
							}
							else {
								$('#div_submit_form').html(`<button type="submit" id="submit_form" class="btn btn-success" onclick="return confirm('شماره موبایل تکراری می باشد آیا از ثبت فرم مطمئنید؟')">ثبت</button>`);
							}
						}
					});
			}
			</script>
	</x-slot>
</x-base>
