<x-base>
    <x-slot name='title'>
        ایجاد مراجع جدید (سریع)
    </x-slot>

    <x-slot name='css'>


        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/file-uploaders/dropzone.css">
        <!-- END: Page CSS-->

        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(ایجاد مراجع جدید)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('customer.index') }}">@t(لیست مراجعین)</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#"> @t(ایجاد مراجع جدید)</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(ایجاد مراجع جدید)</h4>
                    </div>
                    <div class="card-content mt-2">
                        <div class="card-body">
                            {{-- @include('layouts.errors') --}}
                            <form action="{{ route('customer.simple_store') }}" method="POST"
                                class="wizard-horizontal" novalidate>
                                @csrf
                                <div class="row">
                                    <div class="col-md-6" ‍>
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" name="back_url" value="" id="back_url" style="display: none">
                                                <input type="text" class="form-control" id="name" required name="name"
                                                    data-validation-required-message="پر کردن فیلد نام اجباری است."
                                                    value="{{ old('name') }}" placeholder='@t(نام)'>
                                                <label for="name">@t(نام)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="family" required
                                                    name="family"
                                                    data-validation-required-message="پر کردن فیلد نام خانوادگی اجباری است."
                                                    value="{{ old('family') }}"
                                                    placeholder='@t(نام خانوادگی)'>
                                                <label for="family">@t(نام خانوادگی)</label>
                                            </div>
                                        </div>
                                    </div>

										<div class="col-md-6">
											<div class="form-group">
												<label for="location">@t(نحوه آشنایی با مرکز)</label>
												<select class="select2 form-control" id="location" required
													name="Introduction_method">
													@foreach ($introduction_methods as $introduction_method)

														<option value="{{ $introduction_method->id }}">
															{{ $introduction_method->Introduction_method }}</option>
													@endforeach

												</select>
											</div>
										</div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="mobile1" required
                                                    name="mobile1"
                                                    data-validation-required-message="پر کردن فیلد شماره همراه اجباری است."
                                                    value="{{ old('mobile1')}}"
                                                    placeholder='@t( شماره همراه)'>
                                                <label for="mobile1">@t(شماره همراه)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card-content">
                                            <p>جنسیت</p>
                                            <div class="card-body">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-primary" value="1"
                                                                    name="gender" id="man" checked>
                                                                <label class="custom-control-label"
                                                                    for="man">مرد</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-danger" value="2"
                                                                    name="gender" id="woman">
                                                                <label class="custom-control-label"
                                                                    for="woman">زن</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" value="3"
                                                                    class="custom-control-input bg-white"
                                                                    name="gender" id="unknow">
                                                                <label class="custom-control-label"
                                                                    for="unknow">نامشخص</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- <form ></form>

                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">آپلود فایل چندتایی</h4>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <form action="{{ route('admin.dropzone_upload') }}" class="dropzone dropzone-area" method="POST"
                                                        id="dpz-multiple-files">
                                                        @csrf
                                                        <div class="dz-message">فایل های خود را برای ارسال به اینجا
                                                            بکشید</div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-12"></div>
                                    <div class="form-group float-right">
                                        <input type="submit" class="form-control btn btn-success" value="ثبت" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/extensions/dropzone.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/moment.min.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <script>
            if  (getCookie('back_url') != ""){
                $('#back_url').val(getCookie('back_url'));
            }
        
            </script>
        
        <!-- END: Page JS-->

    </x-slot>
</x-base>
