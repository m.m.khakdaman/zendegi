<x-base>
    <x-slot name='title'>
        ویرایش مراجع {{ $user->name . ' ' . $user->family }}
    </x-slot>

    <x-slot name='css'>


        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/ui/prism.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/wizard.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/file-uploaders/dropzone.css">
        <!-- END: Page CSS-->

        <base href="/">
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(ویرایش مراجع)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('customer.index') }}">@t(لیست مراجعین)</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#"> @t(ویرایش مراجع)</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $user->name . ' ' . $user->family }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">@t(ویرایش مراجع) {{ $user->name . ' ' . $user->family }}</h4>
                    </div>
                    <div class="card-content mt-2">
                        <div class="card-body">
                            {{-- @include('layouts.errors') --}}
                            <form action="{{ route('customer.update', $user->id) }}" method="POST" enctype="multipart/form-data" 
                                class="wizard-horizontal" novalidate>
                                @method('PATCH')
                                @csrf
                                <div class="row">
                                    <div class="col-md-3" ‍>
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="name" required name="name"
                                                    data-validation-required-message="پر کردن فیلد نام اجباری است."
                                                    value="{{ old('name') ?? $user->name }}" placeholder='@t(نام)'>
                                                <label for="name">@t(نام)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="family" required
                                                    name="family"
                                                    data-validation-required-message="پر کردن فیلد نام خانوادگی اجباری است."
                                                    value="{{ old('family') ?? $user->family }}"
                                                    placeholder='@t(نام خانوادگی)'>
                                                <label for="family">@t(نام خانوادگی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="father_name"
                                                    name="father_name"
                                                    value="{{ old('father_name') ?? $user->father_name }}"
                                                    placeholder="نام پدر">
                                                <label for="father_name">نام پدر</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" name="date_of_birth" id="date_of_birth"
                                                    {{-- pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                                    data-validation-pattern-message="فرمت فیلد معتبر نیست." --}}
                                                    value="{{ old('date_of_birth') ?? $user->date_of_birth }}"
                                                    placeholder="تاریخ تولد">
                                                <div class="form-control-position">
                                                    <i class="bx bx-calendar"></i>
                                                </div>
                                                <label for="date_of_birth">تاریخ تولد</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="national_code"
                                                    name="national_code"
                                                    value="{{ old('national_code') ?? $user->national_code }}"
                                                    placeholder='@t( کدملی)'>
                                                <label for="national_code">@t(کدملی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group validate">
                                            <div class="controls form-label-group ">
                                                <select class="form-control " id="education_level" name="education_level">
                                                    <option value="">مدرک تحصیلی:</option>
                                                    <option value="1">دایره</option>
                                                    <option value="2">بیضی</option>
                                                    <option value="3">مثلث</option>
                                                    <option value="4">چند ضلعی</option>
                                                </select>
                                                <label for="education_level">@t(مدرک تحصیلی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="field_of_study" name="field_of_study"
                                                    value="{{ old('field_of_study') ?? $user->field_of_study }}" placeholder='@t(رشته تحصیلی)'>
                                                <label for="field_of_study">@t(رشته تحصیلی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile01" name="file">
                                                <label class="custom-file-label" for="inputGroupFile01">@t(عکس پرسنلی)</label>
                                            </div>
                                        </fieldset>
                                        {{-- <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="file" class="form-control" id="pic" name="pic"
                                                    placeholder='@t(عکس پرسنلی)'>
                                                <label for="pic">@t(عکس پرسنلی)</label>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="id_number" 
                                                    name="id_number"
                                                    {{-- data-validation-required-message="پر کردن فیلد شماره شناسنامه اجباری است." --}}
                                                    value="{{ old('mobile') ?? $user->id_number }}"
                                                    placeholder='@t( شماره شناسنامه)'>
                                                <label for="id_number">@t(شماره شناسنامه)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="mobile1" 
                                                    name="mobile1"
                                                    pattern="0[0-9]{10}"
                                                    data-validation-pattern-message="این شماره فرمت معتبری نیست."
                                                    {{-- data-validation-required-message="پر کردن فیلد شماره همراه اجباری است." --}}
                                                    value="{{ old('mobile') ?? $user->mobile }}"
                                                    placeholder='@t( شماره همراه)'>
                                                <label for="mobile1">@t(شماره همراه)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="mobile2" name="mobile2"
                                                    pattern="0[0-9]{10}"
                                                    data-validation-pattern-message="این شماره فرمت معتبری نیست."
                                                    value="{{ old('mobile2') ?? $user->mobile2 }}"
                                                    placeholder='@t(شماره همراه دوم)'>
                                                <label for="mobile2">@t(شماره همراه دوم)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="tel" class="form-control" id="phone" name="phone"
                                                    pattern="[0-9]+"
                                                    data-validation-pattern-message="این شماره فرمت معتبری نیست."
                                                    value="{{ old('phone') ?? $user->phone }}"
                                                    placeholder='@t(شماره ثابت)'>
                                                <label for="phone">@t(شماره ثابت)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-content">
                                            <p>@t(جنسیت)</p>
                                            <div class="card-body">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-primary" value="1"
                                                                name="gender" id="man" @if ($user->gender == 'مرد')
                                                                    checked
                                                                @endif>
                                                                <label class="custom-control-label"
                                                                    for="man">مرد</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input bg-danger" value="2"
                                                                    name="gender" id="woman" @if ($user->gender == 'زن')
                                                                    checked
                                                                @endif>>
                                                                <label class="custom-control-label"
                                                                    for="woman">زن</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" value="3"
                                                                    class="custom-control-input bg-warning"
                                                                    name="gender" id="unknow" @if ($user->gender == 'نامشخص')
                                                                    checked
                                                                @endif>>
                                                                <label class="custom-control-label"
                                                                    for="unknow">نامشخص</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="card-content">
                                            <p>@t(خلق و خوی)</p>
                                            <div class="card-body">
                                                <ul class="list-unstyled mb-0">
                                                    @foreach ($mood as $mood)
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio"
                                                                    class="custom-control-input {{ $mood->color }}"
                                                                    name="moods_id" value="{{ $mood->id }}" id="{{ $mood->id }}"
                                                                    @if ($user->moods_id == $mood->id)
                                                                        checked
                                                                    @endif>
                                                                <label class="custom-control-label"
                                                                    for="{{ $mood->id }}">{{ $mood->mood }}</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="card-content">
                                            <p>@t(مراجع خاص)</p>
                                            <div class="card-body">
                                                <fieldset>
                                                    <div class="checkbox">
                                                        <input name="SpecialUser" type="checkbox" value="1" {{ ($user->SpecialUser==1?'checked':'') }} class="checkbox__input" id="SpecialUser">
                                                        <label for="SpecialUser">@t(مراجع خاص)</label>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="number" class="form-control" id="account_number" name="account_number"
                                                    value="{{ old('account_number') ?? $user->account_number }}" placeholder='@t(شماره حساب بانکی)'>
                                                <label for="account_number">@t(شماره حساب بانکی)</label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="postal_code" name="postal_code"
                                                    value="{{ old('postal_code') ?? $user->postal_code }}" placeholder='@t(کد پستی)'>
                                                <label for="postal_code">@t(کد پستی)</label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="email" name="email"
                                                    value="{{ old('email') ?? $user->email }}" placeholder='@t(ایمیل)'>
                                                <label for="email">@t(ایمیل)</label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="instagram" name="instagram"
                                                    value="{{ old('instagram') ?? $user->instagram }}" placeholder='@t(آی دی اینستاگرام)'>
                                                <label for="instagram">@t(آی دی اینستاگرام)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="number" class="form-control" id="old_file_number" name="old_file_number"
                                                    value="{{ old('old_file_number') ?? $user->old_file_number }}" placeholder='@t(شماره پرونده قدیمی)'>
                                                <label for="old_file_number">@t(شماره پرونده قدیمی)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="address" name="address"
                                                    value="{{ old('address') ?? $user->address }}" placeholder='@t(آدرس)'>
                                                <label for="address">@t(آدرس)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <textarea class="form-control" id="note" name="note"
                                                    placeholder='@t(توضیحات)'>{{ old('note') ?? $user->note }}</textarea>
                                                <label for="note">@t(توضیحات)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                            <label class="font-small-1 mt-0 pt-0" for="marital_status">@t(وضعیت تاهل)</label>
                                        <div class="form-group">
                                            <select name="marital_status"
                                                class="selectd2 form-control"
                                                id="marital_status">
                                                @foreach ($marital_status as $marital_status)
                                                <option 
                                                value="{{ $marital_status->id }}" 
                                                @if ($user->marital_status_id == $marital_status->id)
                                                    selected
                                                @endif>
                                                {{ $marital_status->marital_status }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                                <label for="spouse_name">نام همسر</label>
                                                <input type="text" class="form-control" id="spouse_name"
                                                    name="spouse_name"
                                                    value="{{ old('spouse_name') ?? $user->spouse_name }}"
                                                    placeholder="نام همسر">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="date_of_marrige">تاریخ ازدواج</label>
                                            <input type="text" class="form-control" name="date_of_marrige" id="date_of_marrige"
                                                    {{-- pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                                    data-validation-pattern-message="فرمت فیلد معتبر نیست." --}}
                                                    value="{{ old('date_of_marrige') ?? $user->date_of_marrige }}"
                                                    placeholder="تاریخ ازدواج">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="date_of_divorce">تاریخ جدایی</label>
                                            <input type="text" class="form-control" name="date_of_divorce" id="date_of_divorce"
                                                    {{-- pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                                    data-validation-pattern-message="فرمت فیلد معتبر نیست." --}}
                                                    value="{{ old('date_of_divorce') ?? $user->date_of_divorce }}"
                                                    placeholder="تاریخ جدایی">
                                        </div>
                                    </div>
                                </div>
                                <hr class="mb-3">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group validate">
                                            <div class="controls form-label-group ">
                                                <select class="form-control " id="connection_method" name="connection_method"
                                                    placeholder='@t( روش مناسب ارتباط)'>
                                                    <option value="">روش مناسب ارتباط:</option>
                                                    <option value="1">دایره</option>
                                                    <option value="2">بیضی</option>
                                                    <option value="3">مثلث</option>
                                                    <option value="4">چند ضلعی</option>
                                                </select>
                                                <label for="connection_method">@t(روش مناسب ارتباط)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="best_time_to_call"
                                                    name="best_time_to_call"
                                                    value="{{ old('best_time_to_call') ?? $user->best_time_to_call }}"
                                                    placeholder='@t( بهترین زمان تماس)'>
                                                <label for="best_time_to_call">@t(بهترین زمان تماس)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="controls form-label-group ">
                                                <input type="text" class="form-control" id="time_to_use_internet"
                                                    name="time_to_use_internet"
                                                    value="{{ old('time_to_use_internet') ?? $user->time_to_use_internet }}"
                                                    placeholder='@t( زمان اتصال به اینترنت)'>
                                                <label for="time_to_use_internet">@t(زمان اتصال به اینترنت)</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="number_of_children">@t(تعداد فرزند)</label>
                                            <input type="number" class="form-control" name="number_of_children" id="number_of_children"
                                                value="{{ old('number_of_children') ?? $user->number_of_children }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="jobTitle">@t(شغل)</label>
                                            <input type="text" class="form-control" id="jobTitle" name="job"
                                            value="{{ old('job') ?? $user->job }}"
                                                placeholder="عنوان شغل باید دقیق باشد">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="Income_level">@t(سطح درآمد)</label>
                                            <input type="num" class="form-control" id="Income_level"
                                                name="Income_level"
                                                value="{{ old('Income_level') ?? $user->Income_level }}"
                                                placeholder="@t(سطح درآمد حتی المکان دقیق باشد)">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="categories">@t(در چه زمینه ای نیاز به مهارت و آموزش دارید؟)</label>
                                            <select name="categories"
                                                class="select2 form-control select-light-success" id="categories"
                                                multiple>
                                                @foreach ($category as $category)
                                                    <option 
                                                    value="{{ $category->id }}"
                                                    @if ($category->id == $user->categories_id)
                                                    selected 
                                                    @endif>{{ $category->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="advertising_way">@t(به کدام راه تبلیغاتی توجه می کنید؟)</label>
                                            <select name="advertising_way"
                                                class="select2 form-control select-light-danger"
                                                id="advertising_way" multiple>
                                                @foreach ($introduction_methods as $introduction_method)
                                                    <option 
                                                    value="{{ $introduction_method->id }}"
                                                    @if ($introduction_method->id == $user->Introduction_method_id)
                                                        selected
                                                    @endif>{{ $introduction_method->Introduction_method }}</option>    
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mr-2">@t(از چه منابع آموزشی استفاده می کنید؟)</label>
                                            <div class="c-inputs-stacked">
                                                <div class="d-inline-block mr-2">
                                                    <fieldset>
                                                        <div class="checkbox">
                                                            <input name="educational_resources" type="checkbox" value="1"
                                                                class="checkbox__input" id="checkbox1"
                                                                @if ($user->educational_resources == 1)
                                                                    checked
                                                                @endif>
                                                            <label for="checkbox1">@t(صوتی و تصویری)</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="d-inline-block">
                                                    <fieldset>
                                                        <div class="checkbox">
                                                            <input name="educational_resources" type="checkbox"
                                                                name="book" value="2" class="checkbox__input"
                                                                id="checkbox2" 
                                                                @if ($user->educational_resources == 2)
                                                                    checked
                                                                @endif>
                                                            <label for="checkbox2">@t(کتاب)</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="d-inline-block">
                                                    <fieldset>
                                                        <div class="checkbox">
                                                            <input name="educational_resources" type="checkbox" value="3"
                                                                class="checkbox__input" id="checkbox3"
                                                                @if ($user->educational_resources == 3)
                                                                    checked
                                                                @endif>
                                                            <label for="checkbox3">@t(کلاس آموزشی)</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="d-inline-block">
                                                    <fieldset>
                                                        <div class="checkbox">
                                                            <input name="educational_resources" type="checkbox"
                                                                class="checkbox__input" id="checkbox4" value="4"
                                                                @if ($user->educational_resources == 4)
                                                                    checked
                                                                @endif>
                                                            <label for="checkbox4">@t(فضای مجازی و سایت)</label>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="book_and_mag">@t(کتابها و مجلات مورد علاقه)</label>
                                            <textarea class="form-control" name="book_and_mag" id="book_and_mag">{{ $user->book_and_mag }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="brands">
                                                @t(برندهای مورد علاقه)
                                            </label>
                                            <textarea class="form-control" name="brands" id="brands">{{ $user->brands }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="entertainment">
                                                @t(سرگرمی های مراجع)
                                            </label>
                                            <textarea class="form-control" name="entertainment"
                                                id="entertainment">{{ $user->entertainment }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="location">@t(نحوه آشنایی با مرکز)</label>
                                            <select class="select2 form-control" id="location"
                                                name="Introduction_method">
                                                @foreach ($introduction_methods as $introduction_method)

                                                    <option value="{{ $introduction_method->id }}">
                                                        {{ $introduction_method->Introduction_method }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="everyday_events">
                                                @t(در زندگی روزمره شما چه اتفاقاتی جریان دارد؟)
                                            </label>
                                            <textarea class="form-control" name="everyday_events"
                                                id="everyday_events">{{ $user->everyday_events }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="best_gift">
                                                @t(بهترین هدیه ای که در زندگی گرفته اید چیست؟)
                                            </label>
                                            <input type="text" class="form-control " id="best_gift" name="best_gift"
                                            value="{{ $user->best_gift }}"
                                                placeholder="@t(بهترین هدیه ای که در زندگی گرفته اید چیست؟)">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="color">
                                                @t(چه رنگی را دوست دارید؟)
                                            </label>
                                            <input type="text" class="form-control " id="color" name="color" value="{{ $user->color }}"
                                                placeholder="@t(چه رنگی  را دوست دارید؟)">
                                        </div>
                                    </div>

                                </div>

                                    {{-- <form ></form>

                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">آپلود فایل چندتایی</h4>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <form action="{{ route('admin.dropzone_upload') }}" class="dropzone dropzone-area" method="POST"
                                                        id="dpz-multiple-files">
                                                        @csrf
                                                        <div class="dz-message">فایل های خود را برای ارسال به اینجا
                                                            بکشید</div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="form-group float-right">
                                        <input type="submit" class="form-control btn btn-success" value="ثبت" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/extensions/dropzone.min.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
        <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
        <script src="/assets/vendors/js/extensions/jquery.steps.min.js"></script>
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="/assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/moment.min.js"></script>
        <script src="/assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/forms/validation/form-validation.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->

        <script>
            $('#date_of_birth').datepicker({
                dateFormat: "yy/mm/dd",
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                maxDate: "+0D",
            });
            $('#madrak').select2({
                dropdownAutoWidth: true,
                width: '100%',
                language: "fa",
                placeholder: "مدرک تحصیلی",
            });
            $('#ellat_morje').select2({
                dropdownAutoWidth: true,
                width: '100%',
                language: "fa",
                placeholder: "علت مراجعه",
            });
            Dropzone.options.dpzMultipleFiles = {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 100, // MB
                clickable: true
            }

        </script>
    </x-slot>
</x-base>
