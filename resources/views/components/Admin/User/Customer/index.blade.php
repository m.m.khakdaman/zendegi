<x-base>
    <x-slot name='title'>
        @t(لیست مراجعین)
    </x-slot>

    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(لیست مراجعین)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('customer.index') }}">@t(لیست مراجعین)</a>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        {{-- @can('customer_create') --}}
                        <div class="row">

                            <div class="invoice-create-btn mb-1">
                                <a href="{{ route('customer.create') }}" class="btn btn-primary glow invoice-create"
                                    role="button" aria-pressed="true">@t(ایجاد مراجع جدید)</a>
                            </div>

                            <div class="invoice-create-btn mb-1 ml-1">
                                <a href="{{ route('customer.simple_create') }}"
                                    class="btn btn-primary glow invoice-create" role="button"
                                    aria-pressed="true">@t(ایجاد مراجع جدید(سریع))</a>
                            </div>
                        </div>
                        {{-- @endcan --}}
                        <div class="table-responsive ">
                            @include('partials.flash')
                            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>

                                        <th>#</th>
                                        <th>ویژه</th>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>آواتار</th>
                                        <th>موبایل</th>
                                        <th>جنسیت</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>ویژه</th>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>آواتار</th>
                                        <th>موبایل</th>
                                        <th>جنسیت</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        {{-- <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script> --}}
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        {{-- <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script> --}}
        {{-- <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script> --}}
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
        <script>$('div.alert').delay(3000).slideUp(300);</script>
        <!-- END: Page JS-->
        <script>
            $.fn.dataTable.pipeline = function(opts) {
                // Configuration options
                var conf = $.extend({
                    pages: 2, // number of pages to cache
                    url: '', // script url
                    data: null, // function or object with parameters to send to the server
                    method: 'GET' // Ajax HTTP method
                }, opts);

                // Private variables for storing the cache
                var cacheLower = -1;
                var cacheUpper = null;
                var cacheLastRequest = null;
                var cacheLastJson = null;

                return function(request, drawCallback, settings) {
                    var ajax = false;
                    var requestStart = request.start;
                    var drawStart = request.start;
                    var requestLength = request.length;
                    var requestEnd = requestStart + requestLength;

                    if (settings.clearCache) {
                        // API requested that the cache be cleared
                        ajax = true;
                        settings.clearCache = false;
                    } else if (cacheLower < 0 || requestStart < cacheLower || requestEnd >
                        cacheUpper) {
                        // outside cached data - need to make a request
                        ajax = true;
                    } else if (JSON.stringify(request.order) !== JSON.stringify(cacheLastRequest
                            .order) ||
                        JSON.stringify(request.columns) !== JSON.stringify(cacheLastRequest
                            .columns) ||
                        JSON.stringify(request.search) !== JSON.stringify(cacheLastRequest.search)
                    ) {
                        // properties changed (ordering, columns, searching)
                        ajax = true;
                    }

                    // Store the request for checking next time around
                    cacheLastRequest = $.extend(true, {}, request);

                    if (ajax) {
                        // Need data from the server
                        if (requestStart < cacheLower) {
                            requestStart = requestStart - (requestLength * (conf.pages - 1));

                            if (requestStart < 0) {
                                requestStart = 0;
                            }
                        }

                        cacheLower = requestStart;
                        cacheUpper = requestStart + (requestLength * conf.pages);

                        request.start = requestStart;
                        request.length = requestLength * conf.pages;

                        // Provide the same `data` options as DataTables.
                        if (typeof conf.data === 'function') {
                            // As a function it is executed with the data object as an arg
                            // for manipulation. If an object is returned, it is used as the
                            // data object to submit
                            var d = conf.data(request);
                            if (d) {
                                $.extend(request, d);
                            }
                        } else if ($.isPlainObject(conf.data)) {
                            // As an object, the data given extends the default
                            $.extend(request, conf.data);
                        }

                        return $.ajax({
                            "type": conf.method,
                            "url": conf.url,
                            "data": request,
                            "dataType": "json",
                            "cache": false,
                            "success": function(json) {
                                cacheLastJson = $.extend(true, {}, json);

                                if (cacheLower != drawStart) {
                                    json.data.splice(0, drawStart - cacheLower);
                                }
                                if (requestLength >= -1) {
                                    json.data.splice(requestLength, json.data.length);
                                }

                                drawCallback(json);
                            }
                        });
                    } else {
                        json = $.extend(true, {}, cacheLastJson);
                        json.draw = request.draw; // Update the echo for each response
                        json.data.splice(0, requestStart - cacheLower);
                        json.data.splice(requestLength, json.data.length);

                        drawCallback(json);
                    }
                }
            };

            // Register an API method that will empty the pipelined data, forcing an Ajax
            // fetch on the next draw (i.e. `table.clearPipeline().draw()`)
            $.fn.dataTable.Api.register('clearPipeline()', function() {
                return this.iterator('table', function(settings) {
                    settings.clearCache = true;
                });
            });
            $('.invoice-data-table thead tr').clone(true).appendTo('.invoice-data-table thead');
            $('.invoice-data-table thead tr:eq(1) th').each(function(i) {
                var title = $(this).text();
                if (title != 'عملیات' && title != 'آواتار' && title != 'ویژه') {
                    $(this).html('<input type="text" placeholder="جست جو در ' + title +
                        '"  class="form-control form-control-sm" />');

                }
                if (title == '#') {
                    $(this).html('<input type="tel" placeholder="جست جو در ' + title +
                        '"  class="form-control form-control-sm" />');
                }
                if (title == 'جنسیت') {
                    $(this).html('<select class="form-control">' +
                        '<option value="">جنسیت</option>' +
                        '<option value="نامشخص">نامشخص</option>' +
                        '<option value="مرد">مرد</option>' +
                        '<option value="زن">زن</option>' +
                        '</select>');

                }
            });
            //
            // DataTables initialisation
            //

            $(document).ready(function() {
                $('.invoice-data-table').DataTable({
                    "processing": true,
                    "serverSide": true,
                    orderCellsTop: true,
                    "order": [[ 0, "desc" ]],
                    fixedHeader: true,
                    "ajax": $.fn.dataTable.pipeline({
                        url: "{{ route('customer.get_all_datatable_ajax') }}",
                        pages: 2 // number of pages to cache
                    }),
                    language: {
                        "sEmptyTable": "هیچ داده‌ای در جدول وجود ندارد",
                        "sInfo": "نمایش _START_ تا _END_ از _TOTAL_ ردیف",
                        "sInfoEmpty": "نمایش 0 تا 0 از 0 ردیف",
                        "sInfoFiltered": "(فیلتر شده از _MAX_ ردیف)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ",",
                        "sLengthMenu": "نمایش _MENU_ ردیف",
                        "sLoadingRecords": "در حال بارگزاری...",
                        "sProcessing": "در حال پردازش...",
                        "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                        "oPaginate": {
                            "sFirst": "برگه‌ی نخست",
                            "sLast": "برگه‌ی آخر",
                            "sNext": "بعدی",
                            "sPrevious": "قبلی"
                        },
                        "oAria": {
                            "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                            "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                        },
                        "sSearch": "",
                        "sSearchPlaceholder": "جستجوی",
                    },
                    "columns": [{
                            "data": "id",
                            "name": 'id'
                        },
                        {
                            data: null,
                            'name': 'SpecialUser',
                            render: function(data, type, row) 
                            {
                                if (data.SpecialUser == 1) {
                                    return `<a href="/admin/user/customer/vip/` + data.id + `" class="text-warning" data-toggle="tooltip" data-placement="top" title="@t(برای تبدیل این مراجع به مراجع معمولی کلیک کنید.)"><i class="bx bxs-star font-large-1"></i></a>`;
                                } else {
                                    return `<a href="/admin/user/customer/vip/` + data.id + `" class="text-light-secondary" data-toggle="tooltip" data-placement="top"  title="@t(برای تبدیل این مراجع به مراجع خاص و ویژه کلیک کنید.)"><i class="bx bx-star font-large-1"></i></a>`;
                                }
                                
                            },
                        },
                        {
                            data: null,
                            'name': 'name',
                            render: function(data, type, row) {
                                return `<a href="/admin/user/customer/` + data.id + `">` + data.name +
                                    `</a>`;
                            },
                        },

                        {
                            data: null,
                            'name': 'family',
                            render: function(data, type, row) {
                                return `<a href="/admin/user/customer/` + data.id + `">` + data.family +
                                    `</a>`;
                            },
                        },
                        {
                            data: null,
                            render: function(data, type, row) {
                                return `<div class="avatar mr-1"><img src="/assets/images/profile/customer/avatar/` +
                                    data.pic +
                                    `" alt="group image" height="32" width="32" data-toggle="tooltip" data-placement="top" title="` +
                                    data.name + ` ` + data.family + `"></div>`;

                            },
                        },
                        {
                            "data": "mobile",
                            "name": 'mobile'
                        },
                        {
                            "data": "gender",
                            "name": 'gender'
                        },
                        {
                            data: null,
                            'name': 'status',
                            render: function(data, type, row) 
                            {
                                if (data.status == 'فعال') {
                                    return `<a href="/admin/user/customer/activation/` + data.id + `" class="text-success" data-toggle="tooltip" data-placement="top" title="@t(برای غیر فعال کردن این مراجع کلیک کنید.)">` + data.status +
                                    `</a>`;
                                } else {
                                    return `<a href="/admin/user/customer/activation/` + data.id + `" class="text-danger" data-toggle="tooltip" data-placement="top" title="@t(برای فعال کردن این مراجع کلیک کنید.)">` + data.status +
                                    `</a>`;
                                }
                                
                            },
                        },
                        {
                            data: null,
                            render: function(data, type, row) {
                                return `
                                <a href="/admin/user/customer/` + data.id + `"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="محصولات خریداری شده"
                                                    class="btn btn-icon rounded-circle btn-light-primary mr-1 mb-1">
                                                    <i class="bx bx-cart-alt"></i>
                                                </a>
                                <a href="/admin/Financial/user/` + data.id + `/invoise"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="صورت حساب"
                                                    class="btn btn-icon rounded-circle btn-light-primary mr-1 mb-1">
                                                    <i class="bx bx-money"></i>
                                                </a>
                                <a href="/admin/user/customer/` + data.id + `/edit"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="ویرایش"
                                                    class="btn btn-icon rounded-circle btn-light-warning mr-1 mb-1">
                                                    <i class="bx bx-edit"></i>
                                                </a>
                                <a href="/admin/Financial/user/` + data.id + `/invoise_test"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="صورت حساب تستی"
                                                    class="btn btn-icon rounded-circle btn-light-primary mr-1 mb-1">
                                                    <i class="bx bx-dots-horizontal"></i>
                                                </a>

                                                          `;
                            },
                        }
                    ],
                    "dom": '<"col-12"<"d-flex justify-content-between"lp><t><"d-flex justify-content-between"ip>>',
                    initComplete: function() {
                        // Apply the search
                        this.api().columns().every(function() {
                            var that = this;
                            $('input', $(
                                '.invoice-data-table thead tr:eq(1) th')[
                                this
                                .index()]).on('keyup change clear',
                                function() {
                                    if (that.search() !== this.value) {
                                        that
                                            .search(this.value)
                                            .draw();
                                    }
                                });
                            $('select', $(
                                '.invoice-data-table thead tr:eq(1) th'
                            )[this
                                .index()]).on('keyup change clear',
                                function() {
                                    if (that.search() !== this.value) {
                                        that
                                            .search(this.value)
                                            .draw();
                                    }
                                });
                        });
                        $('[data-toggle="tooltip"]').tooltip()
                    }
                });
            });
        </script>

    </x-slot>
</x-base>
