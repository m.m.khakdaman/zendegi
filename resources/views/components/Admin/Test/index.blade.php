<x-base>
    <x-slot name='title'>
        @t(لیست انتظار)
    </x-slot>

    <x-slot name='css'>

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css"
            href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <!-- END: Page CSS-->
    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(لیست انتظار)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('admin.home') }}">
                                    <i class="bx bx-home-alt">
                                    </i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('customer.index') }}">@t(لیست انتظار)</a>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Content-->
    <section class="invoice-list-wrapper">
        <div class="row">
            <div class="col-3">
                <div class="card background-color">
                    <div class="card-header">
                        {{-- <h6>@t(افزودن){{ ' '.$customer->name.' '.$customer->family.' ' }}(به لیست انتظار)</h6> --}}
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="wizard-vertical" id="waiting_list_form" onSubmit="return store_form()"
                                enctype="multipart/form-data">
                                @csrf
                                <fieldset class="form-group">
                                    <label for="customer_id">نام مراجع</label>
                                    {{-- <select class="form-control select2" id="customer_id">
                                      </select> --}}
                                    <select id="customer_id" class="form-control select2" style="width: 100%"
                                        name="customer_id" placeholder="مراجع" required>
                                    </select>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="advisor_id">نام مشاور</label>
                                    <select id="advisor_id" class="form-control select2" style="width: 100%"
                                        name="advisor_id" placeholder="مراجع" required>
                                    </select>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="category_id">علت مراجعه</label>
                                    <select name="category_id" id="category_id" class="form-control select2" required>
                                        <option value="">@t(علت مراجعه را وارد کنید)</option>
                                        {{-- @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach --}}
                                    </select>
                                </fieldset>
                                <div class="form-group">
                                    <label>@t(تاریخ درخواستی)</label>
                                    <div class="controls form-label-group ">
                                        <input type="text" class="form-control" name="date" id="date_sw" required
                                            pattern="[\u06F0-\u06F90-9]{4}/[\u06F0-\u06F90-9]{2}/[\u06F0-\u06F90-9]{2}"
                                            data-validation-pattern-message="فرمت فیلد معتبر نیست." value=""
                                            placeholder="انتخاب تاریخ" aria-invalid="false">
                                        <div class="form-control-position">
                                            <i class="bx bx-calendar">
                                            </i>
                                        </div>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <fieldset class="form-group">
                                    <label for="time">@t(زمان درخواستی)</label>
                                    <input type="time" name="time" class="form-control" id="time" required>
                                </fieldset>
                                <fieldset class="form-group">
                                    <label for="note">@t(توضیحات)</label>
                                    <textarea name="note" class="form-control" id="note" rows="1"></textarea>
                                </fieldset>
                                <fieldset>
                                    <button type="submit" class="btn btn-success float-right">ثبت</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="card">
                    <div class="card-header">
                        <!-- create invoice button-->
                        <div class="table-responsive ">
                            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>

                                        <th>#</th>
                                        <th>ویژه</th>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>آواتار</th>
                                        <th>موبایل</th>
                                        <th>جنسیت</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>ویژه</th>
                                        <th>نام</th>
                                        <th>نام خانوادگی</th>
                                        <th>آواتار</th>
                                        <th>موبایل</th>
                                        <th>جنسیت</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">

        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="/assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        {{-- <script src="/assets/vendors/js/tables/datatable/buttons.print.min.js"></script> --}}
        <script src="/assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        {{-- <script src="/assets/vendors/js/tables/datatable/pdfmake.min.js"></script> --}}
        {{-- <script src="/assets/vendors/js/tables/datatable/vfs_fonts.js"></script> --}}
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
        <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>

        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>


        <!-- END: Page JS-->
        <script>
            dt = '#waiting';
            /*

            $(dt + ' thead tr').clone(true).appendTo(dt + ' thead');
            $(dt + ' thead tr:eq(1) th').each(function(i) {

                // var title = $(this).text();
                //     var span = '<span class="sorting" onclick="a=$(\'' + dt + ' thead tr:eq(0) th\')[' + i +
                //         '];a.click();this.className=a.className"></span>';
                //     $(this).html('<input type="text" placeholder="جستجوی ' + title + '" />' + span);
                //     $('input', this).on('keyup change', function() {
                //         if (table.column(i).search() !== this.value) {
                //             table
                //                 .column(i)
                //                 .search(this.value)
                //                 .draw();
                //         }
                //     });
                var title = $(this).text();
                if (title != 'عملیات' && title != 'آواتار') {
                    $(this).html('<input id="" type="text" placeholder="جست جو در ' + title +
                        '"  class="form-control form-control-sm" />');

                }
                if (title == '#') {
                    $(this).html('<input type="tel" placeholder="جست جو در ' + title +
                        '"  class="form-control form-control-sm" />');
                }
                if (title == 'وضعیت') {
                    $(this).html('<select class="form-control">' +
                        '<option value="">وضعیت</option>' +
                        '<option value="در انتظار تخصیص">تخصیص یافته</option>' +
                        '<option value="فعال">فعال</option>' +
                        '<option value="غیر فعال">غیر فعال</option>' +
                        '</select>');

                }
            });

            */


            //
            // DataTables initialisation
            //
            const params = new URLSearchParams(window.location.search);
            $(document).ready(function() {

                table = $(dt).DataTable({
                    "processing": true,
                    "serverSide": true,
                    "orderCellsTop": true,
                    "order": [
                        [0, "desc"]
                    ],
                    "ajax": "{{ route('Waitinglist.ajax_get') }}",
                    "fixedHeader": true,
                    "language": persian,
                    "columns": [{
                            "name": "id",
                            "data": "id",
                            "visible": false,
                        },
                        {
                            data: null,
                            'name': 'customer',
                            render: function(data, type, row) {
                                return data.customer.name + ' ' + data.customer.family + '<b> «' + data.customer.mobile+'»</b>';
                            },
                        },
                        {
                            data: null,
                            'name': 'advisor',
                            render: function(data, type, row) {
                                return data.advisor.name + ' ' + data.advisor.family;
                            },
                        },
                        {
                            'data': 'advisor_id',
                            'name': 'advisor_id',
                            "visible": false,
                        },
                        {
                            data: null,
                            'name': 'category',
                            render: function(data, type, row) {
                                return data.category.name;
                            },
                        },
                        {
                            "name": "date",
                            "data": "date",
                        },
                        {
                            "name": "time",
                            "data": "time",
                        },
                        {
                            "name": "status",
                            "data": "status",
                        },
                        {
                            data: null,
                            render: function(data, type, row, i) {
                                return `
                                <a href="/admin/product/sessions?Waitinglist=` + data.id + `#` + data.date +
                                    `"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="تخصیص جلسه"
                                                    class="btn btn-icon btn-sm rounded-circle btn-light-primary mr-1 mb-1" onclick="setCookie('Waitinglist',JSON.stringify(table.rows( ` +
                                    i.row + ` ).data()[0]));">
                                                    <i class="bx font-small-3 bx-cart-alt"></i>
                                                </a>
                                                <a href="/admin/sms/send/` + data.customer_id + `"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="پیامک"
                                                    class="btn btn-icon btn-sm rounded-circle btn-light-primary mr-1 mb-1">
                                                    <i class="bx font-small-3 bx-message"></i>
                                                </a>                                                                         
                                <a href="/admin/Financial/user/` + data.id + `/invoise"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="حذف"
                                                    class="btn btn-icon btn-sm rounded-circle btn-light-primary mr-1 mb-1">
                                                    <i class="bx font-small-3 bx-money"></i>
                                                </a>
                                                            <a href="/admin/user/customer/` + data.id + `/edit"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="ویرایش"
                                                                class="btn btn-icon btn-sm rounded-circle btn-light-warning mr-1 mb-1">
                                                                <i class="bx font-small-3 bx-edit"></i>
                                                            </a>

                                                          `;
                            },
                        }
                    ],

                    "dom": '<"col-12"<"d-flex justify-content-between"lp><t><"d-flex justify-content-between"ip>>',
                    initComplete: function() {
                        // Apply the search
                        var that = this;

                        var tbl = $(dt)[0];

                        var row = tbl.insertRow(0);

                        var that = this.api();
                        n = 0;
                        $(that.settings()[0].aoColumns).each(function(i, o) {
                            if (o.bVisible) {
                                var cell1 = row.insertCell();
                                t = o.sTitle
                                var sort =
                                    '<span style="right:-30px;top:8px" class="sorting" onclick="a=$(\'' +
                                    dt + ' thead tr:eq(1) th\')[' + n +
                                    '];a.click();this.className=a.className"></span>';
                                if (t.search('<select') > 0 || !o.sName) {
                                    $(cell1).html((o.sName ? sort : '') + o.sTitle)

                                } else {
                                    $(cell1).html(
                                        sort +
                                        '<input class="form-control form-control-sm" name="' +
                                        o.sName + '" placeholder="' + o.sTitle + '">')
                                }

                                $(cell1).first().on('keyup change clear', function(n) {
                                    that.columns(i).search(n.target.value).draw();
                                })
                                n++;
                            }
                        })

                        this.api().columns().every(function(o, b, c) {
                            //console.log(o, b, c, this.columns(o).settings());
                            var that = this;
                            /*
                            $('input', $(dt+' thead tr:eq(1) th')[this.index()]).on('keyup change clear',
                                function() {
                                    if (that.search() !== this.value) {
                                        that.search(this.value).draw();
                                    }
                                });
                            $('select', $(dt+' thead tr:eq(1) th')[this.index()]).on('keyup change clear',
                                function() {
                                    if (that.search() !== this.value) {
                                        that.search(this.value).draw();
                                    }
                                });
                            */

                        });






                        $(dt + ' thead input:eq(3)').val(params.get('date'));
                        $('#date_sw').val(params.get('date'));
                        @php
                        if (request('advisor_id')) {
                            $user = \App\Models\User::find(request('advisor_id'));
                        }
                        @endphp
                        @if (isset($user))
                            $('[name=advisor]').val('{{ ($user->name??'') . ' ' . ($user->family??'') }}');
                        @endif





                        $('[data-toggle="tooltip"]').tooltip()
                    }
                });

                if (params.has('date')) {
                    table.columns(5).search((params.get('date')));
                }


                if (params.has('time')) {
                    table.columns(6).search((params.get('time')));
                }

                if (params.has('advisor_id')) {
                    table.columns(2).search('{{ ($user->name??'') . ' ' . ($user->family??'') }}');
                }

                table.draw();








                // $('input,select').each(function(i, o) {

                //     o.oninvalid = function(e) {
                //         e.target.setCustomValidity("");
                //         if (!e.target.validity.valid) {
                //             e.target.setCustomValidity(e.target.attributes.validitymsg.value);
                //         }
                //     };
                //     o.oninput = function(e) {
                //         e.target.setCustomValidity("");
                //     };
                // });
                $('#date_sw').datepicker({
                    dateFormat: "yy/mm/dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                });
                $('.date2').datepicker({
                    dateFormat: "yy/mm/dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                });

                $('#customer_id').select2({
                    ajax: {
                        url: "{{ route('customer.get_all_ajax') }}",
                        dataType: 'json'
                    }
                });
                $('#advisor_id').select2({
                    ajax: {
                        url: "{{ route('advisor.get_all_selection_ajax') }}",
                        dataType: 'json'
                    }
                });


            });

            function store_form() {
                data = {
                    _token: '{{ csrf_token() }}',
                    advisor_id: $('#advisor_id').val(),
                    customer_id: $('#customer_id').val(),
                    date: $('#date_sw').val(),
                    category_id: $('#category_id').val(),
                    time: $('#time').val(),
                    note: $('#note').val(),
                };

                $.post(
                    "{{ route('Waitinglist.store_waiting_list_ajax') }}", data,
                    function(data, status) {
                        $('#customer_id').empty();
                        $('#advisor_id').empty();
                        $('#category_id').empty();
                        table.ajax.reload();
                    });
                return false;
            }
        </script>

    </x-slot>
</x-base>
