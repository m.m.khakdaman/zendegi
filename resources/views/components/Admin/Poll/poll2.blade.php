<x-base>
    <x-slot name="title">
        @t(زندگی عاقلانه نظر سنجی )
    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    </x-slot>
    <!-- table success start -->
    <section id="table-success">
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(نظرسنجی)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.home') }}">
                                        <i class="bx bx-home-alt">
                                        </i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    @t(تنظیمات)
                                </li>
                                <li class="breadcrumb-item">
                                    @t(نظرسنجی)
                                </li>
                                <li class="breadcrumb-item active">
                                    @t(فرم نظرسنجی شماره 2)
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <div class="row">
            <div class="col-12">
                <div class="card background-color">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">
                                <h5>@t(فرم نظرسنجی از مراجعین مرکز مشاوره و خدمات روان شناختی زندگی عاقلانه)</h5>
                                <p>
                                    <span>@t(دوست و همراه گرامی:)</span><span class="font-medium-3 text-secondary">{{ auth()->user()->name.' '.auth()->user()->family.' ' }}</span><br>
                                    <span>@t(باعث افتخار خواهد بود اگر زمانی را برای تکمیل این فرم قرار دهید، یقیناً نظرات و پیشنهادهای شما ما را در ارائه خدمات بهتر یاری خواهد کرد.)</span>
                                </p>
                            </div>
                            <div class="col-4">
                                <div class="row text-center float-right">
                                    <div class="mr-4 mt-2">
                                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" width="80px" />
                                        <p><small>@t(مرکز مشاوره زندگی عاقلانه)</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            
                            <form action="{{ route('poll_save') }}" 
                                class="wizard-validation" 
                                method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                {{-- باکس نظرسنجی از اینجا شروع می شود --}}
                                <br>
                                <div class="card-content rounded-lg" style="border: solid 3px rgb(138, 132, 132)">
                                    <div class="card-body mt-1 pt-1 pb-0">
                                        <div class="card-text line-height-2">
                                            <dl class="row">
                                                <dt class="col-sm-1">1</dt>
                                                <dt class="col-sm-4">@t(آیا گرفتن وقت برای مراجعه به این مرکز آسان بود؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_1" id="poll2_1" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_1" id="poll2_1" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2_1_note" class="form-control font-small-3" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">2</dt>
                                                <dt class="col-sm-4">@t(آیا درمانگر خود را در وقت تعیین شده، ملاقات کردید؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_2" id="poll2_2" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_2" id="poll2_2" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2_2_note" class="form-control font-small-3" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">3</dt>
                                                <dt class="col-sm-4">@t(آیا شرایط اتاق درمان مناسب بود؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_3" id="poll2_3" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_3" id="poll2_3" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2_3_note" class="form-control font-small-3" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">4</dt>
                                                <dt class="col-sm-4">@t(آیا از فعالیتهای دیگر این مرکز (برگزاری کلاسهای آموزشی و ...) اطلاع دارید؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_4" id="poll2_4" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_4" id="poll2_4" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2_4_note" class="form-control font-small-3" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">5</dt>
                                                <dt class="col-sm-4">@t(چقدر احساس می کنید توسط درمانگرتان درک شده اید؟)</dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2 text-left" style="direction: ltr">@t(خیلی زیاد)<input type="radio" name="poll2_5" id="poll2_5" class="mr-25" value="خیلی زیاد"></dt>
                                                        <dt class="col-sm-2 text-left" style="direction: ltr">@t(زیاد)<input type="radio" name="poll2_5" id="poll2_5" class="mr-25" value="زیاد"></dt>
                                                        <dt class="col-sm-2 text-left" style="direction: ltr">@t(تاحدودی)<input type="radio" name="poll2_5" id="poll2_5" class="mr-25" value="تاحدودی"></dt>
                                                        <dt class="col-sm-2 text-left" style="direction: ltr">@t(کم)<input type="radio" name="poll2_5" id="poll2_5" class="mr-25" value="کم"></dt>
                                                        <dt class="col-sm-2 text-left" style="direction: ltr">@t(خیلی کم)<input type="radio" name="poll2_5" id="poll2_5" class="mr-25" value="خیلی کم"></dt>
                                                        <dt class="col-sm-2"><input type="text"  name="poll2_5_note" class="form-control font-small-1" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">6</dt>
                                                <dt class="col-sm-4">@t(آیا راهکار ارائه شده توسط درمانگر منطبق با مشکل شما بوده است؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_6" id="poll2_6" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_6" id="poll2_6" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2_6_note" class="form-control font-small-3" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">7</dt>
                                                <dt class="col-sm-4">@t(آیا تمایل به ادامه درمان با درمانگر فعلی خود را دارید؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2-7" id="poll2-7" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2-7" id="poll2-7" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2-7_note" class="form-control font-small-3" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">8</dt>
                                                <dt class="col-sm-4">@t(آیا درمانگرتان شما را به مراکز درمانی دیگر راهنمایی کرده اند؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_8" id="poll2_8" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_8" id="poll2_8" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2_8_note" class="form-control font-small-3" placeholder="@t(نام مرکز.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">9</dt>
                                                <dt class="col-sm-4">@t(آیا درمانگر به دقت به صحبت های شما گوش می دهد؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_9" id="poll2_9" class="mr-25" value="بله">@t(بله)</dt>
                                                        <dt class="col-sm-2"><input type="radio" name="poll2_9" id="poll2_9" class="mr-25" value="خیر">@t(خیر)</dt>
                                                        <dt class="col-sm-8"><input type="text"  name="poll2_9_note" class="form-control font-small-3" placeholder="@t(در صورت توضیحات، توضیح بفرمایید.)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>   
                                            <dl class="row">
                                                <dt class="col-sm-1">10</dt>
                                                <dt class="col-sm-4">@t(اگر مایلید که زندگی عاقلانه را به عزیزان خود  معرفی نمائید، نام و شماره چه کسانی را می برید؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="text"  name="poll2_name1" class="form-control font-small-3" placeholder="@t(نام و نام خانوادگی)"></dt>
                                                        <dt class="col-sm-6"><input type="text"  name="poll2_mobile1" class="form-control font-small-3" placeholder="@t(شماره موبایل)"></dt>
                                                    </dl>
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="text"  name="poll2_name2" class="form-control font-small-3" placeholder="@t(نام و نام خانوادگی)"></dt>
                                                        <dt class="col-sm-6"><input type="text"  name="poll2_mobile2" class="form-control font-small-3" placeholder="@t(شماره موبایل)"></dt>
                                                    </dl>
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="text"  name="poll2_name3" class="form-control font-small-3" placeholder="@t(نام و نام خانوادگی)"></dt>
                                                        <dt class="col-sm-6"><input type="text"  name="poll2_mobile3" class="form-control font-small-3" placeholder="@t(شماره موبایل)"></dt>
                                                    </dl>
                                                </dd>
                                            </dl>
                                            <dl class="row">
                                                <dt class="col-sm-1">11</dt>
                                                <dt class="col-sm-4">@t(علاقه مند هستید در مورد کدامیک از گارگاه های آموزشی زندگی عاقلانه اطلاعات بیشتری کسب کنید؟)<br></dt>
                                                <dd class="col-sm-7">
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_1" class="mr-50" value="کارگاه اعتماد به نفس" >@t(کارگاه اعتماد به نفس)</dt>
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_2" class="mr-50" value="کارگاه خود آشنایی" >@t(کارگاه خود آشنایی)</dt>
                                                    </dl>
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_3" class="mr-50" value="کارگاه فن بیان" >@t(کارگاه فن بیان)</dt>
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_4" class="mr-50" value="کارگاه پیش از ازدواج" >@t(کارگاه پیش از ازدواج)</dt>
                                                    </dl>
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_5" class="mr-50" value="کارگاه همسرداری" >@t(کارگاه همسرداری)</dt>
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_6" class="mr-50" value="گروه درمانی" >@t(گروه درمانی)</dt>
                                                    </dl>
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_7" class="mr-50" value="کارگاه فرزند پروری" >@t(کارگاه فرزند پروری)</dt>
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_8" class="mr-50" value="پک تستی کشف دنیای درون" >@t(پک تستی کشف دنیای درون)</dt>
                                                    </dl>
                                                    <dl class="row">
                                                        <dt class="col-sm-6"><input type="checkbox"  name="poll2_11_9" class="mr-50" value="کارگاه های تخصصی روانشناسی" >@t(کارگاه های تخصصی روانشناسی)</dt>
                                                    </dl>

                                                </dd>
                                            </dl>                                
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group mr-2 ml-2">  
                                                    <input type="submit" id="submit_form" class="btn btn-success mt-75 mb-1 float-right" value="@t(ارسال فرم)">
                                                    <input type="reset" class="btn btn-warning mt-75 float-right mr-50 mb-1" value="@t(پاک کن)" name="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
		<script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->
        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
		<script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->

    </x-slot>
</x-base>
