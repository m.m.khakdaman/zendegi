<x-base>
    <x-slot name="title">
        @t(زندگی عاقلانه نظر سنجی )
    </x-slot>
    <x-slot name="css">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/datatables.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/tables/datatable/responsive.bootstrap.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    </x-slot>
    <!-- table success start -->
    <section id="table-success">
        <!-- breadcrumb start-->
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">@t(نظرسنجی)</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('admin.home') }}">
                                        <i class="bx bx-home-alt">
                                        </i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    @t(تنظیمات)
                                </li>
                                <li class="breadcrumb-item">
                                    @t(نظرسنجی)
                                </li>
                                <li class="breadcrumb-item active">
                                    @t(نظرسنجی شماره 1)
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end-->
        <div class="row">
            <div class="col-12">
                <div class="card background-color">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-8">
                                <h5>@t(فرم نظرسنجی از مراجعین مرکز مشاوره زندگی عاقلانه)</h5>
                                <p>
                                    <span>@t(دوست و همراه گرامی:)</span><span class="font-medium-3 text-secondary">{{ auth()->user()->name.' '.auth()->user()->family.' ' }}</span><br>
                                    <span>@t(باعث افتخار خواهد بود اگر زمانی را برای تکمیل این فرم قرار دهید، یقیناً نظرات و پیشنهادهای شما ما را در ارائه خدمات بهتر یاری خواهد کرد.)</span>
                                </p>
                            </div>
                            <div class="col-4">
                                <div class="row text-center float-right">
                                    <div class="mr-4 mt-2">
                                        <img src="/assets/images/logo/zendegiaghelane.png" alt="@t(مرکز مشاوره زندگی عاقلانه)" class="img-responsive" width="80px" />
                                        <p><small>@t(مرکز مشاوره زندگی عاقلانه)</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form action="{{ route('poll_save') }}" 
                                class="wizard-validation" 
                                method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                {{-- باکس اول از اینجا شروع می شود --}}
                                <div class="rounded-lg" style="border: rgb(138, 132, 132) solid 3px">
                                    <div class="row">
                                        <div class="col-12 mr-2 ml-2 pt-2">
                                            <p>
                                                @t(در صورتی که نمی خواهید نام و نام خانوادگی شما در این نظر سنجی منتشر شود این گزینه را فعال کنید.)
                                                <input type="checkbox" class="form-group" name="anonymous" value="1">
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group mr-2 ml-2">
                                                <label>@t(جنسیت)</label>
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input bg-primary" name="gender" value="مرد" id="customColorRadio1">
                                                        <label class="custom-control-label" for="customColorRadio1">@t(مرد)</label>
                                                        </div>
                                                    </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input bg-secondary" name="gender" value="زن" id="customColorRadio2">
                                                        <label class="custom-control-label" for="customColorRadio2">@t(زن)</label>
                                                        </div>
                                                    </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group mr-2 ml-2">
                                                <label for="age">@t(سن)</label>
                                                <input type="text" class="form-control" name="age">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="select-box">
                                                    <label for="select2-users-name">@t(تحصیلات)</label>
                                                    <select name="education" class="select2 form-control" id="select2-users-name">
                                                        <option value="0" disabled selected>@t(انتخاب کنید)</option>
                                                        @foreach ($educations as $edu)
                                                            <option value="{{ $edu->id }}">{{ $edu->education_level }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group mr-2 ml-2">
                                                <label for="job">@t(شغل)</label>
                                                <input type="text" class="form-control" name="job">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group mr-2 ml-2">
                                                <div class="select-box">
                                                    <label for="advisor">@t(نام مشاور یا مدرس)</label>
                                                    <select name="advisor" class="select2 form-control" id="advisor">
                                                        <option value="0" disabled selected>@t(انتخاب کنید)</option>
                                                        @foreach ($advisors as $advisor)
                                                            <option value="{{ $advisor->id }}">{{ $advisor->name.' '.$advisor->family }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group mr-2 ml-2">
                                                <label>@t(شماره همراه)</label>
                                                <input type="number" name="mobile" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>@t(تا کنون چند جلسه مراجعه کرده اید؟)</label>
                                                <input type="number" name="session_num" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group mr-2 ml-2">
                                                <div class="select-box">
                                                    <label for="method">@t(از چه طریقی با زندگی عاقلانه آشنا شده اید؟)</label>
                                                    <select name="method" class="select2 form-control" id="method">
                                                        <option value="0" disabled selected>@t(انتخاب کنید)</option>
                                                        @foreach ($introduction_methods as $method)
                                                            <option value="{{ $method->id }}">{{ $method->Introduction_method }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                
                                </div>
                                {{-- باکس دوم از اینجا شروع می شود --}}
                                <br>
                                <div class="card-content rounded-lg" style="border: solid 3px rgb(138, 132, 132)">
                                    <div class="card-body mt-1 pt-1 pb-0 text-center">
                                    <div class="card-text line-height-2">
                                        <dl class="row">
                                        <dt class="col-sm-1">@t(ردیف)</dt>
                                        <dt class="col-sm-4">@t(لطفاً به سوالات زیر پاسخ دهید.)</dt>
                                        <dd class="col-sm-7">
                                            <dl class="row">
                                                <dt class="col-sm-2">@t(عالی)<br><small>4</small></dt>
                                                <dt class="col-sm-2">@t(خیلی خوب)<br><small>3</small></dt>
                                                <dt class="col-sm-2">@t(خوب)<br><small>2</small></dt>
                                                <dt class="col-sm-2">@t(متوسط)<br><small>1</small></dt>
                                                <dt class="col-sm-4">@t(ضعیف)<br><small>0</small></dt>
                                            </dl>
                                        </dd>
                                        </dl>
                                        <dl class="row">
                                            <dt class="col-sm-1">1</dt>
                                            <dt class="col-sm-4">@t(نحوه رفتار پرسنل و ارتباط پذیرش مرکز با شما.)</dt>
                                            <dd class="col-sm-7">
                                                <dl class="row">
                                                    <dt class="col-sm-2"><input type="radio" name="q1" id="q1" value="عالی"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q1" id="q1" value="خیلی خوب"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q1" id="q1" value="خوب"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q1" id="q1" value="متوسط"></dt>
                                                    <dt class="col-sm-4"><input type="radio" name="q1" id="q1" value="ضعیف"></dt>
                                                </dl>
                                            </dd>
                                        </dl>
                                        <dl class="row">
                                            <dt class="col-sm-1">2</dt>
                                            <dt class="col-sm-4">@t(فضای مرکز)</dt>
                                            <dd class="col-sm-7">
                                                <dl class="row">
                                                    <dt class="col-sm-2"><input type="radio" name="q2" id="q2" value="عالی"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q2" id="q2" value="خیلی خوب"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q2" id="q2" value="خوب"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q2" id="q2" value="متوسط"></dt>
                                                    <dt class="col-sm-4"><input type="radio" name="q2" id="q2" value="ضعیف"></dt>
                                                </dl>
                                            </dd>
                                        </dl>
                                        <dl class="row">
                                            <dt class="col-sm-1">3</dt>
                                            <dt class="col-sm-4">@t(کیفیت خدمات ارائه شده.)<small>@t((میزان رضایت شما از مشاورتان))</small></dt>
                                            <dd class="col-sm-7">
                                                <dl class="row">
                                                    <dt class="col-sm-2"><input type="radio" name="q3" id="q3" value="عالی"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q3" id="q3" value="خیلی خوب"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q3" id="q3" value="خوب"></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q3" id="q3" value="متوسط"></dt>
                                                    <dt class="col-sm-4"><input type="radio" name="q3" id="q3" value="ضعیف"></dt>
                                                </dl>
                                            </dd>
                                        </dl>
                                        <dl class="row">
                                            <dt class="col-sm-1">4</dt>
                                            <dt class="col-sm-4">@t(آیا مجدداً به زندگی عاقلانه خواهید آمد؟)<br></dt>
                                            <dd class="col-sm-7">
                                                <dl class="row">
                                                    <dt class="col-sm-2"><input type="radio" name="q4" id="q4" value="بله"><span>@t(بله)</span></dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q4" id="q4" value="خیر">@t(خیر)</dt>
                                                    <dt class="col-sm-8"><input type="text"  name="q4_note" class="form-control font-small-1" placeholder="@t(در صورتی که پاسخ شما خیر است لطفا علت را بنویسید.)"></dt>
                                                </dl>
                                            </dd>
                                        </dl>
                                        <dl class="row">
                                            <dt class="col-sm-1">5</dt>
                                            <dt class="col-sm-4">@t(آیا مایلید با کارگاه های آموزشی مرکز آشنا شوید؟)</dt>
                                            <dd class="col-sm-7">
                                                <dl class="row">
                                                    <dt class="col-sm-2"><input type="radio" name="q5" id="q5" value="بله">@t(بله)</dt>
                                                    <dt class="col-sm-2"><input type="radio" name="q5" id="q5" value="خیر">@t(خیر)</dt>
                                                </dl>
                                            </dd>
                                        </dl>
                                        <dl class="row">
                                            <dt class="col-sm-1">6</dt>
                                            <dt class="col-sm-5">@t(با چه نمره ای از 0 تا 10 احتمال دارد خدمات مارا به دیگران پیشنهاد دهید؟)</dt>
                                            <dd class="col-sm-6">
                                                <dl class="row">
                                                    <dt class="col-sm-12">
                                                        <input type="number" name="q6" class="form-control font-small-1" placeholder="@t(10 بیشترین تمایل به معرفی و 0 عدم تمایل به معرفی)">
                                                    </dt>
                                                </dl>
                                            </dd>
                                        </dl>                                   
                                    </div>
                                    </div>
                                </div>
                                {{-- باکس سوم از اینجا شروع می شود --}}
                                <br>
                                <div class="card-content rounded-lg" style="border: solid 3px rgb(138, 132, 132)">
                                    <div class="card-body mt-1 pt-1 pb-0">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="comment">@t(توضیحات و پیشنهادات)</label>
                                                <textarea name="comment" id="comment" class="form-control" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group mr-2 ml-2">  
                                                <input type="submit" id="submit_form" class="btn btn-success mt-75 mb-1 float-right" value="@t(درج)">
                                                <input type="reset" class="btn btn-warning mt-75 float-right mr-50 mb-1" value="@t(پاک کن)" name="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
		<script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->
        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/tooltip/tooltip.js"></script>
		<script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->

    </x-slot>
</x-base>
