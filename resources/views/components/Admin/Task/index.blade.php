<x-base title="@t(وظایف)">
    <x-slot name="css">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/validation/form-validation.css">
    <!-- END: Page CSS-->
    </x-slot>
    <section id="page-account-settings">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <!-- left menu section -->
                    <div class="col-md-3 mb-2 mb-md-0 pills-stacked">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center active" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="true">
                                    <i class="bx bx-cog"></i>
                                    <span>@t(پرونده های درجریان)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center" id="account-pill-password" data-toggle="pill" href="#account-vertical-password" aria-expanded="false">
                                    <i class="bx bx-lock"></i>
                                    <span>@t(پرونده های مختومه)</span>
                                </a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link d-flex align-items-center" id="account-pill-info" data-toggle="pill" href="#account-vertical-info" aria-expanded="false">
                                    <i class="bx bx-info-circle"></i>
                                    <span>اطلاعات</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center" id="account-pill-social" data-toggle="pill" href="#account-vertical-social" aria-expanded="false">
                                    <i class="bx bxl-twitch"></i>
                                    <span>شبکه های اجتماعی</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center" id="account-pill-connections" data-toggle="pill" href="#account-vertical-connections" aria-expanded="false">
                                    <i class="bx bx-link"></i>
                                    <span>ارتباطات</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center" id="account-pill-notifications" data-toggle="pill" href="#account-vertical-notifications" aria-expanded="false">
                                    <i class="bx bx-bell"></i>
                                    <span>اعلان ها</span>
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                    <!-- right content section -->
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
                                            <div class="accordion collapse-icon accordion-icon-rotate" id="accordionWrapa2">
                                                @foreach ($tasks as $task)
                                                <div class="card collapse-header">
                                                    <div id="heading{{ $task->history_id }}" class="card-header" data-toggle="collapse" data-target="#accordion{{ $task->history_id }}" aria-expanded="false" aria-controls="accordion{{ $task->history_id }}" role="tablist">
                                                        <div class="avatar">
                                                            <img src="/assets/images/profile/admin/avatar/{{ $task->sender->pic }}" alt="{{ $task->sender->name.' '.$task->sender->family }}" height="30" width="30" data-toggle="tooltip" data-placement="right" title="{{ $task->sender->name.' '.$task->sender->family }}">
                                                        </div>
                                                        <span class="badge badge-light-primary badge-pill" data-toggle="tooltip" data-placement="top" title="{{ $task->sender->name.' '.$task->sender->family }}">{{ $task->sender->name.' '.$task->sender->family }}</span>
                                                        <span class="collapse-title">
                                                            {{-- <i class="bx bx-cloud align-middle"></i> --}}
                                                            <span class="align-middle">{{ $task->history_title }}</span>
                                                        </span>
                                                        <span class="badge badge-primary mr-1 mt-50 badge-pill float-right" data-toggle="tooltip" data-placement="top" title="@t(تاریخ ارسال)">{{ $task->date_of_send }}</span>
                                                        @if ($task->status == '0')
                                                        <i class="bx bx-stop-circle mr-25 mt-75 float-right text-danger" data-toggle="tooltip" data-placement="top" title="@t(مختومه)"></i>
                                                        @else
                                                        <a href="{{ route('task.activation',$task->history_id) }}" class="bx bx-play-circle mr-25 mt-75 float-right text-success" data-toggle="tooltip" data-placement="top" title="@t(در جریان)"></a>
                                                        @endif
                                                        <span class="badge badge-danger mr-25 mt-50 badge-pill float-right" data-toggle="tooltip" data-placement="top" title="@t(درجه اهمیت)">{{ $task->importance }}</span>
                                                        {{-- <span class="badge badge-success mr-1 mt-50 badge-pill float-right" data-toggle="tooltip" data-placement="top" title="@t(درگردش)">{{ $task->importance }}</span> --}}
                                                        {{-- <div class="badge badge-pill badge-success mr-25 mt-50 float-right d-inline-flex align-items-center ">
                                                            <a href="dfdfd" class="btn btn-roun">
                                                                <i class="bx bx-play-circle font-size-small mr-25"></i>
                                                                <span>@t(در جریان)</span>
                                                            </a>
                                                        </div> --}}
                                                    </div>
                                                    <div id="accordion{{ $task->history_id }}" role="tabpanel" data-parent="#accordionWrapa2" aria-labelledby="heading{{ $task->history_id }}" class="collapse">
                                                        <div class="card-content">
                                                            <div class="card-body line-height-2">
                                                                @switch($task->task_flag)
                                                                    @case('persona')
                                                                        <h4>{{ $task->history_title }}</h4>
                                                                        <br>
                                                                        {{ $task->note }}
                                                                        <br>
                                                                        {{ $task->suggestions }}
                                                                        <br>
                                                                        <span>@t(برای مصاحبه و تکمیل فرم پرسونا کلیک کنید.)</span>
                                                                        <a href="{{ route('task.create_persona',$task->customer_id ) }}" class="btn btn-secondary">پرسونا</a>
                                                                        <hr>
                                                                    @break
                                                                    @case('session')
                                                                        <h4>{{ $task->history_title }}</h4>
                                                                        <br>
                                                                        {{ $task->note }}
                                                                        <br>
                                                                        {{ $task->suggestions }}
                                                                        <br>
                                                                        <span>@t(برای تکمیل فرم پرسونا کلیک کنید.)</span>
                                                                        <a href="{{ route('task.create_persona',$task->customer_id ) }}" class="btn btn-secondary">پرسونا</a>
                                                                        <hr>
                                                                    @break
                                                                    @case('workshop')
                                                                        <h4>{{ $task->history_title }}</h4>
                                                                        <br>
                                                                        {{ $task->note }}
                                                                        <br>
                                                                        {{ $task->suggestions }}
                                                                        <br>
                                                                        <span>@t(برای تکمیل فرم پرسونا کلیک کنید.)</span>
                                                                        <a href="{{ route('task.create_persona',$task->customer_id ) }}" class="btn btn-secondary">پرسونا</a>
                                                                        <hr>
                                                                    @break
                                                                    @case('product')
                                                                        <h4>{{ $task->history_title }}</h4>
                                                                        <br>
                                                                        {{ $task->note }}
                                                                        <br>
                                                                        {{ $task->suggestions }}
                                                                        <br>
                                                                        <span>@t(برای تکمیل فرم پرسونا کلیک کنید.)</span>
                                                                        <a href="{{ route('task.create_persona',$task->customer_id ) }}" class="btn btn-secondary">پرسونا</a>
                                                                        <hr>
                                                                    @break
                                                                    @case('other')
                                                                        سایر
                                                                    @break
                                                                    {{-- @default --}}
                                                                @endswitch
                                                            </div>
                                                            <div class="card-body">
                                                                <h5>@t(باز فرستادن این وظیفه)</h5>
                                                                <form action="{{ route('task.update_task' ,$task->customer_id) }}" class="wizard-validation" method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                    <div class="row">
                                                                        <div class="col-md-8">
                                                                            <div class="form-group">
                                                                                <label for="note">
                                                                                    @t(توضیحات)
                                                                                </label>
                                                                                <textarea name="note" id="note" class="form-control"  rows="1"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="resiver_user">
                                                                                    @t(مسئول پیگیری)
                                                                                </label>
                                                                                <select class="select2 form-control " id="resiver_user" name="resiver_user" required>
                                                                                    <option value="">@t(مسئول پیگیری)</option>
                                                                                    @foreach ($admins as $admin)
                                                                                        <option value="{{ $admin->id }}">
                                                                                            {{ $admin->name . ' ' . $admin->family }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <input type="hidden" name="history_title" value="@t(فوروارد){{ ' '.$task->history_title }}">
                                                                            <input type="hidden" name="customer_id" value="{{ $task->customer->id }}">
                                                                            <input type="hidden" name="task_flag" value="{{$task->task_flag}}">
                                                                            <input type="hidden" name="suggestions" value="{{ $task->suggestions }}">
                                                                            <input type="hidden" name="importance" value="{{ $task->importance }}">
                                                                            <input type="hidden" name="task_id" value="{{ $task->history_id }}">
                                                                            <input type="submit" id="submit_form" class="btn btn-success btn-black"  value="ثبت">
                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane fade " id="account-vertical-password" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                                            <div class="accordion collapse-icon accordion-icon-rotate" id="accordionWrapa2">
                                                @foreach ($off_tasks as $task)
                                                <div class="card collapse-header">
                                                    <div id="heading{{ $task->history_id }}" class="card-header" data-toggle="collapse" data-target="#accordion{{ $task->history_id }}" aria-expanded="false" aria-controls="accordion{{ $task->history_id }}" role="tablist">
                                                        <div class="avatar">
                                                            <img src="/assets/images/profile/admin/avatar/{{ $task->sender->pic }}" alt="{{ $task->sender->name.' '.$task->sender->family }}" height="30" width="30" data-toggle="tooltip" data-placement="right" title="{{ $task->sender->name.' '.$task->sender->family }}">
                                                        </div>
                                                        <span class="badge badge-light-primary badge-pill" data-toggle="tooltip" data-placement="top" title="{{ $task->sender->name.' '.$task->sender->family }}">{{ $task->sender->name.' '.$task->sender->family }}</span>
                                                        <span class="collapse-title">
                                                            {{-- <i class="bx bx-cloud align-middle"></i> --}}
                                                            <span class="align-middle">{{ $task->history_title }}</span>
                                                        </span>
                                                        <span class="badge badge-primary mr-1 mt-50 badge-pill float-right" data-toggle="tooltip" data-placement="top" title="@t(تاریخ ارسال)">{{ $task->date_of_send }}</span>
                                                        @if ($task->status == '0')
                                                        <i class="bx bx-stop-circle mr-25 mt-75 float-right text-danger" data-toggle="tooltip" data-placement="top" title="@t(مختومه)"></i>
                                                        @else
                                                        <a href="{{ route('task.activation',$task->history_id) }}" class="bx bx-play-circle mr-25 mt-75 float-right text-success" data-toggle="tooltip" data-placement="top" title="@t(در جریان)"></a>
                                                        @endif
                                                        <span class="badge badge-danger mr-25 mt-50 badge-pill float-right" data-toggle="tooltip" data-placement="top" title="@t(درجه اهمیت)">{{ $task->importance }}</span>
                                                        {{-- <span class="badge badge-success mr-1 mt-50 badge-pill float-right" data-toggle="tooltip" data-placement="top" title="@t(درگردش)">{{ $task->importance }}</span> --}}
                                                        {{-- <div class="badge badge-pill badge-success mr-25 mt-50 float-right d-inline-flex align-items-center ">
                                                            <a href="dfdfd" class="btn btn-roun">
                                                                <i class="bx bx-play-circle font-size-small mr-25"></i>
                                                                <span>@t(در جریان)</span>
                                                            </a>
                                                        </div> --}}
                                                    </div>
                                                    <div id="accordion{{ $task->history_id }}" role="tabpanel" data-parent="#accordionWrapa2" aria-labelledby="heading{{ $task->history_id }}" class="collapse">
                                                        <div class="card-content">
                                                            <div class="card-body line-height-2">
                                                                @switch($task->task_flag)
                                                                    @case('persona')
                                                                    پرسونا
                                                                    @break
                                                                    @case('session')
                                                                    جلسه    
                                                                    @break
                                                                    @case('workshop')
                                                                        کارگاه
                                                                    @break
                                                                    @case('product')
                                                                        محصول
                                                                    @break
                                                                    @case('other')
                                                                        سایر
                                                                    @break
                                                                    {{-- @default --}}
                                                                        
                                                                @endswitch
                                                            
                                                                {{ $task->history_title }}
                                                                @t(به ترتیب اولیت زیر:)
                                                                <br>
                                                                {{ $task->suggestions }}
                                                                   {{-- {{ json_decode($task->suggestions) }} --}}
                                                                   {{-- {{ json_encode($task->suggestions) }} --}}
                                                                   {{-- @json($task->suggestions) --}}
                                                                   {{-- {{ json_encode($task->suggestions, true) }} --}}
                                                                <br>
                                                                @t(توضیحات:)
                                                                <br>
                                                                
                                                                {{ $task->note }}
                                                                <hr>
                                                                @if ($task->task_flag == 'persona')
                                                                    @t(برای مصاحبه و تکمیل فرم پرسونا کلیک کنید.)
                                                                    <a href="{{ route('task.create_persona',$task->customer_id ) }}" class="btn btn-secondary">پرسونا</a>
                                                                @endif
                                                            </div>
                                                            <div class="card-body">
                                                                <h5>@t(باز فرستادن این وظیفه)</h5>
                                                                <form action="{{ route('task.update_task' ,$task->customer_id) }}" class="wizard-validation" method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                    <div class="row">
                                                                        <div class="col-md-8">
                                                                            <div class="form-group">
                                                                                <label for="note">
                                                                                    @t(توضیحات)
                                                                                </label>
                                                                                <textarea name="note" id="note" class="form-control"  rows="1"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label for="resiver_user">
                                                                                    @t(مسئول پیگیری)
                                                                                </label>
                                                                                <select class="select2 form-control " id="resiver_user" name="resiver_user" required>
                                                                                    <option value="">@t(مسئول پیگیری)</option>
                                                                                    @foreach ($admins as $admin)
                                                                                        <option value="{{ $admin->id }}">
                                                                                            {{ $admin->name . ' ' . $admin->family }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <input type="hidden" name="history_title" value="@t(فوروارد){{ ' '.$task->history_title }}">
                                                                            <input type="hidden" name="customer_id" value="{{ $task->customer->id }}">
                                                                            <input type="hidden" name="task_flag" value="{{$task->task_flag}}">
                                                                            <input type="hidden" name="suggestions" value="{{ $task->suggestions }}">
                                                                            <input type="hidden" name="importance" value="{{ $task->importance }}">
                                                                            <input type="hidden" name="task_id" value="{{ $task->history_id }}">
                                                                            <input type="submit" id="submit_form" class="btn btn-success btn-black"  value="ثبت">
                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="false">
                                            <form novalidate>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>بیوگرافی</label>
                                                            <textarea class="form-control" id="accountTextarea" rows="3" placeholder="اطلاعات بیوگرافی خود را وارد کنید ..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>تاریخ تولد</label>
                                                                <input type="text" class="form-control birthdate-picker" required placeholder="تاریخ تولد" data-validation-required-message="وارد کردن تاریخ تولد الزامی است">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>کشور</label>
                                                            <select class="form-control" id="accountSelect">
                                                                <option>آمریکا</option>
                                                                <option>هند</option>
                                                                <option>کانادا</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>زبان ها</label>
                                                            <select class="form-control" id="languageselect2" multiple>
                                                                <option value="English" selected>انگلیسی</option>
                                                                <option value="Spanish">اسپانیایی</option>
                                                                <option value="French">فرانسوی</option>
                                                                <option value="Russian">روسی</option>
                                                                <option value="German">آلمانی</option>
                                                                <option value="Arabic" selected>عربی</option>
                                                                <option value="Sanskrit">لورم ایپسوم</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>تلفن</label>
                                                                <input type="text" class="form-control text-left" required placeholder="شماره تلفن" value="(+656) 254 2568" data-validation-required-message="وارد کردن شماره تلفن الزامی است" dir="ltr">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>وب‌سایت</label>
                                                            <input type="text" class="form-control text-left" placeholder="آدرس وب سایت" dir="ltr">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>موزیک مورد علاقه</label>
                                                            <select class="form-control" id="musicselect2" multiple>
                                                                <option value="Rock">راک</option>
                                                                <option value="Jazz" selected>جاز</option>
                                                                <option value="Disco">دیسکو</option>
                                                                <option value="Pop">پاپ</option>
                                                                <option value="Techno">تکنو</option>
                                                                <option value="Folk" selected>فولک</option>
                                                                <option value="Hip hop">هیپ هاپ</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>فیلم های مورد علاقه</label>
                                                            <select class="form-control" id="moviesselect2" multiple>
                                                                <option value="The Dark Knight" selected>شوالیه تاریکی
                                                                </option>
                                                                <option value="Harry Potter" selected>هری پاتر</option>
                                                                <option value="Airplane!">هواپیما!</option>
                                                                <option value="Perl Harbour">پرل هاربور</option>
                                                                <option value="Spider Man">مرد عنکبوتی</option>
                                                                <option value="Iron Man" selected>مرد آهنی</option>
                                                                <option value="Avatar">آواتار</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                        <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">ذخیره تغییرات</button>
                                                        <button type="reset" class="btn btn-light mb-1">انصراف</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade " id="account-vertical-social" role="tabpanel" aria-labelledby="account-pill-social" aria-expanded="false">
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>توییتر</label>
                                                            <input type="text" class="form-control text-left" placeholder="افزودن لینک" value="https://www.twitter.com" dir="ltr">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>فیسبوک</label>
                                                            <input type="text" class="form-control text-left" placeholder="افزودن لینک" dir="ltr">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>گوگل+</label>
                                                            <input type="text" class="form-control text-left" placeholder="افزودن لینک" dir="ltr">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>لینکدین</label>
                                                            <input type="text" class="form-control text-left" placeholder="افزودن لینک" value="https://www.linkedin.com" dir="ltr">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>اینستاگرام</label>
                                                            <input type="text" class="form-control text-left" placeholder="افزودن لینک" dir="ltr">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>کورا</label>
                                                            <input type="text" class="form-control text-left" placeholder="افزودن لینک" dir="ltr">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                        <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">ذخیره تغییرات</button>
                                                        <button type="reset" class="btn btn-light mb-1">انصراف</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="account-vertical-connections" role="tabpanel" aria-labelledby="account-pill-connections" aria-expanded="false">
                                            <div class="row">
                                                <div class="col-12 my-2">
                                                    <a href="javascript:%20void(0);" class="btn btn-info">اتصال به
                                                        <strong>توییتر</strong></a>
                                                </div>
                                                <hr>
                                                <div class="col-12 my-2">
                                                    <button class=" btn btn-sm btn-light-secondary float-right">ویرایش</button>
                                                    <h6>شما به فیسبوک متصل هستید.</h6>
                                                    <p>Johndoe@gmail.com</p>
                                                </div>
                                                <hr>
                                                <div class="col-12 my-2">
                                                    <a href="javascript:%20void(0);" class="btn btn-danger">اتصال به
                                                        <strong>گوگل</strong>
                                                    </a>
                                                </div>
                                                <hr>
                                                <div class="col-12 my-2">
                                                    <button class=" btn btn-sm btn-light-secondary float-right">ویرایش</button>
                                                    <h6>شما به اینستاگرام متصل هستید.</h6>
                                                    <p>Johndoe@gmail.com</p>
                                                </div>
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                    <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">ذخیره تغییرات</button>
                                                    <button type="reset" class="btn btn-light mb-1">انصراف</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="account-vertical-notifications" role="tabpanel" aria-labelledby="account-pill-notifications" aria-expanded="false">
                                            <div class="row">
                                                <h6 class="m-1">فعالیت</h6>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked id="accountSwitch1">
                                                        <label class="custom-control-label mr-1" for="accountSwitch1"></label>
                                                        <span class="switch-label">وقتی کسی به مقاله من دیدگاهی ارسال میکند به من ایمیل بزن</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked id="accountSwitch2">
                                                        <label class="custom-control-label mr-1" for="accountSwitch2"></label>
                                                        <span class="switch-label">وقتی کسی به فرم من پاسخ می دهد به من ایمیل بزن</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" id="accountSwitch3">
                                                        <label class="custom-control-label mr-1" for="accountSwitch3"></label>
                                                        <span class="switch-label">وقتی کسی مرا دنبال می کند به من ایمیل بزن</span>
                                                    </div>
                                                </div>
                                                <h6 class="m-1">نرم افزار</h6>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked id="accountSwitch4">
                                                        <label class="custom-control-label mr-1" for="accountSwitch4"></label>
                                                        <span class="switch-label">اخبار و اطلاعیه ها</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" id="accountSwitch5">
                                                        <label class="custom-control-label mr-1" for="accountSwitch5"></label>
                                                        <span class="switch-label">به روز رسانی هفتگی محصولات</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked id="accountSwitch6">
                                                        <label class="custom-control-label mr-1" for="accountSwitch6"></label>
                                                        <span class="switch-label">خلاصه هفتگی وبلاگ</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                    <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">ذخیره تغییرات</button>
                                                    <button type="reset" class="btn btn-light mb-1">انصراف</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-slot name="script">
     <!-- BEGIN: Vendor JS-->
     <script src="/assets/vendors/js/vendors.min.js"></script>
     <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
     <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
     <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
     <!-- BEGIN Vendor JS-->
 
     <!-- BEGIN: Page Vendor JS-->
     <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
     <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
     <script src="/assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
     <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
     <script src="/assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
     <script src="/assets/vendors/js/extensions/dropzone.min.js"></script>
     <!-- END: Page Vendor JS-->
 
     <!-- BEGIN: Theme JS-->
     <script src="/assets/js/scripts/configs/horizontal-menu.js"></script>
     <script src="/assets/js/core/app-menu.js"></script>
     <script src="/assets/js/core/app.js"></script>
     <script src="/assets/js/scripts/components.js"></script>
     <script src="/assets/js/scripts/footer.js"></script>
     <script src="/assets/js/scripts/customizer.js"></script>
     <!-- END: Theme JS-->
 
     <!-- BEGIN: Page JS-->
     <script src="/assets/js/scripts/pages/page-account-settings.js"></script>
    <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
    <!-- END: Page JS-->    
    </x-slot>
</x-base>