<x-base title="@t(پرسونا)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/forms/select/select2.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/plugins/forms/wizard.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1">@t(پرسونا)</h5>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">@t(پرسونا)
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="basic-tabs-components">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <h4>@t(تکمیل پرسونا و فرم مصاحبه){{ ' '.$customer->name.' '.$customer->family }}</h4>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" aria-controls="home"
                                role="tab" aria-selected="true">
                                <i class="bx bx-home align-middle"></i>
                                <span class="align-middle">@t(پیشنهاد مشاور)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile"
                                aria-controls="profile" role="tab" aria-selected="false">
                                <i class="bx bx-user align-middle"></i>
                                <span class="align-middle">@t(پیشنهاد کارگاه)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="about-tab" data-toggle="tab" href="#about" aria-controls="about"
                                role="tab" aria-selected="false">
                                <i class="bx bx-message-square align-middle"></i>
                                <span class="align-middle">@t(پیشنهاد محصول)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="other-tab" data-toggle="tab" href="#other" aria-controls="other"
                                role="tab" aria-selected="false">
                                <i class="bx bx-message-square align-middle"></i>
                                <span class="align-middle">@t(سایر)</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home" aria-labelledby="home-tab" role="tabpanel">
                            <fieldset>
                                <form action="{{ route('task.store') }}" class="wizard-validation" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion1">
                                                    @t(پیشنهاد ارجاع به مشاور،اولویت 1)
                                                </label>
                                                <select class="select2 form-control" id="suggestion1"
                                                    name="suggestion1">
                                                    <option value="">@t(اولویت اول)</option>
                                                    @foreach ($advisors as $advisor)
                                                        <option value="{{ $advisor->name . ' ' . $advisor->family }}">
                                                            {{ $advisor->name . ' ' . $advisor->family }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion2">
                                                    @t(پیشنهاد ارجاع به مشاور،اولویت 2)
                                                </label>
                                                <select class="select2 form-control" id="suggestion2"
                                                    name="suggestion2">
                                                    <option value="">@t(اولویت دوم)</option>
                                                    @foreach ($advisors as $advisor)
                                                        <option value="{{ $advisor->name . ' ' . $advisor->family }}">
                                                            {{ $advisor->name . ' ' . $advisor->family }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion3">
                                                    @t(پیشنهاد ارجاع به مشاور،اولویت 3)
                                                </label>
                                                <select class="select2 form-control" id="suggestion3"
                                                    name="suggestion3">
                                                    <option value="">@t(اولویت سوم)</option>
                                                    @foreach ($advisors as $advisor)
                                                        <option value="{{ $advisor->name . ' ' . $advisor->family }}">
                                                            {{ $advisor->name . ' ' . $advisor->family }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="importance">
                                                    @t(اهمیت)
                                                </label>
                                                <select class="select2 form-control " id="importance"
                                                    name="importance">
                                                    <option value="@t(معمولی)">@t(معمولی)</option>
                                                    <option value="@t(فوری)">@t(فوری)</option>
                                                    <option value="@t(آنی)">@t(آنی)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="follow_up">
                                                    @t(مسئول پیگیری)
                                                </label>
                                                <select class="select2 form-control " id="follow_up"
                                                    name="follow_up">
                                                    <option value="">@t(مسئول پیگیری)</option>
                                                    @foreach ($admins as $admin)
                                                        <option value="{{ $admin->id }}">
                                                            {{ $admin->name . ' ' . $admin->family }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="note">
                                                    @t(توضیحات مشاور برای جلسه)
                                                </label>
                                                <textarea name="note" id="note" class="form-control"  rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="history_title" value="پیشنهاد رزرو جلسه مشاوره برای{{ ' '.$customer->name.' '.$customer->family }}">
                                    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                    <input type="hidden" name="task_flag" value="session">

                                    <input type="submit" id="submit_form" class="btn btn-success mt-75 float-right" value="ثبت">
                                </form>
                            </fieldset>
                        </div>
                        <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                            <fieldset>
                                <form action="{{ route('task.store') }}" class="wizard-validation" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggested_workshop1">
                                                    @t(پیشنهادارجاع به کارگاه، اولویت1)
                                                </label>
                                                <input type="text" class="form-control " id="suggested_workshop1"
                                                    name="suggestion1" placeholder="@t(اولویت1)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggested_workshop2">
                                                    @t(پیشنهادارجاع به کارگاه، اولویت2)
                                                </label>
                                                <input type="text" class="form-control " id="suggested_workshop2"
                                                    name="suggestion2" placeholder="@t(اولویت2)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggested_workshop3">
                                                    @t(پیشنهادارجاع به کارگاه، اولویت3)
                                                </label>
                                                <input type="text" class="form-control " id="suggested_workshop3"
                                                    name="suggestion3" placeholder="@t(اولویت3)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggested_workshop4">
                                                    @t(پیشنهادارجاع به کارگاه، اولویت4)
                                                </label>
                                                <input type="text" class="form-control " id="suggested_workshop4"
                                                    name="suggestion4" placeholder="@t(اولویت4)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggested_workshop5">
                                                    @t(پیشنهادارجاع به کارگاه، اولویت5)
                                                </label>
                                                <input type="text" class="form-control " id="suggested_workshop5"
                                                    name="suggestion5" placeholder="@t(اولویت5)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="importance">
                                                    @t(اهمیت)
                                                </label>
                                                <select class="select2 form-control " id="importance"
                                                    name="importance">
                                                    <option value="@t(معمولی)">@t(معمولی)</option>
                                                    <option value="@t(فوری)">@t(فوری)</option>
                                                    <option value="@t(آنی)">@t(آنی)</option>
                                                </select>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="follow_up">
                                                    @t(مسئول پیگیری)
                                                </label>
                                                <select class="select2 form-control " id="follow_up"
                                                    name="follow_up">
                                                    <option value="">@t(مسئول پیگیری)</option>
                                                    @foreach ($admins as $admin)
                                                        <option value="{{ $admin->id }}">
                                                            {{ $admin->name . ' ' . $admin->family }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="note">
                                                    @t(توضیحات مشاور برای کارگاه)
                                                </label>
                                                <textarea name="note" id="note" class="form-control"  rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="history_title" value="پیشنهاد کارگاه برای{{ ' '.$customer->name.' '.$customer->family }}">
                                    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                    <input type="hidden" name="task_flag" value="workshop">

                                    <input type="submit" id="submit_form" class="btn btn-success mt-75 float-right" value="ثبت">
                                </form>
                            </fieldset>
                        </div>
                        <div class="tab-pane" id="about" aria-labelledby="about-tab" role="tabpanel">
                            <fieldset>
                                <form action="{{ route('task.store') }}" class="wizard-validation" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion1">
                                                    @t(پیشنهاد محصولات، اولویت1)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion1"
                                                    name="suggestion1" placeholder="@t(اولویت اول)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion2">
                                                    @t(پیشنهاد محصولات، اولویت2)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion2"
                                                    name="suggestion2" placeholder="@t(اولویت دوم)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion3">
                                                    @t(پیشنهاد محصولات، اولویت3)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion3"
                                                    name="suggestion3" placeholder="@t(اولویت سوم)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion4">
                                                    @t(پیشنهاد محصولات، اولویت4)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion4"
                                                    name="suggestion4" placeholder="@t(اولویت چهارم)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion5">
                                                    @t(پیشنهاد محصولات، اولویت5)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion5"
                                                    name="suggestion5" placeholder="@t(اولویت پنجم)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="importance">
                                                    @t(اهمیت)
                                                </label>
                                                <select class="select2 form-control " id="importance"
                                                    name="importance">
                                                    <option value="@t(معمولی)">@t(معمولی)</option>
                                                    <option value="@t(فوری)">@t(فوری)</option>
                                                    <option value="@t(آنی)">@t(آنی)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="follow_up">
                                                    @t(مسئول پیگیری)
                                                </label>
                                                <select class="select2 form-control " id="follow_up"
                                                    name="follow_up">
                                                    <option value="">@t(مسئول پیگیری)</option>
                                                    @foreach ($admins as $admin)
                                        <option value="{{ $admin->id }}">{{ $admin->name.' '.$admin->family }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="note">
                                                    @t(توضیحات مشاور)
                                                </label>
                                                <textarea name="note" id="note" class="form-control"  rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="history_title" value="پیشنهاد محصول برای{{ ' '.$customer->name.' '.$customer->family }}">
                                    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                    <input type="hidden" name="task_flag" value="product">
                                    <input type="submit" id="submit_form" class="btn btn-success mt-75 float-right"
                                        value="ثبت">
                                </form>
                            </fieldset>
                        </div>
                        <div class="tab-pane" id="other" aria-labelledby="other-tab" role="tabpanel">
                            <fieldset>
                                <form action="{{ route('task.store') }}" class="wizard-validation" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion1">
                                                    @t(پیشنهاد اول)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion1"
                                                    name="suggestion1" placeholder="@t(اولویت اول)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion2">
                                                    @t(پیشنهاد دوم)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion2"
                                                    name="suggestion2" placeholder="@t(اولویت دوم)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion3">
                                                    @t(پیشنهاد سوم)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion3"
                                                    name="suggestion3" placeholder="@t(اولویت سوم)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion4">
                                                    @t(پیشنهاد چهارم)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion4"
                                                    name="suggestion4" placeholder="@t(اولویت چهارم)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="suggestion5">
                                                    @t(پیشنهاد پنجم)
                                                </label>
                                                <input type="text" class="form-control " id="suggestion5"
                                                    name="suggestion5" placeholder="@t(اولویت پنجم)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="importance">
                                                    @t(اهمیت)
                                                </label>
                                                <select class="select2 form-control " id="importance"
                                                    name="importance">
                                                    <option value="@t(معمولی)">@t(معمولی)</option>
                                                    <option value="@t(فوری)">@t(فوری)</option>
                                                    <option value="@t(آنی)">@t(آنی)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="follow_up">
                                                    @t(مسئول پیگیری)
                                                </label>
                                                <select class="select2 form-control " id="follow_up"
                                                    name="follow_up">
                                                    <option value="">@t(مسئول پیگیری)</option>
                                                    @foreach ($admins as $admin)
                                        <option value="{{ $admin->id }}">{{ $admin->name.' '.$admin->family }}</option>
                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="note">
                                                    @t(توضیحات مشاور)
                                                </label>
                                                <textarea name="note" id="note" class="form-control"  rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="history_title" value="@t(نظر مشاور درباره ){{ ' '.$customer->name.' '.$customer->family }}">
                                    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                                    <input type="hidden" name="task_flag" value="other">
                                    <input type="submit" id="submit_form" class="btn btn-success mt-75 float-right" value="ثبت">
                                </form>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <script src="/assets/vendors/js/forms/select/select2.full.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/assets/js/scripts/navs/navs.js"></script>
        <script src="/assets/js/scripts/forms/select/form-select2.js"></script>
        <!-- END: Page JS-->


    </x-slot>
</x-base>
