<x-base title="@t(پیامک)">
    <x-slot name='css'>
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/pages/faq.css">
        <!-- END: Page CSS-->

    </x-slot>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1">ویرایش مدرس</h5>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="index.html"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active">استاد جدید
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
    </div>
    <section class="faq-search">
        <div class="row">
            <div class="col-12">
            <div class="card background-color">
                <div class="card-header">
                    <h3>استاد جدید</h3>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form action="{{ route('dadkar_store') }}" 
                            method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-6">
                                    <label for="">نام و نام خانوادگی</label>
                                    <input class="form-control" type="text" name="name" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">نام پدر</label>
                                    <input type="text" class="form-control" name="father" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">شماره شناسنامه</label>
                                    <input class="form-control" type="text" name="sh_sh" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">تاریخ تولد</label>
                                    <input type="text" class="form-control" name="birthday" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">کد مدرسی</label>
                                    <input class="form-control" type="text" name="id_card" value="" required>
                                </div>
                                <div class="col-6">
                                    <label for="">کد ملی</label>
                                    <input type="text" class="form-control" name="national_code" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">تلفن</label>
                                    <input class="form-control" type="text" name="phone" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">موبایل</label>
                                    <input type="text" class="form-control" name="mobile" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">رشته</label>
                                    <input class="form-control" type="text" name="reshte" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">مدرک</label>
                                    <input type="text" class="form-control" name="madrak" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">دانشگاه</label>
                                    <input class="form-control" type="text" name="univercity" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">کشور</label>
                                    <input type="text" class="form-control" name="country" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">آدرس</label>
                                    <input class="form-control" type="text" name="address" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">ایمیل</label>
                                    <input type="text" class="form-control" name="email" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">مبلغ</label>
                                    <input class="form-control" type="text" name="mablagh" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">شماره حساب</label>
                                    <input type="text" class="form-control" name="acount_number" value="">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6">
                                    <label for="">تاریخ نامه</label>
                                    <input class="form-control" type="text" name="date" value="">
                                </div>
                                <div class="col-6">
                                    <label for="">شماره نامه</label>
                                    <input type="text" class="form-control" name="number" value="">
                                </div>
                            </div>
                            <input type="submit" class="btn btn-success float-right mt-2 mb-2" value="ثبت">
                            <a href="{{ route('dadkar_list') }}" class="btn btn-warning float-right mt-2 mb-2 mr-50">بازگشت</a>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    
    <x-slot name="script">
        <!-- BEGIN: Vendor JS-->
        <script src="/assets/vendors/js/vendors.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="/assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/assets/vendors/js/ui/jquery.sticky.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/assets/js/scripts/configs/vertical-menu-dark.js"></script>
        <script src="/assets/js/core/app-menu.js"></script>
        <script src="/assets/js/core/app.js"></script>
        <script src="/assets/js/scripts/components.js"></script>
        <script src="/assets/js/scripts/footer.js"></script>
        <script src="/assets/js/scripts/customizer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <!-- END: Page JS-->


    </x-slot>
</x-base>
