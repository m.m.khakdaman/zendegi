<html>
    <head>
        <title>قرارداد حق التدریس استاد{{ ' '.$ostad->name }}</title>
		<style>
			table, td, th {
			  border: 1px solid black;
			  text-align: center;
			}
			
			table {
			  width: 100%;
			  border-collapse: collapse;
			}
		</style>
    </head>
    <body dir="rtl" style="font-family: B Nazanin; font-size: 17px; padding: 2%">
       <div style="margin-right:10px text-align:justify; border:2px solid black;">
			
			<div style="width: 84%; text-align: center">
					<strong>
						<span style="font-size:24px;">&laquo;به نام خدا&raquo;</span>
						<br>
						<span style="font-size:24px;">&laquo;لیست دروس&raquo;</span>
						<br>
						<span style="font-size:30px;">استاد{{ ' '.$ostad->name }}</span>
					</strong>
			</div>
			<div style="margin-right: 15px; margin-left: 15px;">
				
				<div style="overflow-x:auto;">
					<table>
						<tr>
							<th>عنوان دوره
								ترمی/پودمانی/تکدرس
								</th>
							<th>مقطع</th>
							<th>شماره درس</th>
							<th>عنوان درس</th>
							{{-- <th>شماره گروه *</th> --}}
							<th>تعداد واحد</th>
							<th>ساعت تدریس</th>
							<th>تعداد دانشجو</th>
							<th>عملیات</th>
						</tr>
						@foreach ($darses as $dars)
							<tr>
								<td>ترمی</td>
								<td>{{ $dars->maghta }}</td>
								<td>{{ $dars->code_dars }}</td>
								<td>{{ $dars->dars }}</td>
								{{-- <td>{{ $dars->unit }}</td> --}}
								<td>{{ $dars->unit }}</td>
								<td>{{ $dars->unit * 16 }}</td>
								<td>{{ $dars->stu_num }}</td>
								<td>
									<a href="{{ route('dadkar_edit_dars', $dars->id) }}" title="ویرایش">ویرایش</a>
								</td>
							</tr>
						@endforeach
						<tr>
							@php
								
							@endphp
							<td><b>جمع</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td><b>{{ $darses->sum('unit') }}</b></td>
							<td><b>{{ $darses->sum('unit') * 16 }}</b></td>
							<td><b>{{ $darses->sum('stu_num') }}</b></td>
						</tr>
					</table>
				</div>
				<br><br><br><br><br>
	   	</div>
    </body>
</html>