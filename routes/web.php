<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\Base\BrancheController;
use App\Http\Controllers\Admin\Base\BuildingController;
use App\Http\Controllers\Admin\Base\CategoryController;
use App\Http\Controllers\Admin\Product\CancelReasonController;
use App\Http\Controllers\Admin\Base\EducationLevelController;
use App\Http\Controllers\Admin\Base\TestTypeController;
use App\Http\Controllers\Admin\Financial\PriceListController;
use App\Http\Controllers\Admin\Financial\TransactionController;
use App\Http\Controllers\Admin\Financial\DebtorController;
use App\Http\Controllers\Admin\Financial\YaribargController;
use App\Http\Controllers\Admin\Financial\YaribargTypeController;
use App\Http\Controllers\Admin\Product\WorkShopController;
use App\Http\Controllers\Admin\User\AdminController as UserAdminController;
use App\Http\Controllers\Admin\User\AdvisorController;
use App\Http\Controllers\Admin\Product\SessinoController;
use App\Http\Controllers\Admin\Product\AdvisorSessionController;
use App\Http\Controllers\Admin\Financial\CashController;
use App\Http\Controllers\Admin\Financial\CashTypeController;
use App\Http\Controllers\Admin\User\CustomerController;
use App\Http\Controllers\Admin\Settings\Customer_settingController;
use App\Http\Controllers\Admin\User\RoleController;
use App\Http\Controllers\Admin\Product\RoomController;
use App\Http\Controllers\Admin\Settings\ApproachController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\Storage\StorageController;
use App\Http\Controllers\Admin\Chat\ChatController;
use App\Http\Controllers\Admin\Product\Waiting_listController;
use App\Http\Controllers\Admin\Waitinglist\WaitinglistController;
use App\Http\Controllers\Admin\Reminder\ReminderController;
use App\Http\Controllers\Admin\Product\SaleController;
use App\Http\Controllers\Admin\Settings\SematController;
use App\Http\Controllers\Admin\Settings\SentenceController;
use App\Http\Controllers\Admin\Sms\SmsController;
use App\Http\Controllers\Admin\Product\SessionController;
use App\Http\Controllers\Admin\Task\TaskController;
use App\Http\Controllers\Admin\Thank\ThankController;
use App\Http\Controllers\Admin\DadkarController;
use App\Http\Controllers\Admin\PayrollController;
use App\Http\Controllers\Admin\Poll\PollController;
use App\Http\Controllers\Admin\Product\TestController;
use App\Http\Controllers\Admin\Report\ReportController;
use App\Models\Day;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth')->prefix('admin')->group(function () {
    Route::prefix('user')->group(function () {
        Route::resources([
            '/advisor' => AdvisorController::class,
            '/customer' => CustomerController::class,
            '/role' => RoleController::class,
            '/admin' => UserAdminController::class
        ]);
        Route::get('/advisor/get_all/ajax', [AdvisorController::class, 'get_all_ajax'])->name('advisor.get_all_ajax');
        Route::get('/advisor/get_all_selection/ajax', [AdvisorController::class, 'get_all_selection_ajax'])->name('advisor.get_all_selection_ajax');
        Route::get('/customer/get_all/ajax', [CustomerController::class, 'get_all_ajax'])->name('customer.get_all_ajax');
        Route::get('/customer/simple/create', [CustomerController::class, 'simple_create'])->name('customer.simple_create');
        Route::post('/customer/simple_store', [CustomerController::class, 'simple_store'])->name('customer.simple_store');
        Route::get('/customer/get_all_datatabel/ajax', [CustomerController::class, 'get_all_datatable_ajax'])->name('customer.get_all_datatable_ajax');
        Route::get('/customer/paginate', [CustomerController::class, 'customer_paginate'])->name('customer.paginate');

        Route::get('/product/getevent/show', [AdminController::class, 'product_getevent'])->name('product.getevent');
        Route::get('/advisor/{advisor}/price_list', [AdvisorController::class, 'price_list'])->name('advisor.price_list');
        // فعالسازی و غیر فعالسازی وضعیت کارکنان
        Route::get('/admin/activation/{admin}', [UserAdminController::class, 'activation'])->name('admin_activation');
        // فعالسازی و غیر فعالسازی وضعیت مشاوران
        Route::get('/advisor/activation/{advisor}', [AdvisorController::class, 'activation'])->name('advisor_activation');
        // فعالسازی و غیر فعالسازی مراجعین
        Route::get('/customer/activation/{customer}', [CustomerController::class, 'activation'])->name('customer_activation');
        // فعالسازی و غیرفعالسازی مشتری ویژه
        Route::get('/customer/vip/{customer}', [CustomerController::class, 'vip'])->name('customer_vip');



        //مسیرهای مربوط به فیش حقوق پرسنل و مشاورین
        Route::prefix('/payroll')->group(function () {
            // Route::post('/advisor/print', [AdvisorController::class, 'print_report'])->name('advisor.print');
            Route::get('/', [PayrollController::class, 'index'])->name('payroll_index');
            Route::get('/admin', [PayrollController::class, 'admin_select'])->name('payroll_admin_select');
            Route::post('/admin/create', [PayrollController::class, 'admin_create'])->name('payroll_admin_create');
            Route::post('/admin/print', [PayrollController::class, 'admin_print'])->name('payroll_admin_print');
            Route::get('/advisor', [PayrollController::class, 'advisor_select'])->name('payroll_advisor_select');
            Route::post('/advisor/print', [PayrollController::class, 'advisor_print'])->name('payroll_advisor_print');
        });
    });
    //مسیرهای گزارشات و گزارش گیری
    Route::prefix('report')->group(function () {
        Route::get('/advisor', [AdvisorController::class, 'advisor_report'])->name('advisor.report');
        Route::post('/advisor/print', [AdvisorController::class, 'print_report'])->name('advisor.print');
        Route::get('/report', [UserAdminController::class, 'admin_report'])->name('admin.report');
        Route::post('/report/print', [UserAdminController::class, 'print_report'])->name('admin.print');
        Route::post('/report/admin_shift_print', [UserAdminController::class, 'shift_print_report'])->name('admin.shift_print_report');
        Route::get('/daily_report', [SessionController::class, 'daily_report'])->name('daily.report');
        Route::Post('/daily_report_print', [SessionController::class, 'daily_report_print'])->name('daily.report_print');
        Route::get('/full_report', [UserAdminController::class, 'admin_full_report'])->name('admin.full_report');
        Route::post('/full_report/print', [UserAdminController::class, 'admin_full_report_print'])->name('admin.full_report_print');
        //گزارش جلسات کنسل شده
        Route::get('/canceled_session', [ReportController::class, 'canceled_session'])->name('canceled_session.report');
        Route::post('/canceled_session_print', [ReportController::class, 'canceled_session_print'])->name('canceled_session_print.report');
        //گزارش مراجعین در یک بازه تاریخی
        Route::get('/customer_report_form', [ReportController::class, 'select_customer_date_form'])->name('select_customer_date_form');
        Route::post('/customer_report_print', [ReportController::class, 'select_customer'])->name('select_customer');
        // گزارشگیری از اتاقها
        Route::get('/select_room', [ReportController::class, 'select_room'])->name('select_room');
        Route::post('/room_report_print', [ReportController::class, 'room_report_print'])->name('room_report_print');
    });
    // انتقادات و پیشنهادات
    Route::prefix('thank')->group(function () {
        Route::get('index', [ThankController::class, 'index'])->name('thank.index');
        Route::post('store', [ThankController::class, 'store'])->name('thank.store');

        Route::get('Thanks_reason_list', [ThankController::class, 'Thanks_reason_list'])->name('Thanks_reason_list');
        Route::get('Thanks_reason_create', [ThankController::class, 'Thanks_reason_create'])->name('Thanks_reason_create');
        Route::post('Thanks_reason_store', [ThankController::class, 'Thanks_reason_store'])->name('Thanks_reason_store');
        Route::get('Thanks_reason_edit/{id}', [ThankController::class, 'Thanks_reason_edit'])->name('Thanks_reason_edit');
        Route::get('Thanks_reason_destroy/{id}', [ThankController::class, 'Thanks_reason_destroy'])->name('Thanks_reason_destroy');
        Route::post('Thanks_reason_update/{id}', [ThankController::class, 'Thanks_reason_update'])->name('Thanks_reason_update');


        Route::get('dadkar_list', [ThankController::class, 'dadkar_list'])->name('dadkar_list');
        Route::get('dadkar_ostad/{id}', [ThankController::class, 'dadkar_ostad'])->name('dadkar_ostad');
        Route::get('dadkar_edit/{id}', [ThankController::class, 'dadkar_edit'])->name('dadkar_edit');
        Route::delete('dadkar_delete/{id}', [ThankController::class, 'dadkar_delete'])->name('dadkar_delete');
        Route::post('dadkar_update/{id}', [ThankController::class, 'dadkar_update'])->name('dadkar_update');
        Route::get('dadkar_create', [ThankController::class, 'dadkar_create'])->name('dadkar_create');
        Route::post('dadkar_store', [ThankController::class, 'dadkar_store'])->name('dadkar_store');

        Route::get('dadkar_darse_ostad/{id}', [ThankController::class, 'dadkar_darse_ostad'])->name('dadkar_darse_ostad');
        Route::get('dadkar_edit_dars/{id}', [ThankController::class, 'dadkar_edit_dars'])->name('dadkar_edit_dars');
    });


    Route::get('/Waitinglist', [WaitinglistController::class, 'index'])->name('Waitinglist');
    Route::get('/Waitinglist/ajax_get', [WaitinglistController::class, 'ajax_get'])->name('Waitinglist.ajax_get');
    Route::post('/Waitinglist/store_waiting_list_ajax', [WaitinglistController::class, 'store_waiting_list_ajax'])->name('Waitinglist.store_waiting_list_ajax');

    Route::prefix('base')->group(function () {
        Route::resources([
            '/education_level' => EducationLevelController::class,
            '/building' => BuildingController::class,
            '/category' => CategoryController::class,
            '/branche' => BrancheController::class,
            '/TestType' => TestTypeController::class,
        ]);
    });
    Route::prefix('settings')->group(function () {
        Route::resources([
            '/approach' => ApproachController::class,
            '/semat' => SematController::class,
            '/sentence' => SentenceController::class
        ]);
        Route::get('/activation/{id}', [SentenceController::class, 'activation'])->name('sentence.activation');

        Route::get('/introduction', [Customer_settingController::class, 'index'])->name('introduction.index');
        Route::get('/introduction/create', [Customer_settingController::class, 'create'])->name('introduction.create');
        Route::post('/introduction/store', [Customer_settingController::class, 'store'])->name('introduction.store');
        Route::get('/introduction/edit/{id}', [Customer_settingController::class, 'edit'])->name('introduction.edit');
        Route::patch('/introduction/update/{id}', [Customer_settingController::class, 'update'])->name('introduction.update');
        Route::get('/introduction/active/{id}', [Customer_settingController::class, 'activation'])->name('introduction.activation');
    });
    Route::prefix('chat')->group(function () {
        Route::resources([
            '/chat' => ChatController::class,
        ]);
    });

    Route::resources(['/reminder' => ReminderController::class]);

    Route::resources(['/storage' => StorageController::class,]);
    Route::get('/kalalist', [StorageController::class, 'kalalist'])->name('storage.kalalist');
    Route::post('/kalasave', [StorageController::class, 'save_kala'])->name('storage.save_kala');
    Route::get('/storage/use_kala/{storage}', [StorageController::class, 'use_kala'])->name('storage.use_kala');
    Route::get('/storage/keep_kala/{storage}', [StorageController::class, 'keep_kala'])->name('storage.keep_kala');
    Route::get('/Storage/kala_type', [StorageController::class, 'kala_type'])->name('storage.kala_type');
    Route::get('/Storage/new_kala_type', [StorageController::class, 'new_kala_type'])->name('storage.new_kala_type');
    Route::post('/Storage/kala_type_store', [StorageController::class, 'kala_type_store'])->name('storage.kala_type_store');



    Route::prefix('sms')->group(function () {
        Route::resources([
            'sms' => SmsController::class,
        ]);
        Route::get('/{setting}/activation', [SmsController::class, 'activation'])->name('sms.activation');
        Route::get('/send/{user}', [SmsController::class, 'send'])->name('sms.send');
        Route::post('/send', [SmsController::class, 'send_sms'])->name('sms.send_sms');
    });




    Route::get('/waiting_list/{user}', [Waiting_listController::class, 'list'])->name('wl.list');
    Route::post('/waiting_list/save_to_wl', [Waiting_listController::class, 'save_to_wl'])->name('wl.save');
    Route::get('/waiting_list/del/{user}', [Waiting_listController::class, 'destroy'])->name('wl.delete');


    Route::prefix('task')->group(function () {
        Route::resources([
            '/task' => TaskController::class,
        ]);
        Route::get('/persona/{user}/create', [TaskController::class, 'create_persona'])->name('task.create_persona');
        Route::get('/task/{history}/activation', [TaskController::class, 'activation'])->name('task.activation');
        Route::post('/task/{history}/update_task', [TaskController::class, 'update_task'])->name('task.update_task');
    });

    Route::prefix('product')->group(function () {
        Route::resources([
            '/workshop' => WorkShopController::class,
            '/room' => RoomController::class,
            '/test' => TestController::class,
            // '/sale' => SaleController::class,
            // '/session' => SessionController::class
        ]);
        Route::get('/room/order/{id},{side}', [RoomController::class, 'order'])->name('room.order');


        Route::get('/workshop/members/{workshop}', [WorkShopController::class, 'members'])->name('workshop.members');
        Route::get('/workshop/workshop_sessions/{workshop}', [WorkShopController::class, 'workshop_sessions'])->name('workshop.sessions');
        Route::post('/workshop/add_session', [WorkShopController::class, 'add_session'])->name('workshop.add_session');
        Route::post('/workshop/add_member', [WorkShopController::class, 'add_member'])->name('workshop.add_member');
        Route::get('/workshop/cancel_member_form/{id},{sale_id}', [WorkShopController::class, 'cancel_member_form'])->name('workshop.cancel_member_form');
        Route::post('/workshop/cancel_member', [WorkShopController::class, 'cancel_member'])->name('workshop.cancel_member');
        Route::get('/workshop/attendance_list/{workshop}', [WorkShopController::class, 'attendance_list'])->name('workshop.attendance_list');
        Route::get('/workshop/print_attendance_list/{workshop}', [WorkShopController::class, 'print_attendance_list'])->name('workshop.print_attendance_list');
        Route::get('/workshop/attendance/{customer_id},{product_id},{session_number},{status}', [WorkShopController::class, 'attendance'])->name('workshop.attendance');
        Route::get('/workshop/edit_session/{id}', [WorkShopController::class, 'edit_session'])->name('workshop.edit_session');
        Route::post('/workshop/update_session/{id}', [WorkshopController::class, 'update_session'])->name('workshop.update_session');
        Route::get('/workshop/about/{id}', [WorkShopController::class, 'about'])->name('workshop.about');
        Route::get('/workshop/print_about/{id}', [WorkShopController::class, 'print_about'])->name('workshop.print_about');
        Route::get('/workshop/print_receipt/{wsh},{user}', [WorkShopController::class, 'print_receipt'])->name('workshop.print_receipt');

        Route::post('/workshop/cancel_wsh_member', [SaleController::class, 'cancel_wsh_member'])->name('sale.cancel_wsh_member');



        Route::get('/sale/{sale}/chat', [SaleController::class, 'sale_chat'])->name('sale.chat');
        Route::post('/sale/{sale}/chat/send_message', [SaleController::class, 'send_message'])->name('sale.send_message');


        Route::get('/sale/{sale}/comment', [SaleController::class, 'sale_comment'])->name('sale.comment');
        Route::post('/sale/{sale}/comment/send_comment', [SaleController::class, 'send_comment'])->name('sale.send_comment');

        Route::get('/sessions', [SessionController::class, 'index'])->name('session.index');
        Route::get('/sessions/paid', [SessionController::class, 'indexـpaid'])->name('session.indexـpaid');
        Route::get('/sessions/cancled', [SessionController::class, 'cancled'])->name('session.cancled');
        Route::resource('advisor.session', AdvisorSessionController::class)->shallow();
        Route::get('/advisor/{advisor}/session/print', [AdvisorSessionController::class, 'print_session'])->name('advisor.session.print');
        Route::post('/session_advisor/send_product_session', [AdvisorController::class, 'send_product_session'])->name('advisor.session.send_product_session');
        Route::get('/session/{session}/print', [SessionController::class, 'session_print'])->name('session.print');
        Route::post('/session/{sale}/send_alert_sms_to_advisor', [SessionController::class, 'send_alert_sms_to_advisor'])->name('session.send_alert_sms_to_advisor');
        Route::post('/session/{sale}/send_alert_sms_to_costomer', [SessionController::class, 'send_alert_sms_to_costomer'])->name('session.send_alert_sms_to_costomer');
        Route::get('/session/getevent/show', [AdminController::class, 'session_getevent'])->name('session.getevent');
        Route::get('/session/getevent/deleted', [AdminController::class, 'session_getevent_deleted'])->name('session.session_getevent_deleted');
        Route::post('/calender_sale_store', [SaleController::class, 'calender_store'])->name('sale.calender_store');
        Route::post('/calender_sale_update', [SaleController::class, 'calender_sale_update'])->name('sale.calender_sale_update');
        Route::post('/calender_sale_end_session', [SaleController::class, 'calender_sale_end_session'])->name('sale.calender_sale_end_session');
        Route::post('/calender_sale_delete', [SaleController::class, 'calender_sale_delete'])->name('sale.calender_sale_delete');
        Route::post('/change_session_date', [SaleController::class, 'change_session_date'])->name('sale.change_session_date');
        Route::post('/change_session_length', [SaleController::class, 'change_session_length'])->name('sale.change_session_length');
        // مسیرهای مربوط به جلسات کنسل شده فورس ماژور
        Route::post('/force_major_calender_sale_delete', [SaleController::class, 'force_major_calender_sale_delete'])->name('sale.force_major_calender_sale_delete');
        Route::get('sessions/force_major_canceled_sessions', [SessionController::class, 'force_major_canceled_sessions_list'])->name('force_major_canceled_sessions_list');
        Route::get('sessions/force_major_result/{id},{result}', [SessionController::class, 'force_major_result'])->name('force_major_result');
    });
    // ***** روت دلایل کنسلی *********************
    // route::resources([
    //     '/Cancel_reasons_list' => CancelReasonController::class
    // ]);
    Route::get('Cancel_reasons_list', [CancelReasonController::class, 'index'])->name('Cancel_reasons_list');
    Route::get('Cancel_reasons_new', [CancelReasonController::class, 'create'])->name('Cancel_reasons_new');
    Route::post('Cancel_reasons_store', [CancelReasonController::class, 'store'])->name('Cancel_reasons_store');
    Route::get('Cancel_reasons_edit,{id}', [CancelReasonController::class, 'edit'])->name('Cancel_reasons_edit');
    Route::post('Cancel_reasons_update/{id}', [CancelReasonController::class, 'update'])->name('Cancel_reasons_update');
    Route::get('Cancel_reasons_destroy,{id}', [CancelReasonController::class, 'destroy'])->name('Cancel_reasons_destroy');
    // مسیرهای مربوط به نظرسنجی
    Route::get('/poll1', [PollController::class, 'poll1'])->name('poll1');
    Route::get('/poll2', [PollController::class, 'poll2'])->name('poll2');
    Route::post('/poll/save', [PollController::class, 'poll_save'])->name('poll_save');



    Route::prefix('Financial')->group(function () {
        Route::resources([
            '/price_list' => PriceListController::class,
            // '/trans        action' => TransactionController::class,
            '/user.transactions' => TransactionController::class,
            '/debtor' => DebtorController::class,
            '/cash' => CashController::class,
            '/cashtype' => CashTypeController::class,
            '/yaribarg' => YaribargController::class,
            'yaribargtype' => YaribargTypeController::class,
        ]);
        Route::get('/transaction/all', [TransactionController::class, 'all'])->name('transaction.all');
        Route::get('/transaction/{transaction}/edit', [TransactionController::class, 'edit'])->name('transaction.edit');
        Route::post('/transaction/{back_transaction}/back_transaction', [TransactionController::class, 'back_transaction'])->name('transaction.back_transaction');
        Route::post('/transaction/{transaction}', [TransactionController::class, 'update'])->name('transaction.update');
        Route::get('/transaction/print', [TransactionController::class, 'print'])->name('transaction.print');
        Route::get('/transaction/get_ajax', [TransactionController::class, 'get_ajax'])->name('transaction.get_ajax');
        Route::get('/user/{user}/invoise', [TransactionController::class, 'invoise'])->name('user.invoise');
        Route::get('/user/{user}/invoise_test', [TransactionController::class, 'invoise2'])->name('user.invoise2');
        Route::post('/store/cartkhan_store', [TransactionController::class, 'cartkhan_store'])->name('transaction.cartkhan_store');
        Route::post('/store/cartkh_to_cart_store', [TransactionController::class, 'cartkh_to_cart_store'])->name('transaction.cartkh_to_cart_store');
        Route::post('/store/internet_peyment', [TransactionController::class, 'internet_peyment'])->name('transaction.internet_peyment');
        Route::post('/store/cash_payment_store', [TransactionController::class, 'cash_payment_store'])->name('transaction.cash_payment_store');
        Route::post('/store/pardakht_be_moraje', [TransactionController::class, 'pardakht_be_moraje'])->name('transaction.pardakht_be_moraje');
        Route::post('/transaction/pardakht_be_advisor', [TransactionController::class, 'pardakht_be_advisor_store'])->name('transaction.pardakht_be_advisor.store');
        Route::get('/transaction/pardakht_be_advisor', [TransactionController::class, 'pardakht_be_advisor'])->name('transaction.pardakht_be_advisor');
        Route::get('/transaction/transfer_to_account', [TransactionController::class, 'transfer_to_account'])->name('transaction.transfer_to_account');
        Route::post('/transaction/transfer_to_account/sotre', [TransactionController::class, 'transfer_to_account_store'])->name('transaction.transfer_to_account.store');
        Route::delete('/transaction/{transaction}', [TransactionController::class, 'destroy'])->name('transactions.destroy');
        Route::post('/store/offset_store', [TransactionController::class, 'offset_store'])->name('transaction.offset_store');
        // روت های مربوط به هزینه ها
        // Route::prefix('costs')->group(function (){
        //     Route::resources([
        //         '/cost_titles' => CostTitleController::class
        //     ]);
        //     Route::get('/costs_list', [CostController::class, 'index'])->name('costs_list');
        //     Route::get('/cost_store', [CostController::class, 'cost_store'])->name('cost_store');
        //     Route::post('/cost_create', [CostController::class, 'cost_create'])->name('cost_create');
        // });
    });

    Route::post('check_mobile', function () {
        return User::where('mobile', request()->mobile)->exists();
    });
    Route::post('admin.dropzone_upload', [AdminController::class, 'dropzone_upload'])->name('admin.dropzone_upload');
    Route::get('/', [AdminController::class, 'index'])->name('admin.home');
});
Route::get('/zarinpal/success', [AdminController::class, 'GhaboolePardakht'])->name('zarinpal.success');
Route::get('/', [HomeController::class, 'index']);



Route::group(['namespace' => 'Auth'], function () {

    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::any('/reet/password', [LoginController::class, 'reset_password'])->name('reset_password');
    Route::post('login', [LoginController::class, 'login']);
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
});



Route::get('date', function () {
    // $start = Carbon::parse('2022-03-23')->format('Y-m-d');
    // for ($i = 0; $i < 2000; $i++) {
    //     Day::insert([
    //         "date" => verta()->instance($start)->format('Y/m/d'),
    //         "dey_of_week" => verta()->instance($start)->dayOfWeek+1,
    //         "miladi_date" => $start,
    //     ]);
    //     $start = Carbon::parse($start)->addDays(1)->format('Y-m-d');
    // }

    return 'a';
});
